$(document).ready(function()
{	
	$('#password').keyup(function()
	{
		$('#result').html(checkStrength($('#password').val()))
	})	
		
	function checkStrength(password)
	{
		var strength = 0
		
		if (password.length < 6) { 
			$('#result').removeClass()
			$('#result').addClass('short')
			return 'بسیار کوتاه'
		}
		
		if (password.length > 7) strength += 1
		
		//If password contains both lower and uppercase characters, increase strength value.
		if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))  strength += 1
		
		//If it has numbers and characters, increase strength value.
		if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/))  strength += 1 
		
		//If it has one special character, increase strength value.
		if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/))  strength += 1
		
		//if it has two special characters, increase strength value.
		if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1
		
		
		//Calculated strength value, we can return messages
		
		
		
		//If value is less than 2
		
		if (strength < 2 )
		{
			$('#result').removeClass()
			$('#result').addClass('Weak')
			return 'ضعیف'
		}
		else if (strength == 2 )
		{
			$('#result').removeClass()
			$('#result').addClass('good')
			return 'متوسط'
		}
		else
		{
			$('#result').removeClass()
			$('#result').addClass('strong')
			return 'قوی'
		}
	}
});
var allowsubmit = false;
$(function(){
    //on keypress
    $('#confpass').keyup(function(e){
        //get values
        var pass = $('#pass').val();
        var confpass = $(this).val();

        //check the strings
        if(pass == confpass){
            //if both are same remove the error and allow to submit
            $('.error').text('');
            allowsubmit = true;
        }else{
            //if not matching show error and not allow to submit
            $('.error').text('Password not matching');
            allowsubmit = false;
        }
    });

    //jquery form submit
    $('#form').submit(function(){

        var pass = $('#pass').val();
        var confpass = $('#confpass').val();

        //just to make sure once again during submit
        //if both are true then only allow submit
        if(pass == confpass){
            allowsubmit = true;
        }
        if(allowsubmit){
            return true;
        }else{
            return false;
        }
    });
});
