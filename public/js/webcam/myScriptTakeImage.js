function getBase64Image(img) {
    var canvas = document.createElement("canvas");
    canvas.width = img.width;
    canvas.height = img.height;
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0);
    var dataURL = canvas.toDataURL("image/png");
    return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
}

function setImgToInputdata() {
    var base64 = getBase64Image(document.getElementById("imageprev"));
    document.getElementById("base64image").value = base64;
    console.log("base64 :", base64)

}

Webcam.attach('#my_camera');

// Configure a few settings and attach camera
function configure() {
    var cameras = new Array(); //create empty array to later insert available devices
    navigator.mediaDevices.enumerateDevices() // get the available devices found in the machine
        .then(function(devices) {
            devices.forEach(function(device) {
                var i = 0;
                if(device.kind=== "videoinput"){ //filter video devices only
                    cameras[i]= device.deviceId; // save the camera id's in the camera array
                    i++;
                    alert(i);
                    alert( cameras[i]);
                }
            });
        })

    Webcam.set( 'constraints', { //set the constraints and initialize camera device (0 or 1 for back and front - varies which is which depending on device)
        width: 1920,
        height: 1080,
        sourceId: cameras[1]
    } );
    Webcam.attach('#my_camera');
}

// A button for taking snaps
// preload shutter audio clip

function take_snapshot() {
    var shutter = new Audio();
    shutter.autoplay = false;
    shutter.src = navigator.userAgent.match(/Firefox/) ? 'shutter.ogg' : 'shutter.mp3';
    // play sound effect
    shutter.play();
    // take snapshot and get image data
    Webcam.snap(function (data_uri) {
        // display results in page
        document.getElementById('results').innerHTML =
            '<img id="imageprev" src="' + data_uri + '"/>';
    });

    Webcam.reset();
}

function saveSnap() {
    // Get base64 value from <img id='imageprev'> source
    var base64image = document.getElementById("imageprev").src;

    Webcam.upload(base64image, 'upload.php', function (code, text) {
        console.log('Save successfully');
        //console.log(text);
    });

}

