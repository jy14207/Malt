// ========================================== ready ============================================================== -->
var barChart;
var barChartData;
$(document).ready(function () {
    // ==========================
    $("#videoURL").blur(function () {
        var val = this.value;
        $("#srcVideoPlayer").attr("src", val);
        $("#videPlayer").remove();
        $("#videPlayerDiv").append('<video width="80%" style="FONT-WEIGHT: 500;border-style: solid;border-color: #a430ad;border-width:1px;border-radius: 3px;" height="80%" id="videPlayer" controls><source id="srcVideoPlayer" src="' + val + '"  type="video/mp4"></video>');
        $("#textQ").attr("value","لطفا فایل ویدئویی زیر را ببینید و به ادامه سوالات پاسخ دهید")
        setTimeout(function () {
            document.getElementById('videPlayer').play();
        }, 1000);
    });
    // $(".datepicker0").datepicker({dateFormat: 'yy/mm/dd'});
// =====================Star qType Degree===============================================================================
    if (document.getElementById('questionAnswer')) {
        questionAnswer = document.getElementById('questionAnswer').value;
        questionType = document.getElementById('questionType').value;
        if (questionType == 'degree') {
            $(function () {
                $(".my-rating-9").starRating({
                    initialRating: parseInt(questionAnswer),
                    useFullStars: true,
                    starSize: 50,
                    disableAfterRate: false,
                    onHover: function (currentIndex, currentRating, $el) {
                        $('.live-rating').text(currentIndex);
                    },
                    onLeave: function (currentIndex, currentRating, $el) {
                        $('.live-rating').text(currentRating);

                    }
                });
                $(".my-rating-9").click(function () {
                    val = $('.live-rating').html();
                    $("#questionAnswer").val(val);
                })
            });
        }
//================================================== Prority QType ====================================================
//         questionType = document.getElementById('questionType').value;
        answer = [];
        if (questionType == 'prioritize') {
            $('.check').change(function () {
                alert($(this).parent().find("span").first().html());
                if ($(this).is(':checked'))
                    answer.push(this.value);
                else {
                    answer.splice($.inArray(this.value, answer), 1);
                }
                $("#questionAnswer").val(answer);
            })
        }
    }
    // console.log($(this).parent().find("span").first().html());

//================================ Spectural Question Type ================================================
    $('.btnpectral').hover(
        function () {
            // alert("SDFG")
            $("input").css({'background-color': '#ecf0f5'});
            $(this).css({'background-color': '#287EE3'});
            $(this).click(function () {
                $(this).css({'background-color': '#287EE3'});
                $("#questionAnswer").val($(this).val())
            })
        });

//============================= maxValue Blur ====== تغییر نوع سوال  =================================
    $("#questionTypeSelect").change(function () {
        if($('#videPlayer').length){
            $('#videPlayer').attr('src', '');
            $('#videPlayer').remove();
            $('#videoURL').val('');
        }
        if($('#audioPlayer').length){
            $("#audioPlayer").attr("src",'')
        }

        if (this.value == 6) {
            $("#teifDivQuestionType").prop("hidden", true);
            $("#numberDiv").prop("hidden", false);
            $("#videoDiv").prop("hidden", true);
            $("#imageDiv").prop("hidden", true);
            $("#audioDiv").prop("hidden", true);
        }
       else if (this.value == 7) {
            $("#teifDivQuestionType").prop("hidden", false);
            $("#numberDiv").prop("hidden", true);
            $("#videoDiv").prop("hidden", true);
            $("#audioDiv").prop("hidden", true);
        }

        else if (this.value == 15) {
            $("#teifDivQuestionType").prop("hidden", true);
            $("#numberDiv").prop("hidden", true);
            $("#videoDiv").prop("hidden", false);
            $("#imageDiv").prop("hidden", true);
            $("#audioDiv").prop("hidden", true);

        }
        else if (this.value == 16) {
            $("#teifDivQuestionType").prop("hidden", true);
            $("#numberDiv").prop("hidden", true);
            $("#videoDiv").prop("hidden", true);
            $("#imageDiv").prop("hidden", false);
            $("#audioDiv").prop("hidden", true);
            $("#uploadFile").addClass('required');

        }
       else if(this.value == 17){
            $("#teifDivQuestionType").prop("hidden", true);
            $("#numberDiv").prop("hidden", true);
            $("#videoDiv").prop("hidden", true);
            $("#imageDiv").prop("hidden", true);
            $("#audioDiv").prop("hidden", false);
            $("#uploadAudioFile").addClass('required');

        }
        else{
            $("#teifDivQuestionType").prop("hidden", true);
            $("#numberDiv").prop("hidden", true);
            $("#videoDiv").prop("hidden", true);
            $("#imageDiv").prop("hidden", true);
            $("#audioDiv").prop("hidden", true);
        }
        /*
            12 noResponse متن بدون پاسخ
            15 Video ویدئو
            16 Image عکس(تصویر) نشان دادن
            17 audio صوت(صدا)
            18 uploadVideo درخواست ویدئو از کاربر
            19 uploadImage  درخواست تصویر از کاربر
            20 uploadAudio درخواست صدا از کاربر
        */
        if (this.value == 12 || this.value == 15 || this.value == 16 || this.value == 17 ) {
            $("#allowNull option[value=yes]").attr('selected', 'selected');
            $("#allownull option[value=no]").attr('disabled', 'disabled');
        }
        else {
            $("#allowNull option[value=no]").attr('selected', 'selected');
            $("#allownull option[value=no]").removeAttr('disabled');
        }

    })

// ==========================
    $("#maxValue").blur(function () {
        var maxRange = document.getElementById("maxValue").value;
        if (maxRange > 50 | maxRange < 2) {
            swal("ورودی غیر مجاز", "عددی بین 2 تا 50 وارد کنید", "error");
            $("#maxValue").focus();
            $("#maxValue").val(50);
            exit;
        }
        $("#rangeTeifi").attr("max", maxRange);
        $("#rangeTeifi").attr("value", maxRange);
        $("#currentRangeValue").text(maxRange);
    })
    $("#rangeTeifi").change(function () {
        var currentRange = document.getElementById("rangeTeifi").value;
        $("#currentRangeValue").html(currentRange);
        if (currentRange % 2 == 0) {
            $("#centerLable").prop('disabled', true);
        }
        else {
            $("#centerLable").prop('disabled', false);

        }

    })
    $("#rangeDegree").change(function () {
        var currentRange = document.getElementById("rangeDegree").value;
        $("#currentRangedegreeValue").html(currentRange);
    })
    $("#maxValueDegree").blur(function () {
        var maxRange = document.getElementById("maxValueDegree").value;
        if (maxRange > 50 | maxRange < 2) {
            swal("ورودی غیر مجاز", "عددی بین 2 تا 50 وارد کنید", "error");
            $("#maxValueDegree").focus();
            $("#maxValueDegree").val(50);
            exit;
        }
        $("#rangeDegree").attr("max", maxRange);
    })
    $("#minNumberType").blur(function () {
        var minNumberTypeVal = this.value;
        $("#maxNumberType").prop("min", minNumberTypeVal);

    })
    $("#maxNumberType").blur(function () {
        var minNumberTypeVal = document.getElementById("minNumberType").value;
        var maxNumberTypeVal = this.value;
        if (minNumberTypeVal > maxNumberTypeVal) {
            swal("ورودی غیر مجاز", "مقدار حداقل از حداکثر بزرگتر است.", "error");
            return false;
        }
    })

//=============================== Spinner Loading ===================================
    var obj = {},
        SpinnerShow = $('.SpinnerShow');

    function getValues() {
        obj.textVal = 'لطفا صبر کنید! در حال دریافت / ارسال داده ها.';
        obj.percentVal = $('#percentInput').val();
        obj.durationVal = $('#durationInput').val();
    }

    $('.spinnerStart').on('click', function () {
        getValues();
        SpinnerShow.preloader({
            text: obj.textVal,
            percent: obj.percentVal,
            duration: obj.durationVal
        });
        $('.form-control').each(function (k, v) {
            if (v.value.length == 0) $(v).attr('disabled', true);
        });
    });
    $('#stop').on('click', function () {
        SpinnerShow.preloader('remove');
        $('.form-control').attr('disabled', false);
    });

//=========================== animate Dashboard===================================
    if ($("#barChart").length) {
        $(function () {
            /* ChartJS
             * -------
             * Here we will create a few charts using ChartJS
             */

            //--------------
            //- AREA CHART -
            //--------------

            // Get context with jQuery - using jQuery's .get() method.
            // var areaChartCanvas = $('#areaChart').get(0).getContext('2d')
            // This will get the first returned node in the jQuery collection.
            // var areaChart       = new Chart(areaChartCanvas)

            var areaChartData = {
                labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
                datasets: [
                    {
                        label: 'Electronics',
                        fillColor: 'rgba(210, 214, 222, 1)',
                        strokeColor: 'rgba(210, 214, 222, 1)',
                        pointColor: 'rgba(210, 214, 222, 1)',
                        pointStrokeColor: '#c1c7d1',
                        pointHighlightFill: '#fff',
                        pointHighlightStroke: 'rgba(220,220,220,1)',
                        data: [65, 59, 80, 81, 56, 55, 40]
                    },
                    {
                        label: 'Digital Goods',
                        fillColor: 'rgba(60,141,188,0.9)',
                        strokeColor: 'rgba(60,141,188,0.8)',
                        pointColor: '#3b8bba',
                        pointStrokeColor: 'rgba(60,141,188,1)',
                        pointHighlightFill: '#fff',
                        pointHighlightStroke: 'rgba(60,141,188,1)',
                        data: [28, 48, 40, 19, 86, 27, 90]
                    }
                ]
            }

            // var areaChartOptions = {
            //     //Boolean - If we should show the scale at all
            //     showScale               : true,
            //     //Boolean - Whether grid lines are shown across the chart
            //     scaleShowGridLines      : false,
            //     //String - Colour of the grid lines
            //     scaleGridLineColor      : 'rgba(0,0,0,.05)',
            //     //Number - Width of the grid lines
            //     scaleGridLineWidth      : 1,
            //     //Boolean - Whether to show horizontal lines (except X axis)
            //     scaleShowHorizontalLines: true,
            //     //Boolean - Whether to show vertical lines (except Y axis)
            //     scaleShowVerticalLines  : true,
            //     //Boolean - Whether the line is curved between points
            //     bezierCurve             : true,
            //     //Number - Tension of the bezier curve between points
            //     bezierCurveTension      : 0.3,
            //     //Boolean - Whether to show a dot for each point
            //     pointDot                : false,
            //     //Number - Radius of each point dot in pixels
            //     pointDotRadius          : 4,
            //     //Number - Pixel width of point dot stroke
            //     pointDotStrokeWidth     : 1,
            //     //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
            //     pointHitDetectionRadius : 20,
            //     //Boolean - Whether to show a stroke for datasets
            //     datasetStroke           : true,
            //     //Number - Pixel width of dataset stroke
            //     datasetStrokeWidth      : 2,
            //     //Boolean - Whether to fill the dataset with a color
            //     datasetFill             : true,
            //     //String - A legend template
            //     legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].lineColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
            //     //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            //     maintainAspectRatio     : true,
            //     //Boolean - whether to make the chart responsive to window resizing
            //     responsive              : true
            // }

            //Create the line chart
            // areaChart.Line(areaChartData, areaChartOptions)

            //-------------
            //- LINE CHART -
            //--------------
            // var lineChartCanvas          = $('#lineChart').get(0).getContext('2d')
            // var lineChart                = new Chart(lineChartCanvas)
            // var lineChartOptions         = areaChartOptions
            // lineChartOptions.datasetFill = false
            // lineChart.Line(areaChartData, lineChartOptions)
            //
            // //-------------
            // //- PIE CHART -
            // //-------------
            // // Get context with jQuery - using jQuery's .get() method.
            // var pieChartCanvas = $('#pieChart').get(0).getContext('2d')
            // var pieChart       = new Chart(pieChartCanvas)
            // var PieData        = [
            //     {
            //         value    : 700,
            //         color    : '#f56954',
            //         highlight: '#f56954',
            //         label    : 'Chrome'
            //     },
            //     {
            //         value    : 500,
            //         color    : '#00a65a',
            //         highlight: '#00a65a',
            //         label    : 'IE'
            //     },
            //     {
            //         value    : 400,
            //         color    : '#f39c12',
            //         highlight: '#f39c12',
            //         label    : 'FireFox'
            //     },
            //     {
            //         value    : 600,
            //         color    : '#00c0ef',
            //         highlight: '#00c0ef',
            //         label    : 'Safari'
            //     },
            //     {
            //         value    : 300,
            //         color    : '#3c8dbc',
            //         highlight: '#3c8dbc',
            //         label    : 'Opera'
            //     },
            //     {
            //         value    : 100,
            //         color    : '#d2d6de',
            //         highlight: '#d2d6de',
            //         label    : 'Navigator'
            //     }
            // ]
            // var pieOptions     = {
            //     //Boolean - Whether we should show a stroke on each segment
            //     segmentShowStroke    : true,
            //     //String - The colour of each segment stroke
            //     segmentStrokeColor   : '#fff',
            //     //Number - The width of each segment stroke
            //     segmentStrokeWidth   : 2,
            //     //Number - The percentage of the chart that we cut out of the middle
            //     percentageInnerCutout: 50, // This is 0 for Pie charts
            //     //Number - Amount of animation steps
            //     animationSteps       : 100,
            //     //String - Animation easing effect
            //     animationEasing      : 'easeOutBounce',
            //     //Boolean - Whether we animate the rotation of the Doughnut
            //     animateRotate        : true,
            //     //Boolean - Whether we animate scaling the Doughnut from the centre
            //     animateScale         : false,
            //     //Boolean - whether to make the chart responsive to window resizing
            //     responsive           : true,
            //     // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            //     maintainAspectRatio  : true,
            //     //String - A legend template
            //     legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
            // }
            // //Create pie or douhnut chart
            // // You can switch between pie and douhnut using the method below.
            // pieChart.Doughnut(PieData, pieOptions)

            //-------------
            //- BAR CHART -
            //-------------


// var barChartH=
//         $("#barChartClass").height($("#percentProject").height());
            console.log($('#barChart'));
            var barChartCanvas = $('#barChart').get(0).getContext('2d')

            barChart = new Chart(barChartCanvas)
            barChartData = {
                labels: [],
                datasets: [
                    {
                        label: 'Electronics',
                        fillColor: 'rgba(210, 214, 222, 1)',
                        strokeColor: 'rgba(210, 214, 222, 1)',
                        pointColor: 'rgba(210, 214, 222, 1)',
                        pointStrokeColor: '#c1c7d1',
                        pointHighlightFill: '#fff',
                        pointHighlightStroke: 'rgba(220,220,220,1)',
                        data: []
                    },
                    {
                        label: 'Digital Goods',
                        fillColor: 'rgba(60,141,188,0.9)',
                        strokeColor: 'rgba(60,141,188,0.8)',
                        pointColor: '#3b8bba',
                        pointStrokeColor: 'rgba(60,141,188,1)',
                        pointHighlightFill: '#fff',
                        pointHighlightStroke: 'rgba(60,141,188,1)',
                        data: []
                    }
                ]
            }
            barChartData.datasets[1].fillColor = '#00a65a'
            barChartData.datasets[1].strokeColor = '#00a65a'
            barChartData.datasets[1].pointColor = '#00a65a'
            var barChartOptions = {
                //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
                scaleBeginAtZero: true,
                //Boolean - Whether grid lines are shown across the chart
                scaleShowGridLines: true,
                //String - Colour of the grid lines
                scaleGridLineColor: 'rgba(0,0,0,.05)',
                //Number - Width of the grid lines
                scaleGridLineWidth: 1,
                //Boolean - Whether to show horizontal lines (except X axis)
                scaleShowHorizontalLines: true,
                //Boolean - Whether to show vertical lines (except Y axis)
                scaleShowVerticalLines: true,
                //Boolean - If there is a stroke on each bar
                barShowStroke: true,
                //Number - Pixel width of the bar stroke
                barStrokeWidth: 2,
                //Number - Spacing between each of the X value sets
                barValueSpacing: 5,
                //Number - Spacing between data sets within X values
                barDatasetSpacing: 1,
                //String - A legend template
                legendTemplate: '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
                //Boolean - whether to make the chart responsive
                responsive: true,
                maintainAspectRatio: true
            }

            var idP = 1;
            $.get({
                url: URL_ROOT + "?r=admin/companyChartAjax",
                data: "idCompany=" + idP,
                success: function (data) {
                    var subject = [];
                    var finishPorseshnamehCount = [];
                    var totalPorseshnameh = [];
                    for (row in data) {
                        // console.log(data[row]);
                        subject.push(data[row]["subject"]);
                        finishPorseshnamehCount.push(data[row]["finishPorseshnamehCount"]);
                        totalPorseshnameh.push(data[row]["totalPorseshnameh"]);
                    }
                    // console.log(subject);
                    // console.log(finishPorseshnamehCount);
                    // console.log(totalPorseshnameh);
                    barChartData.datasets[0].data = totalPorseshnameh;
                    barChartData.datasets[1].data = finishPorseshnamehCount;
                    barChartData.labels = subject;
                    barChart.Bar(barChartData, barChartOptions)

                },
            })
            barChartOptions.datasetFill = false
            barChart.Bar(barChartData, barChartOptions)
        })
    }


//======================Animate Number  ========================================

    $('.count').each(function () {
        $(this).prop('Counter', 0).animate({
            Counter: $(this).text()
        }, {
            duration: 1000,
            easing: 'swing',
            step: function (now) {
                $(this).text(Math.ceil(now));
            }
        });
    });


    $("#selectPolicy").change(function () {
        idPolicy = $("#selectPolicy").val();//1=SocialClass;   2=Stop;   3=Count;
        if (idPolicy == 1) {
            $("#socialClassDiv").show("slow");
            $("#StopDiv").hide("");
            $("#CountDiv").hide("");
            $("#idPolicysocialClassDiv").attr("value", idPolicy);
        }
        if (idPolicy == 2) {
            $("#socialClassDiv").hide("");
            $("#StopDiv").show("slow");
            $("#CountDiv").hide("");
            $("#idPolicyStopDiv").attr("value", idPolicy);

        }
        if (idPolicy == 3) {
            $("#socialClassDiv").hide("");
            $("#StopDiv").hide("");
            $("#CountDiv").show("slow");
            $("#idPolicyCountDiv").attr("value", idPolicy);

        }

    })
    $('body').on('click', 'img', function () {
        // Get the modal
        var id = $(this).attr('id');
        // alert(id);
        var modal = document.getElementById('myModal');
// Get the image and insert it inside the modal - use its "alt" text as a caption
        var img = document.getElementById(id);
        // alert(img);
        var modalImg = document.getElementById("img01");
        var captionText = document.getElementById("caption");
        img.onclick = function () {
            modal.style.display = "block";
            modalImg.src = this.src;
            captionText.innerHTML = this.alt;
        }
// Get the <span> element that closes the modal
        var span = document.getElementsByClassName("modal")[0];
// When the user clicks on <span> (x), close the modal
        span.onclick = function () {
            modal.style.display = "none";
        }
    })
    var tRowCount = $('table#optionsTable tr:last').index() + 1;
    // $("#scoreInput").val(tRowCount);
    // آپلود فایل عکی image Uploder
    $("#uploadFile").on("change", function () {
        var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
        if (/^image/.test(files[0].type)) { // only image file
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
            reader.onloadend = function () { // set image data as background of div
                if(files[0].size>2000000)
                {
                    swal("خطا...!", "اندازه تصویر نباید از دو مگابایت بیشتر باشد!", "error");
                    $("#uploadFile").val(null);
                }
                else
                {
                    $("#imageHideWhenNofile").prop("hidden", false);
                    $("#imagePreview").css("background-image", "url(" + this.result + ")");
                    $("#imagePreview").css("display", "inline-block");
                    $("#textQ").attr("value","لطفا تصویر زیر را مشاهده نمائید و ادامه سوالات را پاسخ دهید!");
                    $("#tempImg").hide();
                    $("#tempImg").attr('src' , null);
                }
            }
        }
        else {
            swal("خطا...!", "نوع فایل انتخابی تصویر نیست!", "error");
            $("#uploadFile").val(null);
        }
    });
// آپلود فایل صوتی audio Uploder
    $("#uploadAudioFile").on("change", function () {
        // alert("asasas");
        var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
        if (/^audio/.test(files[0].type)) { // only image file
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
            reader.onloadend = function () { // set image data as background of div
                if(files[0].size>5242880)
                {
                    swal("خطا...!", "اندازه فایل صوتی نباید از پنج مگا بایت بایت بیشتر باشد!", "error");
                    $("#uploadAudioFile").val(null);
                }
                else
                {
                    var val = files[0];
                    var blob = window.URL || window.webkitURL;
                    var fileURL = blob.createObjectURL(val);
                    // alert(fileURL);
                    $("#audioPlayer").attr("src",fileURL)
                    $("#textQ").attr("value","لطفا به فایل صوتی زیر گوش دهید و ادامه سوالات را پاسخ دهید!")
                    setTimeout(function () {
                        document.getElementById('audioPlayer').play();
                    }, 500);
                }
            }
        }
        else {
            swal("خطا...!", "نوع فایل انتخابی mp3 نیست!", "error");
            $("#uploadAudioFile").val(null);
        }

    });




    $(".goBack").click(function () {
        window.history.back();
    })
//Only Number
    $(function () {
        $('#staticParent').on('keydown', '#nationalCode', function (e) {
            -1 !== $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) || (/65|67|86|88/.test(e.keyCode) && (e.ctrlKey === true || e.metaKey === true)) && (!0 === e.ctrlKey || !0 === e.metaKey) || 35 <= e.keyCode && 40 >= e.keyCode || (e.shiftKey || 48 > e.keyCode || 57 < e.keyCode) && (96 > e.keyCode || 105 < e.keyCode) && e.preventDefault()
        });
    })
    $('#TextAreaOption').blur(function () {
        var text = $('#TextAreaOption').val();
        var modifiedtext = text.replace(/(?:(?:\r\n|\r|\n)\s*){2}/gm, "");
        $(this).val(modifiedtext);
    });
    $('#TextAreaScore').blur(function () {
        var text1 = $('#TextAreaScore').val();
        var modifiedtext1 = text1.replace(/(?:(?:\r\n|\r|\n)\s*){2}/gm, "");
        $(this).val(modifiedtext1);
    });
    $("#ExportProjectTable").click(function () {
        var idP = $("#idP").val();
        var d;
        var ProjectTable = new Array();
        ProjectTable.push(["آی دی پروژه", "آی دی شرکت", "موضوع", "تاریخ شروع", "تاریخ پایان", "تعداد سوال", "تعداد پاسخگو"]);
        $.get({
            url: URL_ROOT + "?r=reports/getProjectRowForReports",
            data: "idP=" + idP,
            // format:'json',
            success: function (data) {
                console.log(data);
                ProjectTable.push([data.id, data.idCompany, data.subject, data.startDate, data.endDate, data.questionunmer, data.pasokhgooNumber]);
                drawAjaxArayToHtmlTable(ProjectTable, 7, 'dvTable')
                SpinnerShow.preloader('remove');
                $('.form-control').attr('disabled', false)
            },
        });
    })
//=========================================================================
    $("#ExportQuestionTable").click(function () {
        var idP = $("#idP").val();
        var questionTable = new Array();
        questionTable.push(["آی دی سوال", "آی دی پروژه", "ردیف", "آی دی نوع سوال", "متن", "عدم پاسخ"]);
        $.get({
            url: URL_ROOT + "?r=reports/getQuestionRowsForReports",
            data: "idP=" + idP,
            success: function (data) {
                $.each(data, function (i, item) {
                    questionTable.push([item.id, item.idProject, item.rowNumber, item.idQuestionType, item.text, item.allowNull]);

                });
                drawAjaxArayToHtmlTable(questionTable, 6, 'dvQuestionTable')
                SpinnerShow.preloader('remove');
                $('.form-control').attr('disabled', false)
            },
        })
    })
//=========================================================================
    $("#ExportPorseshnameTable").click(function () {
        var idP = $("#idP").val();
        var PorseshnameTable = new Array();
        PorseshnameTable.push(["آی دی پرسشنامه", "شماره پرسشنامه", "آی دی پروژه", "آی دی پرسشگر", "تاریخ تکمیل", "ساعت شروع", "ساعت پایان"]);
        $.get({
            url: URL_ROOT + "?r=reports/getPorseshnameRowsForReports",
            data: "idP=" + idP,
            success: function (data) {
                $.each(data, function (i, item) {
                    PorseshnameTable.push([item.id, item.porseshnamehNumber, item.idProject, item.idPorseshgar, item.completedDate, item.startTime, item.endTime]);

                });
                drawAjaxArayToHtmlTable(PorseshnameTable, 7, 'dvPorseshnameTable')
                SpinnerShow.preloader('remove');
                $('.form-control').attr('disabled', false)
            },
        })
    })
//=========================================================================
    $("#ExportPasokhgooTable").click(function () {
        var idP = $("#idP").val();
        var PasokhgooeTable = new Array();
        PasokhgooeTable.push(["آی دی پرسشنامه", "آی دی پاسخگو", "نام", "نام خانوادگی", "آدرس محل سکونت", "آدرس محل کار", "نام شهر", "شماره تلفن", "شماره همراه"]);
        $.get({
            url: URL_ROOT + "?r=reports/getPasokhgooRowsForReports",
            data: "idP=" + idP,
            success: function (data) {
                $.each(data, function (i, item) {
                    // alert(item.homeAddress);
                    PasokhgooeTable.push([item.id, item.idPasokhgoo, item.name, item.fName, item.homeAddress, item.workAddress, item.cityName, item.phoneNumber, item.mobileNumber]);
                });

                drawAjaxArayToHtmlTable(PasokhgooeTable, 9, 'dvPasokhgooTable')
                SpinnerShow.preloader('remove');
                $('.form-control').attr('disabled', false)
            },
        })
    })


//=========================================================================
    $("#ExportOptionsTable").click(function () {
        var idP = $("#idP").val();
        var OptionsTable = new Array();
        OptionsTable.push(["آی دی گزینه", "آی دی سوال", "متن گزینه", "امتیاز"]);
        $.get({
            url: URL_ROOT + "?r=reports/getOptionsRowsForReports",
            data: "idP=" + idP,
            success: function (data) {
                $.each(data, function (i, item) {
                    // alert(item.homeAddress);
                    OptionsTable.push([item.id, item.idQuestion, item.textOption, item.score]);
                });

                drawAjaxArayToHtmlTable(OptionsTable, 4, 'dvOptionsTable')
                SpinnerShow.preloader('remove');
                $('.form-control').attr('disabled', false)
            },
        })
    })
//=========================================================================
    $("#ExportAnswerTable").click(function () {
        var idP = $("#idP").val();
        var AnswerTable = new Array();
        AnswerTable.push(["آی دی جواب", "آی دی پرسشنامه", "آی دی سوال", "آی دی گزینه", "متن سوالات تشریحی یا سایر"]);
        $.get({
            url: URL_ROOT + "?r=reports/getAnswersRowsForReports",
            data: "idP=" + idP,
            success: function (data) {
                $.each(data, function (i, item) {
                    // alert(item.homeAddress);
                    AnswerTable.push([item.id, item.idPorseshname, item.idQuestion, item.idOption, item.text]);
                });

                drawAjaxArayToHtmlTable(AnswerTable, 5, 'dvAnswerTable')
                SpinnerShow.preloader('remove');
                $('.form-control').attr('disabled', false)
            },
        })
    })
//=========================================================================
    $("#ExportDatamainingِTable").click(function () {
        var idP = $("#idP").val();
        $("#dvDatamainingTable").empty();
        var DatamainingTable = new Array();
        $.get({
            url: URL_ROOT + "?r=reports/databaseTemplateReport",
            data: "idP=" + idP,
            success: function (data) {
                var table = $("<table class='tRep table table-bordered '   style='white-space:nowrap;padding: 20px;'>");
                $("#dvDatamainingTable").append($(table));
                var tr = $("<tr></tr>");
                $(table).append(tr);
                $.map(data["template"], function (val, i) {
                    // console.log(val, i);
                    $(tr).append($("<th style=' background: #53edea;border:1px solid black;text-align: center';>" + val + "</th>"));
                });
                for (i in data) {
                    if (i == "template") continue;
                    var tr = $("<tr></tr>");
                    $(table).append(tr);
                    $.map(data[i], function (val, i) {
                        // console.log(val, i);
                        $(tr).append($("<td style='border: 1px solid black'>" + val + "</td>"));
                    });
                }
                SpinnerShow.preloader('remove');
                $('.form-control').attr('disabled', false);
            },
        })
    })

//=========================================================================
    function drawAjaxArayToHtmlTable(reportName, colCount, divName) {
        var table = '<table class="tRep table table-bordered"  >';
        for (var j = 0; j < reportName.length; j++) {
            table = table + '<tr>';
            for (k = 0; k < colCount; k++) {
                table = table + '<td style="border:1px solid black;text-align: center">';
                table = table + reportName[j][k];
                table = table + '</td>';
            }
            table = table + '</tr>';
        }
        table = table + '</table>';
        $("#" + divName).html(table);

    }

    $("#rowNumber").blur(function () {
        var rowNumber = $("#rowNumber").val();
        var idP = $("#idP").val();
        // alert(rowNumber +"/"+ idP);
        $.get({
            url: URL_ROOT + "?r=Question/checkForDuplicateRow",
            data: "rowNumber=" + rowNumber + "&idP=" + idP,
            success: function (data) {
                if (data.length > 0) {
                    swal("هشدار!", "شماره ردیف سوال تکراری است", "warning");
                    // $("#insertQuestion").hide();
                }
                else {
                    $("#insertQuestion").show();
                }
            }
        }).done(function () {
        })
    })

    $("#porseshnamehNumber").blur(function () {
        // alert("sdsdsd");
        var porseshnamehNumber = $("#porseshnamehNumber").val();
        // alert(rowNumber +"/"+ idP);
        $.get({
            url: URL_ROOT + "?r=porseshname/checkForDuplicateRowajax",
            data: "porseshnamehNumber=" + porseshnamehNumber,
            success: function (data) {
                // console.log("data:",data);
                if (data.length > 0) {
                    swal("خطا", "شماره پرسشنامه تکراری است", "error");
                    $("#Btn_sabt").hide();
                }
                else {
                    $("#Btn_sabt").show();
                }
            }
        }).done(function () {
        })
    })
    $("#userName").blur(function () {
        var userName = $("#userName").val();
        $.get({
            url: URL_ROOT + "?r=Users/checkForDuplicateRow",
            data: "userName=" + userName,
            success: function (data) {
                if (data.isDuplicate == true) {
                    swal("خطا", "نام کاربری تکراری است", "error");
                    $("#btnInsert").hide();
                }
                else {
                    $("#btnInsert").show();
                }
            }
        }).done(function () {
        })
    })
    $(".form").submit(function (e) {
        var alowSubmit = true;
        $(".datepicker0").each(function () {
            if (!checkDate($(this).val())) {
                swal("خطا در عملیات", "تاریخ های ورودی اشتباه است", "error");
                $(this).focus();
                alowSubmit = false;
                return false;
            }
        })
        $(".required").each(function () {
            if (!($(this).val()) & (!$(this).is(":hidden"))) {
                tx = $(this).attr("alt") + " نمیتواند خالی باشد";
                swal("خطا در عملیات", tx, "error");
                $(this).focus();
                alowSubmit = false;
                return false
            }
        })
        $(".tellNumber").each(function () {
            var valTell = this.value;
            if (valTell[0] != '0' | valTell.length < 10 | valTell.length > 11) {

                $(this).css({'color': 'red'});
                $(this).focus();
                alowSubmit = false;
                return false
            }

        })
        $(".no_not_select").each(function () {
            if ($(this).val() == "0") {
                swal("خطا در عملیات", "مقادیر فیلد های کشویی نمیتواند روی گزینه انتخاب کنید باشد.", "error");
                $(this).focus();
                alowSubmit = false;
                return false
            }
        })
        ;
        $(".mytextarea").each(function () {
            var TextAreaOption = $("#TextAreaOption").val();
            var lines = TextAreaOption.split("\n");
            var countTextAreaOption = lines.length;
            var TextAreaScore = $("#TextAreaScore").val();
            var lines = TextAreaScore.split("\n");
            var countTextAreaScore = lines.length;

            if (countTextAreaScore > 1) {

                if (countTextAreaScore != countTextAreaOption) {
                    swal("خطا در عملیات", "تعداد گزینه ها با امتیازات برابر نیست.", "error");
                    alowSubmit = false;

                }
            }
        });
        if (!alowSubmit) {
            e.preventDefault();
        }


    })
// alert("page ready");
// $(".datepicker0").datepicker({dateFormat: 'yy/mm/dd'});
// $(".datepicker2").datepicker({
//     dateFormat: 'yy/mm/dd',
//     altField: '#crime_day',
//     // altFormat: 'DD، d MM yy'
//     altFormat: 'DD'
// });
// $('.timepicker').timepicker({
//     timeFormat: 'h:mm',
//     interval: 60,
//     minTime: '1',
//     maxTime: '12',
//     defaultTime: '1',
//     startTime: '01:00',
//     dynamic: false,
//     dropdown: true,
//     scrollbar: true
// });
    $(".def_focus").focus();
//================================================== showHideOthers ====================================================
// $(".question_form input[type=radio] ").click(function () {
//     if ($(this).attr("id") == "Other")  {
//         // sweetAlert($(this).data("label"));
//         $("#others").toggle()
//     } else {
//         $("#others").val("")
//         $("#others").toggle(false)
//     }
// })
// $(".question_form input[type=checkbox] ").change(function () {
//     if ($(this).attr("id") == "Other")  {
//         // sweetAlert($(this).data("label"));
//         $("#others").toggle()
//     } else {
//         $("#others").val("")
//         $("#others").toggle(false)
//     }
// })
//================================================== showHideOthers ====================================================
    $(".question_form input[type=radio] ").click(function () {
        if ($(this).data("label") == "Other") {
            $("#questionAnswer").toggle($(this).is(":checked"))
            $("#questionAnswer").val($("#tempquestionAnswer").val());

        } else {
            $("#questionAnswer").val("");
            $("#questionAnswer").toggle(false)
        }
    })
    $(".question_form input[type=checkbox] ").click(function () {
        if ($(this).data("label") == "Other") {
            $("#questionAnswer").toggle($(this).is(":checked"))
            $("#questionAnswer").val($("#tempquestionAnswer").val());
            if (!($(this).is(":checked")))
                $("#questionAnswer").val("");
            // alert("dfghj");
        } else if ($(this).attr("id") == "Other") {

            $("#questionAnswer").toggle(false)
        }
    })
    $(".question_form").submit(function (e) {
        $(".requiredIfEnabled").each(function () {
            if (!($(this).val()) & (!$(this).is(":disabled"))) {
                tx = "متن گزینه نمی تواند خالی باشد";
                swal("خطا در عملیات", tx, "error");
                $(this).focus();
                e.preventDefault(e);
                return false
            }
        })
    })

    //==========================================
    $("#questionForm").submit(function () {
        questionAnswer = document.getElementById("questionAnswer");
        if ($("#questionAnswer").css('display') == 'none') {
            // $("#questionAnswer").val(null)
        }
    })
//================================================== show option areatext==============================================
    $("#questionTypeSelect").change(function () {
        $val = $("#questionTypeSelect option:selected").val();
        if ($val == 3) {
            $("#TextAreaOption").val('')
            $("#TextAreaScore").val('')
            $("#optionTextarea").hide();

        }
        else {
            $("#optionTextarea").show();
            $("#lableForisOtherOption").show();
        }
        if ($val == 4 | $val == 5) {
            $("#isOtherOption").show();
        }
        else {
            $("#isOtherOption").hide();
            $("#lableForisOtherOption").hide();
        }
    })

//================================================== validate date ====================================================
    function checkDate(date) {
        val1 = Date.parse(date);
        if (isNaN(val1) == true && date !== "") {
            return false;
        }
        return true;
    }


//===============================================change font size===============================
    $(".p").click(function () {
        var id = $(this).attr('id');
        if (!$("#increase_size").is(':checked')) {
            var fontSize = parseInt($(this).css("font-size"));
            fontSize = fontSize - 1 + "px";
            $('#' + id).css({'font-size': fontSize});
        }
        else {
            var fontSize = parseInt($(this).css("font-size"));
            fontSize = fontSize + 1 + "px";
            $('#' + id).css({'font-size': fontSize});
        }
        $("#current_size").html(fontSize);
    });

//================================================== Show More Div ====================================================
    $("a.confirming").click(function (e) {
        if (!confirm("آیا مطمین هستید ؟")) {
            e.preventDefault();
        }
    })

    $("a.confirming_important").click(function (e) {
        if (!confirm("توجه : حذف پرونده از این قسمت به منزله حذف به صورت دائم بوده و دیگر قابل بازیافت نخواهد بود!آیا مطمین هستید ؟")) {
            e.preventDefault();
        }
    })
    $(".state_to_city").change(function () {
        var where = ($('.state_to_city option:selected').val());
        $("#citySelect").empty();
        $.get(
            'city.php?act=load_city&where=' + where,
            function (data) {
                // console.log(data)
                for (d in data) {
                    $("#citySelect").append("<option value='" + d + "'>" + data[d] + "</option>");
                }
            }
        );
    })
    $("#add_p_text").click(function () {
        $("#more_prepared_text").toggle();
    })
    $(".prepared_texts").change(function () {
        var where = ($('.prepared_texts option:selected').val());
        // $("#matn").val("سلام");
        $.get(
            'city.php?act=load_prepared_text&where=' + where,
            function (data) {
                for (d in data) {
                    $("#matn").val(data[d]);
                }
            }
        )
    })
    $("form").submit(function (e) {

            if ($(".allowNull").val() == "no") {
                var QuestionType = $("input[name='questionType']").val();
                switch (QuestionType) {
                    case  "singleResponse":
                    case  "singleResponseOthers":
                        if ($('input[name=answer]:checked').length <= 0) {
                            swal("خطا", "پاسخ به این سوال اجباری می باشد", "error");
                            e.preventDefault(e);
                        }
                        break;

                    case  "multipleChoice":
                    case  "multipleChoiceOthers":
                    case "openOptions":
                        if ($('input[class*="check"]:checked').length <= 0) {
                            swal("خطا", "پاسخ به این سوال اجباری می باشد", "error");
                            e.preventDefault(e);
                        }
                        break;
                    case  "openQuestion":
                    case "numeric":
                    case "email":
                    case "websitelink":
                    case "spectral":
                    case "degree":
                    case "prioritize":
                        questionAnswer = document.getElementById('questionAnswer').value;

                        if (!questionAnswer) {
                            swal("خطا", "پاسخ به این سوال اجباری می باشد", "error");
                            e.preventDefault(e);
                        }
                    case "uploadImage":
                        var tempImgSrc = document.getElementById('tempImg').src ;
                        if (tempImgSrc.search('noImageSelected')>0){
                            swal("خطا", "پاسخ به این سوال اجباری می باشد", "error");
                            e.preventDefault();
                        }
                }
            }
        }
    );


    //================================================== open Option QType ====================================================
    $(".check").change(function () {
        questionType = document.getElementById('questionType').value;
        if (questionType == "openOptions") {
            idCheckbox = this.id;
            idInput = '#answerOption' + idCheckbox;
            checkBox = document.getElementById(idInput);
            if (!$(this).is(":checked")) {
                $(idInput).val("0");
                $(idInput).attr('disabled', 'disabled');
            }
            else {
                $(idInput).removeAttr('disabled');
                $(idInput).focus();
            }
        }
    })
})
//================================================================================================================================================================
//================================================== Show More Div ====================================================
function hide_more_div() {
    var x = document.getElementById("more_action");
    if (x.style.display === "none") {
        x.style.display = "block";
    }
    else {
        x.style.display = "none";
    }
}


//==================================================  To Rial ==========================================================
function ToRial(str) {
    str = str.replace(/\,/g, "");
    var objRegex = new RegExp("(-?[0-9]+)([0-9]{3})");
    while (objRegex.test(str)) {
        str = str.replace(objRegex, "$1,$2");
    }
    return str;
}

//================================================= to excel ===========================================================
function toexcel(title, divName) {
    $("#" + divName).table2excel({
        exclude: ".noExl",
        name: "Excel Document Name",
        filename: title,
        fileext: ".xls",
        exclude_img: true,
        exclude_links: true,
        exclude_inputs: true
    });
}

///////////////////////////////////////////////////////////////////////////////

function ConfirmForDelete(text, rootPath) {
    swal({
            title: "آیا مطمین هستید؟",
            text: text,
            type: "warning",
            showCancelButton: true,
            cancelButtonText: "لغو",
            confirmButtonText: "تائید",
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        },
        function () {
            location.href = 'index.php?r=' + rootPath;
            // {{--                    location.href =  {{url("Question/storeQuestion")}};--}}
        });
}

function checkForm(form) {
    if (form.name.value == "") {
        swal("خطا", "نام نمی تواند خالی باشد!", "error")
        // alert("Error: name cannot be blank!");
        loginform.name.focus();
        return false;
    }
    if (form.userName.value == "") {
        swal("خطا", "نام کاربری نمی تواند خالی باشد!", "error")
        form.userName.focus();
        return false;
    }

    if (form.oldPassword.value == "1") {
        swal("خطا", "گذر واژه قدیم نمی تواند خالی باشد!", "error")
        // alert("Error: name cannot be blank!");
        form.oldPassword.focus();
        return false;
    }
    // re = /^\w+$/;
    // if(!re.test(form.username.value)) {
    //     swal("خطا","کاراکتر غیر مجاز در نام کاربری","error")
    //     form.username.focus();
    //     return false;
    // }

    if (form.pwd1.value != "" && form.pwd1.value == form.pwd2.value) {
        if (form.pwd1.value.length < 6) {
            swal("خطا", "گذر واژه نمی تواند کمتر از شش کاراکتر باشد!", "error")
            form.pwd1.focus();
            return false;
        }
        if (form.pwd1.value == form.username.value) {
            swal("خطا", "نام کاربری نمیتواند با گذر واژه یکسان باشد!", "error")
            form.pwd1.focus();
            return false;
        }
        // re = /[0-9]/;
        // if(!re.test(form.pwd1.value)) {
        //     alert("Error: password must contain at least one number (0-9)!");
        //     form.pwd1.focus();
        //     return false;
        // }
        // re = /[a-z]/;
        // if(!re.test(form.pwd1.value)) {
        //     alert("Error: password must contain at least one lowercase letter (a-z)!");
        //     form.pwd1.focus();
        //     return false;
        // }
        // re = /[A-Z]/;
        // if(!re.test(form.pwd1.value)) {
        //     alert("Error: password must contain at least one uppercase letter (A-Z)!");
        //     form.pwd1.focus();
        //     return false;
        //
        // }
    } else {
        swal("خطا", "گذر واژه با تکرار آن باید یکسان باشد!", "error")
        form.pwd1.focus();
        return false;
    }
    // alert("You entered a valid password: " + form.pwd1.value);
    return true;
}

function checkFormchangepass(form) {
    if (form.oldPassword.value == "") {
        swal("خطا", "گذر واژه قدیم نمی تواند خالی باشد!", "error")
        // alert("Error: name cannot be blank!");
        form.oldPassword.focus();
        return false;
    }
    if (form.oldPassword.value == form.pwd1.value) {
        swal("خطا", "گذرواژه جدید نمی تواند با گذر واژه قدیم یکسان باشد!", "error")
        // alert("Error: name cannot be blank!");
        form.oldPassword.focus();
        return false;
    }
    if (form.pwd1.value != "" && form.pwd1.value == form.pwd2.value) {
        if (form.pwd1.value.length < 6) {
            swal("خطا", "گذر واژه نمی تواند کمتر از شش کاراکتر باشد!", "error")
            form.pwd1.focus();
            return false;
        }
        if (form.pwd1.value == form.username.value) {
            swal("خطا", "نام کاربری نمیتواند با گذر واژه یکسان باشد!", "error")
            form.pwd1.focus();
            return false;
        }
    } else {
        swal("خطا", "گذر واژه با تکرار آن باید یکسان باشد!", "error")
        form.pwd1.focus();
        return false;
    }
    // alert("You entered a valid password: " + form.pwd1.value);
    return true;
}

function validationSocialClass(form) {
    if (form.SECName.value == "") {
        swal("خطا", "نام طبقه اجتماعی نمی تواند خالی باشد!", "error")
        // alert("Error: name cannot be blank!");
        form.SECName.focus();
        return false;
    }
    if (form.caption.value == "") {
        swal("خطا", "عنوان نمی تواند خالی باشد!", "error")
        // alert("Error: name cannot be blank!");
        form.caption.focus();
        return false;
    }
    if (form.SECScoreMin.value == "") {
        swal("خطا", "حد پایین نمی تواند خالی باشد!", "error")
        // alert("Error: name cannot be blank!");
        form.SECScoreMin.focus();
        return false;
    }
    if (form.SECScoreMax.value == "") {
        swal("خطا", "حد بالا نمی تواند خالی باشد!", "error")
        // alert("Error: name cannot be blank!");
        form.SECScoreMax.focus();
        return false;
    }
    re = /[0-9]/;
    if (!re.test(form.SECScoreMax.value) | !re.test(form.SECScoreMin.value) | !re.test(form.count.value)) {
        swal("خطا", "ورودی غیرمجاز(ورودی فیلد های عدد را کنترل کنید)!", "error")
        return false;
    }
    if (parseInt(form.SECScoreMin.value) > parseInt(form.SECScoreMax.value)) {
        swal("خطا", "مقدار فیلد حد پایین نمی تواند از حد بالا بزرگتر باشد!", "error")
        return false;
    }
    re = /[A-Z]/;
    if (!re.test(form.SECName.value)) {
        swal("خطا", "نام(به انگلیسی) باید حتما با حروف بزرگ و انگلیسی وارد شود!", "error")
        form.SECName.focus();
        return false;
    }
    return true;
}

function validateSearchPorseshname(form) {
    if (form.search_value.value == "") {
        swal("خطا", "مورد جستجو نمیتواند خالی باشد!", "error")
        return false;
    }

    return true;

}


// =======================================================================================================================================================