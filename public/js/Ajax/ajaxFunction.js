/*
function getProjectRowValueByAjaxForUpdate(idProject) {
    // sweetAlert("تااینجا");
    $.get({
        url: URL_ROOT + "?r=projects/getRowForUpdate",
        data: "idP=" + idProject,
        success: function (data) {
            $("#detectAction").val("update")
            $("#idProject").val(data["id"])
            $("#subject").val(data["subject"])
            $("#startDate").val(data["startDate"])
            $("#endDate").val(data["endDate"])
            $("#questionunmer").val(data["questionunmer"])
            $("#pasokhgooNumber").val(data["pasokhgooNumber"])
            $("#geoLocationRequire").val(data["geoLocationRequire"])
            // $("#collapse3").addClass("in")
            $("#subject").focus();
            // $('.collapse').collapse("in")
        },
        error: function () {

            sweetAlert("حطا");
        }
    }).done(function () {
        // $("#spin").spinner();

    })
}
*/


function getProjectRowValueByAjaxForUpdate(idProject) {
    // sweetAlert("تااینجا");
    $('.panel-collapse1').collapse('hide');
    $('.panel-collapse0').collapse('show');
    $.get({
        url: URL_ROOT + "?r=projects/getRowForUpdate",
        data: "idP=" + idProject,
        success: function (data) {
            $("#detectAction").val("update")
            $("#idProject").val(data["id"])
            $("#subject").val(data["subject"])
            $("#startDate").val(data["startDate"])
            $("#endDate").val(data["endDate"])
            $("#questionunmer").val(data["questionunmer"])
            $("#pasokhgooNumber").val(data["pasokhgooNumber"])
            $("#geoLocationRequire").val(data["geoLocationRequire"])
            // $("#collapse3").addClass("in")
            $("#subject").focus();
            // $('.collapse').collapse("in")
        },
        error: function () {

            sweetAlert("حطا");
        }
    }).done(function () {
        // $("#spin").spinner();

    })
}

function getPasokhgooRowForUpdate(idPasokhgoo) {
    $.get({
            url: URL_ROOT + "?r=pasokhgoo/getRowForUpdate",
            data: "id=" + idPasokhgoo,
            success:function (data) {
                $("#action").val(["update"])
                $("#id").val(data["id"])
                $("#idPorseshname").val(data["idPorseshname"])
                $("#name").val(data["name"])
                $("#fName").val(data["fName"])
                $("#cityName").val(data["cityName"]).change();
                $("#cityArea").val(data["cityArea"]).change();
                $("#homeAddress").val(data["homeAddress"])
                $("#workAddress").val(data["workAddress"])
                $("#phoneNumber").val(data["phoneNumber"])
                $("#mobileNumber").val(data["mobileNumber"])
                $("#name").focus();
                $("#Btn_sabt").removeAttr("disabled");
                // $("#Btn_sabt").attr("disabled", "enabled");
            },
        error: function () {
            sweetAlert("خطا!داده ای از سمت سرور دریافت نشد");
        }
    }).done(function () {
        // $("#spin").spinner();

    })
}

function getQuestionAndOptionRowForUpdate(idQuestion) {
    alert(idQuestion);
    $.get({
        url: URL_ROOT + "?r="
    })
}

function getRowForUpdateSocialClass(id) {
    // sweetAlert("تا اینجا");
    $.get({
        url: URL_ROOT + "?r=projects/getSocialclassByAjaxForupdate",
        data: "id=" + id,
        success: function (data) {
            $("#act").val("update")
            $("#idSocialClass").val(data["id"])
            $("#SECName").val(data["SECName"])
            $("#SECcaption").val(data["caption"])
            $("#count").val(data["count"])
            $("#SECScoreMin").val(data["SECScoreMin"])
            $("#SECScoreMax").val(data["SECScoreMax"])
            $("#SECName").focus()
        },
        error: function () {

        },
        done: function () {

        }

    })
}

function getRowForEditQuestion(idQuestion) {
    $('#collapse3').collapse('show');
    $('#collapse2').collapse('hide');
    $.get({
        url: URL_ROOT + "?r=Question/getRowForEdit",
        data: "idQuestion=" + idQuestion,
        success: function (data) {
            $("#actionQuestion").val("update")
            $("#idQ").val(data["id"])
            $("#rowNumber").val(data['rowNumber'])
            $("#questionTypeSelect").val(data['idQuestionType']).change()
            if(data['idQuestionType']=='6'){
                $("#minNumberType").val(data['minNumberType'])
                $("#maxNumberType").val(data['maxNumberType'])
            }
            if(data['idQuestionType']=='7'){
                $("#rangeValue").val(data['rangeValue'])
                $("#rigthLable").val(data['rigthLable'])
                $("#centerLable").val(data['centerLable'])
                $("#leftLeble").val(data['leftLeble'])
                $("#rangeTeifi").val(data['rangeTeifi'])
                $("#currentRangeValue").val(data['rangeTeifi'])
            }
            if(data['idQuestionType']=='15'){
                $("#videoURL").val(data['videoURL']);
                $("#videoURL").setFocus();

            }
            if(data['idQuestionType']=='16'){
                var imgURL=data['idProject']+data['idQuestionType']+data['rowNumber']+'.jpeg';
                imgFullDir=URL_ROOT+"/public/imagesUpload/question/"+ imgURL + '?' + Math.random();
                $("#imageHideWhenNofile").prop("hidden", false);
                $("#tempImg").attr('src',imgFullDir);
                $("#uploadFile").removeClass('required');


            }
            if(data['idQuestionType']=='17'){
                var audioURL=data['idProject']+data['idQuestionType']+data['rowNumber']+'.mp3';
                audioFullDir=URL_ROOT+"/public/imagesUpload/question/"+ audioURL + '?' + Math.random();
                $("#audioPlayer").attr('src',audioFullDir);
                $("#uploadAudioFile").removeClass('required');

                setTimeout(function () {
                    document.getElementById('audioPlayer').play();
                }, 500);


            }

            $("#questionCode").val(data['questionCode'])
            $("#textQ").val(data['text'])
            $("#questionCode").val(data['questionCode'])
            $("#moreExplanation").val(data['moreExplanation'])
            $("#haveImage").val(data['haveImage'])
            $("#allowNull").val(data['allowNull']).change()
        },
        error: function () {
            sweetAlert("خطا");
        }
    }).done(function () {
        // $("#spin").spinner();

    })

}
$(document).ready(function(){
    $(".lockState").click(function(e){
        e.preventDefault();
        // url=$(this).attr("href");
        id=($(this).data("id"));
        curentLockedValue=($(this).data("lockedbyeditor"));
        var thisElement=$(this);
        $.get({
            url:URL_ROOT + "?r=porseshname/changeLockStateByAdminProjectAjax",
            data:"id="+id+"&curentLockedValue="+curentLockedValue,
            success:function (data) {
                $(thisElement).data("lockedbyeditor",data.result)
                if(data.result==0){
                   $(thisElement).find("img:last").show();
                    $(thisElement).find("img:first").hide();
                }else{
                    $(thisElement).find("img:last").hide();
                    $(thisElement).find("img:first").show();
                }

            }
        }).done(function(data){
            // alert("done")
        })
    })
})



