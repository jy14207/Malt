<?php
/**
 * Created by PhpStorm.
 * User: Jafari
 * Date: 22/11/2017
 * Time: 01:02 PM
 */

namespace App\Models;


class options extends Model
{
    function __construct()
    {
        parent::__construct();
        $this->table_name = "options";
    }

    public function storeOption($idInsertedQuestion, $option, $moreExplanation, $score, $haveImage)
    {
        return $this->insertToDatabase(array(
            "idQuestion" => $idInsertedQuestion,
            "textOption" => $option,
            "moreExplanation" => $moreExplanation,
            "haveImage" => $haveImage,
            "score" => $score), "options");
    }

    public function updateOption($id, $idQuestion, $option, $moreExplanation, $score)
    {
        return $this->_update(array(
            "idQuestion" => $idQuestion,
            "textOption" => $option,
            "moreExplanation" => $moreExplanation,
            "score" => $score), $id, "options");
    }
    public function updateTextOptionForChangeQType($id,$textOption)
    {
        return $this->_update(array(
            "textOption" => $textOption), $id, "options");
    }

    public function updateImgOption($id, $haveImage)
    {
        return $this->_update(array(
            "haveImage" => $haveImage), $id, "options");
    }

    public function deleteOption($idQuestion)
    {
        $this->delete($this->table_name, "idQuestion", $idQuestion);

    }

    public function getRowForEditQuestion($qID)
    {
        return $this->_select("*", "`idQuestion`='" . $qID . "'", $this->table_name);
    }

    public function getThisQuestionOptionsf($idQuestion)
    {
//        dd($idQuestion);
        return $this->_select("*", "`idQuestion`='" . $idQuestion . "' ORDER BY `id` ASC");

    }

    public function getTextOption($id)
    {
        return $this->_selectOne("*", "`id`='" . $id . "'");
    }
    public function getScoreById($id)
    {
        return $this->_selectOne("score", "`id`='" . $id . "'");
    }

    public function deleteOpt($id)
    {
        $this->delete($this->table_name, "id", $id);
    }

    public function selectOptionByScore($idQ, $score)
    {
        return $this->_selectOne("*", "`idQuestion`='" . $idQ . "' AND `score`='" . $score . "'");
    }
    public function getById($id)
    {
        return $this->_selectOne("*","`id`='$id'");

    }
    public function maxScore($idQuestion)
    {
        return $this->getMaxScore($idQuestion);
    }

    //================================================= Android Function
    public function getOptionsForAndroid($idProject)
    {
        $stmt = $this->db->query("SELECT options.* FROM options INNER JOIN questions ON options.idQuestion=questions.id WHERE questions.idProject= '" . $idProject . "'");
        $stmt->execute();
        $row = $stmt->fetchall();
        return $row;
    }
}