<?php
/**
 * Created by PhpStorm.
 * User: Jafari
 * Date: 06/11/2017
 * Time: 11:41 AM
 */

namespace App\Models;
use App\Classes\Timer;
use  App\DB;
use PDO;
use App\Classes\Auth;
class Model extends DB
{
    public $userInfo;
    function __construct()
    {
//        $this->userInfo=Auth::getUser();
        parent::__construct();
    }
    protected $table_name;
//    -----------------------------------------------------------------------------------------function _select
    function _select($fields = "*", $where = "1", $tableName = null)
    {
        Timer::counter();
        if (!$tableName)
            $tableName = $this->table_name;
        $this->connectToDb();
        if (is_array($fields)) {
            $cama = "";
            foreach ($fields as $field) {
                $fieldSet = "$cama`$field`";
                $cama = ",";
            }
        } else {
            $fieldSet = $fields;
        }
        $stmt = $this->db->query("SELECT $fieldSet  FROM `$tableName` WHERE $where");
//        var_dump($stmt);
        $stmt->execute();
        $row = $stmt->fetchall();
        return $row;
    }

    function _selectOne($fields = "*", $where = "1", $tableName = null)
    {
//        die($where);
        if (!$tableName)
            $tableName = $this->table_name;
//        echo $tableName;
        $this->connectToDb();
        if (is_array($fields)) {
            $cama = "";
            foreach ($fields as $field) {
                $fieldSet = "$cama`$field`";
                $cama = ",";
            }
        } else {
            $fieldSet = $fields;
        }
        $stmt = $this->db->query("SELECT $fieldSet  FROM `$tableName` WHERE $where LIMIT 1");
//        var_dump($stmt);
        $stmt->execute();
        $row = $stmt->fetch();
        return $row;
    }
//    -----------------------------------------------------------------------------------------function _insert
    public function insertToDatabase($insertArray, $tableName = null)
    {
        if (!$tableName)
            $tableName = $this->table_name;
        $set = "";
        $cama = "";
        foreach ($insertArray as $key => $value) {
            $set .= "$cama`$key`";
            $cama = ",";
        }
        $valueSet = "";
        $cama = "";
        foreach ($insertArray as $key => $value) {
            $valueSet .= "$cama :$key";
            $cama = ",";
        }
        $sql = "INSERT INTO `$tableName` ($set) VALUES ($valueSet)";
        $st = $this->db->prepare($sql);
        foreach ($insertArray as $key => $value) {
            $st->bindValue(":$key", $value);
        }
        $st->execute();
        $id = $this->db->lastInsertId();
        $st = $this->db->prepare("select * from $tableName where `id`=:id");
        $st->bindvalue(":id", $id, PDO::PARAM_INT);
        $st->execute();
        return $st->fetch();;
    }
//    ----------------------------------------------------------------------------------------function _update
    function _update($updateArray, $id, $tableName = null)
    {
        if (!$tableName)
            $tableName = $this->table_name;
        $set = "";
        $cama = "";
        foreach ($updateArray as $key => $value) {
            $set .= "$cama`$key`=:$key";
            $cama = ",";
        }
        $sql = "UPDATE `$tableName` SET $set WHERE `id`=:id";
        $st = $this->db->prepare($sql);
        $st->bindValue(":id", $id);
        foreach ($updateArray as $key => $value) {
            $st->bindValue(":$key", $value);
        }
        $st->execute();
        return $st->rowCount();
    }
    public function all()
    {
        return $this->_select('*');
    }
    public function delete($table_name, $field_name, $where_condition)
    {
        try {
            $stmt = $this->db->prepare("DELETE FROM `" . $table_name . "` WHERE `" . $field_name . "`='" . $where_condition . "'");
//            var_dump($stmt);exit(0);
            $stmt->execute();
        } catch (PDOException $e) {

            ?>
            <script>alert(متاسفانه  در  انجام عمل حذف خطایی رخ  داده است. );</script><?php
        }
    }
    public function selectJoinPorseshnamePasokhdooForReport($idP)
    {

        $stmt = $this->db->query("SELECT pasokhgoo.* FROM `prseshname` INNER JOIN pasokhgoo on prseshname.id=pasokhgoo.idPorseshname WHERE prseshname.idProject=" . $idP);
        $stmt->execute();
//        var_dump($stmt);
        $row = $stmt->fetchall();
        return $row;
    }
    public function selectJoinOptionsForReport($idP)
    {
        $stmt = $this->db->query("SELECT options.*,questions.id,questions.idProject FROM options INNER JOIN questions ON options.idQuestion = questions.id WHERE questions.idProject =" . $idP);
        $stmt->execute();
        $row = $stmt->fetchall();
        return $row;
    }
    public function getPorseshgarsList($idCompany, $idProject, $userType)
    {
        $stmt = $this->db->query("SELECT userproject.*,users.* FROM `userproject` INNER JOIN users ON userproject.idUser=users.id WHERE userproject.`idCompany`='$idCompany' AND `idProject`='$idProject' AND users.`level`='$userType'");
        $stmt->execute();
        $row = $stmt->fetchall();
//        var_dump($row);
        return $row;
    }
    public function getListPorseshnameForaProject($idProject)
    {
        $stmt = $this->db->query("SELECT prseshname.*,users.name,users.fName  FROM `prseshname` INNER JOIN users ON prseshname.idPorseshgar=users.id WHERE prseshname.softDelete='No' AND prseshname.idProject=$idProject");
        $stmt->execute();
        $row = $stmt->fetchall();
//        var_dump($row);
        return $row;
    }
    public function selectJoinAnswersForReport($idP)
    {
        $stmt = $this->db->query("SELECT answer.*,prseshname.id AS idp,prseshname.idProject FROM prseshname INNER JOIN answer ON answer.idPorseshname = prseshname.id WHERE prseshname.idProject =" . $idP);
        $stmt->execute();
        $row = $stmt->fetchall();
        return $row;
    }
    public function selectJoinDatamainingForReport($idP)
    {
        $stmt = $this->db->query("SELECT project.*,prseshname.*,pasokhgoo.*,questions.*,answer.* FROM `project` INNER JOIN prseshname ON project.id = prseshname.idProject INNER JOIN pasokhgoo on prseshname.id = pasokhgoo.idPorseshname INNER JOIN questions ON project.id=questions.idProject INNER JOIN answer ON prseshname.id=answer.idPorseshname  WHERE project.id = " . $idP);
        $stmt->execute();
        $row = $stmt->fetchall();
        return $row;
    }
    public function innerJoinGetAnswerForporseshnameReviwer($idPorseshname)
    {
        $stmt = $this->db->query("SELECT answer.*,questions.id AS idQ,questions.questionCode,questions.text AS textQ,questions.rowNumber AS rowNumber FROM `answer` INNER JOIN questions ON answer.idQuestion=questions.id WHERE idPorseshname=$idPorseshname");
        $stmt->execute();
        $row = $stmt->fetchall();
        return $row;
    }
    public function innerjoidGetPolicyStop($idPolicy, $idProject, $idQuestion)
    {

//        $stmt = $this->db->query("SELECT policyset.*,options.textOption,questions.rowNumber,questions.text FROM policyset INNER JOIN options ON policyset.idOption=options.id INNER JOIN questions ON questions.id=policyset.JumpDestIdQ WHERE policyset.idPolicy='$idPolicy' AND policyset.idProject='$idProject' AND policyset.idQuestion='$idQuestion'");
        $stmt = $this->db->query("SELECT policyset.*,options.idQuestion,options.textOption FROM policyset INNER JOIN options ON policyset.idOption=options.id WHERE  policyset.idQuestion = $idQuestion AND idProject=$idProject AND idPolicy = $idPolicy");
        $stmt->execute();
        $row = $stmt->fetchall();
//        var_dump($stmt);
        return $row;
//        dd($stmt);
    }
    public function innerjoidGetPolicyjump($idPolicy, $idProject, $idQuestion)
    {

//        $stmt = $this->db->query("SELECT policyset.*,options.textOption,questions.rowNumber,questions.text FROM policyset INNER JOIN options ON policyset.idOption=options.id INNER JOIN questions ON questions.id=policyset.JumpDestIdQ WHERE policyset.idPolicy='$idPolicy' AND policyset.idProject='$idProject' AND policyset.idQuestion='$idQuestion'");
        $stmt = $this->db->query("SELECT policyset.*,questions.rowNumber AS rowNumber ,questions.text,options.* FROM policyset INNER JOIN questions ON policyset.JumpDestIdQ=questions.id INNER JOIN options ON policyset.idOption = options.id WHERE policyset.idPolicy=$idPolicy AND policyset.idProject=$idProject AND policyset.idQuestion=$idQuestion");
        $stmt->execute();
        $row = $stmt->fetchall();
//        var_dump($stmt);
        return $row;
//        dd($stmt);
    }
    public function innerjoinPolicyansToOtherQPol($idPolicy, $idProject, $idQuestion)
    {
//        var_dump($idQuestion);
        $stmt = $this->db->query("SELECT policyset.*,questions.id AS idQ,questions.text,questions.rowNumber FROM `policyset` INNER JOIN questions on questions.id=policyset.idOtherQ WHERE policyset.idProject=$idProject AND policyset.idPolicy=$idPolicy AND policyset.idQuestion=$idQuestion ");
        $stmt->execute();
        $row = $stmt->fetchall();
//        var_dump($stmt);
        return $row;
    }
    public function getTopUserRandom($idCompany)
    {
        $stmt = $this->db->query("SELECT * FROM users WHERE `idCompany`='$idCompany' AND `status`='Active' ORDER BY RAND() LIMIT 8");
        $stmt->execute();
        $row = $stmt->fetchall();
//        var_dump($stmt);
        return $row;
    }
    public function getRowsForCalacSocialClassM($idPorseshnameh)
    {
        $stmt = $this->db->query("SELECT policyset.*,answer.* FROM policyset INNER JOIN answer on policyset.idQuestion=answer.idQuestion WHERE policyset.idPolicy=1 AND answer.idPorseshname =" . $idPorseshnameh);
        $stmt->execute();
        $row = $stmt->fetchall();
//        var_dump($stmt);
        return $row;
    }
    public function getPolicyansToOtherM($idPorseshnameh, $idquestion)
    {
        $stmt = $this->db->query("SELECT answer.idOption AS idOptionans,policyset.* FROM `answer` INNER JOIN policyset on answer.idQuestion=policyset.idQuestion WHERE answer.idPorseshname=$idPorseshnameh AND policyset.idOtherQ=$idquestion AND policyset.idPolicy=4");
        $stmt->execute();
        $row = $stmt->fetchall();
//        var_dump($stmt);
        return $row;
    }
    public function getListPorseshnameOfPorseshgarJoin($idProject, $ididPorseshgar)
    {
        $stmt = $this->db->query("SELECT prseshname.id, prseshname.porseshnamehNumber,prseshname.completedDate,prseshname.softDelete ,prseshname.Confirmed,prseshname.EditedByReviewer,prseshname.lockedByEditor ,pasokhgoo.name,pasokhgoo.fName FROM prseshname INNER JOIN pasokhgoo ON prseshname.id=pasokhgoo.idPorseshname   WHERE prseshname.idProject=$idProject AND prseshname.idPorseshgar=$ididPorseshgar AND prseshname.softDelete='No'");
        $stmt->execute();
        $row = $stmt->fetchall();
//        var_dump($stmt);
        return $row;
    }
    public function createDtatamainingTableModel()
    {
//        $fieldListArray,$projectName
        $stmt = $this->db->query("CREATE TABLE `malt`.`datamainingT1` ( `id` INT NOT NULL , `idPorseshname` INT NOT NULL , `completedDate` VARCHAR(255) NOT NULL , `startTime` VARCHAR(255) NOT NULL , `endTime` VARCHAR(255) NOT NULL ) ENGINE = MyISAM");
//        $stmt->execute();
//        $row = $stmt->fetchall();
//        dd("تا اینجا");
    }
    public function getAllAnswerAllPorseshnamehThisProjectM($idProject)
    {
        $stmt = $this->db->query("SELECT answer.* FROM answer INNER JOIN prseshname ON answer.idPorseshname=prseshname.id where prseshname.idProject=$idProject ORDER BY `idPorseshname` ASC");
        $stmt->execute();
//        var_dump($stmt);
        $row = $stmt->fetchall();
        return $row;
//        dd($stmt);
    }
    public function getAllOptionThisProjectM($idProject)
    {
        $stmt = $this->db->query("SELECT options.* FROM options INNER JOIN questions ON options.idQuestion=questions.id where questions.idProject=$idProject");
        $stmt->execute();
//        var_dump($stmt);
        $row = $stmt->fetchall();
        return $row;
//        dd($stmt);
    }
    public function getPorseshgarOthisProjectm($idProject)
    {
        $stmt = $this->db->query("SELECT users.* FROM users INNER JOIN prseshname ON users.id=prseshname.idPorseshgar WHERE prseshname.idProject=$idProject GROUP BY users.id");
        $stmt->execute();
        $row = $stmt->fetchall();
//        var_dump($stmt);
        return $row;
//        dd($stmt);
    }
    public function getMaxScore($idQuestion)
    {
        $stmt = $this->db->query("SELECT MAX(score)AS maxScore FROM `options` WHERE idQuestion = $idQuestion");
        $stmt->execute();
        $row = $stmt->fetchall();
        return $row;
    }
    function shiftUpOrDown($tableName, $fieldName, $fromValue, $method ,$idProject)
    {
        if (!$tableName)
            $tableName = $this->table_name;
        if ($method == "+") {
//            $sql = "UPDATE $tableName SET $fieldName=$fieldName+1 WHERE $fieldName>=$fromValue";
            $sql = "UPDATE $tableName SET $fieldName=$fieldName+1 WHERE $fieldName>=$fromValue AND `idProject` = $idProject";
        } elseif ($method == "-") {
            $sql = "UPDATE $tableName SET $fieldName=$fieldName-1 WHERE $fieldName>=$fromValue AND `idProject` = $idProject";
        }
        $st = $this->db->prepare($sql);
//        dd($st);
        $st->execute();
        return $st->rowCount();
    }


    // Android

    public function getUserCompanyForAndroidModel($idUser)
    {
        $stmt = $this->db->query("SELECT users.* , company.Name AS companyName,company.webSiteLink AS webSiteLink FROM `users` INNER JOIN company ON users.idCompany = company.id WHERE users.id=" . $idUser." LIMIT 1");
        $stmt->execute();
//        var_dump($stmt);
        $row = $stmt->fetch();
        return $row;
    }
    public function _updateByApp($updateArray, $idProject, $porseshnamehNumber, $tableName = null)
    {
        if (!$tableName)
            $tableName = $this->table_name;
        $set = "";
        $cama = "";
        foreach ($updateArray as $key => $value) {
            $set .= "$cama`$key`=:$key";
            $cama = ",";
        }
        $sql = "UPDATE `$tableName` SET $set WHERE `idProject`=:idProject AND `porseshnamehNumber`=:porseshnamehNumber";
        $st = $this->db->prepare($sql);
        $st->bindValue(":idProject", $idProject);
        $st->bindValue(":porseshnamehNumber", $porseshnamehNumber);
        foreach ($updateArray as $key => $value) {
            $st->bindValue(":$key", $value);
        }
        $st->execute();
        return $st->rowCount();
    }
}


