<?php
/**
 * Created by PhpStorm.
 * User: Jafari
 * Date: 14/04/2018
 * Time: 09:56 PM
 */

namespace App\Models;
class userproject extends Model
{
    function __construct()
    {
        parent::__construct();
        $this->table_name = "userproject";
    }
    public function setProject($idCompany, $idUser, $idProject)
    {

        $this->insertToDatabase(array(
            "idCompany" => $idCompany,
            "idUser" => $idUser,
            "idProject" => $idProject,
        ), "userproject");
    }
    public function getUserProject($idUser)
    {
        return $this->getListUserProject($idUser);
    }
    private function getListUserProject($idUser)
    {
        $stmt = $this->db->query("SELECT userproject.* ,project.* FROM project INNER JOIN userproject ON project.id =userproject.idProject WHERE userproject.idUser='" . $idUser . "' AND project.status='Active'");
//        var_dump($stmt);
        $stmt->execute();
        $row = $stmt->fetchall();
        return $row;
    }
    public function adminPAllProjects($idUser)
    {
        $stmt = $this->db->query("SELECT userproject.* ,project.* FROM project INNER JOIN userproject ON project.id =userproject.idProject WHERE userproject.idUser=$idUser");
        $stmt->execute();
        $row = $stmt->fetchall();
        return $row;
    }
    public function adminPActiveProjects($idUser)
    {
        $stmt = $this->db->query("SELECT userproject.* ,project.* FROM project INNER JOIN userproject ON project.id =userproject.idProject WHERE userproject.idUser=$idUser AND project.status='Active'");
        $stmt->execute();
        $row = $stmt->fetchall();
        return $row;
    }
    public function adminPinActiveProjects($idUser)
    {
        $stmt = $this->db->query("SELECT userproject.* ,project.* FROM project INNER JOIN userproject ON project.id =userproject.idProject WHERE userproject.idUser=$idUser AND project.status='inActive'");
        $stmt->execute();
        $row = $stmt->fetchall();
        return $row;
    }
    public function deleteUserProject($id)
    {
        $this->delete($this->table_name, "id", $id);
    }
    /*    public function getIdUserProject($idUser,$idCompany,$idProject)
        {
            return $this->_selectOne("*","`idCompany`='$idCompany' AND `idUser`")
        }*/
    public function getListProjectPorseshgars($idCompany, $idProject, $userType)
    {
        return $this->getPorseshgarsList($idCompany, $idProject, $userType);
    }
    public function getListPorseshgarProject($idProject)
    {

        $stmt = $this->db->query("SELECT userproject.* ,project.* FROM project INNER JOIN userproject ON project.id =userproject.idProject WHERE project.id='" . $idProject . "' AND project.status='Active'");
//        dd($stmt);
        $stmt->execute();
        $row = $stmt->fetchall();
        return $row;
    }
    public function getByIdUserAndIdProject($idUser, $idProject)
    {
        return $this->_selectOne("*","`idUser`='$idUser' AND `idProject`='$idProject'");
    }

    public function selectUserProjectForAndroid($idUser){
        return $this->_select('*',"`idUser`='$idUser'");
    }
    public function insertoHacketan($postData)
    {
        $HacketanRowID = $this->insertToDatabase(array(
            "nameFname" => $postData['nameFname'],
            "expertise" => $postData['expertise'],
            "email" => $postData['email'],
            "mobileNum" => $postData['mobileNum'],
            "moreDetails" => $postData['moreDetails'],), 'Hacketan');
        return $HacketanRowID;
    }
}