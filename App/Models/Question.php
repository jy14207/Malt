<?php
/**
 * Created by PhpStorm.
 * User: Jafari
 * Date: 08/11/2017
 * Time: 03:50 PM
 */

namespace App\Models;


class Question extends Model
{
    function __construct()
    {
        parent::__construct();
        $this->table_name = "questions";
    }

    public function getQuetion($idProject, $rowNum)
    {
        $row = $this->getQuestionOptions($idProject, $rowNum);
        return $row;
    }

    public function getQuestionOptions($idProject, $rowNum)
    {
        $stmt = $this->db->query("SELECT questions.*,options.id AS idOption,options.idQuestion as opIdQuestion,options.textOption AS opTextOption,options.moreExplanation AS opMoreExplanation,options.score AS opScore ,options.haveImage AS opHaveImage FROM options INNER JOIN questions ON options.idQuestion=questions.id WHERE questions.idProject= '" . $idProject . "'  AND questions.rowNumber='" . $rowNum . "'");
//        var_dump($stmt);
        $stmt->execute();
        $row = $stmt->fetchall();
        return $row;
    }

    public function getQuestionType()
    {
        return $this->_selectOne("*", "`idProject`='" . _get("idProject") . "' AND `rowNumber`='" . _get("rowNum") . "'");
    }

    public function storeQuestion($idProject,$rowNumber,$idQuestionType,$minNumberType,$maxNumberType,$rangeValue,$rigthLable,$centerLable,$leftLeble,$questionCode,$text,$moreExplanation,$allowNull,$videoURL)
    {
//
        $insertedRow = $this->insertToDatabase(array(
            "idProject" => $idProject,
            "rowNumber" => $rowNumber,
            "idQuestionType" => $idQuestionType,
            "minNumberType" => $minNumberType,
            "maxNumberType" => $maxNumberType,
            "rangeValue" => $rangeValue,
            "rigthLable" => $rigthLable,
            "centerLable" => $centerLable,
            "leftLeble" => $leftLeble,
            "questionCode" => $questionCode,
            "text" => $text,
            "moreExplanation" => $moreExplanation,
            "haveImage" => "No",
            "allowNull" =>$allowNull,
            "videoURL" => $videoURL), $this->table_name);
        return $insertedRow["id"];
    }

    public function updateQuestion($idProject,$rowNumber,$idQuestionType,$minNumberType,$maxNumberType,$rangeValue,$rigthLable,$centerLable,$leftLeble,$questionCode,$text,$moreExplanation,$allowNull,$videoURL,$idQ)
    {
//        dd($idQ);
        $updatedRow = $this->_update(array(
            "idProject" => $idProject,
            "rowNumber" => $rowNumber,
            "idQuestionType" => $idQuestionType,
            "minNumberType" => $minNumberType,
            "maxNumberType" => $maxNumberType,
            "rangeValue" => $rangeValue,
            "rigthLable" => $rigthLable,
            "centerLable" => $centerLable,
            "leftLeble" => $leftLeble,
            "questionCode" => $questionCode,
            "text" => $text,
            "moreExplanation" => $moreExplanation,
            "allowNull" =>$allowNull,
            "videoURL" =>$videoURL),$idQ, $this->table_name);
        return $updatedRow["id"];

//
      /* $updatedRow = $this->_update(array(
            "idProject" => _post("idProject"),
            "rowNumber" => _post("rowNum"),
            "idQuestionType" => _post("idQuestionType"),
            "text" => _post("text"),
            "questionCode" => _post("questionCode"),
            "moreExplanation" => _post("moreExplanation"),
            "allowNull" => _post("allowNull")), $idQ, $this->table_name);
        return $updatedRow["id"];*/
    }

    public function getLastNumbrOfQuestionRow($idP)
    {
//        SELECT `rowNumber`  FROM `questions` WHERE `idProject`=3 ORDER BY `rowNumber` ASC
        return $this->_select("`rowNumber`", "`idProject`='" . $idP . "' ORDER BY `rowNumber` ASC", "questions");

    }

    public function checkForDuplicatedValueForRowNumber($rowNumber, $idProject)
    {
        return $this->_select("*", "`rowNumber`='" . $rowNumber . "' AND `idProject`='" . $idProject . "'");
    }

    public function getQuestionList($idProject)
    {
        return $this->_select("*", "`idProject`='" . $idProject . "' ORDER BY `rowNumber` ASC");
    }

    public function getQuestionsCount($idProject)
    {
//        SELECT COUNT(id) AS questionsCount FROM `questions` WHERE 1
        return $this->_select("COUNT(id) AS questionsCount", "idProject=" . $idProject);

    }

    public function deletequestion($id)
    {
        $this->delete($this->table_name, "id", $id);

    }

    public function getById($id)
    {
        return $this->_selectOne("*", "`id`='" . $id . "'", $this->table_name);
    }

    public function updateImg($id, $haveImage)
    {
//        dd($id);
        return $this->_update(array(
            "haveImage" => $haveImage), $id, "questions");
    }

    public function getAllquestion($idProject)
    {
        return $this->_select("*", "`idProject`='" . $idProject . "' ORDER BY `rowNumber` ASC");
    }

    public function getQuestionCode($id, $idProject)
    {
        return $this->_select("questionCode", "`id`='" . $id . "' AND `idProject`='" . $idProject . "'");
    }
    public function getQuestionTypeList()
    {
        return $this->_select("*","1","questiontype");
    }

    public function getQuestionTypeById($id)
    {
        return $this->_selectOne("*", "`id`='" . $id . "'",'questiontype');
    }

}