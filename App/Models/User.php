<?php
/**
 * Created by PhpStorm.
 * User: Jafari
 * Date: 06/11/2017
 * Time: 11:40 AM
 */

namespace App\Models;


class User extends Model
{
    function __construct()
    {
        parent::__construct();
        $this->table_name = "users";

    }

    public function insertUser($idCo)
    {
        $this->insertToDatabase(array(
            "idCompany" => $idCo,
            "level" => _post("level"),
            "name" => _post("name"),
            "fName" => _post("fName"),
            "nationalCode" => _post("nationalCode"),
            "username" => _post("userName"),
            "password" => _post("password"),
            "homeAdd" => _post("homeAdd"),
            "status" => "Active"), $this->table_name);
    }

    public function check($userName, $password)
    {
        return $this->_selectOne("*", "`username`='$userName' AND `password`='$password'  AND `status`='Active'");
    }

    public function getAuthenticatedUser()
    {
        $user = $this->_selectOne("*", "remember_Token= '$_SESSION[user_token]'");
        return $user;
    }

    public function checkForDuplicateRow($userName)
    {
        return $this->_select("*", "`username`='" . $userName . "'");
    }

    public function getUsers()
    {
        return $this->_select("*", "1");
    }
    public function companyGetUsers($idCompany)
    {
        return $this->_select("*", "`idCompany`='" . $idCompany . "'");
    }
    public function getPorseshgarList($idCompany)
    {
        return $this->_select("*", "`idCompany`='" . $idCompany . "' AND `level` = 'porseshgar'");
    }
    public function getadminProjectList($idCompany)
    {
        return $this->_select("*", "`idCompany`='" . $idCompany . "' AND `level` = 'projectAdmin'");
    }

    public function getPorseshgarInf($id)
    {
        return $this->_selectOne("*", "`id`=" . $id);
    }

    public function ChangeState($setState, $id)
    {
        $this->_update(array("status" => $setState), $id, "users");
    }

    public function updateUser($id, $level, $name, $fName, $nationalCode, $homeAdd)
    {
        $updatedRow = $this->_update(array(
            "level" => $level,
            "name" => $name,
            "fName" => $fName,
            "nationalCode" => $nationalCode,
            "homeAdd" => $homeAdd), $id, $this->table_name);
        return $updatedRow["id"];

    }
    public function insertRegisterUser($idCo,$name,$userName,$password)
    {
        $this->insertToDatabase(array(
            "idCompany" => $idCo,
            "level" =>"companyAdmin",
            "name" => $name,
            "fName" => "",
            "nationalCode" => "",
            "username" => $userName,
            "password" =>$password,
            "homeAdd" =>"",
            "status" => "Active"), $this->table_name);
    }

    public function updatePassword($id,$password)
    {
        $updatedRow = $this->_update(array(
            "password" => $password), $id, $this->table_name);
        return $updatedRow["id"];

    }
}