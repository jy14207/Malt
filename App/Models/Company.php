<?php
/**
 * Created by PhpStorm.
 * User: Jafari
 * Date: 02/05/2018
 * Time: 03:37 PM
 */

namespace App\Models;


class Company extends Model
{
    function __construct()
    {
        parent::__construct();
        $this->table_name = "company";
    }
    public function store($name)
    {
        return $this->insertToDatabase(array(
            "name"=>$name),$this->table_name);
    }
    public function getCompany($id)
    {
        return $this->_selectOne("*","`id`='".$id."'");
    }
    public function getUserCompanyForAndroid($idUser){
        return $this->getUserCompanyForAndroidModel($idUser);

    }

}