<?php
/**
 * Created by PhpStorm.
 * User: Jafari
 * Date: 08/11/2017
 * Time: 01:45 PM
 */

namespace App\Models;


class projects extends Model
{
    function __construct()
    {
        parent::__construct();
        $this->table_name = "project";
    }

    public function getProjectById($id)
    {
        return $this->_selectOne("*", "`id`='" . $id . "'");
    }

    public function activeProjects()
    {
        $rows = $this->_select("*", "`status`='Active'  AND `softDelete`='No'");
        return $rows;
    }
    public function companyActiveProjects($idCompany)
    {
        $rows = $this->_select("*", "`status`='Active' AND `idCompany`='" . $idCompany . "' AND `softDelete`='No'");
        return $rows;
    }

    public function inActiveProjects()
    {
        $rows = $this->_select("*", "`status`='inActive'  AND `softDelete`='No'");
        return $rows;
    }
    public function companyInActiveProjects($idCompany)
    {
        $rows = $this->_select("*", "`status`='inActive' AND `idCompany`='" . $idCompany . "' AND `softDelete`='No'");
        return $rows;
    }

    public function insertProject()
    {
        $this->insertToDatabase(array(
            "idCompany" => $_SESSION["idCompany"],
            "subject" => _post("subject"),
            "startDate" => _post("startDate"),
            "startDate" => _post("startDate"),
            "endDate" => _post("endDate"),
            "questionunmer" => _post("questionunmer"),
            "pasokhgooNumber" => _post("pasokhgooNumber"),
            "status" => "Active",
            "softDelete" => "No",
            "geoLocationRequire" => _post("geoLocationRequire")), "project");
    }

    public function getRowForUpdate($idProject)
    {
        return $this->_selectOne("*", "`id`='" . $idProject . "'", "project");
    }

    public function updateProject($idProject)
    {
        $this->_update(array(
            "idCompany" => $_SESSION["idCompany"],
            "subject" => _post("subject"),
            "startDate" => _post("startDate"),
            "endDate" => _post("endDate"),
            "questionunmer" => _post("questionunmer"),
            "pasokhgooNumber" => _post("pasokhgooNumber"),
            "status" => "Active",
            "softDelete" => "No",
            "geoLocationRequire" => _post("geoLocationRequire")), $idProject, "project");
    }

    public function projectChangeState($setState, $id)
    {
        $this->_update(array(
            "status" => $setState), $id, "project");
    }

    public function allProjects()
    {
        $rows = $this->_select("*", "  `softDelete`='No'");
        return $rows;
    }
    public function companyAllProjects($idCompany)
    {
        $rows = $this->_select("*", " `idCompany`='" . $idCompany . "' AND `softDelete`='No'");
        return $rows;
    }

    public function insertCity($idProject, $name)
    {
        $this->insertToDatabase(array(
            "idProject" => $idProject,
            "name" => $name
        ), "city");
    }

    public function getCity($idP)
    {
        return $this->_select("*", "`idProject`='" . $idP . "'", "city");
    }

    public function deleleCity($idCity)
    {
        $this->delete("city", "id", $idCity);
    }

    public function deleteProject($idProject)
    {
        $this->_update(array("softDelete" => "Yes"), $idProject, $this->table_name);
//        $this->delete("project","id",$idProject);
    }
}