<?php
/**
 * Created by PhpStorm.
 * User: Jafari
 * Date: 20/04/2018
 * Time: 02:32 PM
 */

namespace App\Models;


class policy extends Model
{
    function __construct()
    {
        parent::__construct();
//        $this->table_name=""

    }

    public function insertSocialclass($idProject, $SECName, $SECScoreMin, $SECScoreMax, $caption, $count)
    {
        return $this->insertToDatabase(array(
            "idProject" => $idProject,
            "SECName" => $SECName,
            "SECScoreMin" => $SECScoreMin,
            "SECScoreMax" => $SECScoreMax,
            "caption" => $caption,
            "count" => $count), "sec");
    }

    public function updateSocialclass($idProject, $SECName, $SECScoreMin, $SECScoreMax, $caption, $count, $id)
    {
        return $this->_update(array(
            "idProject" => $idProject,
            "SECName" => $SECName,
            "SECScoreMin" => $SECScoreMin,
            "SECScoreMax" => $SECScoreMax,
            "caption" => $caption,
            "count" => $count), $id, "sec");
    }

    public function deleteSocialclass($id)
    {
        $this->delete("sec", "id", $id);
    }

    public function getSocialClass($idProject)
    {
        return $this->_select("*", "`idProject`='" . $idProject . "'", "sec");
    }

    public function getSocialclassByAjaxForupdate($id)
    {
        return $this->_selectOne("*", "`id`='" . $id . "'", "sec");

    }

    public function getPolicyList()
    {
        return $this->_select("*", "1 ORDER BY `id` ASC", "policyslist");
    }

    public function setPolicy($idPolicy, $idProject, $idQuestion, $idOption, $count,$idOtherQ,$JumpDestIdQ)
    {

        return $this->insertToDatabase(array(
            "idPolicy" => $idPolicy,
            "idProject" => $idProject,
            "idQuestion" => $idQuestion,
            "idOption" => $idOption,
            "count" => $count,
            "idOtherQ" => $idOtherQ,
            "JumpDestIdQ" => $JumpDestIdQ), "policyset");
    }

    public function getPolicy($idPolicy, $idProject, $idQuestion)
    {
        if ($idPolicy == 1)
            return $this->_selectOne("*", "`idPolicy`='$idPolicy' AND `idProject`='$idProject' AND `idQuestion`='$idQuestion'", "policyset");
        else
            return $this->_select("*", "`idPolicy`='$idPolicy' AND `idProject`='$idProject' AND `idQuestion`='$idQuestion'", "policyset");

    }

    public function getPolicyStop($idPolicy, $idProject, $idQuestion)
    {
        return $this->innerjoidGetPolicyStop($idPolicy, $idProject, $idQuestion);

    }
    public function getPolicyjump($idPolicy, $idProject, $idQuestion)
    {
        return $this->innerjoidGetPolicyjump($idPolicy, $idProject, $idQuestion);

    }

    public function getPolicyansToOtherQPol($idPolicy, $idProject,$idQuestion)
    {
        return $this->innerjoinPolicyansToOtherQPol($idPolicy, $idProject,$idQuestion);

    }
    public function deletePolicy($id)
    {
        $this->delete("policyset", "id", $id);
    }

    public function getPolicySetForCheck($idProject, $idQuestion)
    {
        return $this->_select("*", "`idProject`='$idProject' AND `idQuestion`='$idQuestion'", "policyset");

    }
    public function getPolicySetForCheckWhenAnswer($idProject, $idQuestion,$idOption)
    {
        return $this->_select("*", "`idProject`='$idProject' AND `idQuestion`='$idQuestion' AND `idOption`=$idOption", "policyset");

    }

    public function getRowsForCalacSocialClass($idPorseshnameh)
    {
        return $this->getRowsForCalacSocialClassM($idPorseshnameh);

    }
    public function getAllPolicysetforAndroid($idProject){
        return $this->_select("*", "`idProject`='$idProject'", "policyset");
    }
}