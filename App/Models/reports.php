<?php
/**
 * Created by PhpStorm.
 * User: Jafari
 * Date: 17/01/2018
 * Time: 02:04 PM
 */

namespace App\Models;


class reports extends Model
{
    public function getProjectRowForReports($idP)
    {
        return $this->_selectOne("*", "`id`='" . $idP . "'", "project");
    }

    public function selectAllQuestionOfProject($idP)
    {
        return $this->_select("*", "`idProject`='" . $idP . "'", "questions");
    }

    public function getPorseshnameRowsForReports($idP)
    {
        return $this->_select("*", "`idProject`='" . $idP . "'", "prseshname");
    }

    public function getPasokhgooRowsForReports($idP)
    {
        return $this->selectJoinPorseshnamePasokhdooForReport($idP);
    }

    public function getOptionsRowsForReports($idP)
    {
        return $this->selectJoinOptionsForReport($idP);
    }

    public function getAnswersRowsForReports($idP)
    {
        return $this->selectJoinAnswersForReport($idP);
    }

    public function getDatamainingRowsForReports($idP)
    {
        return $this->selectJoinDatamainingForReport($idP);
    }
    public function createDtatamainingTable()
    {
       $this->createDtatamainingTableModel();
    }

}