<?php
/**
 * Created by PhpStorm.
 * User: Jafari
 * Date: 24/11/2017
 * Time: 04:46 PM
 */

namespace App\Models;


class porseshname extends Model
{
    function __construct()
    {
        parent::__construct();
        $this->table_name = "prseshname";
    }

    public function insetPorseshname($idProject, $idUser)
    {
        $posreshnameRowID = $this->insertToDatabase(array(
            "porseshnamehNumber" => _post("porseshnamehNumber"),
            "idProject" => $idProject,
            "idPorseshgar" => $idUser,
            "completedDate" => _post("completedDate"),
            "startTime" => _post("startTime"),
            "endTime" => _post("endTime"),
            "Confirmed" => "Yes",
            "softDelete" => "No",
            "Longitude" => _post("Longitude"),
            "Latitude" =>_post("Latitude")), $this->table_name);
        return $posreshnameRowID;
    }
    public function isPorseshnameForThisUserAndThisProject($idPorseshnameh, $idUser, $idProject)
    {
        return $this->_select("*", "`id`='" . $idPorseshnameh . "' AND `idPorseshgar`='" . $idUser . "' AND `idProject`='" . $idProject . "'", $this->table_name);
    }
    public function updatePorseshnameForFinished($idPorseshname, $nowTime)
    {
        $this->_update(array("endTime" => $nowTime), $idPorseshname, $this->table_name);
    }
    public function updatePorseshnameForLocked($idPorseshname,$lockState)
    {
       return $this->_update(array("lockedByEditor" => "$lockState"), $idPorseshname, $this->table_name);
    }
    public function getAllPorseshname($idP)
    {
        return $this->_select("*", "`idProject`=" . $idP ." AND softDelete= 'No' ORDER BY id ASC LIMIT 25");
    }
    public function listPorseshnameForaProject($idProject)
    {
        return $this->getListPorseshnameForAProject($idProject);

    }
    public function confirm($id, $val)
    {
//        dd($val);
        return $this->_update(array("Confirmed" => $val), $id, $this->table_name);
    }
    public function softDeletePorseshnameh($id)
    {
        return $this->_update(array("softDelete" => "Yes"), $id, $this->table_name);
    }
    public function getOnePorseshname($id,$idProject)
    {
        return $this->_selectOne("*", "id= $id AND idProject =$idProject" );

    }
    public function thisPorseshnameForThisProject($id,$idProject)
    {
        return $this->_selectOne("*", "id= $id AND idProject =$idProject" );

    }
    public function updatePorseshnameForEditedByViewer($idPorseshname, $value)
    {
        $this->_update(array("EditedByReviewer" => $value), $idPorseshname, $this->table_name);
    }
    public function getFinishedCount($idProject)
    {
        return (($this->_select("*", "`idProject`='$idProject' AND `endTime` <> '' AND `softDelete` ='No'")));
    }
    public function getReviewCount($idProject)
    {
        return ($this->_select("*", "`idProject`='$idProject' AND `EditedByReviewer` = 'Yes' AND `softDelete` ='No'"));
    }
    public function getbyIdAndLockedByEditor($idPorseshnamehT)
    {
        return $this->_selectOne("*", "id= $idPorseshnamehT AND `lockedByEditor`=0");

    }
    public function getbyIdProjectAndpNumber($porseshnamehNumber, $idProject, $idPorseshgar)
    {
        return $this->_selectOne("*", "`porseshnamehNumber`=$porseshnamehNumber AND `idProject`=$idProject AND `idPorseshgar`=$idPorseshgar AND `lockedByEditor`=0");

    }
    public function getbyIdProjectAndCompleteDate($completeDate, $idProject)
    {
        return $this->_selectOne("*", "`completedDate`='$completeDate' AND `idProject`=$idProject");


    }
    public function PorseshnameCount($idProject)
    {
        return $this->_select("Count(id) as count", "`idProject`='$idProject' AND `softDelete` ='No' AND `Confirmed` ='Yes' AND `endTime` <> ''");

    }
    public function getForDuplicate($porseshnamehNumber, $idProject)
    {
        return $this->_selectOne("*", "`porseshnamehNumber`='$porseshnamehNumber' AND `idProject`='$idProject'  AND `softDelete`= 'No' ");

    }
    public function getForDuplicateAjax($porseshnamehNumber, $idProject)
    {
        return $this->_select("*", "`porseshnamehNumber`='$porseshnamehNumber' AND `idProject`='$idProject' AND `softDelete`= 'No'");

    }
    public function getListPorseshnameOfPorseshgar($idProject, $ididPorseshgar)
    {
        return $this->getListPorseshnameOfPorseshgarJoin($idProject, $ididPorseshgar);
    }
    public function getPorseshgarOthisProject($idProject)
    {
        return $this->getPorseshgarOthisProjectm($idProject);

    }

//    Android Func
    public function getOnePorseshnameByIdProjectAndPorseshnamehNumber($idProject,$porseshnamehNumber)
    {
        return $this->_selectOne("*", "idProject= $idProject AND porseshnamehNumber = $porseshnamehNumber AND softDelete = 'No'" );
    }
    public function updatePorseshnameByAppWhenDuplicated($idPorseshgar, $completedDate, $startTime, $endTime, $Longitude ,$latitude,$idProject, $porseshnamehNumber)
    {
       return $this->_updateByApp(array(
            "idPorseshgar" => $idPorseshgar,
            "completedDate" => $completedDate,
            "startTime" => $startTime,
            "endTime" => $endTime,
            "Longitude" => $Longitude,
            "latitude" => $latitude,
            ), $idProject, $porseshnamehNumber, $this->table_name);
    }
    public function insertPorseshnameByAppWhenDuplicated($porseshnamehNumber,$idProject, $idUser, $completedDate, $startTime,$endTime, $Longitude ,$latitude )
    {
        $posreshnameRowID = $this->insertToDatabase(array(
            "porseshnamehNumber" => $porseshnamehNumber,
            "idProject" => $idProject,
            "idPorseshgar" => $idUser,
            "completedDate" =>$completedDate,
            "startTime" => $startTime,
            "endTime" => $endTime,
            "Confirmed" => "Yes",
            "softDelete" => "No",
            "Longitude" => $Longitude,
            "Latitude" =>$latitude), $this->table_name);
        return $posreshnameRowID;
    }




}