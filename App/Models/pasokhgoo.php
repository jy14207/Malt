<?php
/**
 * Created by PhpStorm.
 * User: Jafari
 * Date: 10/11/2017
 * Time: 06:13 PM
 */

namespace App\Models;
class pasokhgoo extends Model
{
    function __construct()
    {
        parent::__construct();
        $this->table_name = "pasokhgoo";
    }
    public function insertPasokhgoo($idPorseshname, $name, $fName, $homeAddress, $workAddress, $cityName, $cityArea, $phoneNumber, $mobileNumber, $EditedByReviewer )
    {
            $rec = $this->insertToDatabase(array(
                "idPorseshname" => $idPorseshname,
                "name" => $name,
                "fName" => $fName,
                "homeAddress" => $homeAddress,
                "workAddress" => $workAddress,
                "cityName" => $cityName,
                "cityArea" => $cityArea,
                "phoneNumber" => $phoneNumber,
                "mobileNumber" => $mobileNumber,
                "EditedByReviewer" => "No"), $this->table_name);
        return $rec;
    }
    public function searchPasokhgoo($id)
    {

        return $this->_selectOne("*", "`idPorseshname` ='" . $id . "'", $this->table_name);
    }
    public function getCity($idProject)
    {
        return $this->_select("*", "`idProject`='" . $idProject . "'", "city");
    }
    public function getPasokhgoo($idPorseshname)
    {
        return $this->_selectOne("*", "`id` ='" . $idPorseshname . "'", $this->table_name);
    }
    public function getPasokhgooForReport($idPorseshname)
    {
        return $this->_selectOne("*", "`idPorseshname` ='" . $idPorseshname . "'", $this->table_name);
    }
    public function updatePasokhgoo($idPorseshname, $name, $fName, $homeAddress, $workAddress,
                                    $cityName, $cityArea, $phoneNumber, $mobileNumber, $id)
    {
        $rec = $this->_update(array(
            "idPorseshname" => $idPorseshname,
            "name" => $name,
            "fName" => $fName,
            "homeAddress" => $homeAddress,
            "workAddress" => $workAddress,
            "cityName" => $cityName,
            "cityArea" => $cityArea,
            "phoneNumber" => $phoneNumber,
            "mobileNumber" => $mobileNumber,
            "EditedByReviewer" => "Yes"), $id, $this->table_name);
        return $rec;
    }
    public function getPasokhgooByIdPorseshnameh($idPorseshname)
    {
        return $this->_selectOne('*', 'idPorseshname =' . $idPorseshname);
    }

}