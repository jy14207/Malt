<?php
/**
 * Created by PhpStorm.
 * User: Jafari
 * Date: 11/11/2017
 * Time: 12:57 AM
 */

namespace App\Models;

use App\Classes\Auth;

class answer extends Model
{
    function __construct()
    {
        parent::__construct();
        $this->table_name = "answer";
    }

    public function storeAnswer($idPorseshname, $answer1, $idQ,$questionType)
    {
 /*       var_dump($idPorseshname);
        var_dump($answer1);
        var_dump($idQ);
        var_dump($questionType);
        dd("h");*/
        if (is_array($answer1)) {

            $answer = implode(",", $answer1);

        } elseif ($answer1 == null) {
            $answer = "notAnswered";
        } else {
            $answer = $answer1;
        }

        $text =$answer1;// _post("openQuestionAnswer");
        if (_post("others")) {
            $others = _post("others");
        } else if (_post("answerOption")) {
            $others =implode(',', _post("answerOption"));
        }
        else
        {
            $others = "null";
        }
        $isanswer = self::insertOrUpdateAnswer($idPorseshname, $idQ);
        if ($questionType=="openQuestion" |$questionType=="numeric" |$questionType=="spectral" |
            $questionType=="degree" |$questionType=="websitelink" |$questionType=="email"|
            $questionType=="spectral"|$questionType=="dateFormat" |$questionType=="uploadImage") {
            if ($isanswer) {
                $this->_update(array(
                    "idPorseshname" => $idPorseshname,
                    "idQuestion" => $idQ,
                    "idOption" => $questionType,
                    "text" => $text), $isanswer["id"], $this->table_name);
            } else {
                $this->insertToDatabase(array(
                    "idPorseshname" => $idPorseshname,
                    "idQuestion" => $idQ,
                    "idOption" => $questionType,
                    "text" => $text));
            }
        } else {
//            dd($others);

            if ($isanswer) {
                $this->_update(array(
                    "idPorseshname" => $idPorseshname,
                    "idQuestion" => $idQ,
                    "idOption" => $answer,
                    "text" => $others), $isanswer["id"], $this->table_name);
            } else {
                $this->insertToDatabase(array(
                    "idPorseshname" => $idPorseshname,
                    "idQuestion" => $idQ,
                    "idOption" => $answer,
                    "text" => $others));
            }

        }
    }

    public function insertOrUpdateAnswer($idPorseshname, $idQuestion)
    {
        $stmt = $this->db->query("SELECT * FROM `answer` WHERE `idPorseshname`='$idPorseshname' AND `idQuestion` ='$idQuestion'");
        $stmt->execute();
        $row = $stmt->fetch();
        return $row;
    }

    public function getAnswer($idQuestion, $idPorseshname)
    {
        return $this->_selectOne("*", "`idPorseshname`='" . $idPorseshname . "' AND `idQuestion`='" . $idQuestion . "'");
    }

    public function getAnswerForReport($idQuestion, $idporseshname)
    {
        return $this->_selectOne("*", "`idPorseshname`='" . $idporseshname . "' AND `idQuestion`='" . $idQuestion . "'");
    }

    public function getAnswerForporseshnameReviwer($idPorseshnameh)
    {
        return $this->innerJoinGetAnswerForporseshnameReviwer($idPorseshnameh);

    }

    public function getAnswerRowForcheckPolicyCount($idQuestion, $idOption)
    {
        return $this->_select("Count(*) as CountID", "`idQuestion`='" . $idQuestion . "' AND `idOption`='" . $idOption . "'");
    }

    public function getAnswerByidQuestion($idQuestion)
    {
        return $this->_selectOne("*", " `idQuestion`='" . $idQuestion . "'");
    }

    public function getPolicyansToOther($idporseshnameh, $idQuestion)
    {
        return $this->getPolicyansToOtherM($idporseshnameh, $idQuestion);
    }

    public function deleteByIdQuestion($idQ)
    {
        return $this->delete("answer", "idQuestion", $idQ);
    }

    public function getAllAnswerAllPorseshnamehThisProject($idProject)
    {
        return $this->getAllAnswerAllPorseshnamehThisProjectM($idProject);

    }
    //Android
    public function insertAnswer($idPorseshname, $idQuestion, $idOption, $text){
        $this->insertToDatabase(array(
            "idPorseshname" => $idPorseshname,
            "idQuestion" => $idQuestion,
            "idOption" => $idOption,
            "text" => $text));
    }
}