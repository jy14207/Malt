<?php
namespace App;
use PDO;
class DB
{
    protected $table_name;
    public  $db;
    function __construct()
    {
        $this->connectToDb();
    }

    function connectToDb()
    {
        try
        {
            //aradeBartar Config
/*            $dbName= "aradebar_Malt";
            $dbhostName="localhost";
            $dbUserName="aradebar_malt";
            $password="3668413311";*/
            //ANS-Survay Config
/*            $dbName= "anssurve__Malt";
            $dbhostName="localhost";
            $dbUserName="anssurve_malt";
            $password="3668413311";*/

            // laptop Config
            $dbName= "malt";
            $dbhostName="localhost";
            $dbUserName="root";
            $password="";
            $this->db = new PDO("mysql:host=$dbhostName;dbname=$dbName",$dbUserName,$password);
//            var_dump($this->db);
            if(!$this->db)
            {
                die("خطا در اتصال به دیتابیس");
            }
            $this->db -> setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
            $this->db->exec("SET NAMES utf8");
        }
        catch (PDOException $error)
        {
            echo $error->getMessage ();
        }
    }
}