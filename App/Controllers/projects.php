<?php
/**
 * Created by PhpStorm.
 * User: Jafari
 * Date: 08/11/2017
 * Time: 01:43 PM
 */

namespace App\Controllers;

use App\Classes\AccessPrimision;
use App\Classes\Response;
use App\Models\Model;
use App\Models\policy;
use App\Models\projects as ProjectsModel;
use App\Models\Question as QuestionModel;
use App\Classes\Auth as Auth;
use App\Models\policy as plicyM;
use App\Models\User;
use App\Models\userproject;
use App\Models\Model as Model1;
use App\Models\pasokhgoo as pasokhgooModel;
use App\Models\policy as policiM;

class projects extends BaseController
{
    public function index()
    {
        $projects = new ProjectsModel();
        $activeP = new userproject();
        $User = new Auth();
        $userInf = $User->getUser();
        $project = $activeP->getUserProject($userInf["id"]);
//        var_dump($project);
        $this->view("projects.all", compact("project"));
    }

    public function insertProject()
    {
//        dd(_post("detectAction"));
        if (_post("detectAction") == "insert") {
            $projects = new ProjectsModel();
            $projects->insertProject();
            $this->redirect(url("admin/allProjects"));
        } else if (_post("detectAction") == "update") {
            $projects = new ProjectsModel();
            $projects->updateProject(_post("idProject"));
            $this->redirect(url("admin/allProjects"));
        }
    }

    public function getRowForUpdate()
    {
        $acP=new AccessPrimision();
        if(($acP->checkProjectAccess(_get("idP")))==false)
        {
            $this->redirect(url("login/logout",["message" => "true"]));
        }
        $projects = new ProjectsModel();
        $valueForUpdate = $projects->getRowForUpdate(_get("idP"));
        echo Response::ajax($valueForUpdate);

    }

    public function projectChangeState()
    {
        $acP=new AccessPrimision();
        if(($acP->checkProjectAccess(_get("id")))==false)
        {
            $this->redirect(url("login/logout",["message" => "true"]));
            dd("cvn");
        }
        $projects = new ProjectsModel();
        $getProjectByid = $projects->getProjectById(_get("id"));
        $projectNowState = $getProjectByid["status"];
        if ($projectNowState == "Active") {
            $setState = "inActive";
        } else if ($projectNowState == "inActive") {
            $setState = "Active";
        }
        $projects->projectChangeState($setState, _get("id"));
        $this->redirect(url("admin/allProjects"));


    }

    public function deleteProject()
    {
        // Select * from proseshname by idProject
        // Delete from pasokhgoo by idproseshname
        // Delete from pasokhgoo by idproseshname
        // Select * From Question by idProject
        // Delete From Option By idQuestion
        // Delete From answer By idQuestion
        // Delete From Porseshname by idProject
        // Delete From Question by idProject
        $acP=new AccessPrimision();
        if(($acP->checkProjectAccess(_get("idP")))==false)
        {
            $this->redirect(url("login/logout",["message" => "true"]));
            dd("cvn");
        }
        $projects = new ProjectsModel();
        $projects->deleteProject(_get("idP"));
        $this->redirect(url("admin/allProjects"));


    }

    public function insertCity()
    {
        $pm = new ProjectsModel();
        $pm->insertCity($_SESSION["idProject"], _post("cityName"));
        $this->redirect(url("admin/selectedProject",["collapsin"=>"setCity"]));

    }

    public function getCity($idP)
    {
        $gc = new ProjectsModel();
        return $gc->getCity($idP);

    }

    public function deleteCity()
    {
        $dc = new ProjectsModel();
        $dc->deleleCity(_get("id"));
        $this->redirect(url("admin/selectedProject",["collapsin"=>"setCity"]));


    }

    public function loadQuestionPreviewAdmin()
    {
        $projectAuth=new Auth();
        $projectId=$projectAuth->getProject()["id"];
        $question = new QuestionModel();
        $questionRow = $question->getQuetion($projectId, _get("rowNum"));
        if (!empty($questionRow)) {
            $questionType = $this->getQuestionType($questionRow[0]["idQuestionType"]);
            $this->view("question.showPreviewquestionInAdmin", compact("questionRow", "questionType"));
        }

    }

    function getQuestionType($idQuestionType)
    {
        if ($idQuestionType == 1) return ["questionType" => "singleResponse"];
        if ($idQuestionType == 2) return ["questionType" => "multipleChoice"];
        if ($idQuestionType == 3) return ["questionType" => "openQuestion"];
        if ($idQuestionType == 4) return ["questionType" => "singleResponseOthers"];
        if ($idQuestionType == 5) return ["questionType" => "multipleChoiceOthers"];

        if ($idQuestionType == 6) return ["questionType" => "numeric"];
        if ($idQuestionType == 7) return ["questionType" => "spectral"];
        if ($idQuestionType == 8) return ["questionType" => "email"];
        if ($idQuestionType == 9) return ["questionType" => "degree"];
        if ($idQuestionType == 10) return ["questionType" => "websitelink"];

        if ($idQuestionType == 11) return ["questionType" => "prioritize"];
        if ($idQuestionType == 12) return ["questionType" => "noResponse"];
        if ($idQuestionType == 13) return ["questionType" => "openOptions"];
        if ($idQuestionType == 14) return ["questionType" => "dateFormat"];
    }

    public function setSocialclass()
    {
        $SocialClassModel = new plicyM();
        $act = _post("act");
        $pInfo=new Auth();
        $projectInfo=$pInfo->getProject();
        $idProject = $projectInfo["id"];
        if ($act == "insert") {
            $SocialClassModel->insertSocialclass($idProject, _post("SECName"), _post("SECScoreMin"), _post("SECScoreMax"), _post("caption"), _post("count"));
        }
        if ($act == "update") {
            $id = _post("idSocialClass");
            $SocialClassModel->updateSocialclass($idProject, _post("SECName"), _post("SECScoreMin"), _post("SECScoreMax"), _post("caption"), _post("count"), $id);
        }

        $this->redirect(url("admin/selectedProject",["collapsin"=>"SocialClass"]));


    }

    public function deleteSocialclass()
    {
        $SocialClassModel = new plicyM();
        $SocialClassModel->deleteSocialclass(_get("id"));
        $this->redirect(url("admin/selectedProject",["collapsin"=>"SocialClass"]));

    }

    public function getSocialclassByAjaxForupdate()
    {
        $SocialClassModel = new plicyM();
        $data = $SocialClassModel->getSocialclassByAjaxForupdate(_get("id"));
        echo Response::ajax($data);
    }

    public function setPorseshgar()
    {

        $idUser=_post("idPorseshgar");
        if(_get("t")=="pa")
            $collapsin="setprojectAdmin";
        elseif (_get("t")=="po")
            $collapsin="setPorseshgar";

//        dd($idUser);
        $pInfo=new Auth();
        $projectInfo=$pInfo->getProject();
        $companyInfo=$pInfo->getCompany();
        $upModel = new userproject();
        $upModel->setProject($companyInfo["id"], $idUser, $projectInfo["id"]);
        $this->redirect(url("admin/selectedProject",["collapsin"=>$collapsin]));

    }

    public function deletePorseshgarProject()
    {

        $id=_get("id");
        if(_get("t")=="pa")
            $collapsin="setprojectAdmin";
        elseif (_get("t")=="po")
            $collapsin="setPorseshgar";

        $userProjectModel=new userproject();
        $userProjectModel->deleteUserProject($id);
        $this->redirect(url("admin/selectedProject",["collapsin"=>$collapsin]));
    }
    private function checkProjectAccessPrimision($idP)
    {

    }



//=================================================================== Android ========================================
    // Android Function getCity
    public function androidGetCity()
    {
        $remember_Token = _post("remember_Token");
        $idProject = _post("idProject");
        $checkIsLogin = new Model1();
        $isLogin = $checkIsLogin->_selectOne("*", "`remember_Token`='" . $remember_Token . "'", "users");
        if ($isLogin) {
            $pasokhgoo = new pasokhgooModel();
            $getCity = $pasokhgoo->getCity($idProject);
            if ($getCity)
                echo Response::ajax($getCity);
            else
                echo Response::ajax(["success" => true, "message" => "شهری به پروژه اختصاص داده نشده است لطفا با مدیر پروژه تماس بگیرید!"]);
        } else {
            echo Response::ajax(["success" => false, "message" => "به دلایل امنیتی از نرم افزار خارح می شوید! لطفا دوباره وارد شوید"]);
        }
    }
// Android Function getPolicySet
    public function androidGetPolicySet()
    {
        $remember_Token = _post("remember_Token");
        $idProject = _post("idProject");
        $checkIsLogin = new Model1();
        $isLogin = $checkIsLogin->_selectOne("*", "`remember_Token`='" . $remember_Token . "'", "users");
        if ($isLogin) {
            $policyList = new policiM();
            $getPolicySet = $policyList->getAllPolicysetforAndroid($idProject);
            if ($getPolicySet)
                echo Response::ajax($getPolicySet);
            else
                echo Response::ajax(["success" => false, "message" => "سیاستی یافت نشد!"]);
        } else {
            echo Response::ajax(["success" => false, "message" => "به دلایل امنیتی از نرم افزار خارح می شوید! لطفا دوباره وارد شوید"]);
        }
    }

// Android Function getPolicySet
    public function androidGetPolicyList()
    {
        $remember_Token = _post("remember_Token");
        $checkIsLogin = new Model1();
        $isLogin = $checkIsLogin->_selectOne("*", "`remember_Token`='" . $remember_Token . "'", "users");
        if ($isLogin) {
            $policyList = new policiM();
            $getPolicyList = $policyList->getPolicyList();
            if ($getPolicyList)
                echo Response::ajax($getPolicyList);
            else
                echo Response::ajax(["success" => true, "message" => "سیاستی برای پروژه ست نشده است !"]);
        } else {
            echo Response::ajax(["success" => false, "message" => "به دلایل امنیتی از نرم افزار خارح می شوید! لطفا دوباره وارد شوید"]);
        }
    }
    public function androidGetSEC()
    {
        $remember_Token = _post("remember_Token");
        $idProject = _post("idProject");
        $checkIsLogin = new Model1();
        $isLogin = $checkIsLogin->_selectOne("*", "`remember_Token`='" . $remember_Token . "'", "users");
        if ($isLogin) {
            $policyList = new policiM();
            $getPolicyList = $policyList->getSocialClass($idProject);
            if ($getPolicyList)
                echo Response::ajax($getPolicyList);
            else
                echo Response::ajax(["success" => true, "message" => "سیاستی برای پروژه ست نشده است !"]);
        } else {
            echo Response::ajax(["success" => false, "message" => "به دلایل امنیتی از نرم افزار خارح می شوید! لطفا دوباره وارد شوید"]);
        }
    }

    public function androidGetUserProject()
    {
        $remember_Token = _post("remember_Token");
        $checkIsLogin = new Model1();
        $isLogin = $checkIsLogin->_selectOne("*", "`remember_Token`='" . $remember_Token . "'", "users");
        if ($isLogin) {
            $activeP = new userproject();
            $userProject = $activeP->selectUserProjectForAndroid($isLogin["id"]);
            if ($userProject)
                echo Response::ajax($userProject);
            else
                echo Response::ajax(["success" => true, "message" => "پروژه ای به کاربر اختصاص داده نشده است!"]);
        } else {
            echo Response::ajax(["success" => false, "message" => "به دلایل امنیتی از نرم افزار خارح می شوید! لطفا دوباره وارد شوید"]);
        }
    }



}
