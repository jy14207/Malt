<?php
/**
 * Created by PhpStorm.
 * User: Jafari
 * Date: 10/03/2018
 * Time: 09:24 PM
 */

namespace App\Controllers;

use App\Classes\imageUploader as imageUploade;
use App\Models\options as optionModel;
use App\Classes\imageUploader as imageUploader;
use App\Models\Model;
use App\Classes\Response;


class option extends BaseController
{
    public function storeOption()
    {
        $collapsin="optiona";

//        dd(_file($_FILES["uploadFiles"]));
        if (_file($_FILES["uploadFile"]) == true) {
            $file = $_FILES["uploadFile"];
            $imageUp = new imageUploade();
            if ($imageUp->checkFileIsaActualImage($file) == false) {
                $_SESSION["messagesTitle"] = "خطا";
                $_SESSION["messagesText"] = "فایل انتخاب شده تصویر نیست!";
                $_SESSION["messagesType"] = "error";
                $this->redirect(url("Question/manageOptions", ["idQ" => _post("idQ"), "error" => "true","collapsin"=>$collapsin]));

            } else if ($imageUp->checkFileSize($file) == false) {
                $_SESSION["messagesTitle"] = "خطا";
                $_SESSION["messagesText"] = "حجم تصویر انتخاب شده بیشتر از حد مجاز است!";
                $_SESSION["messagesType"] = "error";
                $this->redirect(url("Question/manageOptions", ["idQ" => _post("idQ"), "error" => "true","collapsin"=>$collapsin]));
            } else {
                $op = new optionModel();
                $op = $op->storeOption(_post("idQ"), _post("textOption"), _post("moreExplanation"), _post("score"), "Yes");
                $targetDir = public_path("imagesUpload/options/");
                $imgUpload = $imageUp->uploadImage($targetDir, $file, $op["id"]);
                $this->redirect(url("Question/manageOptions", ["idQ" => _post("idQ"),"collapsin"=>$collapsin]));
            }
        } else {
            $op = new optionModel();
            $op = $op->storeOption(_post("idQ"), _post("textOption"), _post("moreExplanation"), _post("score"), "No");

            $this->redirect(url("Question/manageOptions", ["idQ" => _post("idQ"),"collapsin"=>$collapsin]));
        }
    }

    public function editOptions()
    {
        $opModel = new optionModel();
        $optionRow = $opModel->getTextOption(_get("opId"));
        $optionImg = public_path("imagesUpload/options/") . $optionRow["id"] . ".jpg";
        if(_get("error")=="true")
        {
            $error="true";
            $this->view("admin.editOptions", compact("optionRow", "optionImg","error"));
        }
        else
        {
            $this->view("admin.editOptions", compact("optionRow", "optionImg"));
        }
    }

    public function updateTextOption()
    {
        $op = new optionModel();
        $op = $op->updateOption(_post("id"), _post("idQ"), _post("textOption"), _post("moreExplanation"), _post("score"));
        $this->redirect(url("Question/manageOptions", ["idQ" => _post("idQ")]));
        if (!isset($_FILES["upassloadFile"])) die("eerror");
        if (_file($_FILES["upaloadFile"]) == true) {
            $file = $_FILES["upassloadFile"];
            $imageUp = new imageUploade();
            if ($imageUp->checkFileIsaActualImage($file) == false) {
                $_SESSION["messagesTitle"] = "خطا";
                $_SESSION["messagesText"] = "فایل انتخاب شده تصویر نیست!";
                $_SESSION["messagesType"] = "error";
                $this->redirect(url("option/editOptions", ["idQ" => _post("idQ"), "error" => "true"]));

            } else if ($imageUp->checkFileSize($file) == false) {
                $_SESSION["messagesTitle"] = "خطا";
                $_SESSION["messagesText"] = "حجم تصویر انتخاب شده بیشتر از حد مجاز است!";
                $_SESSION["messagesType"] = "error";
                $this->redirect(url("option/editOptions", ["idQ" => _post("idQ"), "error" => "true"]));
            } else {
                $op = new optionModel();
                $op = $op->updateOption(_post("id"), _post("idQ"), _post("textOption"), _post("moreExplanation"), _post("score"), "Yes");
                $targetDir = public_path("imagesUpload/options/");
                $imgUpload = $imageUp->uploadImage($targetDir, $file, $op["id"]);
                $this->redirect(url("option/storeEditOption", ["idQ" => _post("idQ")]));
            }
        } else {

        }
    }

    public function updateImgOption()
    {

//        $opModel = new optionModel();
//        $optionRow = $opModel->getTextOption(_post("id"));
       if (_file($_FILES["uploadFile"]) == true) {
            $file = $_FILES["uploadFile"];
            $imageUp = new imageUploade();
            if ($imageUp->checkFileIsaActualImage($file) == false) {
                $_SESSION["messagesTitle"] = "خطا";
                $_SESSION["messagesText"] = "فایل انتخاب شده تصویر نیست!";
                $_SESSION["messagesType"] = "error";
                $this->redirect(url("option/editOptions", ["opId" => _post("id"), "error" => "true"]));

            }
            else if ($imageUp->checkFileSize($file) == false) {
                $_SESSION["messagesTitle"] = "خطا";
                $_SESSION["messagesText"] = "حجم تصویر انتخاب شده بیشتر از حد مجاز است!";
                $_SESSION["messagesType"] = "error";
                $this->redirect(url("option/editOptions", ["opId" => _post("id"), "error" => "true"]));
            }
            else {
                $op = new optionModel();
                $op = $op->updateImgOption(_post("id"), "Yes");
                $targetDir = "/imagesUpload/options/";
                $dir=DOC_ROOT."/public/imagesUpload/options/";
                $imgUpload = $imageUp->uploadImage($targetDir, $file,$dir,_post("id"));
                $this->redirect(url("Question/manageOptions", ["idQ" => _post("idQ"),"opId"=>_post("id"),"collapsin"=>"optiona"]));
            }
        }
        else
        {
            $_SESSION["messagesTitle"] = "خطا";
            $_SESSION["messagesText"] = "تصویری انتخاب نشده است";
            $_SESSION["messagesType"] = "error";
            $this->redirect(url("option/editOptions", ["idQ" => _post("idQ"),"opId"=>_post("id"), "error" => "true"]));
        }

    }

    public function deleteDeleteOption(){
        $id=_get("id");
        $opmod=new optionModel();
        $opmod->deleteOpt($id);
        $imageUp = new imageUploade();
        $delDirImage=DOC_ROOT."/public/imagesUpload/options/".$id.".jpg";
        $imageUp->existImage($delDirImage);
        $this->redirect(url("Question/manageOptions", ["idQ" => _get("idQ"),"collapsin"=>"optiona"]));

    }
    public function deleteImgOption()
    {
        $op=new optionModel();
        $imgUp=new imageUploader();
        $imgUp->existImage(DOC_ROOT."/imagesUpload/options/12.jpg");

        $op->updateImgOption(_get("idop"),"No");
//        dd(_get("idQ"));
        $this->redirect(url("Question/manageOptions", ["idQ" => _get("idQ"),"collapsin"=>"optiona"]));
    }
    public function storeOtherOption()
    {
        $op = new optionModel();
        $selectOptionByScore= $op->selectOptionByScore(_post("idQ"),"999");
//        dd($selectOptionByScore);
        if($selectOptionByScore==false)
        {
            $op = $op->storeOption(_post("idQ"),"Other", "", "999", "No");
        }
        $this->redirect(url("Question/manageOptions", ["idQ" => _post("idQ"),"collapsin"=>"optiona"]));
    }
    public function androidGetOptions()
    {
        $remember_Token = _post("remember_Token");
        $idProject = _post("idProject");
        $checkIsLogin = new Model();
        $isLogin = $checkIsLogin->_selectOne("*", "`remember_Token`='" . $remember_Token . "'", "users");
        if ($isLogin) {
            $question = new optionModel();
            $questionRows = $question->getOptionsForAndroid($idProject);
            if ($questionRows)
                echo Response::ajax($questionRows);
            else
                echo Response::ajax(["success" => true, "message" => "گزینه ای یافت نشد، نشد لطفا با مدیر پروژه تماس بگیرید!"]);
        } else {
            echo Response::ajax(["success" => false, "message" => "به دلایل امنیتی از نرم افزار خارح می شوید! لطفا دوباره وارد شوید"]);

        }
    }



}