<?php

namespace App\Controllers;
use App\Classes\Response;
use App\Models\porseshname as prseshnameModel;
use App\Classes\Auth;
use App\Models\pasokhgoo as pasokhgooModel;
use App\Models\User as userModel;
use App\Models\answer;
use App\Models\options;
use Symfony\Component\Validator\Constraints\Count;
class porseshname extends BaseController
{
    public function PorseshnameformView()
    {
        if (_get("idProject")) {
            $_SESSION["idProject"] = _get("idProject");
        }
        $aut = new Auth();
        $idProject = $aut->getProject()["id"];
        $geoLocationRequireProject = $aut->getProject()["geoLocationRequire"];
        $idUser = $aut->getUser()["id"];
        $pModel = new prseshnameModel();
        $thisUserPorseshnamesList = $pModel->getListPorseshnameOfPorseshgar($idProject, $idUser);
        $todayDate = jdate('Y/m/d');
        $nowTime = jdate('H:i');
        if (_get("error") == "True") {
            $error = "True";
            $this->view("porseshname.porseshname", compact("todayDate", "nowTime", "thisUserPorseshnamesList","geoLocationRequireProject" , "error"));
        } else {
            $this->view("porseshname.porseshname", compact("todayDate", "nowTime", "thisUserPorseshnamesList" ,"geoLocationRequireProject"));
        }
    }
    public function insertPorseshname()
    {
        $aut = new Auth();
        $idProject = $aut->getProject()["id"];
        $idUser = $aut->getUser()["id"];
        $project = $aut->getProject();
        $porseshnamehNumber = _post("porseshnamehNumber");
        $pModel = new prseshnameModel();
        $duplicate = $pModel->getForDuplicate($porseshnamehNumber, $idProject);
//        dd($duplicate);
        if ($duplicate == false) {
            if ($this->getPorseshnameCount($idProject) < $project["pasokhgooNumber"]) {
                $PoModel = new prseshnameModel();
                $storePorseshname = $PoModel->insetPorseshname($idProject, $idUser);
                $_SESSION["idPorseshname"] = $storePorseshname["id"];
                $this->redirect(url("pasokhgoo/viewPasokhgoo"));
            } else {
                $_SESSION["messagesTitle"] = "پیام سیستم";
                $_SESSION["messagesText"] = "تعداد مجاز پرسشنامه برای این پروژه پر شده است!";
                $_SESSION["messagesType"] = "info";
                $this->redirect(url("porseshname/PorseshnameformView", ["error" => "True", "idProject" => $idProject]));
//                $this->view("porseshname.porseshname", ["error" => "True", "todayDate" => $todayDate, "nowTime" => $nowTime]);
            }
        } else {
            $_SESSION["messagesTitle"] = "پیام سیستم";
            $_SESSION["messagesText"] = "شماره پرسشنامه تکراری است!";
            $_SESSION["messagesType"] = "error";
            $todayDate = jdate('Y/m/d');
            $nowTime = jdate('H:i');
            $this->view("porseshname.porseshname", ["error" => "True", "todayDate" => $todayDate, "nowTime" => $nowTime, "idProject" => $idProject]);
        }
    }
    public function searchPorseshname()
    {
        $aut = new Auth();
        $CurrentUser = $aut->getUser();
        $todayDate = jdate('Y/m/d');
        $nowTime = jdate('H:i');
        $idPorseshnameh = 0;
//        dd(_post("searchPasokhgooItem"));
        if (_post("searchPasokhgooItem") == "id") {
            $pModel = new prseshnameModel();
            $idPorseshnamehT = _post("search_value");
            $idPorseshnameh = $pModel->getbyIdAndLockedByEditor($idPorseshnamehT)["id"];
        }
        if (_post("searchPasokhgooItem") == "porseshnamehNumber") {
//            dd(_post("searchPasokhgooItem"));
            $idProject = $aut->getProject()["id"];
            $pModel = new prseshnameModel();
            $porseshnamehNumber = _post("search_value");
            $idPorseshnameh = $pModel->getbyIdProjectAndpNumber($porseshnamehNumber, $idProject, $CurrentUser["id"])["id"];
        }
        if (_post("searchPasokhgooItem") == "completedDate") {
//            dd(_post("searchPasokhgooItem"));
            $idProject = $aut->getProject()["id"];
            $pModel = new prseshnameModel();
            $completeDate = _post("search_value");
            $idPorseshnameh = $pModel->getbyIdProjectAndCompleteDate($completeDate, $idProject, $CurrentUser["id"])["id"];
        }
//        dd($idPorseshnameh);
        if ($idPorseshnameh) {
            $_SESSION["idPorseshname"] = $idPorseshnameh;
            $aut->getPorseshname();
            $this->redirect(url("pasokhgoo/viewPasokhgoo", ["methode" => "search"]));
        } else {
            $_SESSION["messagesTitle"] = "متاسفیم";
            $_SESSION["messagesText"] = "موردی یافت نشد";
            $_SESSION["messagesType"] = "error";
            $this->redirect(url("porseshname/PorseshnameformView", ["error" => "True"]));
        }
    }
    public function updatePorseshnameForFinished($idPorseshname)
    {
        $nowTime = jdate('H:i');
        $updatePorseshnameForFinished = new prseshnameModel();
        $updatePorseshnameForFinished->updatePorseshnameForFinished($idPorseshname, $nowTime);
    }
    public function confirmYes()
    {
//        dd(_get("id"));
        $pModel = new prseshnameModel();
        $pModel->confirm(_get("id"), "Yes");
        $this->redirect(url("admin/selectedProject", ["collapsin" => "listPorseshname"]));
    }
    public function confirmNo()
    {
//        dd(_get("id"));
        $pModel = new prseshnameModel();
        $pModel->confirm(_get("id"), "No");
        $this->redirect(url("admin/selectedProject", ["collapsin" => "listPorseshname"]));
    }
    public function softDeletePorseshnameh()
    {
        $pModel = new prseshnameModel();
        $pModel->softDeletePorseshnameh(_get("id"));
        $this->redirect(url("admin/selectedProject", ["collapsin" => "listPorseshname"]));
    }
    public function manage()
    {
        $id = _get("id");
        $aut = new Auth();
        $idUser = $aut->getUser()["id"]; //$_SESSION["idUser"];
        $idProject = $aut->getProject()["id"];
        $pasokhgooM = new pasokhgooModel();
        $pasokhgoo = $pasokhgooM->searchPasokhgoo($id);
        $getCity = $pasokhgooM->getCity($idProject);
        $pModel = new prseshnameModel();
        $porseshname = $pModel->getOnePorseshname($id,$idProject);
        //get Answer this porseshname
        $answerM = new answer();
        $listAnswers = $answerM->getAnswerForporseshnameReviwer($id);
//        var_dump($listAnswers);
        //Convert idOption Array To textOption Array
        $finalListAnswers = $this->convertidOptionArrayTotextOptionArray($listAnswers);
//        var_dump($finalListAnswers);
        $porseshgarM = new userModel();
        $porseshgarInf = $porseshgarM->getPorseshgarInf($porseshname["idPorseshgar"]);
//        dd($finalListAnswers);
        $this->view("porseshname.manage", compact("pasokhgoo", "getCity", "porseshname", "porseshgarInf", "finalListAnswers"));
    }
    public function finishPorseshname()
    {
        $policyCheckResultage = _get("policyCheckResult");
        switch ($policyCheckResultage) {
            case "CountPolicyIsTrue": {
                $policyCheckResultMessage = "گزینه ای که انتخاب کرده اید محدودیت در تعداد جواب هارا موجب شد";
                break;
            }
            case "StopPolicyIsTrue": {
                $policyCheckResultMessage = "گزینه ای که انتخاب کرده اید باعث خاتمه دادن به مصاحبه شده است";
                break;
            }
            case "finish": {
                $policyCheckResultMessage = "اتمام سوالات";
                break;
            }
        }
        $this->view("porseshname.finishPorseshname", compact("policyCheckResultMessage"));
    }
    private function convertidOptionArrayTotextOptionArray($listAnswers)
    {
        foreach ($listAnswers as $j => $listAnswer) {
            $idOptions = $listAnswer["idOption"];
//            var_dump($idOptions);
            $options = explode(",", $idOptions);
            $arr = array();
            foreach ($options as $i => $option) {
                if ($option == "openQuestion") $arr[$i] = " تشریحی";
                elseif ($option == "numeric") $arr[$i] = " تشریحی";
                elseif ($option == "spectral") $arr[$i] = " طیفی";
                elseif ($option == "degree") $arr[$i] = " درجه بندی";
                elseif ($option == "websitelink") $arr[$i] = " لینک وب سایت";
                elseif ($option == "email") $arr[$i] = "ایمیل";
                elseif ($option == "uploadVideo") $arr[$i] = "uploadVideo";
                elseif ($option == "uploadImage") $arr[$i] = "uploadImage";
                elseif ($option == "uploadAudio") $arr[$i] = "uploadAudio";
                else {
                    $optionM = new options();
                    $rowOption = $optionM->getTextOption($option);
                    if ($rowOption["textOption"] == "Other") {
                        $arr[$i] = "سایر";
                    } else {
                        $arr[$i] = $rowOption["textOption"] . ',';
                    }
                }
            }
//            var_dump($arr);
            $listAnswers[$j]["idOption"] = implode($arr);
        }
        return $listAnswers;
    }
    private function getPorseshnameCount($idProject)
    {
        $pModel = new prseshnameModel();
        $rowCount = $pModel->PorseshnameCount($idProject);
        return $rowCount[0]["count"];
    }
    public function checkForDuplicateRowajax()
    {
        $aut = new Auth();
        $idProject = $aut->getProject()["id"];
        $pModel = new prseshnameModel();
        $aj = $pModel->getForDuplicateAjax(_get("porseshnamehNumber"), $idProject);
        echo Response::ajax($aj);
    }
    public function lockByPorseshgar()
    {
        $aut = new Auth();
        $idPorseshname = $aut->getPorseshname()["id"];
        $updatePorseshnameForLocked = new prseshnameModel();
        $updatePorseshnameForLocked->updatePorseshnameForLocked($idPorseshname, 1);
        $this->redirect(url("porseshname/porseshnameFormView"));
    }
    public function changeLockStateByAdminProjectAjax()
    {
        $aut = new Auth();
        $idProject = $aut->getProject()["id"];
        $pModel = new prseshnameModel();
        $id = _get("id");
        $curentLockedValue = _get("curentLockedValue");
        $validate = $pModel->thisPorseshnameForThisProject($id, $idProject);
        if ($validate) {
            if ($curentLockedValue == 1) {
                $data = $pModel->updatePorseshnameForLocked($id, 0);
            } elseif ($curentLockedValue == 0) {
                $data = $pModel->updatePorseshnameForLocked($id, 1);
            }
            $res=$pModel->thisPorseshnameForThisProject($id, $idProject)["lockedByEditor"];
            echo  Response::ajax(["result"=>$res]);
        } else {
            echo Response::ajax(["error"=>"true"]);
        }
    }


}