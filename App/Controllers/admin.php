<?php
/**
 * Created by PhpStorm.
 * User: Jafari
 * Date: 21/11/2017
 * Time: 09:23 AM
 */

namespace App\Controllers;
use App\Classes\AccessPrimision;
use App\Classes\Auth;
use App\Classes\Response;
use App\Models\projects AS projectsModel;
use App\Models\policy as plicyM;
use App\Models\User;
use App\Models\userproject;
use App\Models\porseshname;
use App\Classes\DashboardActiveProject;
use App\Models\Question;
class admin extends BaseController
{
    public function index()
    {
        //گرفتن مقادر داشورد ردیف اول یا کلیات
        $aut = new Auth();
        $companyInfo = $aut->getCompany();
        $userInfo = $aut->getUser();
//        dd($userInfo);
        $p = new projectsModel();
        $up = new userproject();
        switch ($userInfo["level"]) {
            case "admin" :
                $allProjectsCount = count($p->allProjects());
                $ActiveProjectCount = count($p->activeProjects());
                $inActiveProjectCount = count($p->inActiveProjects());
                $uM = new User();
                $totalUserCount = count($uM->getUsers());
                // گرفتن آرایه برای داشبورد پروژه های فعال و در یک نگاه
                $dClass = new DashboardActiveProject();
                $dashboardDataArrays = $dClass->datsboardTableArray($companyInfo["id"]);
                //گرفتن کاربران شرکت
//                $companyUsers=$uM->getUsers();
                $companyUsers = $dClass->getCompanyUsers("1");
                break;
            case "companyAdmin" :
                $allProjectsCount = count($p->companyAllProjects($companyInfo["id"]));
                $ActiveProjectCount = count($p->companyActiveProjects($companyInfo["id"]));
                $inActiveProjectCount = count($p->companyInActiveProjects($companyInfo["id"]));
                $uM = new User();
                $totalUserCount = count($uM->companyGetUsers($companyInfo["id"]));
                // گرفتن آرایه برای داشبورد پروژه های فعال و در یک نگاه
                $dClass = new DashboardActiveProject();
                $dashboardDataArrays = $dClass->datsboardTableArray($companyInfo["id"]);
                //گرفتن کاربران شرکت
                $companyUsers = $dClass->getCompanyUsers($companyInfo["id"]);
                break;
            case "projectAdmin" :
                $allProjectsCount = count($up->adminPAllProjects($userInfo["id"]));
                $ActiveProjectCount = count($up->adminPActiveProjects($userInfo["id"]));
                $inActiveProjectCount = count($up->adminPinActiveProjects($userInfo["id"]));
                $uM = new User();
                $totalUserCount = count($uM->companyGetUsers($companyInfo["id"]));
                // گرفتن آرایه برای داشبورد پروژه های فعال و در یک نگاه
                $dClass = new DashboardActiveProject();
                $dashboardDataArrays = $dClass->datsboardTableArray($companyInfo["id"]);
                //گرفتن کاربران شرکت
                $companyUsers = $dClass->getCompanyUsers($companyInfo["id"]);
                break;
        }
        $this->view("admin.managementProjects", compact("allProjectsCount", "ActiveProjectCount", "inActiveProjectCount", "totalUserCount", "dashboardDataArrays", "companyUsers"));
    }
    public function allProjects()
    {
        $ap = new AccessPrimision();
        $allProjects = $ap->allProject();
        $this->view("admin.manageAllProjects", compact("allProjects"));
    }
    public function selectedProject()
    {
        if (_get("idP")) {
            $_SESSION["idProject"] = _get("idP");
            $_SESSION["nP"] = _get("nP");
            $acP = new AccessPrimision();
            if (($acP->checkProjectAccess(_get("idP"))) == false) {
                $this->redirect(url("login/logout", ["message" => "true"]));
            }
        }
        $pInfo = new Auth();
        $companyInfo = $pInfo->getCompany();
        $projectInfo = $pInfo->getProject();
        if (_get("nextRowNumber")) {
            $nextRowNumber = _get("nextRowNumber");
        } else {
            $t = new Question();
            $t = $t->getLastNumbrOfQuestionRow($projectInfo["id"]);
            if (!empty($t)) {
                $d = max($t);
            } else {
                $d = 0;
            }
            $nextRowNumber = $d["rowNumber"] + 1;
        }
        $Question = new Question();
        $QuestionList = $Question->getQuestionList($projectInfo["id"]);
        $pm = new projectsModel();
        $citysProject = $pm->getCity($projectInfo["id"]);
        $SocialClassModel = new plicyM();
        $SocialClass = $SocialClassModel->getSocialClass($projectInfo["id"]);
        $userProjectModel = new userproject();
        $thisProjectPorseshgars = $userProjectModel->getListProjectPorseshgars($companyInfo["id"], $projectInfo["id"], "porseshgar");
        $thisProjectprojectAdmins = $userProjectModel->getListProjectPorseshgars($companyInfo["id"], $projectInfo["id"], "projectAdmin");
        $pList = new User();
        $PorseshgarLists = $pList->getPorseshgarList($companyInfo["id"]);
        $adminProjectLists = $pList->getadminProjectList($companyInfo["id"]);
        $collapsin = _get("collapsin");// "insertQ";
//        $collapsin="listQ";
//        $collapsin="setPorseshgar";
//        $collapsin="listPorseshname";
//        $collapsin="setCity";
        $qM = new Question();
        $questiontypes = $qM->getQuestionTypeList();
        $porseshnameh = new porseshname();
        $listPorseshnamehs = $porseshnameh->listPorseshnameForaProject($projectInfo["id"]);
        if (_get("error") == "true") $error = "true";
        $this->view("admin.manageActiveProject", compact("nextRowNumber", "QuestionList", "citysProject", "SocialClass", "projectInfo", "PorseshgarLists", "thisProjectPorseshgars", "collapsin", "listPorseshnamehs", "questiontypes", "adminProjectLists", "thisProjectprojectAdmins", "error"));
//        $this->view("question.manageQuestions", compact("nextRowNumber", "QuestionList", "citysProject", "SocialClass", "projectInfo", "PorseshgarLists", "thisProjectPorseshgars", "collapsin", "listPorseshnamehs","questiontypes","adminProjectLists","thisProjectprojectAdmins"));
    }
    public function underDesign()
    {
        $this->view("temp.underDesign");
    }
    public function registerCompany()
    {
        // sabt sherkat
    }
    public function viewActiveProjectListForReports()
    {
        /*        $activeP = new projectsModel();
                $aut = new Auth();
                $companyInf = $aut->getCompany();

                $activeProject = $activeP->activeProject($companyInf["id"]);*/
        $ac = new AccessPrimision();
        $activeProject = $ac->activeProjects();
        $this->view("admin.activeProjectListForReports", compact("activeProject"));
    }
    public function reports()
    {
        $nP = _get("nP");
        $idP = _get("idP");
        $this->view("admin.reports", compact("nP", "idP"));
    }
    public function help()
    {
        $this->view("admin.help");
    }
    public function companyChartAjax()
    {
        $idCompany = _get("idCompany");
//        dd($idCompany);
        $dClass = new DashboardActiveProject();
        $dashboardDataArrays = $dClass->datsboardTableArray($idCompany);
//        dd($dashboardDataArrays);
        echo Response::ajax($dashboardDataArrays);
    }
    public function testfor()
    {
        for ($i = 1; $i < 10000; $i++) {
            for ($j = 1; $j < 1000; $j++) {
                var_dump($j);
            }
        }
    }
//ENTER THE RELEVANT INFO BELOW
//or add 5th parameter(array) of specific tables:    array("mytable1","mytable2","mytable3") for multiple tables
//Export_Database($mysqlHostName,$mysqlUserName,$mysqlPassword,$DbName,  $tables=false, $backup_name=false );
    function Export_Database()
    {
        $mysqlUserName = "anssurve_malt";
        $mysqlPassword = "3668413311";
        $mysqlHostName = "localhost";
        $DbName = "anssurve__Malt";
        $dbName = "anssurve__Malt";
        $dbhostName = "localhost";
        $dbUserName = "anssurve_malt";
        $password = "3668413311";
        $backup_name = "ansSurvay.sql";
        $tables = array(
            "answer", "city", "company", "options",
            "pasokhgoo", "policyset","policyslist", "project",
            "prseshname",'questions',"questiontype", "sec",
            "userproject","users");
        $mysqli = new \mysqli($mysqlHostName, $mysqlUserName, $mysqlPassword, $DbName);
        $mysqli->select_db($DbName);
        $mysqli->query("SET NAMES 'utf8'");
        $queryTables = $mysqli->query('SHOW TABLES');
        while ($row = $queryTables->fetch_row()) {
            $target_tables[] = $row[0];
        }
        if ($tables !== false) {
            $target_tables = array_intersect($target_tables, $tables);
        }
        foreach ($target_tables as $table) {
            $result = $mysqli->query('SELECT * FROM ' . $table);
            $fields_amount = $result->field_count;
            $rows_num = $mysqli->affected_rows;
            $res = $mysqli->query('SHOW CREATE TABLE ' . $table);
            $TableMLine = $res->fetch_row();
            $content = (!isset($content) ? '' : $content) . "\n\n" . $TableMLine[1] . ";\n\n";
            for ($i = 0, $st_counter = 0; $i < $fields_amount; $i++, $st_counter = 0) {
                while ($row = $result->fetch_row()) { //when started (and every after 100 command cycle):
                    if ($st_counter % 100 == 0 || $st_counter == 0) {
                        $content .= "\nINSERT INTO " . $table . " VALUES";
                    }
                    $content .= "\n(";
                    for ($j = 0; $j < $fields_amount; $j++) {
                        $row[$j] = str_replace("\n", "\\n", addslashes($row[$j]));
                        if (isset($row[$j])) {
                            $content .= '"' . $row[$j] . '"';
                        } else {
                            $content .= '""';
                        }
                        if ($j < ($fields_amount - 1)) {
                            $content .= ',';
                        }
                    }
                    $content .= ")";
                    //every after 100 command cycle [or at last line] ....p.s. but should be inserted 1 cycle eariler
                    if ((($st_counter + 1) % 100 == 0 && $st_counter != 0) || $st_counter + 1 == $rows_num) {
                        $content .= ";";
                    } else {
                        $content .= ",";
                    }
                    $st_counter = $st_counter + 1;
                }
            }
            $content .= "\n\n\n";
        }
        //$backup_name = $backup_name ? $backup_name : $name."___(".date('H-i-s')."_".date('d-m-Y').")__rand".rand(1,11111111).".sql";
        $backup_name = $backup_name ? $backup_name : $DbName . ".sql";
        header('Content-Type: application/octet-stream');
        header("Content-Transfer-Encoding: Binary");
        header("Content-disposition: attachment; filename=\"" . $backup_name . "\"");
        echo $content;
        exit;
    }
}