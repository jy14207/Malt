<?php
/**
 * Created by PhpStorm.
 * User: Jafari
 * Date: 07/11/2017
 * Time: 09:53 AM
 */

namespace App\Controllers;
use App\Classes\AccessPrimision;
use App\Models\User as userModer;
use App\Classes\Response;
use App\Models\User;
use App\Models\projects as projectsModel;
use App\Models\userproject;
use App\Classes\Auth;
use App\Classes\imageUploader as imageUploader;
class Users extends BaseController
{
    public function manageUsers()
    {
        /*        $aut = new Auth();
                $idCompany = $aut->getCompany()["id"];*/
        $acp = new AccessPrimision();
        $companyUsers = $acp->getUsers();
        if (_get("error") == "true") {

            $error = _get("error");
            $this->view("admin.manageUsers", compact("error", "companyUsers"));
        }
        $this->view("admin.manageUsers", compact("companyUsers"));
    }
    public function insertUser()
    {
        if ($this->checkForDuplicateRowByPhp() == true) {
            $_SESSION["messagesTitle"] = "خطا";
            $_SESSION["messagesText"] = "نام کاربری تکراری است";
            $_SESSION["messagesType"] = "error";
            $this->redirect(url("Users/manageUsers", ["error" => "true"]));
        } else {
            $aut = new Auth();
            $idCompany = $aut->getCompany()["id"];
            $user = new userModer();
            $user->insertUser($idCompany);
            $this->redirect(url("Users/manageUsers"));
        }
    }
    private function checkForDuplicateRowByPhp()
    {
        $user = new userModer();
        $t = $user->checkForDuplicateRow(_post("userName"));
        if ($t)
            return true;
        else
            return false;
    }
    public function checkForDuplicateRow()
    {
        $user = new userModer();
        $t = $user->checkForDuplicateRow(_get("userName"));
        if ($t)
            echo Response::ajax(array("isDuplicate" => true));
        else
            echo Response::ajax(array("isDuplicate" => false));
    }
    public function ChangeState()
    {
        $Users = new User();
        $getUserByid = $Users->getPorseshgarInf(_get("id"));
        $acP = new AccessPrimision();
        if ($acP->checkUserIsCompany(_get("id")) == false) {
            $acP->errorAlert();
            $this->redirect(url("login/logout", ["message" => "true"]));
        } else {
            $userNowState = $getUserByid["status"];
//        dd($userNowState);
            if ($userNowState == "Active") {
                $setState = "inActive";
            } else if ($userNowState == "inActive") {
                $setState = "Active";
            }
            $Users->ChangeState($setState, _get("id"));
            $this->redirect(url("Users/manageUsers"));
        }
    }
    public function moreManageUsers()
    {
        $idUser = _get("id");
        $acP = new AccessPrimision();
        if ($acP->checkUserIsCompany($idUser) == false) {
            $acP->errorAlert();
            $this->redirect(url("login/logout", ["message" => "true"]));
        } else {

            $uModel = new User();
            $Userinf = $uModel->getPorseshgarInf($idUser);
            $accessPrim = new AccessPrimision();
            $AllActiveprojects = $accessPrim->activeProjects($Userinf["level"]);
            $aut = new Auth();
            $idCompany = $aut->getCompany()["id"];
            $userLevel = $aut->getUser()["level"];
            if ($Userinf["idCompany"] != $idCompany && $userLevel != "admin") {
                $accessPrim->errorAlert();
                $this->redirect(url("login/logout", ["message" => "true"]));
            } else {
                $activeP = new userproject();
                $UserProjects = $activeP->getUserProject($idUser);
                if (_get("error") == "true") {
                    $error = _get("error");
                    $this->view("admin.moreManageUsers", compact("Userinf", "UserProjects", "AllActiveprojects", "error"));
                } else {
                    $this->view("admin.moreManageUsers", compact("Userinf", "UserProjects", "AllActiveprojects"));
                }
            }
        }
    }
    public function editUser()
    {
//        $idCompany = $_SESSION["idCompany"];
        $uModel = new User();
        $Userinf = $uModel->getPorseshgarInf(_get("id"));
        $acP = new AccessPrimision();
        if ($acP->checkUserIsCompany(_get("id")) == false) {
            $acP->errorAlert();
            $this->redirect(url("login/logout", ["message" => "true"]));
        } else {
            $this->view("admin.EditUser", compact("Userinf"));
        }
    }
    public function updateUser()
    {
        $acP = new AccessPrimision();
        if ($acP->checkUserIsCompany(_post("id")) == false) {
            $acP->errorAlert();
            $this->redirect(url("login/logout", ["message" => "true"]));
        } else {

            $idCompany = $_SESSION["idCompany"];
            $uModel = new User();
            if (_post("level") == null) $level = "admin"; else $level = _post("level");
            $uModel->updateUser(_post("id"), $level, _post("name"), _post("fName"), _post("nationalCode"), _post("homeAdd"));
            $this->redirect(url("Users/manageUsers"));
        }
    }
    public function setProject()
    {
        $acP = new AccessPrimision();
        if ($acP->checkUserIsCompany(_post("idUser")) == false) {
            $acP->errorAlert();
            $this->redirect(url("login/logout", ["message" => "true"]));
        } else {
            $pInfo = new Auth();
            $projectId = _post("idProject");
            $companyInfo = $pInfo->getCompany();
            $idUser = _post("idUser");
            $upModel = new userproject();
            $upModel->setProject($companyInfo["id"], $idUser, $projectId);
            $this->redirect(url("Users/moreManageUsers", ["id" => $idUser]));
        }
    }
    public function deleteUserProject()
    {

        $idUser = _get("idU");
        $idUserP = _get("idUP");
        $acP = new AccessPrimision();
        if ($acP->checkUserIsCompany($idUser) == false) {
            $acP->errorAlert();
            $this->redirect(url("login/logout", ["message" => "true"]));
        } else {
            $upModel = new userproject();
//        dd($idUser);
            $upModel->deleteUserProject($idUserP);
            $this->redirect(url("Users/moreManageUsers", ["id" => $idUser]));
        }
    }
    public function changePassword()
    {

        $idUser = _post("idUser");
        $acP = new AccessPrimision();
        if ($acP->checkUserIsCompany($idUser) == false) {
            $acP->errorAlert();
            $this->redirect(url("login/logout", ["message" => "true"]));
        } else {
            $userModel = new User();
            $userModel->updatePassword($idUser, _post("password"));
            $_SESSION["messagesTitle"] = "تراکنش موفق";
            $_SESSION["messagesText"] = "گذر واژه تغییر کرد";
            $_SESSION["messagesType"] = "success";
            $this->redirect(url("Users/moreManageUsers", ["error" => "true", "id" => $idUser]));
        }
    }
   /* public function changePassword()
    {

        $idUser = _post("idUser");
        $acP = new AccessPrimision();
        if ($acP->checkUserIsCompany($idUser) == false) {
            $acP->errorAlert();
            $this->redirect(url("login/logout", ["message" => "true"]));
        } else {
            $userModel = new User();
            $oldPassword = $userModel->getPorseshgarInf($idUser)["password"];
//        dd(_post("oldPassword"));
            if ($oldPassword == _post("oldPassword")) {
                $userModel->updatePassword($idUser, _post("oldPassword"));
//            dd("j");
                $_SESSION["messagesTitle"] = "تراکنش موفق";
                $_SESSION["messagesText"] = "گذر واژه تغییر کرد";
                $_SESSION["messagesType"] = "success";
                $this->redirect(url("Users/moreManageUsers", ["error" => "true", "id" => $idUser]));
            } else {
                $_SESSION["messagesTitle"] = "خطا!";
                $_SESSION["messagesText"] = "گذرواژه قدیم صحیح نیست";
                $_SESSION["messagesType"] = "error";
                $this->redirect(url("Users/moreManageUsers", ["error" => "true", "id" => $idUser]));
            }
        }
    }*/
    public function setProfileImage()
    {
        $idUser = _post("idUser");
        $acP = new AccessPrimision();
        if ($acP->checkUserIsCompany($idUser) == false) {
            $acP->errorAlert();
            $this->redirect(url("login/logout", ["message" => "true"]));
        } else {
            $imageUp = new imageUploader();
            if (_file($_FILES["uploadFile"]) == true) {
                $file = $_FILES["uploadFile"];
                $imageUp = new imageUploader();
                if ($imageUp->checkFileIsaActualImage($file) == false) {
//                dd("1");
                    $_SESSION["messagesTitle"] = "خطا";
                    $_SESSION["messagesText"] = "فایل انتخاب شده تصویر نیست!";
                    $_SESSION["messagesType"] = "error";
                    $this->redirect(url("Users/moreManageUsers", ["error" => "true", "id" => $idUser]));
                } else if ($imageUp->checkFileSize($file) == false) {
//                dd("2");
                    $_SESSION["messagesTitle"] = "خطا";
                    $_SESSION["messagesText"] = "حجم تصویر انتخاب شده بیشتر از حد مجاز است!";
                    $_SESSION["messagesType"] = "error";
                    $this->redirect(url("Users/moreManageUsers", ["error" => "true", "id" => $idUser]));
                } else {
//                dd($idUser);
                    $targetDir = "/imagesUpload/Profiles/";
                    $dir = DOC_ROOT . "/public/imagesUpload/Profiles/";
                    $imgUpload = $imageUp->uploadImage($targetDir, $file, $dir, $idUser);
                    $_SESSION["messagesTitle"] = "تراکنش موفق";
                    $_SESSION["messagesText"] = "تصویر پروفایل تنظیم شد";
                    $_SESSION["messagesType"] = "success";
                    $this->redirect(url("Users/moreManageUsers", ["error" => "true", "id" => $idUser]));
                }
            } else {
                $_SESSION["messagesTitle"] = "خطا";
                $_SESSION["messagesText"] = "تصویری انتخاب نشده است";
                $_SESSION["messagesType"] = "error";
                $this->redirect(url("Users/moreManageUsers", ["error" => "true", "id" => $idUser]));
            }
        }
    }
}