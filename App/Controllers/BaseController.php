<?php
namespace App\Controllers;
use App\Classes\Auth;
use App\Classes\templ;
use App\Classes\Vars;
use Illuminate\View\View;

class BaseController
{
    public $userInfo;
    function __construct()
    {
        $this->userInfo=Auth::getUser();
    }

    public function view($view,$params=null){
        templ::view($view,$params);
    }
    public function redirect($url){
        header("Location:".$url);
    }
}