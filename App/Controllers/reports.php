<?php
/**
 * Created by PhpStorm.
 * User: Jafari
 * Date: 17/01/2018
 * Time: 02:04 PM
 */

namespace App\Controllers;

use App\Classes\DatabaseTemplateReport;
use App\Classes\Timer;
use App\Models\answer;
use App\Models\options;
use App\Models\reports as mReports;
use App\Classes\exportToExcell as Excell;
use App\Classes\Response;
use App\Models\projects as ProjectsModel;
use App\Models\porseshname as prseshnameModel;
use App\Models\User as porseshgarmodel;
use App\Models\Question as questionModel;
use \Gurukami\Helpers\Arrays;


class reports extends BaseController
{
    public function getProjectRowForReports()
    {
        $idP = _get("idP");
        $mReport = new mReports();
        $data = $mReport->getProjectRowForReports($idP);
        echo Response::ajax($data);

    }

    public function getQuestionRowsForReports()
    {
        $idP = _get("idP");
        $mReport = new mReports();
        $data = $mReport->selectAllQuestionOfProject($idP);
        echo Response::ajax($data);

    }

    public function getPorseshnameRowsForReports()
    {
        $idP = _get("idP");
        $mReport = new mReports();
        $data = $mReport->getPorseshnameRowsForReports($idP);
//        dd($data);
        echo Response::ajax($data);

    }

    public function getPasokhgooRowsForReports()
    {
        $idP = _get("idP");
        $mReport = new mReports();
        $data = $mReport->getPasokhgooRowsForReports($idP);
        echo Response::ajax($data);
    }

    public function getOptionsRowsForReports()
    {
        $idP = _get("idP");
        $mReport = new mReports();
        $data = $mReport->getOptionsRowsForReports($idP);
        echo Response::ajax($data);
    }

    public function getAnswersRowsForReports()
    {
        $idP = _get("idP");
        $mReport = new mReports();
        $data = $mReport->getAnswersRowsForReports($idP);
        echo Response::ajax($data);
    }

    public function getDatamainingRowsForReports()
    {
        $idP = _get("idP");
        $mReport = new mReports();
        $data = $mReport->getDatamainingRowsForReports($idP);
        echo Response::ajax($data);
    }

    public function exportToExcell()
    {
        $excell = new Excell();
        $rows = $this->selectAllQuestionOfProject(_get("idP"));
//        dd($rows);
        $excell->exportToExcell($rows);
    }

    private function selectAllQuestionOfProject($idP)
    {
        $reportM = new mReports();
        $allQuestionRows = $reportM->selectAllQuestionOfProject($idP);
        return $allQuestionRows;
    }

    public function createDtatamainingTable()
    {
        $reportM = new mReports();
        $allQuestionRows = $reportM->createDtatamainingTable();
//        return $allQuestionRows;
    }

    public function databaseTemplateReport()
    {
        $idP = _get("idP");
        $projects = new ProjectsModel();
        $getProjectByid = $projects->getProjectById($idP);
//        Timer::start();

        $allPorseshname = new prseshnameModel();
        $allporseshnameOfThisProject = $allPorseshname->getAllPorseshname($idP); //ALL PORSESHNAMEH
//        $allporseshnameOfThisProject = $allPorseshname->getFinishedCount($idP); //ALL PORSESHNAMEH
//        Timer::checkPoint("getFinishedCountPorseshname");
//        dd($allporseshnameOfThisProject);

        $allporseshgar=$allPorseshname->getPorseshgarOthisProject($idP);
//        Timer::checkPoint("getPorseshgarOthisProject");

        $allQuestion = new questionModel();
        $allQuestionsOfThisProject = $allQuestion->getQuestionList($idP); // ALL QUESTION
//        Timer::checkPoint("getQuestionList");


        $mReport = new mReports();
        $allPasokhgooOfThisProject = $mReport->getPasokhgooRowsForReports($idP); // ALL PASOKHGOO


        $mAnswer=new answer();
        $allAnswerOfThisProject=$mAnswer->getAllAnswerAllPorseshnamehThisProject($idP);
//        Timer::checkPoint("getAllAnswerAllPorseshnamehThisProject");

        $mOption=new options();
        $allOptionOfThisProject=$mOption->getAllOptionThisProjectM($idP); //ALL OPTIONS
//        Timer::checkPoint("getAllOptionThisProjectM");

        $newOfDatabaseTemplateReport = new DatabaseTemplateReport();
        $QuestionsFeildsArray = $newOfDatabaseTemplateReport->createQuestionsFeildsArray($getProjectByid, $allQuestionsOfThisProject);
//        dd($QuestionsFeildsArray);
        $getfillDatabaseTemplateArray = $newOfDatabaseTemplateReport->fillDatabaseTemplateArray($idP, $QuestionsFeildsArray, $allporseshnameOfThisProject,$allporseshgar,$allPasokhgooOfThisProject,$allAnswerOfThisProject,$allOptionOfThisProject,$allQuestionsOfThisProject);


        echo Response::ajax($getfillDatabaseTemplateArray);


    }

    public function testKeyBy()
    {
//        $books = [
//            [
//                "id" => 5,
//                "name" => "pascal",
//                "translator_id" => 3
//            ], [
//                "id" => 6,
//                "name" => "C++",
//                "translator_id" => 2
//            ], [
//                "id" => 78,
//                "name" => "VB",
//                "translator_id" => 4
//            ], [
//                "id" => 458,
//                "name" > "Cl",
//                "translator_id" => 5
//            ], [
//                "id" => 789,
//                "name" => "Os",
//                "translator_id" => 33
//            ]];
//        $translators = [
//            [
//                "id" => 1,
//                "name" => "Hossein Joon"
//            ],
//            [
//                "id" =>5,
//                "name" => "Hossein Joon2"
//            ],
//            [
//                "id" => 4,
//                "name" => "Hossein Joon3"
//            ],
//            [
//                "id" => 33,
//                "name" => "Hossein Joon4"
//            ],
//            [
//                "id" =>3,
//                "name" => "Hossein Joon5"
//            ],
//        ];
//
//
//        $translatorKeyBy=[];
//        foreach ($translators as $translator){
//            $translatorKeyBy[$translator["id"]]=$translator;
//        }
//
//        var_dump($translatorKeyBy);
//        var_dump($translatorKeyBy[$books[0]["translator_id"]]["name"]);

        $data = [
            'k0' => 'v0',
            'k1' => [
                'k1-1' => 'v1-1'
            ],
            'complex_[name]_!@#$&%*^' => 'complex',
            'k2' => 'string'
        ];

      if( Arrays::exists('k0', $data) ){
       var_dump($data["k0"]);
      } // returns: true
        Arrays::exists('k9', $data); // returns: false

        Arrays::exists('[k1][k1-1]', $data); // returns: true
        Arrays::exists('[k1][k1-2]', $data); // returns: false

        Arrays::exists('["complex_[name]_!@#$&%*^"]', $data); // returns: true

        Arrays::exists('[k2][2]', $data); // returns: fals

    }
}