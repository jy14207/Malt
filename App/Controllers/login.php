<?php
/**
 * Created by PhpStorm.
 * User: Jafari
 * Date: 07/11/2017
 * Time: 11:17 AM
 */

namespace App\Controllers;
use App\Classes\Auth;
use App\Classes\Response;
use Illuminate\Contracts\Session\Session;
use App\Models\Company as companyModel;
use App\Models\User as userModel;
use App\Models\Model as Model1;
use App\Models\userproject;
use App\Models\Company;
//Android
use App\Models\porseshname;
use App\Models\pasokhgoo;
use App\Models\answer;
class Login extends BaseController
{
    public function form()
    {
        $this->view("login.form", ["message" => _get("message")]);
    }
    public function login()
    {
        $inp_username = _post("inp_username");
        $inp_password = _post("inp_password");
        $attemp = new Auth();
        $userInfo = $attemp->attemp($inp_username, $inp_password);
        if (is_array($userInfo)) {
            $_SESSION["idUser"] = $userInfo["id"];
            $level = $userInfo["level"];
            if ($level == "companyAdmin" | $level == "projectAdmin") {
                $_SESSION["idCompany"] = $userInfo["idCompany"];
                $this->redirect(url("admin/index"));
            }
            if ($level == "porseshgar") {
                $this->redirect(url("projects/index"));
            }
            if ($level == "admin") {
                $this->redirect(url("admin/index"));
            }
        } else {
            $_SESSION["messagesTitle"] = "خطا";
            $_SESSION["messagesText"] = "نام کاربری یا گذر واژه اشتباه است!";
            $_SESSION["messagesType"] = "error";
            $this->redirect(url("login/form", ["message" => "true"]));
//            $this->view("login.form", ["message" => "true"]);
        }
    }
    public function logout()
    {
        session_destroy();
        $message = "false";
        if (_get("message") == "true")
            $message = "true";
        else
            $message = "false";
        $this->view("login.form", ["message" => $message]);
    }
    public function registerForm()
    {
        $this->view("login.register", ["message" => _get("message")]);
    }
    public function register()
    {
        /*        $_SESSION["messagesTitle"] = "با عرض پوزش";
                $_SESSION["messagesText"] = "موقتا امکان ثبت نام میسر نمی باشد";
                $_SESSION["messagesType"] = "error";
                $this->redirect(url("login/registerForm", ["message" => "true"]));*/
//        dd(_post("name"));
        $companyM = new companyModel();
        $infoCompany = $companyM->store(_post("name"));
//        dd($idCompany);
        if ($this->checkForDuplicateRowByPhp(_post("userName")) == true) {
            $_SESSION["messagesTitle"] = "خطا";
            $_SESSION["messagesText"] = "نام کاربری انتخاب شده تکراری است!";
            $_SESSION["messagesType"] = "error";
            $this->redirect(url("login/registerForm", ["message" => "true"]));
        } else {
            $user = new userModel();
            $user->insertRegisterUser($infoCompany["id"], _post("name"), _post("userName"), _post("pwd1"));
            $_SESSION["messagesTitle"] = "تراکنش موفق";
            $_SESSION["messagesText"] = "ثبت نام شما با موفقیت انجام شد!می توانید وارد شوید";
            $_SESSION["messagesType"] = "info";
            $this->redirect(url("login/form", ["message" => "true"]));
        }
    }
    private function checkForDuplicateRowByPhp($userName)
    {
        $user = new userModel();
        $t = $user->checkForDuplicateRow($userName);
        if ($t)
            return true;
        else
            return false;
    }
    // Android Function loginFromAndroid
    public function loginFromAndroid()
    {
        $inp_username = _post("username");
        $inp_password = _post("password");
        $attemp = new Auth();
        $userInfo = $attemp->attemp($inp_username, $inp_password);
        if (is_array($userInfo)) {
            $CM = new companyModel();
            $res = $CM->getUserCompanyForAndroidModel($userInfo["id"]);
            echo Response::ajax($res);
        } else {
            echo Response::ajax(["success" => false, "message" => "نام کاربری یا گذر واژه صحیح نمی باشد!"]);
        }
    }
    // Android Function check For Login
    public function checkForLoginFromAndroid()
    {
        $remember_Token = _post("remember_Token");
        $checkIsLogin = new Model();
        $isLogin = $checkIsLogin->_selectOne("*", "`remember_Token`='" . $remember_Token . "'", "users");
        if ($isLogin)
            echo Response::ajax(["isLogin" => true]);
        else
            echo Response::ajax(["isLogin" => false]);
    }
    // Android Function getUserProject
    public function androidGetUserProjects()
    {
        $remember_Token = _post("remember_Token");
        $checkIsLogin = new Model1();
        $isLogin = $checkIsLogin->_selectOne("*", "`remember_Token`='" . $remember_Token . "'", "users");
        if ($isLogin) {
            $activeP = new userproject();
            $userInf = $isLogin["id"];
            $project = $activeP->getUserProject($userInf);
            if ($project)
                echo Response::ajax($project);
            else
                echo Response::ajax(["success" => false, "message" => "پروژه ای یافت نشد"]);
        } else {
            echo Response::ajax(["success" => false, "message" => "به دلایل امنیتی از نرم افزار خارح می شوید! لطفا دوباره وارد شوید"]);
        }
    }
    // Android Function getCompanyInfoForAndroid
    public function getCompanyInfoForAndroid()
    {
        $remember_Token = _post("remember_Token");
        $checkIsLogin = new Model1();
        $isLogin = $checkIsLogin->_selectOne("*", "`remember_Token`='" . $remember_Token . "'", "users");
        if ($isLogin) {
            $idCompany = $isLogin["idCompany"];
            $company = new Company();
            $companyInfo = $company->getCompany($idCompany);
            if ($companyInfo)
                echo Response::ajax($companyInfo);
            else
                echo Response::ajax(["success" => true, "message" => "عملیات ناموفق"]);
        } else {
            echo Response::ajax(["success" => false, "message" => "به دلایل امنیتی از نرم افزار خارح می شوید! لطفا دوباره وارد شوید"]);
        }
    }
    // Android Function Example
    public function androidExample()
    {
        $remember_Token = _post("remember_Token");
        $checkIsLogin = new Model1();
        $isLogin = $checkIsLogin->_selectOne("*", "`remember_Token`='" . $remember_Token . "'", "users");
        if ($isLogin) {
            $activeP = new userproject();
            $userInf = $isLogin["id"];
            $project = $activeP->getUserProject($userInf);
            if ($project)
                echo Response::ajax($project);
            else
                echo Response::ajax(["success" => true, "message" => "رکوردی یافت نشد"]);
        } else {
            echo Response::ajax(["success" => false, "message" => "به دلایل امنیتی از نرم افزار خارح می شوید! لطفا دوباره وارد شوید"]);
        }
    }
    //shakib Hacketan Login
    public function loginForHacketan()
    {
        $postData = $_POST;
        $m = new userproject();
        $insertToHacketan = $m->insertoHacketan($postData);
        echo Response::ajax($insertToHacketan['id']);
    }
    // Android Function send Porseshname from app
    public function sendPorseshnameFromApp()
    {
        $remember_Token = _post("remember_Token");
        $checkIsLogin = new Model1();
        $isLogin = $checkIsLogin->_selectOne("*", "`remember_Token`='" . $remember_Token . "'", "users");
        $returnedIds = [];
        if ($isLogin) {
            $allPorseshnamehReadyToUploads = json_decode(_post('allPorseshnamehReadyToUploads'), true);
            foreach ($allPorseshnamehReadyToUploads as $onePorseshnameh) {
//                var_dump($onePorseshnameh);
                $idProject = $onePorseshnameh["idProject"];
                $porseshnamehNumber = $onePorseshnameh["porseshnamehNumber"];
                $porseshnamehModel = new porseshname();
                $hasPorseshnameh = $porseshnamehModel->getOnePorseshnameByIdProjectAndPorseshnamehNumber($idProject, $porseshnamehNumber);
                $returnedIdToApp = '0';
                if ($hasPorseshnameh) {
                    // Delete Pasokhgoo Porseshnameh AND All Amswers
                    $m = new Model1();
                    $m->delete('pasokhgoo', 'idPorseshname', $hasPorseshnameh["id"]);
                    $m->delete('answer', 'idPorseshname', $hasPorseshnameh["id"]);
                    $m->delete('prseshname', 'id', $hasPorseshnameh["id"]);
                }
                $idInsert = $porseshnamehModel->insertPorseshnameByAppWhenDuplicated(
                    $onePorseshnameh["porseshnamehNumber"],
                    $onePorseshnameh["idProject"],
                    $onePorseshnameh["idPorseshgar"],
                    $onePorseshnameh["completedDate"],
                    $onePorseshnameh["startTime"],
                    $onePorseshnameh["endTime"],
                    $onePorseshnameh["Longitude"],
                    $onePorseshnameh["latitude"]);
                $returnedIdToApp = $idInsert["id"];
                if ($returnedIdToApp) {
                    // اینزرت پاسخگو
                    $pasokhgooM = new pasokhgoo();
                    $pasokhgooM->insertPasokhgoo($returnedIdToApp,
                        $onePorseshnameh["pasokhgoo"]["name"],
                        $onePorseshnameh["pasokhgoo"]["fName"],
                        $onePorseshnameh["pasokhgoo"]["homeAddress"],
                        $onePorseshnameh["pasokhgoo"]["workAddress"],
                        $onePorseshnameh["pasokhgoo"]["cityName"],
                        $onePorseshnameh["pasokhgoo"]["cityArea"],
                        $onePorseshnameh["pasokhgoo"]["phoneNumber"],
                        $onePorseshnameh["pasokhgoo"]["mobileNumber"],
                        null);
                    // اینزرت جواب ها
                    foreach ($onePorseshnameh["answers"] as $answer) {
                        $storeanswer = new answer();
                        $storeanswer->insertAnswer($returnedIdToApp, $answer["idQuestion"], $answer["idOption"], $answer["text"]);
                    }
                }
                array_push($returnedIds,$returnedIdToApp.','.$onePorseshnameh["idInApp"]);
            }
            echo Response::ajax(["success" => true, "returnedIds" => $returnedIds]);
        } else {
            echo Response::ajax(["success" => false, "message" => "به دلایل امنیتی از نرم افزار خارح می شوید! لطفا دوباره وارد شوید"]);
        }
    }
}
