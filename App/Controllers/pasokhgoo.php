<?php

namespace App\Controllers;

use App\Classes\getElements;
use Illuminate\Contracts\Session\Session;
use App\Models\pasokhgoo as pasokhgooModel;
use App\Classes\Response;
use App\Classes\Auth;

class pasokhgoo extends BaseController
{
    public function viewPasokhgoo()
    {
        $aut = new Auth();
        $idProject = $aut->getProject()["id"];
        $porseshnamehInf = $aut->getPorseshname();
        $pasokhgoo = new pasokhgooModel();
        $getCity = $pasokhgoo->getCity($idProject);
        if (_get("methode") == "search") {
            $searchPasokhgoo = new pasokhgooModel();
            $getPasokhgoo = $searchPasokhgoo->searchPasokhgoo($porseshnamehInf["id"]);
        }
        $this->view("pasokhgoo.pasokhgoo", compact("getCity", "idPorseshname", "getPasokhgoo", "porseshnamehInf"));

    }

    public function viewPasokhgooFromCompletedList()
    {
        $_SESSION["idPorseshname"] = _get("idPo");
        $aut = new Auth();
        $idProject = $aut->getProject()["id"];
        $porseshnamehInf = $aut->getPorseshname();
        if(!$porseshnamehInf){
            $this->redirect(url("porseshname/PorseshnameformView"));
        }
        else
        {
            $pasokhgoo = new pasokhgooModel();
            $getCity = $pasokhgoo->getCity($idProject);
            $searchPasokhgoo = new pasokhgooModel();
            $getPasokhgoo = $searchPasokhgoo->searchPasokhgoo($porseshnamehInf["id"]);
            $this->view("pasokhgoo.pasokhgoo", compact("getCity", "idPorseshname", "getPasokhgoo", "porseshnamehInf"));
        }

    }

    public function insertPasokhgoo()
    {
        $aut = new Auth();
        $idPorseshname = $aut->getPorseshname()["id"];
        $pasokhgooM = new pasokhgooModel();
        $action=$pasokhgooM->getPasokhgooByIdPorseshnameh($idPorseshname);
        if($action){
            $idPasokhgoo=$action["id"];
            $pasokhgooM->updatePasokhgoo(
                $idPorseshname,
                _post("name"),
                _post("fName"),
                _post("homeAddress"),
                _post("workAddress"),
                _post("cityName"),
                _post("cityArea"),
                _post("phoneNumber"),
                _post("mobileNumber"),
                $idPasokhgoo);
        }
        else
        {
            $pasokhgooM->insertPasokhgoo($idPorseshname,_post("name"),_post("fName"), _post("homeAddress"),
                                            _post("workAddress"), _post("cityName"), _post("cityArea"),_post("phoneNumber"),
                                            _post("mobileNumber"));
        }
        $this->redirect(url("Question/loadQuestion", ["rowNum" => 1]));
    }

    public function searchPasokhgoo()
    {
        $searchPasokhgoo = new pasokhgooModel();
        $id = _post("search_value");
        $getPasokhgoo = $searchPasokhgoo->searchPasokhgoo($id);
        $this->view("pasokhgoo.pasokhgoo", compact("getPasokhgoo"));
    }

    public function getRowForUpdate()
    {

        $searchPasokhgoo = new pasokhgooModel();
        $id = _get("id");
        $getPasokhgoo1 = $searchPasokhgoo->getPasokhgoo($id);
        echo Response::ajax($getPasokhgoo1);

    }

    public function updatePasokhgoo()
    {
        $id = _post("id");
        $idPorseshname = _post("idPorseshname");
        if (_post("action") == "update") {

            $pModel = new pasokhgooModel();
            $pModel->updatePasokhgoo(_post("idPorseshname"), _post("name"), _post("fName"),
                _post("homeAddress"), _post("workAddress"), _post("cityName"),
                _post("cityArea"), _post("phoneNumber"), _post("mobileNumber"), $id);
        }
        $this->redirect(url("porseshname/manage", ["id" => $idPorseshname]));

    }


}