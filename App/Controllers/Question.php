<?php
/**
 * Created by PhpStorm.
 * User: Jafari
 * Date: 08/11/2017
 * Time: 03:47 PM
 */

namespace App\Controllers;
use App\Classes\AccessPrimision;
use App\Classes\PolicyCheck;
use App\Classes\Response;
use App\Models\answer;
use App\Models\Model;
use App\Models\options as optionModel;
use App\Models\Question as QuestionModel;
use App\Models\answer as answerModel;
use App\Classes\imageUploader as imageUploade;
use App\Classes\Auth as Auth;
use App\Models\policy;
use App\Classes\Policy as PolicyClass;
use App\Models\porseshname as porseshnameModel;
use App\Classes\RowNumberManagment;
class Question extends BaseController
{
    public function loadQuestion()
    {
        $aut = new Auth();
        $idPorseshname = $aut->getPorseshname()["id"];
        $aut = new Auth();
        $idProject = $aut->getProject()["id"];
        $question = new QuestionModel();
        $questionRow = $question->getQuetion($idProject, _get("rowNum"));
//        var_dump($questionRow);
        if (!empty($questionRow)) {
            $pM = new policy();
            $policyCheck = new PolicyCheck();
            $socialName = $policyCheck->calcSocialClass($idPorseshname, $idProject)["socialName"];
//            var_dump($socialName);
            $textPolicyansToOther = $policyCheck->getPolicyansToOther($idPorseshname, $questionRow[0]["opIdQuestion"]);
            $SocialPolicyIs = "false";
            $StopPolicyIs = "false";
            $CountPolicyIs = "false";
            $policyRows = $pM->getPolicySetForCheck($idProject, $questionRow[0]["opIdQuestion"]);
            foreach ($policyRows as $policyRow) {
                if ($policyRow["idPolicy"] == "1") $SocialPolicyIs = "True";
                if ($policyRow["idPolicy"] == "2") $StopPolicyIs = "True";
                if ($policyRow["idPolicy"] == "3") $CountPolicyIs = "True";
                $policyCheckResult = $policyCheck->policyCheckForStop($policyRow, $questionRow[0]["opIdQuestion"]);
            }
            $qModel = new QuestionModel();
            $questionType = $qModel->getQuestionTypeById($questionRow[0]["idQuestionType"]);
            $answer = new answerModel();
            $getThisQuestionAnswer = $answer->getAnswer($questionRow[0]["opIdQuestion"], $idPorseshname);
            $questionsCount = $this->getQuestionsCount($idProject);
            $porseshnamehInf = $aut->getPorseshname();
            $p = ((_get("rowNum") * 100));
            $porseshnamePercent = round(((int)$p / (int)$questionsCount[0][0]));
            $progressbarType = 0;
            if ($porseshnamePercent > 0 & $porseshnamePercent < 25) $progressbarType = "danger";
            if ($porseshnamePercent >= 25 & $porseshnamePercent < 50) $progressbarType = "warning";
            if ($porseshnamePercent >= 50 & $porseshnamePercent < 75) $progressbarType = "info";
            if ($porseshnamePercent >= 75 & $porseshnamePercent <= 100) $progressbarType = "success";
//            dd($questionRow);
            $this->view("question.showquestion", compact("questionRow", "questionType", "getThisQuestionAnswer", "porseshnamehInf", "questionsCount", "socialName", "SocialPolicyIs", "StopPolicyIs", "CountPolicyIs", "textPolicyansToOther", "porseshnamePercent", "progressbarType"));
        } else {
            $updatePorseshnameForFinished = new porseshname();
            $updatePorseshnameForFinished->updatePorseshnameForFinished($idPorseshname);
            $this->redirect(url("porseshname/finishPorseshname", ["policyCheckResult" => "finish"]));
        }
    }
    public function answer()
    {
//        dd($_POST);
        $p = 0;
        $s = 0;
        $aut = new Auth();
        $idPorseshname = $aut->getPorseshname()["id"];
        $storeanswer = new answerModel();
        $questionType = _post("questionType");
        $storeanswer->storeAnswer($idPorseshname, _post("answer"), _post("idQ"), _post("questionType"));
        if (_post('base64image')) {
            $img = _post('base64image');
            $dir = DOC_ROOT . "/public/imagesUpload/answer/uploadImage/";
            $img = str_replace('data:image/png;base64,', '', $img);
            $img = str_replace(' ', '+', $img);
            $data = base64_decode($img);
//            $file = $dir . uniqid() . '.jpg';
            $file = $dir . _post("answer") . '.jpg';
            $success = file_put_contents($file, $data);
        } else if (_file($_FILES["uploadFile"]) == true) {
            $file = $_FILES["uploadFile"];
            $imageUp = new imageUploade();
            $op = new optionModel();
            $op = $op->updateImgOption(_post("id"), "Yes");
            $targetDir = "/imagesUpload/question/";
            $dir = DOC_ROOT . "/public/imagesUpload/question/";
            $dir = DOC_ROOT . "/public/imagesUpload/answer/uploadImage/";
            $uploadedFileName = _post("answer");
            $imgUpload = $imageUp->uploadImage($targetDir, $file, $dir, $uploadedFileName);
        }
        $rowNum = _get("rowNum") + 1;
        //چک کن آیا پالیسی دارد یا نه؟
        $aut = new Auth();
        $idProject = $aut->getProject()["id"];
        $pM = new policy();
        $policyRows = $pM->getPolicySetForCheck($idProject, _post("idQ"));
        $policyCheck = new PolicyCheck();
        foreach ($policyRows as $policyRow) {
            $postAnswers = _post("answer");
            if (is_array(_post("answer"))) {
                foreach ($postAnswers as $postAnswer) {
                    if ($policyRow["idOption"] == $postAnswer) {
                        $policyCheckResult = $policyCheck->policyCheckForStop($policyRow, $postAnswer);
                    }
                    if (isset($policyCheckResult)) {
                        if ($policyCheckResult == "CountPolicyIsTrue") {
                            $p = 1;
                        }
                        if ($policyCheckResult == "StopPolicyIsTrue") {
                            $s = 1;
                        }
                        if ($policyCheckResult != "StopPolicyIsTrue" && $policyCheckResult != "CountPolicyIsTrue" && $policyCheckResult != null) {
                            $idQForLoad = $policyCheckResult;
                            $qM = new QuestionModel();
                            $rowNum = $qM->getById($idQForLoad)["rowNumber"];
                        }
                    }
                }
            } else {
                $postAnswer2 = _post("answer");
                if ($policyRow["idOption"] == $postAnswer2) {
                    $policyCheckResult = $policyCheck->policyCheckForStop($policyRow, $postAnswer2);
                }
                if (isset($policyCheckResult)) {

                    if ($policyCheckResult == "CountPolicyIsTrue") {
                        $p = 1;
                    }
                    if ($policyCheckResult == "StopPolicyIsTrue") {
                        $s = 1;
                    }
                    if ($policyCheckResult != "StopPolicyIsTrue" && $policyCheckResult != "CountPolicyIsTrue" && $policyCheckResult != null) {
                        $idQForLoad = $policyCheckResult;
                        $qM = new QuestionModel();
                        $rowNum = $qM->getById($idQForLoad)["rowNumber"];
                    }
                }
            }
        }
//        dd($p);
        if ($p == 1) {
            $this->redirect(url("porseshname/finishPorseshname", ["policyCheckResult" => "CountPolicyIsTrue"]));
        } elseif ($s == 1) {
            $this->redirect(url("porseshname/finishPorseshname", ["policyCheckResult" => "StopPolicyIsTrue"]));
        } else {
//            dd("sdasdd");
            $this->redirect(url("Question/loadQuestion", ["rowNum" => $rowNum]));
        }
    }
    public function updateQuestion()
    {
//        dd($_POST);
        $minNumberType = null;
        $maxNumberType = null;
        $rangeTeifi = null;
        $maxValue = null;
        $rangeValue = null;
        $rigthLable = null;
        $centerLable = null;
        $leftLeble = null;
        $maxValueDegree = null;
        if (_post("idQuestionType") == 6) {
            $minNumberType = _post("minNumberType");
            $maxNumberType = _post("maxNumberType");
        }
        if (_post("idQuestionType") == 7) {
            $rangeValue = _post("rangeTeifi");
            $maxValue = _post("maxValue");
            $rigthLable = _post("rigthLable");
            $centerLable = _post("centerLable");
            $leftLeble = _post("leftLeble");
        }
        if (_post("idQuestionType") == 9) {
            $rangeValue = _post("rangeDegree");
            $maxValueDegree = _post("maxValueDegree");
        }
        $aut = new Auth();
        $idProject = $aut->getProject()["id"];
        $storQ = new QuestionModel();
        $rM = new RowNumberManagment();
        $idUpdateQuestion = $storQ->updateQuestion($idProject, _post("rowNumber"), _post("idQuestionType"), $minNumberType, $maxNumberType, $rangeValue, $rigthLable, $centerLable, $leftLeble, _post("questionCode"), _post("text"), _post("moreExplanation"), _post("allowNull"), _post("idQ"));
        $op = new optionModel();
        $idOptionOfThisQuestion = ($op->getThisQuestionOptionsf(_post("idQ"))[0]["id"]);
        if (_post("idQuestionType") == 3) {
            $op->updateTextOptionForChangeQType($idOptionOfThisQuestion, "QIsTashrihi");
        }
        if (_post("idQuestionType") == 6) {
            $op->updateTextOptionForChangeQType($idOptionOfThisQuestion, "QIsNumber");
        }
        if (_post("idQuestionType") == 7) {
            $op->updateTextOptionForChangeQType($idOptionOfThisQuestion, "QIsTeif");
        }
        if (_post("idQuestionType") == 8) {
            $op->updateTextOptionForChangeQType($idOptionOfThisQuestion, "QIsEmail");
        }
        if (_post("idQuestionType") == 9) {
            $op->updateTextOptionForChangeQType($idOptionOfThisQuestion, "QIsDegree");
        }
        if (_post("idQuestionType") == 10) {
            $op->updateTextOptionForChangeQType($idOptionOfThisQuestion, "QIsWebsiteLink");
        }
        if (_post("idQuestionType") == 12) {
            $op->updateTextOptionForChangeQType($idOptionOfThisQuestion, "QIsNoResponse");
        }
        if (_post("idQuestionType") == 14) {
            $op->updateTextOptionForChangeQType($idOptionOfThisQuestion, "QIsdateFormat");
        }
        $this->redirect(url("admin/selectedProject"));
        /*updateQ = new QuestionModel();
        $updateQ->updateQuestion(_post("idQ"));
        */
    }
    public function storeQuestion()
    {
//        var_dump(_file($_FILES['uploadAudioFile']));
//        dd($_FILES['uploadFile']['name']['soti']);
//        dd($_FILES);
        $minNumberType = 0;
        $maxNumberType = null;
        $rangeTeifi = null;
        $maxValue = null;
        $rangeValue = null;
        $centerLable = null;
        $leftLeble = null;
        $maxValueDegree = null;
        $rigthLable = null;
        $videoURL = null;
        if (_post("idQuestionType") == 6) {
            $minNumberType = _post("minNumberType");
            $maxNumberType = _post("maxNumberType");
        }
        if (_post("idQuestionType") == 7) {
            $rangeValue = _post("rangeTeifi");
            $maxValue = _post("maxValue");
            $rigthLable = _post("rigthLable");
            $centerLable = _post("centerLable");
            $leftLeble = _post("leftLeble");
        }
        if (_post("idQuestionType") == 9) {
            $rangeValue = _post("rangeDegree");
            $maxValueDegree = _post("maxValueDegree");
        }
        if (_post("idQuestionType") == 15) {
            $videoURL = _post("videoURL");
        }
        if (_post("idQuestionType") == 16) {
            if (_file($_FILES["uploadFile"]) == true) {
                $file = $_FILES["uploadFile"];
                $imageUp = new imageUploade();
                if ($imageUp->checkFileIsaActualImage($file) == false) {
                    $_SESSION["messagesTitle"] = "خطا";
                    $_SESSION["messagesText"] = "فایل انتخاب شده تصویر نیست!";
                    $_SESSION["messagesType"] = "error";
//                    dd("0");
                    $this->redirect(url("admin/selectedProject", ["collapsin" => "insertQ", "error" => "true"]));
                } else if ($imageUp->checkFileSize($file) == false) {
                    $_SESSION["messagesTitle"] = "خطا";
                    $_SESSION["messagesText"] = "حجم تصویر انتخاب شده بیشتر از حد مجاز است!";
                    $_SESSION["messagesType"] = "error";
//                    dd("1");
                    $this->redirect(url("admin/selectedProject", ["collapsin" => "insertQ", "error" => "true"]));
                } else {
//                    dd("2");
                    $op = new optionModel();
                    $op = $op->updateImgOption(_post("id"), "Yes");
                    $targetDir = "/imagesUpload/question/";
                    $dir = DOC_ROOT . "/public/imagesUpload/question/";
                    $uploadedFileName = _post("idProject") . _post("idQuestionType") . _post('rowNumber');
                    $imgUpload = $imageUp->uploadImage($targetDir, $file, $dir, $uploadedFileName);
                }
            }
        }
        if (_post("idQuestionType") == 17) {
            if (_file($_FILES["uploadAudioFile"]) == true) {
                $imageUp = new imageUploade();
                $file = $_FILES["uploadAudioFile"];
                if ((explode("/", $_FILES['uploadAudioFile']['type'])[0] === "audio") == false) {
                    $_SESSION["messagesTitle"] = "خطا";
                    $_SESSION["messagesText"] = "فایل انتخاب شده صوتی نیست!";
                    $_SESSION["messagesType"] = "error";
                    $this->redirect(url("admin/selectedProject", ["collapsin" => "insertQ", "error" => "true"]));
                } else if ($imageUp->checkFileSize($file, 5) == false) {
                    $_SESSION["messagesTitle"] = "خطا";
                    $_SESSION["messagesText"] = "حجم فایل انتخاب شده بیشتر از حد مجاز است!";
                    $_SESSION["messagesType"] = "error";
                    $this->redirect(url("admin/selectedProject", ["collapsin" => "insertQ", "error" => "true"]));
                } else {

                    $op = new optionModel();
                    $op = $op->updateImgOption(_post("id"), "Yes");
                    $targetDir = "/imagesUpload/question/";
                    $dir = DOC_ROOT . "/public/imagesUpload/question/";
                    $uploadedFileName = _post("idProject") . _post("idQuestionType") . _post('rowNumber');
                    $imgUpload = $imageUp->uploadImage($targetDir, $file, $dir, $uploadedFileName);
                }
            }
        }
        $aut = new Auth();
        $idProject = $aut->getProject()["id"];
        $storQ = new QuestionModel();
        //        برای اینکه در قسمت بیشتر سوالات بخش گزینه ها نباشد با تکنیک زیر یک ردیف در جدول گزینه ها ایجاد میکنیم که موقعی که برروی بیشتر کلیک شد بخش گزینه ها دیده نشود
        if (_post("actionQuestion") == "update") {
            $update = $storQ->updateQuestion($idProject, _post("rowNumber"), _post("idQuestionType"), $minNumberType, $maxNumberType, $rangeValue, $rigthLable, $centerLable, $leftLeble, _post("questionCode"), _post("text"), _post("moreExplanation"), _post("allowNull"), $videoURL, _post("idQ"));
            $idInsertedQuestion = _post("idQ");
            $op = new optionModel();
            $idOptionOfThisQuestion = ($op->getThisQuestionOptionsf(_post("idQ"))[0]["id"]);
            if (_post("idQuestionType") == 3) {
                $op->updateTextOptionForChangeQType($idOptionOfThisQuestion, "QIsTashrihi");
            }
            if (_post("idQuestionType") == 6) {
                $op->updateTextOptionForChangeQType($idOptionOfThisQuestion, "QIsNumber");
            }
            if (_post("idQuestionType") == 7) {
                $op->updateTextOptionForChangeQType($idOptionOfThisQuestion, "QIsTeif");
            }
            if (_post("idQuestionType") == 8) {
                $op->updateTextOptionForChangeQType($idOptionOfThisQuestion, "QIsEmail");
            }
            if (_post("idQuestionType") == 9) {
                $op->updateTextOptionForChangeQType($idOptionOfThisQuestion, "QIsDegree");
            }
            if (_post("idQuestionType") == 10) {
                $op->updateTextOptionForChangeQType($idOptionOfThisQuestion, "QIsWebsiteLink");
            }
            if (_post("idQuestionType") == 12) {
                $op->updateTextOptionForChangeQType($idOptionOfThisQuestion, "QIsNoResponse");
            }
            if (_post("idQuestionType") == 14) {
                $op->updateTextOptionForChangeQType($idOptionOfThisQuestion, "QIsdateFormat");
            }
            if (_post("idQuestionType") == 15) {
                $op->updateTextOptionForChangeQType($idOptionOfThisQuestion, "QIsVideo");
            }
            if (_post("idQuestionType") == 16) {
                $op->updateTextOptionForChangeQType($idOptionOfThisQuestion, "QIsImage");
            }
            if (_post("idQuestionType") == 17) {
                $op->updateTextOptionForChangeQType($idOptionOfThisQuestion, "QIsAudio");
            }
            if (_post("idQuestionType") == 18) {
                $op->updateTextOptionForChangeQType($idOptionOfThisQuestion, "QIsUploadVideo");
            }
            if (_post("idQuestionType") == 19) {
                $op->updateTextOptionForChangeQType($idOptionOfThisQuestion, "QIsUploadImage");
            }
            if (_post("idQuestionType") == 20) {
                $op->updateTextOptionForChangeQType($idOptionOfThisQuestion, "QIsUploadAudio");
            }
//            dd("qwerer");
        } elseif (_post("actionQuestion") == "insert") {

            $rM = new RowNumberManagment();
            $rownumber = _post("rowNumber");
            $Aut = new Auth();
            $idProject = $Aut->getProject()["id"];
            $rM->shift("questions", "rowNumber", _post("rowNumber"), "+", $idProject);//مرتب سازی ردیف ها
            $idInsertedQuestion = $storQ->storeQuestion($idProject, _post("rowNumber"), _post("idQuestionType"), $minNumberType, $maxNumberType, $rangeValue, $rigthLable, $centerLable, $leftLeble, _post("questionCode"), _post("text"), _post("moreExplanation"), _post("allowNull"), $videoURL);
            $op = new optionModel();
            if (_post("idQuestionType") == 3) {
                $op->storeOption($idInsertedQuestion, "QIsTashrihi", "", 1, "No");
            }
            if (_post("idQuestionType") == 6) {
                $op->storeOption($idInsertedQuestion, "QIsNumber", "", 1, "No");
            }
            if (_post("idQuestionType") == 7) {
                $op->storeOption($idInsertedQuestion, "QIsTeif", "", 1, "No");
            }
            if (_post("idQuestionType") == 8) {
                $op->storeOption($idInsertedQuestion, "QIsEmail", "", 1, "No");
            }
            if (_post("idQuestionType") == 9) {
                $op->storeOption($idInsertedQuestion, "QIsDegree", "", 1, "No");
            }
            if (_post("idQuestionType") == 10) {
                $op->storeOption($idInsertedQuestion, "QIsWebsiteLink", "", 1, "No");
            }
            if (_post("idQuestionType") == 12) {
                $op->storeOption($idInsertedQuestion, "QIsNoResponse", "", 1, "No");
            }
            if (_post("idQuestionType") == 14) {
                $op->storeOption($idInsertedQuestion, "QIsdateFormat", "", 1, "No");
            }
            if (_post("idQuestionType") == 15) {
                $op->storeOption($idInsertedQuestion, "QIsVideo", "", 1, "No");
            }
            if (_post("idQuestionType") == 16) {
                $op->storeOption($idInsertedQuestion, "QIsImage", "", 1, "No");
            }
            if (_post("idQuestionType") == 17) {
                $op->storeOption($idInsertedQuestion, "QIsAudio", "", 1, "No");
            }
            if (_post("idQuestionType") == 18) {
                $op->storeOption($idInsertedQuestion, "QIsUploadVideo", "", 1, "No");
            }
            if (_post("idQuestionType") == 19) {
                $op->storeOption($idInsertedQuestion, "QIsUploadImage", "", 1, "No");
            }
            if (_post("idQuestionType") == 20) {
                $op->storeOption($idInsertedQuestion, "QIsUploadAudio", "", 1, "No");
            }
        }
//        برای اینکه بعد از زدن گزینه ثبت به بخش گزینه ها نرویم این شرط گذاشته شده است
        if (_post("idQuestionType") != 3
            && _post("idQuestionType") != 6
            && _post("idQuestionType") != 7
            && _post("idQuestionType") != 8
            && _post("idQuestionType") != 9
            && _post("idQuestionType") != 10
            && _post("idQuestionType") != 12
            && _post("idQuestionType") != 14
            && _post("idQuestionType") != 15
            && _post("idQuestionType") != 16
            && _post("idQuestionType") != 17
            && _post("idQuestionType") != 18
            && _post("idQuestionType") != 19
            && _post("idQuestionType") != 20) {
//            dd("تا اینجا");
            $this->redirect(url("Question/manageOptions", ["idQ" => $idInsertedQuestion, "collapsin" => "optiona"]));
        } else {

            $this->redirect(url("admin/selectedProject", ["collapsin" => "insertQ"]));
        }
    }
    function getQuestionType($idQuestionType)
    {
        if ($idQuestionType == 1) return ["questionType" => "singleResponse"];
        if ($idQuestionType == 2) return ["questionType" => "multipleChoice"];
        if ($idQuestionType == 3) return ["questionType" => "openQuestion"];
        if ($idQuestionType == 4) return ["questionType" => "singleResponseOthers"];
        if ($idQuestionType == 5) return ["questionType" => "multipleChoiceOthers"];
        if ($idQuestionType == 6) return ["questionType" => "numeric"];
        if ($idQuestionType == 7) return ["questionType" => "spectral"];
        if ($idQuestionType == 8) return ["questionType" => "email"];
        if ($idQuestionType == 9) return ["questionType" => "degree"];
        if ($idQuestionType == 10) return ["questionType" => "websitelink"];
        if ($idQuestionType == 11) return ["questionType" => "prioritize"];
        if ($idQuestionType == 12) return ["questionType" => "noResponse"];
        if ($idQuestionType == 13) return ["questionType" => "openOptions"];
        if ($idQuestionType == 14) return ["questionType" => "dateFormat"];
    }
    public function getLastNumbrOfQuestionRow($idP)
    {
        $storQ = new QuestionModel();
        return $storQ->getLastNumbrOfQuestionRow($idP);
    }
    private function checkForDuplicatedValueForRowNumber($rowNumber, $idProject)
    {
        $QuestionModel = new QuestionModel();
        return $QuestionModel->checkForDuplicatedValueForRowNumber($rowNumber, $idProject);
    }
    public function getQuestionList($idProject)
    {
        $QuestionModel = new QuestionModel();
        $QuestionList = $QuestionModel->getQuestionList($idProject);
        return $QuestionList;
    }
    public function checkForDuplicateRow()
    {
        $QuestionModel = new QuestionModel();
        $t = $QuestionModel->checkForDuplicatedValueForRowNumber(_get("rowNumber"), _get("idP"));
        echo Response::ajax($t);
    }
    public function getQuestionsCount($idProject)
    {
        $qmodel = new QuestionModel();
//        dd($qmodel->getQuestionsCount($idProject));
        return $qmodel->getQuestionsCount($idProject);
    }
    public function deleteQuestion()
    {
        $acP = new AccessPrimision();
        if (($acP->checkQuestionForProject(_get("id"))) == false) {
            $this->redirect(url("login/logout", ["message" => "true"]));
        }
        //delete Option
        $delOp = new optionModel();
        $delOption = $delOp->deleteOption(_get("id"));
        //Delete Answers From ThisQuestion
        $ansM = new answer();
        $ansM->deleteByIdQuestion(_get("id"));
        // get Question rowNumber For ShiftUpDown
        $qModel = new QuestionModel();
        $qRow = $qModel->getById(_get("id"));
        $rownumber = $qRow["rowNumber"];
        $qModel->deletequestion(_get("id"));
        $imageUp = new imageUploade();
        // آدرس تصویر
        if ($qRow["idQuestionType"] == 16) {
            $delDirImage = DOC_ROOT . "/public/imagesUpload/question/" . $qRow["idProject"] . $qRow["idQuestionType"] . $qRow['rowNumber'] . ".jpg";
            $imageUp->existImage($delDirImage);
        }
        if ($qRow["haveImage"] == "Yes") {
            $delDirImage = DOC_ROOT . "/public/imagesUpload/question/" . $qRow["id"] . ".jpg";
            $imageUp->existImage($delDirImage);
        }
        $Aut = new Auth();
        $idProject = $Aut->getProject()["id"];
        $rM = new RowNumberManagment();
        $rM->shift("questions", "rowNumber", $rownumber, "-", $idProject);
        $this->redirect(url("admin/selectedProject", ["collapsin" => "listQ"]));
    }
    public function editQuestion()
    {
        $aut = new Auth();
        $project = $aut->getProject();
        $acP = new AccessPrimision();
        if (($acP->checkQuestionForProject(_get("idQ"))) == false) {
            $this->redirect(url("login/logout", ["message" => "true"]));
        }
        $idQ = _get("idQ");
        $qModel = new QuestionModel();
        $qRow = $qModel->getById($idQ);
//        dd($qRow);
        $qM = new QuestionModel();
        $questiontypes = $qM->getQuestionTypeList();
        if (_get("error") == "true") {
            $error = "true";
            $this->view("admin.editQuestion", compact("qRow", "error", "project", "questiontypes"));
        } else {
            $this->view("admin.editQuestion", compact("qRow", "project", "questiontypes"));
        }
    }
    public function manageOptions()
    {
//        var_dump($_GET);
        $acP = new AccessPrimision();
        if (($acP->checkQuestionForProject(_get("idQ"))) == false) {
            $this->redirect(url("login/logout", ["message" => "true"]));
        }
        $Aut = new Auth();
        $idProject = $Aut->getProject()["id"];
        $idQ = _get("idQ");
        $qModel = new QuestionModel();
        $rowQuestion = $qModel->getById($idQ);
        $allQuestions = $qModel->getAllquestion($idProject);
//        dd($allQuestions);
        $opt = new optionModel();
        $optionsLists = $opt->getThisQuestionOptionsf($idQ);
        $PC = new PolicyClass();
        $getPolicySocial = $PC->getPolicySocial($idProject, $idQ);
        $getPolicyStops = $PC->getPolicyStop($idProject, $idQ);
        $getPolicyCount = $PC->getPolicyCount($idProject, $idQ);
        $getPolicyJumps = $PC->getPolicyJump($idProject, $idQ);
//        var_dump($getPolicyJumps);
        $getPolicyansToOtherQPols = $PC->getPolicyansToOtherQPol($idProject, $idQ);
//        var_dump($getPolicyansToOtherQPols);
        $policyM = new policy();
        $policyslists = $policyM->getPolicyList();
        $error = _get("error");
        $collapsin = _get("collapsin");
        $opModel = new optionModel();
        $nextScore = $opModel->maxScore($idQ)[0][0] + 1;
        if ($error == true) {
            $this->view("admin.manageOptions", compact("idQ", "rowQuestion", "optionsLists", "policyslists", "getPolicySocial", "getPolicyStops", "getPolicyCount", "getPolicyJumps", "collapsin", "allQuestions", "getPolicyansToOtherQPols", "error", "nextScore"));
        } else {
            $this->view("admin.manageOptions", compact("idQ", "rowQuestion", "optionsLists", "getPolicySocial", "getPolicyStop", "getPolicyStops", "getPolicyCount", "getPolicyJumps", "collapsin", "policyslists", "allQuestions", "getPolicyansToOtherQPols", "nextScore"));
        }
    }
    public function updateImg()
    {

        if (_file($_FILES["uploadFile"]) == true) {
            $file = $_FILES["uploadFile"];
            $imageUp = new imageUploade();
            if ($imageUp->checkFileIsaActualImage($file) == false) {
                $_SESSION["messagesTitle"] = "خطا";
                $_SESSION["messagesText"] = "فایل انتخاب شده تصویر نیست!";
                $_SESSION["messagesType"] = "error";
                $this->redirect(url("Question/editQuestion", ["idQ" => _post("idQ"), "error" => "true"]));
            } else if ($imageUp->checkFileSize($file) == false) {
                $_SESSION["messagesTitle"] = "خطا";
                $_SESSION["messagesText"] = "حجم تصویر انتخاب شده بیشتر از حد مجاز است!";
                $_SESSION["messagesType"] = "error";
                $this->redirect(url("Question/editQuestion", ["idQ" => _post("idQ"), "error" => "true"]));
            } else {
                $targetDir = "/imagesUpload/question/";
                $dir = DOC_ROOT . "/public/imagesUpload/question/";
                $imgUpload = $imageUp->uploadImage($targetDir, $file, $dir, _post("idQ"));
                $qM = new QuestionModel();
                $Q = $qM->updateImg(_post("idQ"), "Yes");
                $this->redirect(url("Question/editQuestion", ["idQ" => _post("idQ")]));
            }
        } else {
            $_SESSION["messagesTitle"] = "خطا";
            $_SESSION["messagesText"] = "تصویری انتخاب نشده است";
            $_SESSION["messagesType"] = "error";
            $this->redirect(url("Question/editQuestion", ["idQ" => _post("idQ"), "opId" => _post("id"), "error" => "true"]));
        }
    }
    public function deleteImg()
    {

        $qM = new QuestionModel();
        $Q = $qM->updateImg(_get("idQ"), "No");
        $this->redirect(url("Question/editQuestion", ["idQ" => _get("idQ")]));
    }
    public function updateByReviewer()
    {
        $projectAuth = new Auth();
        $projectId = $projectAuth->getProject()["id"];
        $question = new QuestionModel();
        $questionRow = $question->getQuetion($projectId, _get("rowNum"));
//        var_dump($questionRow);
        if (!empty($questionRow)) {
            $qModel = new QuestionModel();
            $questionType = $qModel->getQuestionTypeById($questionRow[0]["idQuestionType"])["text"];
            $answer = new answerModel();
            $getThisQuestionAnswer = $answer->getAnswer($questionRow[0]["id"], _get("idPorseshname"));
            $questionsCount = $this->getQuestionsCount($projectId);
            $questionsCount1 = $questionsCount[0];
            $idPorseshname = _get("idPorseshname");
            $this->view("question.updateByReviewer", compact("questionRow", "questionType", "getThisQuestionAnswer", "idPorseshname", "questionsCount"));
        }
    }
    public function reviewerAnswer()
    {
        // Update Porseshname Edit By Viwer
        $pM = new porseshnameModel();
        $pM->updatePorseshnameForEditedByViewer(_get("idPorseshname"), "Yes");
        $storeanswer = new answerModel();
        $storeanswer = new answerModel();
        $questionType = _post("questionType");
        $storeanswer->storeAnswer(_get("idPorseshname"), _post("answer"), _post("idQ"), _post("questionType"));
        $storeanswer->storeAnswer(_get("idPorseshname"), _post("answer"), _post("idQ"), _post("questionType"));
        $this->redirect(url("porseshname/manage", ["id" => _get("idPorseshname")]));
    }
    public function setPolicy()
    {
//        dd($_POST);
        $idPolicy = _post("idPolicy");//1=SocialClass;   2=Stop;   3=Count;
        $idQuestion = _post("idQuestion");//1=SocialClass;   2=Stop;   3=Count;
        $idOtherQ = _post("idOtherQ");
        $AuthC = new Auth();
        $idProject = $AuthC->getProject()["id"];
        $policyModel = new policy();
        $_SESSION["messagesTitle"] = "تراکنش موفق";
        $_SESSION["messagesText"] = "اختصاص سیاست";
        $_SESSION["messagesType"] = "info";
        $count = _post("count");
        $idOption = _post("idOption");
        //===============================================================================================================
        $JumpDestIdQ = _post("JumpDestIdQ");
//        dd($_POST);
        $policyModel->setPolicy($idPolicy, $idProject, $idQuestion, $idOption, $count, $idOtherQ, $JumpDestIdQ);
        $this->redirect(url("Question/manageOptions", ["idQ" => $idQuestion, "error" => "true", "collapsin" => _post("collapsin")]));
    }
    public function deletePolicy()
    {
        $idPolicy = _get("idPol");//1=SocialClass;   2=Stop;   3=Count;
        $idQuestion = _get("idQuestion");//1=SocialClass;   2=Stop;   3=Count;
//        dd($idPolicy);
        $AuthC = new Auth();
        $idProject = $AuthC->getProject()["id"];
        $policyModel = new policy();
        $PC = new PolicyClass();
        $idPolicySocial = $PC->getPolicySocial($idProject, $idQuestion)["id"];
//        DD($idPolicySocial);
        if ($idPolicy == 1) {
            $policyModel->deletePolicy($idPolicySocial);
//            dd($_GET);
            $this->redirect(url("Question/manageOptions", ["collapsin" => _get("collapsin"), "idQ" => $idQuestion]));
        }
        if ($idPolicy = 2) {
            $policyModel->deletePolicy(_get("id"));
            $this->redirect(url("Question/manageOptions", ["collapsin" => _get("collapsin"), "idQ" => $idQuestion]));
        }
        if ($idPolicy = 3) {

        }
    }
    public function jump()
    {
        $this->redirect(url("Question/loadQuestion", ["rowNum" => _post("rowNumberJamp")]));
    }
    public function qList()
    {
        $pInfo = new Auth();
        $projectInfo = $pInfo->getProject();
        $Question = new Question();
        $QuestionList = $Question->getQuestionList($projectInfo["id"]);
        $this->view("question.list", compact("QuestionList", "projectInfo"));
    }
    public function getRowForEdit()
    {
        $aut = new Auth();
        $acP = new AccessPrimision();
        if (($acP->checkQuestionForProject(_get("idQuestion"))) == false) {
            $this->redirect(url("login/logout", ["message" => "true"]));
        }
        $idQ = _get("idQuestion");
        $qModel = new QuestionModel();
        $qRow = $qModel->getById($idQ);
        $qM = new QuestionModel();
        $questiontypes = $qM->getQuestionTypeList();
//        dd($qRow);
        echo Response::ajax($qRow);
    }
    // Android Function getPolicySet
    public function androidGetQuestionsAndOptions()
    {
        $remember_Token = _post("remember_Token");
        $idProject = _post("idProject");
        $checkIsLogin = new Model();
        $isLogin = $checkIsLogin->_selectOne("*", "`remember_Token`='" . $remember_Token . "'", "users");
        if ($isLogin) {
            $question = new QuestionModel();
            $questionRows = $question->getQuestionList($idProject);
            if ($questionRows)
                echo Response::ajax($questionRows);
            else
                echo Response::ajax(["success" => true, "message" => "سوالی برای پروژه یافت نشد لطفا با مدیر پروژه تماس بگیرید!"]);
        } else {
            echo Response::ajax(["success" => false, "message" => "به دلایل امنیتی از نرم افزار خارح می شوید! لطفا دوباره وارد شوید"]);
        }
    }
    // Android Function getQuestionType
    public function androidGetQuestionsType()
    {
        $remember_Token = _post("remember_Token");
        $idProject = _post("idProject");
        $checkIsLogin = new Model();
        $isLogin = $checkIsLogin->_selectOne("*", "`remember_Token`='" . $remember_Token . "'", "users");
        if ($isLogin) {
            $question = new QuestionModel();
            $Rows = $question->getQuestionTypeList();
            if ($Rows)
                echo Response::ajax($Rows);
            else
                echo Response::ajax(["success" => true, "message" => "سوالی برای پروژه یافت نشد لطفا با مدیر پروژه تماس بگیرید!"]);
        } else {
            echo Response::ajax(["success" => false, "message" => "به دلایل امنیتی از نرم افزار خارح می شوید! لطفا دوباره وارد شوید"]);
        }
    }
}
