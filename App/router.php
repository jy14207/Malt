<?php

/**
 * Created by PhpStorm.
 * User: Jafari
 * Date: 06/11/2017
 * Time: 10:46 AM
 */
namespace App;
use App\Classes\Auth;
class Router
{
    function __construct()
    {
        $this->route();
    }

    function route()
    {
        $route=_get("r");
        if($route==null) $route="login/form";
        $route=explode("/",$route);
        if($route[0]=="api"){
            $controller=$route[1];
            $action=$route[2];
        }
        else{
            $controller=$route[0];
            $action=$route[1];
            if(($controller!="login") & ($controller!="admin")){
                Auth::redirectIfNotLogin();
            }
            if(($controller=="admin") & ($action!="adminLoginForm")){
                Auth::redirectIfNotLogin();
            }
        }

        $controller="App\\Controllers\\".$controller;
        $controller=new $controller();
        $controller->$action();
    }
}