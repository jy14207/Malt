<?php
/**
 * Created by PhpStorm.
 * User: Jafari
 * Date: 28/06/2018
 * Time: 05:46 PM
 */

namespace App\Classes;
use App\Models\answer;
use App\Models\policy;
use App\Models\options;
class PolicyCheck
{
    public function policyCheckForStop($row, $idOptionAnswer)
    {
        if ($row["idPolicy"] == "2") {
            if ($row["idOption"] == $idOptionAnswer) {
                return "StopPolicyIsTrue";
            }
        }
        if ($row["idPolicy"] == "3") {
            $policyCount = $row["count"];
            $answerCount = $this->getAnswerRowCountForThisOption($row["idQuestion"], $idOptionAnswer);
            if ($policyCount <= $answerCount[0]["CountID"]) {
                return "CountPolicyIsTrue";
            }
        }
        if ($row["idPolicy"] = "5") {
            if ($row["idOption"] == $idOptionAnswer) {
                return $row["JumpDestIdQ"];
            }
        }
    }
    private function getAnswerRowCountForThisOption($idQuestion, $idOption)
    {
        $ansM = new answer();
        return $ansM->getAnswerRowForcheckPolicyCount($idQuestion, $idOption);
    }
    public function calcSocialClass($idPorseshnameh, $idProject)
    {
        $socialName = "0";
        $socialVal = 0;
        $pM = new policy();
        $rows = $pM->getRowsForCalacSocialClass($idPorseshnameh);
        if ($rows != null) {
            foreach ($rows as $row) {
                $optionsOfRow = explode(",", $row["idOption"]);
                foreach ($optionsOfRow as $optionOfRow) {
                    $opM = new options();
                    $score = $opM->getById($optionOfRow)["score"];
                    $socialVal = $socialVal + $score;
                }
            }
            $socialClassForThisProjectrows = $pM->getSocialClass($idProject);
            foreach ($socialClassForThisProjectrows as $classForThisProjectrow) {
                if ($socialVal >= $classForThisProjectrow["SECScoreMin"] & $socialVal <= $classForThisProjectrow["SECScoreMax"]) {
                    $socialName = $classForThisProjectrow["SECName"] . " (" . $classForThisProjectrow["caption"] . ")";
                }
            }
        }
        return array("socialVal" => $socialVal, "socialName" => $socialName);
    }
    public function getPolicyansToOther($idPorseshname, $idQuestion)
    {
        $ansM = new answer();
        $rows = $ansM->getPolicyansToOther($idPorseshname, $idQuestion);
        $textPolicyansToOther = "";
        if ($rows) {
            foreach ($rows as $row) {
                $idOptionans = explode(",", $row["idOptionans"]);
                $opM = new options();
                foreach ($idOptionans as $idOptionan) {
                    $textOption = $opM->getTextOption($idOptionan)["textOption"];
                    $textPolicyansToOther = $textPolicyansToOther . "," . $textOption;
                }
            }
        }
//        dd($textPolicyansToOther);
        return $textPolicyansToOther;
    }
}