<?php
/**
 * Created by PhpStorm.
 * User: Jafari
 * Date: 03/10/2018
 * Time: 03:12 PM
 */

namespace App\Classes;
use App\Models\projects;
use App\Models\Question;
use App\Models\User;
use App\Models\userproject;
class AccessPrimision
{
    public function openPagePrimision()
    {
        //ساختار دسترسی به صفحات
    }
    public function allProject()
    {
        $aut = new Auth();
        $userType = $aut->getUser()["level"];
        $idCompany = $aut->getCompany()["id"];
        $userId = $aut->getUser()["id"];
        $up = new userproject();
        $pM = new projects();
        switch ($userType) {
            case "admin":
                return $pM->allProjects();
                break;
            case "companyAdmin":
                return $pM->companyAllProjects($idCompany);
                break;
            case "projectAdmin":
                return $up->adminPAllProjects($userId);
                break;
        }
    }
    public function activeProjects()
    {
        $aut = new Auth();
        $userType = $aut->getUser()["level"];
        $userId = $aut->getUser()["id"];
        switch ($userType) {
            case "admin":
                $aut = new Auth();
                $idCompany = $aut->getCompany()["id"];
                $projects = new projects();
                return $projects->activeProjects();
                break;
            case "companyAdmin":
                $aut = new Auth();
                $idCompany = $aut->getCompany()["id"];
                $projects = new projects();
                return $projects->companyActiveProjects($idCompany);
                break;
            case "projectAdmin":
                $activeP = new userproject();
                return $activeP->getUserProject($userId);
                break;
        }
    }
    public function getUsers()
    {
        $aut = new Auth();
        $userType = $aut->getUser()["level"];
        $idCompany = $aut->getCompany()["id"];
        $alluser = new User();
        switch ($userType) {
            case "admin":
                return $alluser->getUsers();
                break;
            case "companyAdmin":
                return $alluser->companyGetUsers($idCompany);
                break;
            case "projectAdmin":
                break;
        }
    }
    public function checkProjectAccess($idProject)
    {
        $aut = new Auth();
        $userType = $aut->getUser()["level"];
        switch ($userType) {
            case "admin":
                $projectM = new projects();
                $idProjectById = $projectM->getProjectById($idProject);
                if ($idProjectById) {
                    return true;
                } else {
                    $this->errorAlert();
                    return false;
                }
                break;
            case "companyAdmin":
                $aut = new Auth();
                $idCurrentCompany = $aut->getCompany()["id"];
                $projectM = new projects();
                $idProjectById = $projectM->getProjectById($idProject);
                if ($idProjectById["idCompany"] == $idCurrentCompany)
                    return true;
                else {
                    $this->errorAlert();
                    return false;
                }
                break;
            case "projectAdmin":
                $aut = new Auth();
                $idCurrentUser = $aut->getUser()["id"];
                $userProjectM = new userproject();
                $idUser = $aut->getUser()["id"];
                $getByIdUserAndIdProject = $userProjectM->getByIdUserAndIdProject($idUser, $idProject);
                if ($getByIdUserAndIdProject)
                    return true;
                else {
                    $this->errorAlert();
                    return false;
                }
                break;
        }
    }
    public function checkQuestionForProject($idQuestion)
    {
        $aut = new Auth();
        $userType = $aut->getUser()["level"];
        $idCurrentProject = $aut->getProject()["id"];
        $qM = new Question();
        $idp = $qM->getById($idQuestion)["idProject"];
        if ($idCurrentProject != $idp) {
            $this->errorAlert();
            return false;
        } else {
            return true;
        }
    }
    public function errorAlert()
    {
        $_SESSION["messagesTitle"] = "خطا";
        $_SESSION["messagesText"] = "به دلایل امنیتی شما به این صفحه ارجاع داده شده اید!لطفا مجددا وارد شوید.";
        $_SESSION["messagesType"] = "error";
    }
    public function checkUserIsCompany($idUser)
    {
        $aut = new Auth();
        $userType = $aut->getUser()["level"];
        $idCompany = $aut->getCompany()["id"];
        switch ($userType) {
            case "admin":
                return true;
                break;
            case "companyAdmin":
                $userM = new User();
                $idCompanyUser = $userM->getPorseshgarInf($idUser)["idCompany"];
                if ($idCompany == $idCompanyUser) {
                    return true;
                } else {
                    return false;
                }
                break;
            case "projectAdmin":
                return false;
                break;
        }
    }
    public function noName($userType)
    {

        switch ($userType) {
            case "admin":
                break;
            case "companyAdmin":
                break;
            case "projectAdmin":
                break;
        }
    }
}