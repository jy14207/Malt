<?php
/**
 * Created by PhpStorm.
 * User: Jafari
 * Date: 08/08/2018
 * Time: 12:59 PM
 */

namespace App\Classes;


class Timer
{
    private static $time = 0;
    private static $log=[];
    private static $counter=0;

    public static function counter(){
        return self::$counter++;
    }
    public static function start($restart=false)
    {
        if (self::$time == 0)
            self::$time = microtime(true);
        else if($restart){
            self::$log=[];
            self::$time = microtime(true);
        }

    }

    public static function checkPoint($name = "noName")
    {

        $time=(microtime(true)-self::$time);
        self::$log[]=["name"=>$name,"time"=>$time];
        self::$time=microtime(true);
        return $time;
    }

    public static function getLog(){
        return self::$log;
    }
}