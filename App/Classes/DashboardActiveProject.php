<?php
/**
 * Created by PhpStorm.
 * User: Jafari
 * Date: 23/06/2018
 * Time: 05:46 PM
 */

namespace App\Classes;

use  App\Models\projects;
use App\Models\porseshname;
use App\Models\userproject;
use Symfony\Component\Validator\Constraints\Count;
use App\Models\User;

class DashboardActiveProject
{
    public function datsboardTableArray($idCompany)
    {
        $aut=new Auth();
        $userLevel=$aut->getUser()["level"];
        $accessPrim=new AccessPrimision();
//        $AllActiveprojects =
//        $pM = new projects();

        $projectLists = $accessPrim->activeProjects($userLevel);

        $dashboardDataArray = array();
        foreach ($projectLists as $index => $projectList) {
            $dashboardDataArray[$index]["id"] = $projectList["id"];
            $dashboardDataArray[$index]["subject"] = $projectList["subject"];
            $dashboardDataArray[$index]["startDate"] = $projectList["startDate"];

            $dashboardDataArray[$index]["totalPorseshnameh"] = $projectList["pasokhgooNumber"];

            $pModel = new porseshname();
            $finishPorseshnamehCount = count($pModel->getFinishedCount($projectList["id"]));
            $dashboardDataArray[$index]["finishPorseshnamehCount"] = $finishPorseshnamehCount;

            $reviewPorseshnamehCount = count($pModel->getReviewCount($projectList["id"]));
            $dashboardDataArray[$index]["reviewPorseshnamehCount"] = $reviewPorseshnamehCount;

            $uM = new userproject();
            $porseshgarCount = count($uM->getListPorseshgarProject($projectList["id"]));
            $dashboardDataArray[$index]["porseshgarCount"] = $porseshgarCount;

            $projectPercent = round((($finishPorseshnamehCount * 100) / $projectList["pasokhgooNumber"]), 2);
            $dashboardDataArray[$index]["projectPercent"] = $projectPercent;

            if ($projectPercent < 25) {
                $dashboardDataArray[$index]["barType"] = "danger";
            }
            if ($projectPercent >= 25 & $projectPercent < 50) {
                $dashboardDataArray[$index]["barType"] = "warning";
            }
            if ($projectPercent >= 50 & $projectPercent < 75) {
                $dashboardDataArray[$index]["barType"] = "info";
            }

            if ($projectPercent > 75) {
                $dashboardDataArray[$index]["barType"] = "success";
            }

        }
        return $dashboardDataArray;

    }
    public function getCompanyUsers($idCompany)
    {
        $uM=new User();
        return $uM->getTopUserRandom($idCompany);

    }

}