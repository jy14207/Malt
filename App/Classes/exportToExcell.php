<?php
/**
 * Created by PhpStorm.
 * User: Jafari
 * Date: 03/02/2018
 * Time: 10:47 PM
 */

namespace App\Classes;
use Port\Csv\CsvReader;
use Port\Csv\CsvWriter;

//use Port\Doctrine\DoctrineWriter;



class exportToExcell
{
    public function exportToExcell($rows)
    {

        $writer = new CsvWriter();
        $writer->setStream(fopen('output.csv', 'w'));
        $writer->writeItem(['آی دی','آی دی پروژه','ردیف سوال','ای دی نوع سوال','متن سوال','اجباری /اختیاری']);
        foreach ($rows as $row)
        {
            $writer->writeItem([$row["id"], $row["idProject"],$row["rowNumber"],$row["idQuestionType"], $row["text"],$row["allowNull"]]);
        }
        $writer->writeItem(['first', 'last']);

// Write some data
        $writer->writeItem(['James', 'Bond']);
        $writer->writeItem(['Auric', 'Goldfinger']);
        $writer->finish();
    }
}