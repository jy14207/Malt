<?php
/**
 * Created by PhpStorm.
 * User: Jafari
 * Date: 10/03/2018
 * Time: 08:54 PM
 */

namespace App\Classes;
use function PHPSTORM_META\type;
class imageUploader
{
    public function uploadImage($target_dir, $file, $dir, $id)
    {
//        dd("uploadImage");

//        var_dump("تا اینجا");exit;
        $filename = $id . "." . pathinfo(basename($file["name"]), PATHINFO_EXTENSION);
        $target_file = $target_dir . $filename;
//        dd($target_file);
        $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
        if ($this->allowCertainFileFormat($imageFileType)) {
            $this->existImage($target_file);
            $this->doUpload($file, $dir, $id);
        }
    }
    public function checkFileIsaActualImage($file)
    {
        // Check if image file is a actual image or fake image
        $check = getimagesize($file["tmp_name"]);
        if ($check == false) {
            return false;
        } else {
            return true;
        }
    }
    public function checkFileSize($file, $maxFilesizeMB = null)
    {
        // Check file size
        if ($maxFilesizeMB == null)
        {
            $maxFilesizeMB = 2000000;
        }
        else
            $maxFilesizeMB = ($maxFilesizeMB * 1024 * 1024);
        if ($file["size"] > $maxFilesizeMB) {
            return false;
        } else {
            return true;
        }
    }
    public function allowCertainFileFormat($imageFileType)
    {
        // Allow certain file formats
        if ($imageFileType != "jpg" && $imageFileType != "jpeg" && $imageFileType != "mp3") {
//            dd("inha");
            return false;
        } else {
            return true;
        }
    }
    public function existImage($fileName)
    {
        // Check if file already exists
        if (file_exists($fileName)) {
            unlink($fileName);
            return false;
        } else {
            return true;
        }
    }
    public function doUpload($file, $dir, $id)
    {
        $file["name"] = $id;
        $extention=(substr(strrchr($file["type"],'/'),1));
//        var_dump($file["type"]);
//        dd($extention);
        if (move_uploaded_file($file["tmp_name"], $dir . $id . ".".$extention)) {
            return true;
        } else {
            return false;
        }
    }
}