<?php
/**
 * Created by PhpStorm.
 * User: Jafari
 * Date: 21/02/2018
 * Time: 03:09 PM
 */

namespace App\Classes;

use App\Models\options as optionModel;
use App\Models\User as userModel;
use App\Models\pasokhgoo as pasokhgooModel;
use App\Models\answer as answerM;
use App\Models\Question;
use \Gurukami\Helpers\Arrays;
use Symfony\Component\Debug\Debug;
use Symfony\Component\Validator\Constraints\Time;

class DatabaseTemplateReport
{
    public $keybyIdAllOption = [];
    public $keyByIdAllAnswers = [];
    public $keyByAllQuestionsOfThisProject = [];

    public function createQuestionsFeildsArray($ProjectByid, $AllQuestionsOfThisProject)
    {
        $finalArray = array(
            "id" => "آی دی",
            "idP" => "آی دی پروژه",
            "idPorseshname" => "آی دی پرسشنامه",
            "porseshnamehNumber" => "شماره پرسشنامه",
            "completDate" => "تاریخ تکمیل",
            "startTime" => "ساعت شروع",
            "endTime" => "ساعت پایان",
            "pasokhgooScore" => "جمع امتیاز پاسخگو",
            "socialClass" => "طبقه اجتماعی",
            "idPorseshgar" => "آی دی پرسشگر",
            "porseshgarName" => "نام پرسشگر",
            "porseshgarfName" => "نام خانوادگی پرسشگر",
            "idPasokhgoo" => "آی دی پاسخگو",
            "pasokhgooName" => "نام پاسخگو",
            "pasokhgooFname" => "نام خانوادی پاسخگو",
            "homeAddress" => "آدرس منزل",
            "workAddress" => "آدرس محل کار",
            "cityName" => "نام شهر",
            "cityArea" => "منطقه شهرداری",
            "phoneNumber" => "تلفن",
            "mobileNumber" => "تلفن همراه",

        );

        $idP = $ProjectByid["id"];
        $fieldName = "";
        foreach ($AllQuestionsOfThisProject as $QuestionsOfThisProject) {
            $idQuestionType = $QuestionsOfThisProject["idQuestionType"];
//            var_dump($idQuestionType);
            switch ($idQuestionType) {
                case 1:     //singleResponse تک پاسخی
                    $fieldName = "singleResponse_" . $QuestionsOfThisProject["id"] . "_AnsQ_" . $QuestionsOfThisProject["rowNumber"];
                    $finalArray[$fieldName] = $fieldName;
                    break;
                case 2:     //multipleChoice چند پاسخی
//                case 11:    // prioritize اولویت دهی
                    $options = new optionModel();
                    $getthisQuestionOptions = $options->getThisQuestionOptionsf($QuestionsOfThisProject["id"]);
                    foreach ($getthisQuestionOptions as $getthisQuestionOption) {
                        $fieldName = "multipleChoice_" . $QuestionsOfThisProject["id"] . "_" . $getthisQuestionOption["id"] . "_AnsQ_" . $QuestionsOfThisProject["rowNumber"];
                        $finalArray[$fieldName] = $fieldName;
                    }
//                    dd($fieldName);
                    break;
                case 3:     //openQuestionتشریحی
                case 6:     //numeric عددی
                case 7:     //spectral طیفی
                case 8:     //Email ایمیل
                case 9:     //degree درجه بندی
                case 10:     //websitelink وبسایت
//                    $fieldName = "openQuestion_" . $QuestionsOfThisProject["id"] . "_AnsQ_" . $QuestionsOfThisProject["rowNumber"];
//                    $finalArray[$fieldName] = '0';
                    $fieldNameTash = "openQuestion_" . $QuestionsOfThisProject["id"] . "_AnsQ_" . $QuestionsOfThisProject["rowNumber"] . "_OpenQ";
                    $finalArray[$fieldNameTash] = $fieldNameTash;

                    break;
                case 4:     //singleResponseOthersتک پاسخی با سایر
                    $fieldName = "singleResponseOthers_" . $QuestionsOfThisProject["id"] . "_AnsQ_" . $QuestionsOfThisProject["rowNumber"];
                    $finalArray[$fieldName] = $fieldName;
                    $fieldNameSayer = "singleResponseOthers_" . $QuestionsOfThisProject["id"] . "_AnsQ_" . $QuestionsOfThisProject["rowNumber"] . "_Sayer";
                    $finalArray[$fieldNameSayer] = $fieldNameSayer;

                    break;
                case 5:    //multipleChoiceOthersچند پاسخی با سایر
                    $options = new optionModel();
                    $getthisQuestionOptions = $options->getThisQuestionOptionsf($QuestionsOfThisProject["id"]);
                    foreach ($getthisQuestionOptions as $getthisQuestionOption) {
                        $fieldName = "multipleChoiceOthers_" . $QuestionsOfThisProject["id"] . "_" . $getthisQuestionOption["id"] . "_AnsQ_" . $QuestionsOfThisProject["rowNumber"];
                        $finalArray[$fieldName] = $fieldName;
                    }
                    $fieldName = "multipleChoiceOthers_" . $QuestionsOfThisProject["id"] . "_" . $getthisQuestionOption["id"] . "_AnsQ_" . $QuestionsOfThisProject["rowNumber"] . "_Sayer";
                    $finalArray[$fieldName] = $fieldName;
                    break;
                case 11://degree دزجه بندی
                    $fieldNameTash = "degree_" . $QuestionsOfThisProject["id"] . "_AnsQ_" . $QuestionsOfThisProject["rowNumber"];
//                    var_dump($fieldNameTash);
                    $finalArray[$fieldNameTash] = $fieldNameTash;
                    break;
                case 12:
                    break;
                case 13:     //openOption وبسایت
                    $fieldNameTash = "openOption_" . $QuestionsOfThisProject["id"] . "_AnsQ_" . $QuestionsOfThisProject["rowNumber"] ;
                    $finalArray[$fieldNameTash] = $fieldNameTash;
                    $fieldNameTash = "openOption_" . $QuestionsOfThisProject["id"] . "_AnsQ_" . $QuestionsOfThisProject["rowNumber"] . "_OpenQ";
                    $finalArray[$fieldNameTash] = $fieldNameTash;
                    break;
            }
        }
        return $finalArray;

    }

    public function fillDatabaseTemplateArray($idP, $QuestionsFeildsArray, $allporseshnameOfThisProject, $allporseshgar, $allPasokhgooOfThisProject, $allAnswerOfThisProject, $allOptionOfThisProject, $allQuestionsOfThisProject)
    {

        $keyByAllPasokhgoo = [];
        foreach ($allPasokhgooOfThisProject as $p) {
            $keyByAllPasokhgoo[$p["idPorseshname"]] = $p;
        }

        foreach ($allOptionOfThisProject as $option) {
            $this->keybyIdAllOption[$option["id"]] = $option;
        }
        foreach ($allAnswerOfThisProject as $answer) {
            $this->keyByIdAllAnswers[$answer["id"]] = $answer;
        }
        foreach ($allQuestionsOfThisProject as $questionsOfThisProject) {
            $this->keyByAllQuestionsOfThisProject[$questionsOfThisProject["id"]] = $questionsOfThisProject;
        }
        $countPorseshname = count($allporseshnameOfThisProject);
        $QuestionsFeildsArray_temp = $QuestionsFeildsArray;

        $QuestionsFeildsArray = array();
        $QuestionsFeildsArray["template"] = $QuestionsFeildsArray_temp;
        $policyCheck = new PolicyCheck();
//Timer::getLog("BeforGetAnswer");
        foreach ($allporseshnameOfThisProject as $i => $thisPorseshnameOfThisProject) {
            $socialClass=$policyCheck->calcSocialClass($thisPorseshnameOfThisProject["id"],$idP);
            $idPorseshnameh = $thisPorseshnameOfThisProject["id"];
            $QuestionsFeildsArray[$i + 1]["id"] = $i;
            $QuestionsFeildsArray[$i + 1]["idP"] = $idP;
            $QuestionsFeildsArray[$i + 1]["idPorseshname"] = $thisPorseshnameOfThisProject["id"];
            $QuestionsFeildsArray[$i + 1]["porseshnamehNumber"] = $thisPorseshnameOfThisProject["porseshnamehNumber"];
            $QuestionsFeildsArray[$i + 1]["completedDate"] = $thisPorseshnameOfThisProject["completedDate"];
            $QuestionsFeildsArray[$i + 1]["startTime"] = $thisPorseshnameOfThisProject["startTime"];
            $QuestionsFeildsArray[$i + 1]["endTime"] = $thisPorseshnameOfThisProject["endTime"];
            $QuestionsFeildsArray[$i + 1]["pasokhgooScore"] =  $socialClass["socialVal"];
            $QuestionsFeildsArray[$i + 1]["socialClass"] = $socialClass["socialName"];;
            $QuestionsFeildsArray[$i + 1]["idPorseshgar"] = $thisPorseshnameOfThisProject["idPorseshgar"];

            // Get Porseshgar Infp by idPorseshgar
            $porseshgarInf = $this->searchInArray($allporseshgar, "id", $thisPorseshnameOfThisProject["idPorseshgar"]);
            $QuestionsFeildsArray[$i + 1]["porseshgarName"] = $porseshgarInf["name"];
            $QuestionsFeildsArray[$i + 1]["porseshgarfName"] = $porseshgarInf["fName"];

            // GET PASOKHGOO IN BY ID PASOKHGOO
            $pasokhgooInf = $this->searchInArray($allPasokhgooOfThisProject, "idPorseshname", $thisPorseshnameOfThisProject["id"]);
            $QuestionsFeildsArray[$i + 1]["idPasokhgoo"] = $pasokhgooInf["id"];// $pasokhgooInf["id"];
            $QuestionsFeildsArray[$i + 1]["pasokhgooName"] = $pasokhgooInf["name"];
            $QuestionsFeildsArray[$i + 1]["pasokhgooFname"] = $pasokhgooInf["fName"];
            $QuestionsFeildsArray[$i + 1]["'homeAddress"] = $pasokhgooInf["homeAddress"];
            $QuestionsFeildsArray[$i + 1]["workAddress"] = $pasokhgooInf["workAddress"];
            $QuestionsFeildsArray[$i + 1]["cityName"] = $pasokhgooInf["cityName"];
            $QuestionsFeildsArray[$i + 1]["cityArea"] = $pasokhgooInf["cityArea"];
            $QuestionsFeildsArray[$i + 1]["phoneNumber"] = $pasokhgooInf["phoneNumber"];
            $QuestionsFeildsArray[$i + 1]["mobileNumber"] = $pasokhgooInf["mobileNumber"];

            $questionCol = false;
//            dd("فقط پاسخگو ها");
            foreach ($QuestionsFeildsArray["template"] as $k => $v) {
                if ($questionCol == true) {
                    $ans = $this->getAnsForThisQ($k, $thisPorseshnameOfThisProject["id"], $allAnswerOfThisProject, $allOptionOfThisProject);
                    if ($ans == null) {
                        $QuestionsFeildsArray[$i + 1][$k] = 0;
                    } else {
                        $QuestionsFeildsArray[$i + 1][$k] = $ans;
                    }
                }
                if ($k == "mobileNumber") {
                    $questionCol = true;
                }
            }
        }
        Timer::getLog("AfterGetAnswer");
//        dd(Timer::getLog());

// GET QUESTIONS CODE
        $i = 0;
        foreach ($QuestionsFeildsArray["template"] as $key => $val) {
            if ($i > 20) {
                $colTitle = explode("_", $key);
                $idQuestion = $colTitle[1];
                $qCode = $this->keyByAllQuestionsOfThisProject[$idQuestion]["questionCode"];
//                dd($qCode);
                if ($colTitle[0] == "multipleChoiceOthers" | $colTitle[0] == "multipleChoice") {
                    $questionCode = $qCode;
                    $idOption = $colTitle[2];
                    $opScore = $this->getTextOptionS($idOption)["score"];
//                    dd($opScore);
                    if ($opScore[0] == "999") $opScore[0] = "Other";
                    $QuestionsFeildsArray["template"][$val] = $questionCode . "_" . $opScore;
                } else {
                    $questionCode = $qCode;
                    $QuestionsFeildsArray["template"][$val] = $questionCode;
                }
            }
            $i++;
        }

        return ($QuestionsFeildsArray);
    }

    private function getAnsForThisQ($colKeyName, $idporseshname, $allAnswerOfThisProject, $allOptionOfThisProject)
    {
        $ketType = explode("_", $colKeyName);
//        var_dump($ketType);
        switch ($ketType[0]) {
            case "singleResponse":
                $row = $this->getAnswerForReport($ketType[1], $idporseshname, $allAnswerOfThisProject);
                return ($this->getTextOptionS($row["idOption"])["textOption"]);

                break;
            case "multipleChoice":
                $row = $this->getAnswerForReport($ketType[1], $idporseshname, $allAnswerOfThisProject);
                $idOptInColKeyName = $ketType[2];
                $idoptionsAnswerTable = explode(",", $row["idOption"]);
                foreach ($idoptionsAnswerTable as $idoptionsAnswerTableone) {
                    if ($idoptionsAnswerTableone == $idOptInColKeyName) {
                        return $this->getTextOptionS($idOptInColKeyName)["textOption"];
                    }
                }
                break;
            case "openQuestion":
                if (count($ketType) == 4) {
                    $row = $this->getAnswerForReport($ketType[1], $idporseshname, $allAnswerOfThisProject);
                    return $this->getTextOptionS($row["idOption"]);
                }
                if (count($ketType) == 5) {
                    return $this->getAnswerForReport($ketType[1], $idporseshname, $allAnswerOfThisProject)["text"];
                }
                break;
            case "singleResponseOthers":
                $thisQans = new answerM();
                if (count($ketType) == 4) {
                    $row = $this->getAnswerForReport($ketType[1], $idporseshname, $allAnswerOfThisProject);
                    return $this->getTextOptionS($row["idOption"])["textOption"];
                }
                if (count($ketType) == 5) {
                    $row = $this->getAnswerForReport($ketType[1], $idporseshname, $allAnswerOfThisProject);
                    return $row["text"];
                }
                break;
            case "multipleChoiceOthers":
                $thisQans = new answerM();
                if (count($ketType) == 5) {
                    $row = $this->getAnswerForReport($ketType[1], $idporseshname, $allAnswerOfThisProject);
                    $idOptInColKeyName = $ketType[2];
                    $idoptionsAnswerTable = explode(",", $row["idOption"]);
                    foreach ($idoptionsAnswerTable as $idoptionsAnswerTableone) {
                        if ($idoptionsAnswerTableone == $idOptInColKeyName) {
                            return $this->getTextOptionS($idOptInColKeyName)["textOption"];
                        }
                    }
                }

                if (count($ketType) == 6) {
                    $row = $this->getAnswerForReport($ketType[1], $idporseshname, $allAnswerOfThisProject);
                    $idOptInColKeyName = $ketType[2];
                    $idoptionsAnswerTable = explode(",", $row["idOption"]);
                    foreach ($idoptionsAnswerTable as $idoptionsAnswerTableone) {
                        if ($idoptionsAnswerTableone == $idOptInColKeyName) {
                            if ($this->getTextOptionS($idOptInColKeyName)["textOption"] == "Other") {
                                return $row["text"];
                            } else {
                                return $this->getTextOptionS($idOptInColKeyName)["textOption"];
                            }
                        }
                    }
                }
                break;
            case "degree":
//                dd($ketType);
                $row = $this->getAnswerForReport($ketType[1], $idporseshname, $allAnswerOfThisProject);
                $idOptions = explode(',', $row["idOption"]);
                $textOptions = '';
                foreach ($idOptions as $idOption) {
                    $textOptions = $textOptions . $this->getTextOptionS($idOption)["textOption"] . ",";
//                    var_dump($textOptions);
                }
//                dd($textOptions);
                return $textOptions;// $this->getTextOptionS($row["idOption"]);
                break;
            case "openOption":
                if (count($ketType) == 4) {
                    $row = $this->getAnswerForReport($ketType[1], $idporseshname, $allAnswerOfThisProject);
                     $idOptions=explode(",",$row["idOption"]);
//                     dd($idOptions);
                     $ans=null;
                     foreach ($idOptions as $idOption)
                     {
                         $ans=$ans.','.$this->getTextOptionS($idOption)["textOption"];
                     }
                    return $ans;
                }
                if (count($ketType) == 5) {
                    return $this->getAnswerForReport($ketType[1], $idporseshname, $allAnswerOfThisProject)["text"];
                }
                break;
        }
    }

    private function searchInArray($arrayName, $fieldName, $valForSearch)
    {
        foreach ($arrayName as $thisArrayName) {
            if ($thisArrayName[$fieldName] == $valForSearch) return $thisArrayName;
        }
        return null;
    }

    private function getAnswerForReport($idquestion, $idporseshname, $allAnswerOfThisProject)
    {
//        Timer::start();
//        foreach ($allAnswerOfThisProject as $AnswerOfThisProject) {
//            if ($AnswerOfThisProject["idPorseshname"] == $idporseshname && $AnswerOfThisProject["idQuestion"] == $idquestion) {
//                return $AnswerOfThisProject;
//            }
//        }
        $ansN=new answerM();
        return $ansN->getAnswer($idquestion, $idporseshname);
//        Timer::checkPoint("check");
//        dd(Timer::getLog());
        return null;
    }

    private function getTextOptionS($idOption)
    {
        $result = Arrays::exists($idOption, $this->keybyIdAllOption);
        if ($result) {
            return $this->keybyIdAllOption[$idOption];

        }
    }

    private function getTextOptionQCode($idOption, $allOptionOfThisProject)
    {
        $result = Arrays::exists($idOption, $allOptionOfThisProject);
        if ($result) {
            return $this->keybyIdAllOption[$idOption];

        }
    }
}

