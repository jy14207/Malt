<?php
/**
 * Created by PhpStorm.
 * User: Jafari
 * Date: 10/12/2017
 * Time: 08:34 AM
 */

namespace App\Classes;


class Response
{
    public static function ajax($data)
    {
        header('Content-type: text/json');
        header('Content-type: application/json');
        return (json_encode($data));

    }
}