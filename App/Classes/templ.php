<?php
/**
 * Created by PhpStorm.
 * User: Jafari
 * Date: 06/11/2017
 * Time: 10:44 AM
 */
namespace App\Classes;
use Jenssegers\Blade\Blade;

class templ
{
    static function view($view, $params = null)
    {
        if (!is_array($params))
            $params = array();
        $blade = new Blade(DOC_ROOT . '/views', DOC_ROOT . '/cache');
        echo $blade->make($view, $params);
    }
}