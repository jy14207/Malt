<?php
/**
 * Created by PhpStorm.
 * User: Jafari
 * Date: 20/05/2018
 * Time: 02:51 PM
 */

namespace App\Classes;

use App\Models\policy as policyM;

class Policy
{
    public static function getPolicySocial($idProject, $idQuestion)
    {
        $pM = new policyM();
        return $pM->getPolicy("1",$idProject,$idQuestion);

    }
    public static function getPolicyStop($idProject, $idQuestion)
    {
        $pM = new policyM();
        return $pM->getPolicyStop("2",$idProject,$idQuestion);

    }
    public static function getPolicyCount($idProject, $idQuestion)
    {
        $pM = new policyM();
        return $pM->getPolicyStop("3",$idProject,$idQuestion);

    }
    public static function getPolicyJump($idProject, $idQuestion)
    {
        $pM = new policyM();
        return $pM->getPolicyjump("5",$idProject,$idQuestion);

    }
    public static function getPolicyansToOtherQPol($idProject,$idQuestion)
    {
        $pM = new policyM();
        return $pM->getPolicyansToOtherQPol("4",$idProject,$idQuestion);

    }

}