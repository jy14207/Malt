<?php
/**
 * Created by PhpStorm.
 * User: Jafari
 * Date: 04/08/2018
 * Time: 01:37 PM
 */

namespace App\Classes;

use App\Models\Model;


class RowNumberManagment
{
    public function shift($tableName, $fieldName,$minValue, $method,$idProject)
    {
        $m=new Model();
        $m->shiftUpOrDown($tableName, $fieldName,$minValue,$method,$idProject);
    }
}