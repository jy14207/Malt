<?php
/**
 * Created by PhpStorm.
 * User: Jafari
 * Date: 07/11/2017
 * Time: 10:55 AM
 */

namespace App\Classes;

use App\Controllers\BaseController as BaseController;
use App\Models\Model As Model;
use App\Models\porseshname;
use App\Models\User;
use App\Models\Company;
use App\Models\projects;

class Auth
{
    public static function attemp($username, $password)
    {
        $user = new User();

        $userInfo = $user->check($username, $password);
        if (!empty($userInfo)) {
            self::login($userInfo);
            $userInfo = $user->check($username, $password);
            return $userInfo;
        }
        return false;
    }

    private static function login($userInfo)
    {
        $remember_Token = self::create_token();
        $updateUserInfo = new User();
        $updateUserInfo->_update(array("remember_Token" => $remember_Token), $userInfo["id"]);
        $_SESSION["user_token"] = $remember_Token;
        return true;
    }

    public static function redirectIfNotLogin()
    {
//        return true;
        if(isset($_SESSION["user_token"]))
        {
            $checkIsLogin= new Model();
            $isLogin=$checkIsLogin->_selectOne("*","`remember_Token`='".$_SESSION["user_token"]."'","users");
        }
        else
        {
            $BaseController0=new BaseController();
            $_SESSION["messagesTitle"] = "خروج اضطراری";
            $_SESSION["messagesText"] = "به دلایل امنیتی به این صفحه هدایت شده اید،دلیل : طولانی شدن زمان ارسال درخواست به سرور!";
            $_SESSION["messagesType"] = "error";
            $BaseController0->redirect(url("login/logout"));
        }
//        var_dump($isLogin);
        if( $_SESSION["user_token"]!=$isLogin["remember_Token"])
        {
            $_SESSION["messagesTitle"]="خروج اضطراری";
            $_SESSION["messagesText"]="به دلایل امنیتی هویت شما مورد تائید نمی باشد!";
            $_SESSION["messagesType"]="error";
            $BaseController0=new BaseController();
            $BaseController0->redirect(url("login/logout"));
        }
    }

    public static $userInfo,$projectInfo,$companyInfo, $porseshnamehInfo;

    public static function getUser()
    {
        if(!isset($_SESSION["user_token"]))
        return null;
        if (!empty(self::$userInfo))
            return self::$userInfo;
        $user = new User();
        self::$userInfo = $user->getAuthenticatedUser();
        return self::$userInfo;
    }
    public static function getCompany()
    {
        if(!isset($_SESSION["idCompany"]))
            return null;
        if(!empty(self::$companyInfo))
            return self::$companyInfo;
        $company=new Company();
        self::$companyInfo=$company->getCompany($_SESSION["idCompany"]);
        return self::$companyInfo;
    }
    public static function getProject()
    {
//        dd($_SESSION["idProject"]);
        if(!isset($_SESSION["idProject"]))
            return null;
        if(!empty(self::$projectInfo))
            return self::$projectInfo;
        $project=new projects();
        self::$projectInfo=$project->getProjectById($_SESSION["idProject"]);

        return self::$projectInfo;
    }
    public static function getPorseshname()
    {
        if(!isset($_SESSION["idPorseshname"]))
            return null;
        if(!empty(self::$porseshnamehInfo))
            return self::$porseshnamehInfo;
        $idProject=self::getProject()["id"];
        $porseshname=new porseshname();
        self::$porseshnamehInfo=$porseshname->getOnePorseshname($_SESSION["idPorseshname"],$idProject);
        return self::$porseshnamehInfo;
    }
    protected static function create_token()
    {
        return $token = bin2hex(openssl_random_pseudo_bytes(32));
    }


}