@extends("AdminLteMaster")
@section("content")
    <title>مدیریت کاربران</title>
    <strong>
        @if(isset($error))
            @include("messages.messages")
        @endif
    </strong>
    <div id="spin"></div>
    <div class="row padding5 ">
        <div class="panel panel-primary ">
            <div id="collapse1" class="panel-collapse collapse in">
                <div class="panel-body">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">تعریف کاربر</a>
                                </h4>
                            </div>
                            <div id="collapse3" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <form class="form" action="{{url("Users/insertUser")}}" method="post">
                                        <div class="row">
                                            <div class="col-md-8 col-md-offset-2">
                                                <label class="align-right">نوع کاربر :</label>
                                                <select name="level" class="form-control questionTypeSelect nopadding"
                                                        id="level">
                                                    <option value="porseshgar">پرسشگر</option>
                                                    <option value="projectAdmin">مدیر پروژه</option>
                                                    {{--<option value="admin">مدیر سامانه</option>--}}
                                                </select>
                                            </div>
                                        </div>
                                        <hr/>
                                        <div class="row">
                                            <div class="col-md-3 col-md-offset-2">
                                                <label>نام:</label>
                                                <input type="text" id="name" class="form-control required" alt="نام"
                                                       name="name"/>
                                            </div>
                                            <div class="col-md-3 col-md-offset-2">
                                                <label>نام خانوادگی:</label>
                                                <input type="text" id="fName" class="form-control required"
                                                       alt="نام خانوادگی" name="fName"/>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 col-md-offset-2" id="staticParent">
                                                <label>کد ملی:</label>
                                                <input type="text" minlength="10" maxlength="10" id="nationalCode"
                                                       class="form-control required" alt="کد ملی" name="nationalCode"/>
                                            </div>
                                            <div class="col-md-3 col-md-offset-2">
                                                <label>آدرس محل سکونت:</label>
                                                <input type="text" id="homeAdd" class="form-control required"
                                                       alt="آدرس محل سکونت" name="homeAdd"/>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 col-md-offset-2">
                                                <label>نام کاربری:</label>
                                                <input type="text" id="userName" class="form-control required userName"
                                                       alt="نام کاربری" name="userName"/>
                                            </div>
                                            <div id="register" class="col-md-3 col-md-offset-2">
                                                <label>گذر واژه:</label> <span  id="result"></span>
                                                <input type="text" id="password" class="form-control required"
                                                       alt="گذرواژه" name="password"/>
                                            </div>

                                        </div>
                                        <div class="row margintop5">
                                            <div class="col-md-2 col-md-offset-10 ">
                                                <input type="submit" id="btnInsert" class="btn-success btn-lg"
                                                       value="ثبت"/>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">لیست
                                        کاربران</a>
                                </h4>
                            </div>
                            <div id="collapse2" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <table class="table table-bordered margintop5">
                                        <thead>
                                        <tr>
                                            <th class="text-center hidden-xs">ردیف</th>
                                            <th class="text-center ">نوع کاربر</th>
                                            <th class="text-center">نام</th>
                                            <th class="text-center">نام خانوادگی</th>
                                            <th class="text-center hidden-xs">نام کاربری</th>
                                            {{--<th class="text-center hidden-xs">تصویر پروفایل</th>--}}
                                            <th class="text-center hidden-xs"> وضعیت</th>
                                            <th class="text-center hidden-xs">ویرایش</th>
                                            {{--<th class="text-center">حذف</th>--}}
                                            <th class="text-center">بیشتر</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($companyUsers as $companyUser)
                                            <tr>
                                                <td class="text-center hidden-xs">{{$loop->iteration }}</td>
                                                <td class="text-center">
                                                    @if($companyUser["level"]=="porseshgar")
                                                        {{"پرسشگر"}}
                                                    @endif
                                                    @if($companyUser["level"]=="companyAdmin")
                                                        {{"مدیر"}}
                                                    @endif
                                                    @if($companyUser["level"]=="projectAdmin")
                                                        {{"مدیر پروژه"}}
                                                    @endif
                                                </td>
                                                <td class="text-center">{{$companyUser["name"]}}</td>
                                                <td class="text-center">{{$companyUser["fName"]}}</td>
                                                <td class="text-center hidden-xs">{{$companyUser["username"]}}</td>
                                                {{--<td class="hidden-xs">--}}
                                                    {{--<div id="myModal" class="modal">--}}
                                                        {{--<span class="close">&times;</span>--}}
                                                        {{--<img class="modal-content" id="img01">--}}
                                                        {{--<div id="caption"></div>--}}
                                                    {{--</div>--}}
                                                    {{--<img alt="تصویر کاربر" class="myImg" id="{{$companyUser["id"]}}"--}}
                                                         {{--src="{{public_url("/imagesUpload/Profiles/").$companyUser["id"].".jpg"}}" onerror="this.onerror=null;this.src='{{public_url("/imagesUpload/Profiles/Def.jpg")}}';">--}}
                                                {{--</td>--}}
                                                <td class="text-center handcursor hidden-xs">
                                                    @if($companyUser["level"]!="admin")
                                                        @if($companyUser["status"]=="Active")
                                                            <a href="{{url("Users/ChangeState",["id"=>$companyUser["id"]])}}">
                                                                <img title=" فعال/برای غیرفعال کردن کلیک کنید"
                                                                     src="{{public_url("./images/active.png")}}"/>
                                                            </a>
                                                        @endif
                                                        @if($companyUser["status"]=="inActive")

                                                            <a href="{{url("Users/ChangeState",["id"=>$companyUser["id"]])}}">
                                                                <img title="غیر فعال/برای فعال کردن کلیک کنید"
                                                                     src="{{public_url("./images/inactive.png")}}"/>
                                                            </a>
                                                        @endif
                                                    @endif
                                                    @if($companyUser["level"]=="admin")
                                                        <a onclick="swal('خطا','کاربر مدیر را نمی توان غیر فعال کرد','error')">
                                                            <img title=" فعال/برای غیرفعال کردن کلیک کنید"
                                                                 src="{{public_url("./images/active.png")}}"/>
                                                        </a>
                                                    @endif


                                                </td>
                                                <td class="hidden-xs">
                                                    <a class="handcursor hidden-xs"
                                                       href="{{url("Users/editUser",["id"=>$companyUser["id"]])}}">
                                                        <img title="ویرایش"
                                                             src="{{public_url("./images/update.png")}}"/></a>
                                                </td>
                                                {{--<td>--}}
                                                    {{--@if($companyUser["level"]=="admin")--}}
                                                        {{--<a class="handcursor"--}}
                                                           {{--onclick="swal('خطا','کاربر مدیر را نمی توان حذف کرد','error')">--}}
                                                            {{--<img title="حذف"--}}
                                                                 {{--src="{{public_url("./images/del.png")}}"/>--}}
                                                        {{--</a>--}}
                                                    {{--@endif--}}
                                                    {{--@if($companyUser["level"]!="admin")--}}
                                                        {{--<a class="handcursor"--}}
                                                           {{--onclick="ConfirmForDelete('کاربر انتخاب شده حذف خواهد شد!!!','Users//deleteUser&id={{$companyUser["id"]}}');">--}}
                                                            {{--<img title="حذف"--}}
                                                                 {{--src="{{public_url("./images/del.png")}}"/>--}}
                                                        {{--</a>--}}
                                                    {{--@endif--}}

                                                {{--</td>--}}


                                                <td class=" text-center" style="text-align: center">
                                                    <a class="handcursor"
                                                       href="{{url("Users/moreManageUsers",["id"=>$companyUser["id"]])}}">

                                                        <img title="بیشتر"
                                                             src="{{public_url("./images/more.png")}}"/></a>
                                                </td>
                                            </tr>
                                            <tr></tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection