@extends("AdminLteMaster")
@section("content")
	<title>تعریف پروژه</title>
	<strong>
		@if(isset($error))
			@include("messages.messages")
		@endif
	</strong>
	<script defer>
        $(document).ready(function () {
            $('.jdate').persianDatepicker({
                altField: '#tarikhAlt',
                altFormat: 'X',
                format: 'YYYY/MM/DD',
                observer: true,
                timePicker: {
                    enabled: true
                },
            })
        })
	</script>
	<div id="spin"></div>
	<div class="row padding5">
		<div class="panel panel-primary ">
			<div class="panel-collapse collapse in">
				<div class="panel-body">
					<div class="panel-group" id="accordion7">
						@if(authUser("level")!="projectAdmin")
							<div class="panel panel-primary">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordion8" href="#collapse3">تعریف
											پروژه</a>
									</h4>
								</div>
								<div id="collapse3" class="panel-collapse collapse in panel-collapse0">
									<div class="panel-body">
										<form class="form" action="{{url("projects/insertProject")}}" method="post">
											<div class="row buttom10">
												<input type="hidden" value="insert" name="detectAction"
													   id="detectAction"/>
												<input type="hidden" name="idProject" id="idProject"/>
												<div class="col-md-3 col-md-offset-1">
													<label>نام/تیتر/موضوع :</label>
													<input type="text" id="subject" class="form-control required"
														   alt="نام/تیتر/موضوع"
														   name="subject"/>
												</div>
												<div class="col-md-3">
													<label>تاریخ شروع :</label>
													<input type="text" id="startDate"
														   class="form-control required jdate ltrDir"
														   alt="تاریخ شروع"
														   name="startDate"/>
												</div>
												<div class="col-md-3">
													<label>تاریخ پایان :</label>
													<input type="text" id="endDate"
														   class="form-control required jdate ltrDir"
														   alt="تاریخ پایان"
														   name="endDate"/>
												</div>
											</div>
											<div class="row">
												<div class="col-md-3 col-md-offset-1">
													<label>تعداد سوال :</label>
													<input type="number" id="questionunmer"
														   class="form-control required"
														   alt="تعداد سوال"
														   name="questionunmer"/>
												</div>
												<div class="col-md-3">
													<label>تعداد پاسخگو :</label>
													<input type="number" id="pasokhgooNumber"
														   class="form-control required"
														   alt="تعداد پاسخگو"
														   name="pasokhgooNumber"/>
												</div>
												<div class="col-md-3">
														<label>درخواست موقعیت مکانی :</label>
														<select name="geoLocationRequire" class="form-control nopadding" id="geoLocationRequire">
															<option name="geoLocationRequire" value="1">ضروری می باشد</option>
															<option name="geoLocationRequire" value="0">ضرورتی ندارد</option>
														</select>
												</div>
											</div>
											<div class="row">
												<div class="col-md-8 col-md-offset-4 leftAlign top10">
													<input type="submit" class="btn-success btn-lg" value="ثبت"/>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						@endif
						<div class="panel panel-primary">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion6" href="#collapse2">لیست پروژه
										ها</a>
								</h4>
							</div>
							<div id="collapse2" class="panel-collapse collapse in panel-collapse1">
								<div class="panel-body">
									<table class="table table-bordered margintop5 table-responsive">
										<thead>
										<tr>
											<th class="text-center hidden-xs">ردیف</th>
											<th class="text-center ">نام/تیتر/موضوع</th>
											<th class="text-center">تاریخ شروع</th>
											<th class="text-center">تاریخ پایان</th>
											<th class="text-center hidden-xs">تعداد سوال</th>
											<th class="text-center hidden-xs">تعداد پاسخگو</th>
											{{--<th class="text-center"> وضعیت فعلی</th>--}}
											<th class="text-center hidden-xs"> وضعیت</th>
											@if(authUser("level")!="projectAdmin")
											<th class="text-center hidden-xs">ویرایش</th>
											<th class="text-center hidden-xs">حذف</th>
											@endif
											<th class="text-center">بیشتر</th>

										</tr>
										</thead>
										<tbody>
										@foreach($allProjects as $allProject)
											<tr>
												<td class="text-center hidden-xs">{{$loop->iteration }}</td>
												<td class="text-center ">{{$allProject["subject"]}}</td>
												<td class="text-center datepicker0">{{$allProject["startDate"]}}</td>
												<td class="text-center">{{$allProject["endDate"]}}</td>
												<td class="text-center hidden-xs">{{$allProject["questionunmer"]}}</td>
												<td class="text-center hidden-xs">{{$allProject["pasokhgooNumber"]}}</td>
												{{--                                                <td class="text-center">{{$allProject["status"]}}</td>--}}
												<td class="text-center hidden-xs">
													@if($allProject["status"]=="Active")
														<a href="{{url("projects/projectChangeState",["id"=>$allProject["id"]])}}">
															<img title=" فعال/برای غیرفعال کردن کلیک کنید"
																 src="{{public_url("./images/active.png")}}"/>
														</a>
													@endif
													@if($allProject["status"]=="inActive")

														<a href="{{url("projects/projectChangeState",["id"=>$allProject["id"]])}}">
															<img title="غیر فعال/برای فعال کردن کلیک کنید"
																 src="{{public_url("./images/inactive.png")}}"/>
														</a>
													@endif
												</td>
												@if(authUser("level")!="projectAdmin")
												<td class=" text-center hidden-xs">
													<a class="handcursor"
													   onclick="getProjectRowValueByAjaxForUpdate({{$allProject["id"]}})">
														<img title="ویرایش"
															 src="{{public_url("./images/update.png")}}"/></a>
												</td>
												<td class=" text-center hidden-xs">
													<a class="handcursor"
													   onclick="ConfirmForDelete('رکورد انتخاب شده حذف خواهد شد!!!','projects/deleteProject&idP={{$allProject["id"]}}')">
														<img title="حذف" src="{{public_url("./images/del.png")}}"/></a>
												</td>
												@endif
												<td class=" text-center" style="text-align: center">
													<a class="handcursor" target="_self"
													   href="{{url("admin/selectedProject",["idP"=>$allProject["id"]])}}" {{--onclick="getProjectRowValueByAjaxForUpdate({{$allProject["id"]}})"--}}>
														<img title="بیشتر"
															 src="{{public_url("./images/more.png")}}"/></a>

												</td>
											</tr>

											<tr></tr>
										@endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
@endsection