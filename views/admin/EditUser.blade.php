@extends("AdminLteMaster")
@section("content")

    <strong>
        @if(isset($error))
            @include("messages.messages")
        @endif
    </strong>
    <div id="spin"></div>
    <div class="row padding5">
        <div class="panel panel-primary">
            <div id="collapse1" class="panel-collapse collapse in">
                <div class="panel-body">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">ویرایش کاربر
                                        :</a>
                                </h4>
                            </div>
                            <div id="collapse3" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <form class="form" action="{{url("Users/updateUser")}}" method="post">
                                        <input type="hidden" name="id" value="{{$Userinf["id"]}}">
                                            <div class="row">
                                                <div class="col-md-8 col-md-offset-2">
                                                    <label class="align-right">نوع کاربر :</label>
                                                    <select name="level"
                                                            class="form-control questionTypeSelect nopadding"
                                                            id="level">
                                                        <option @if($Userinf["level"]=="porseshgar") selected="selected"
                                                                @endif value="porseshgar">پرسشگر
                                                        </option>
                                                        <option @if($Userinf["level"]=="adminProject") selected="selected"
                                                                @endif value="projectAdmin">مدیر پروژه
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                        <hr/>
                                        <div class="row">
                                            <div class="col-md-3 col-md-offset-2">
                                                <label>نام:</label>
                                                <input type="text" id="name" class="form-control required" alt="نام"
                                                       value="{{$Userinf["name"]}}"
                                                       name="name"/>
                                            </div>
                                            <div class="col-md-3 col-md-offset-2">
                                                <label>نام خانوادگی:</label>
                                                <input type="text" id="fName" class="form-control required"
                                                       value="{{$Userinf["fName"]}}"
                                                       alt="نام خانوادگی" name="fName"/>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 col-md-offset-2" id="staticParent">
                                                <label>کد ملی:</label>
                                                <input type="text" minlength="10" maxlength="10" id="nationalCode"
                                                       value="{{$Userinf["nationalCode"]}}"
                                                       class="form-control required" alt="کد ملی" name="nationalCode"/>
                                            </div>
                                            <div class="col-md-3 col-md-offset-2">
                                                <label>آدرس محل سکونت:</label>
                                                <input type="text" id="homeAdd" class="form-control required"
                                                       value="{{$Userinf["homeAdd"]}}"
                                                       alt="آدرس محل سکونت" name="homeAdd"/>
                                            </div>
                                        </div>
                                        <div class="row margintop5">
                                            <div class="col-md-2 col-md-offset-10 ">
                                                <input type="submit" id="btnInsert" class="btn-success btn-lg"
                                                       value="ثبت"/>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection