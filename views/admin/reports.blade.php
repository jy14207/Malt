@extends("AdminLteMaster")
@section("content")
    <title>گزارشات</title>

    <strong>
        @if(isset($error))
            @include("messages.messages")
        @endif
    </strong>
    <div class="row padding5" >
        <div class="panel panel-primary ">
            <div id="collapse0" class="panel-collapse collapse in">
                <div class="panel-body">
                    <div class="panel-group" id="accordion">
                        <div class="alert alert-success ">
                            <div class="row">
                                <div class="col-xs-8 col-sm-10 col-md-10"> پروژه :<strong>{{$nP}}</strong></div>
                                <div class="col-xs-4 col-sm-2 col-md-1 pull-left">
                                    <a class="btn btn-info" href="{{url("admin/viewActiveProjectListForReports",["active"=>"rep"])}}">برگشت</a>
                                    <a class="btn btn-info" href="{{url("admin/Export_Database",["active"=>"manageP"])}}">خروجی کل دیتابیس</a>

                                </div>
                            </div>
                            {{--<a class="btn btn-info" href="{{url("admin/viewActiveProjectListForReports",["active"=>"rep"])}}">برگشت</a>
                            <div class="col-md-11">پروژه : <strong>{{$nP}}&nbsp; &nbsp; </strong> </div>--}}
                        </div>
                        <div class="panel panel-primary SpinnerShow">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">خروجی در قالب اکسل</a>
                                </h4>
                            </div>
                            <input type="hidden" id="idP" value="{{$idP}}">
                            <div id="collapse1" class="panel-collapse collapse  ">
                                <div class="panel-body">
                                    <div class="row" id="report_setting">
                                        <ul class="nav nav-tabs" style="margin: 3px;">
                                            <li class="active"><a data-toggle="tab" href="#home"> جدول پروژه</a>
                                            </li>
                                            <li><a data-toggle="tab" href="#menu1"> جدول سوالات</a></li>
                                            <li><a data-toggle="tab" href="#menu4"> جدول گزینه ها</a></li>
                                            <li><a data-toggle="tab" href="#menu2"> جدول پرسشنامه ها</a></li>
                                            <li><a data-toggle="tab" href="#menu3"> جدول پاسخگوها</a></li>
                                            <li><a data-toggle="tab" href="#menu5"> جدول جواب ها</a></li>
                                            <li><a data-toggle="tab" href="#menu6"> جدول کلی پروژه جهت داده کاوی</a></li>
                                        </ul>
                                        <div class="tab-content" style="padding-right: 10px;">
                                            <div id="home" class="tab-pane fade in active">
                                                <div class="col-md-12">
                                                    <a href="#" id="ExportProjectTable" class="spinnerStart">پیش نمایش</a> |
                                                    <a href="#" onClick="toexcel('خروجی از جدول پروژه','dvTable');">خروجی
                                                        excel</a>
                                                    <hr>
                                                    <div class="reportHTMLTables pre-scrollable" id="dvTable" ></div>
                                                </div>
                                            </div>
                                            <div id="menu1" class="tab-pane fade">
                                                <div class="col-md-12">
                                                    <a href="#" id="ExportQuestionTable" class="spinnerStart">پیش نمایش</a> |
                                                    <a href="#"
                                                       onClick="toexcel('خروجی از جدول سوالات','dvQuestionTable');">خروجی
                                                        excel</a>
                                                    <hr>
                                                    <div class="pre-scrollable" id="dvQuestionTable"></div>
                                                </div>
                                            </div>
                                            <div id="menu2" class="tab-pane fade">
                                                <div class="col-md-12">
                                                    <a href="#" id="ExportPorseshnameTable" class="spinnerStart">پیش نمایش</a> |
                                                    <a href="#"
                                                       onClick="toexcel('خروجی از جدول پرسشنامه ها','dvPorseshnameTable');">خروجی
                                                        excel</a>
                                                    <hr>
                                                    <div class="pre-scrollable" id="dvPorseshnameTable"></div>
                                                </div>
                                            </div>
                                            <div id="menu3" class="tab-pane fade">
                                                <div class="col-md-12">
                                                    <a href="#" id="ExportPasokhgooTable">پیش نمایش</a> |
                                                    <a href="#"
                                                       onClick="toexcel('خروجی از جدول پاسخگو ها','dvPasokhgooTable');">خروجی
                                                        excel</a>
                                                    <hr>
                                                    <div class="pre-scrollable" id="dvPasokhgooTable"></div>
                                                </div>
                                            </div>
                                            <div id="menu4" class="tab-pane fade">
                                                <div class="col-md-12">
                                                    <a href="#" id="ExportOptionsTable" class="spinnerStart">پیش نمایش</a> |
                                                    <a href="#"
                                                       onClick="toexcel('خروجی از جدول گزینه ها','dvOptionsTable');">خروجی
                                                        excel</a>
                                                    <hr>
                                                    <div class="pre-scrollable" id="dvOptionsTable"></div>
                                                </div>
                                            </div>
                                            <div id="menu5" class="tab-pane fade">
                                                <div class="col-md-12">
                                                    <a href="#" id="ExportAnswerTable" class="spinnerStart">پیش نمایش</a> |
                                                    <a href="#" onClick="toexcel('خروجی از جدول جواب ها','dvAnswerTable');">خروجی
                                                        excel</a>
                                                    <hr>
                                                    <div class="pre-scrollable" id="dvAnswerTable"></div>
                                                </div>
                                            </div>
                                            <div id="menu6" class="tab-pane fade">
                                                <div class="col-md-12">
                                                    {{--<li><a href="{{url("reports/databaseTemplateReport",["idP"=>_get("idP")])}}"><i class="fa fa-dashboard"></i> <span>Database Template</span></a></li>--}}

                                                    <a href="#" id="ExportDatamainingِTable" class="spinnerStart" >پیش نمایش</a> |
                                                    <a href="#" onClick="toexcel('خروجی از جدول کلی پروژه جهت داده کاوی','dvDatamainingTable');">خروجی
                                                        excel</a>
                                                    <hr>
                                                    <div class="pre-scrollable" id="dvDatamainingTable"></div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">                                        گزارش بر اساس فعالیت پرسشگران</a>
                                </h4>
                            </div>
                            <div id="collapse2" class="panel-collapse collapse ">
                                <div class="panel-body">
                                    <span>تعداد پرسشنامه های کال هرپرسشگر</span>
                                    <span>تعداد نافص</span>
                                    <span>تعداد کامل</span>
                                    <span></span>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">گزارشات
                                        تحلیلی</a>
                                </h4>
                            </div>
                            <div id="collapse3" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection