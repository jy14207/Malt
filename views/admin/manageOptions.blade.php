@extends("AdminLteMaster")
@section("content")
	<title>مدیریت گزینه ها</title>
	<strong>
		@if(isset($error))
			@include("messages.messages")
		@endif
	</strong>
	<!-- ======================================== پیغام ها  ================================== -->
	<div class="row padding5">
		<div class="panel panel-primary ">
			<div id="collapse1" class="panel-collapse collapse in">
				<div class="panel-body">
					<div class="panel-group" id="accordion">
						<div class="alert alert-success ">
							<a class="btn btn-info" href="{{url("admin/selectedProject")}}">برگشت</a>
							<div class="col-md-11"> سوال:
								<strong>{{$rowQuestion["rowNumber"]."- ".$rowQuestion["text"]}}</strong></div>
						</div>
						@if($rowQuestion["idQuestionType"]!="3" && $rowQuestion["idQuestionType"]!="6" &&
						 $rowQuestion["idQuestionType"]!="7" && $rowQuestion["idQuestionType"]!="8" &&
						 $rowQuestion["idQuestionType"]!="9" && $rowQuestion["idQuestionType"]!="10" &&
						 $rowQuestion["idQuestionType"]!="12" && $rowQuestion["idQuestionType"]!="14" &&
						 $rowQuestion["idQuestionType"]!="15" && $rowQuestion["idQuestionType"]!="16" &&
						 $rowQuestion["idQuestionType"]!="17" && $rowQuestion["idQuestionType"]!="18" &&
						 $rowQuestion["idQuestionType"]!="19" && $rowQuestion["idQuestionType"]!="20")
							<div class="panel panel-primary">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordion" href="#collapse3"> گزینه
											ها</a>
									</h4>
								</div>
								<div id="collapse3"
									 class="panel-collapse collapse @if($collapsin=="optiona") {{"in"}} @endif">
									<div class="panel-body">
										<form id="storeOtherOption" action="{{url("option/storeOtherOption")}}"
											  method="post">
											<input type="hidden" value="{{_get("idQ")}}" name="idQ">

										</form>
										<form class="form" action="{{url("option/storeOption")}}" method="post"
											  enctype="multipart/form-data">
											<input type="hidden" value="{{_get("idQ")}}" name="idQ">
											<div class="row buttom10" style="  vertical-align: middle;">
												<div class="col-md-3 col-md-offset-3 ">
													<label>متن گزینه :</label>
													<input type="text" name="textOption"
														   class="form-control required"
														   alt="متن گزینه" autofocus>
												</div>

												<div class="col-md-3">
													<label>امتیاز :</label>
													<input type="text" id="scoreInput" name="score"
														   value="{{$nextScore}}"
														   class="form-control "
														   alt="امتیاز">
												</div>
											</div>
											<div class="row buttom10">
												<div class="col-md-6 col-md-offset-3 buttom10">
													<label>توضیح بیشتر(برای راهنمایی پرسشگر): </label>
													<input type="text" id="moreExplanation" name="moreExplanation"
														   class="form-control "
														   alt="توضیح بیشتر">
												</div>
												<div class="col-md-6 col-md-offset-3">
													<label>تصویر گزینه :</label>
													<label class="myLabel">
														<input type="file" name="uploadFile" id="uploadFile"
															   accept="image/jpeg"/>
														<span>انتخاب کنید</span>
													</label>
												</div>
											</div>
											<div class="row buttom10">
												<div class="col-md-3 col-md-offset-8 buttom10">
													<div id="imagePreview" class="imagePreview"></div>
												</div>
											</div>
											<div class="row">
											</div>
											<div class="col-md-12 centerAlign buttom10">
												<input type="submit" id="insertQuestion" value="ثبت"
													   class="btn-success btn-lg">
												@if($rowQuestion["idQuestionType"]==4 |$rowQuestion["idQuestionType"]==5)
													<input type="submit" class="btn btn-info btn-lg"
														   value="درج گزینه سایر" form="storeOtherOption">
												@endif
											</div>
										</form>
										<div class="row">
											<hr>
										</div>
										<div class="row">
											<div class="col-md-12 margintop5 ">
												<div class="panel-body">
													<table id="optionsTable"
														   class="reportHTMLTables table table-bordered margintop5">
														<thead>
														<tr>
															{{--<th class="text-center">ردیف </th>--}}
															<th class="text-center">متن</th>
															<th class="text-center">امتیاز</th>
															<th class="text-center">توضیح بیشتر</th>
															<th class="text-center"> تصویر</th>
															<th class="text-center">ویرایش</th>
															<th class="text-center">حذف</th>
														</tr>
														</thead>
														<tbody>
														@foreach($optionsLists as $optionsLista)
															<tr class="text-center">
																<td>@if($optionsLista["textOption"]=="Other") {{"سایر"}} @else  {{$optionsLista["textOption"]}} @endif </td>
																<td> {{$optionsLista["score"]}}</td>
																<td> {{$optionsLista["moreExplanation"]}}</td>
																<td>
																	@if($optionsLista["haveImage"]=="Yes")
																		<div id="myModal" class="modal">
																			<span class="close">&times;</span>
																			<img class="modal-content" id="img01">
																			<div id="caption"></div>
																		</div>
																		<img class="myImg"
																			 id="{{$optionsLista["id"]}}"
																			 src="{{public_url("imagesUpload/options/").$optionsLista["id"].".jpeg"}}">
																	@else
																		<a class="handcursor"
																		   href="{{url("option/editOptions",["opId"=>$optionsLista["id"]])}}">افزودن
																			تصویر</a>
																	@endif
																</td>

																<td>
																	<a class="handcursor"
																	   href="{{url("option/editOptions",["opId"=>$optionsLista["id"]])}}">
																		<img title="ویرایش"
																			 src="{{public_url("./images/update.png")}}"/></a>
																</td>
																<td>
																	<a class="handcursor"
																	   onclick="ConfirmForDelete('گزینه انتخاب شده حذف خواهد شد!!!','option/deleteDeleteOption&id={{$optionsLista["id"]}}&idQ={{_get("idQ")}}');">
																		{{--                                                       href="{{url("Question/deleteQuestion",["id"=>$optionsLista["id"]])}}">--}}
																		<img title="حذف"
																			 src="{{public_url("./images/del.png")}}"/>
																	</a>
																</td>
																<td style="visibility: hidden"></td>
															</tr>
														@endforeach
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						@endif
						@if($rowQuestion["idQuestionType"]!="12")
							<div class="panel panel-primary">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordion" href="#collapse4"> اختصاص
											سیاست
											به سوال: &nbsp;<a target="_blank"
															  href="{{url("admin/help",["tag"=>"QPalicy"])}}"><i
														class="fa fa-question-circle"></i></a></a></h4>
								</div>
								<div id="collapse4"
									 class="panel-collapse collapse @if($collapsin=="socialPol" |$collapsin=="stopPol" |$collapsin=="countPol" |$collapsin=="ansToOtherQPol"|$collapsin=="jumpPol" )  {{"in"}} @endif"/>
								<div class="panel-body">
									<div id="collapse1" class="panel-collapse collapse in">
										<div class="panel panel-primary" id="accordion13">
											<div class="panel panel-primary">
												<div class="panel-heading">
													<h4 class="panel-title">
														<a data-toggle="collapse" data-parent="#accordion13"
														   href="#collapse012">لحاظ شدن در محاسبه طبقه اجتماعی
															<a target="_blank"
															   href="{{url("admin/help",["tag"=>"QPalicy"])}}"><i
																		class="fa fa-question-circle"></i></a></a></a>
													</h4>
												</div>
												<div id="collapse012"
													 class="panel-collapse collapse @if($collapsin=="socialPol") {{"in"}} @endif">
													<div class="panel-body">
														<div class="alert alert-success "> سوال:
															<strong>{{$rowQuestion["rowNumber"]."- ".$rowQuestion["text"]}}</strong>
														</div>
														<div class="col-md-12">
															@if($getPolicySocial["id"]==null)
																<h5> * برای اختصاص دادن این سیاست به سوال دکمه ثبت را
																	کلیک نمائید.</h5>

																<div class="row leftAlign">

																	<form id="PolicysocialClassForm"
																		  action="{{url("Question/setPolicy")}}"
																		  method="post">
																		<input type="hidden" name="collapsin"
																			   value="socialPol">
																		<input type="hidden" name="idPolicy"
																			   id="idPolicysocialClassDiv"
																			   value="{{$policyslists[0][0]}}">
																		<input type="hidden" name="idQuestion"
																			   id="idQuestion"
																			   value="{{$rowQuestion["id"]}}">
																		<input type="submit" value="ثبت"
																			   class="btn-success btn-lg">

																	</form>
																	@endif
																	@if($getPolicySocial["id"]!=null)
																		<h5>*این سیاست به سوال جاری اختصاص داده شده
																			است</h5>
																		<h5> * برای حذف سیاست برروی دکمه حذف کلیک
																			کنید</h5>
																		<div class="col-md-12 leftAlign button10">
																			<a class="btn-danger btn-lg"
																			   style="cursor: hand;"
																			   onclick="ConfirmForDelete('سیاست اختصاص داده شده حذف خواهد شد!!!','Question/deletePolicy&idPol={{$policyslists[0][0]}}&collapsin=socialPol&idQuestion={{$rowQuestion["id"]}}');">
																				حذف</a>
																			@endif
																		</div>
																</div>
														</div>
													</div>
												</div>
											</div>
											<div class="panel panel-primary">
												<div class="panel-heading">
													<h4 class="panel-title">
														<a data-toggle="collapse" data-parent="#accordion13"
														   href="#collapse013">خاتمه دادن به ادامه مصاحبه
															<a target="_blank"
															   href="{{url("admin/help",["tag"=>"QPalicy"])}}"><i
																		class="fa fa-question-circle"></i></a></a></a>
													</h4>
												</div>
												<div id="collapse013"
													 class="panel-collapse collapse @if($collapsin=="stopPol") {{"in"}} @endif">
													<div class="panel-body">
														<div class="alert alert-success "> سوال:
															<strong>{{$rowQuestion["rowNumber"]."- ".$rowQuestion["text"]}}</strong>
														</div>

														<form action="{{url("Question/setPolicy")}}" method="post"
															  onsubmit="">
															<input type="hidden" name="collapsin" value="stopPol">
															<input type="hidden" name="idPolicy"
																   id="idPolicyStopDiv"
																   value="{{$policyslists[1][0]}}"/>
															<input type="hidden" name="idQuestion" id="idQuestion"
																   value="{{$rowQuestion["id"]}}">
															{{--<input type="text" name="idPolicy" id="idPolicyStopDiv">--}}
															<div class="col-md-10 col-md-offset-1 buttom10">
																<label class="align-right">کدام گزینه(ها) مصاحبه را
																	خاتمه دهد
																	:</label>
																<select name="idOption" class="form-control nopadding"
																		id="idOption">
																	@foreach($optionsLists as $optionsLista)
																		<option value="{{$optionsLista["id"]}}">{{$optionsLista["textOption"]}}</option>
																	@endforeach
																</select>
															</div>
															<div class="col-md-11 leftAlign">
																<input type="submit" id="insertQuestion" value="ثبت"
																	   class="btn-success btn-lg">
															</div>
														</form>
														<div class="col-md-12">
															<table id="optionsTable"
																   class="reportHTMLTables table table-bordered margintop5">
																<thead>
																<tr>
																	<th class="text-center">متن گزینه</th>
																	<th class="text-center">حذف سیاست</th>
																</tr>
																</thead>
																<tbody>

																@foreach($getPolicyStops as $getPolicyStop)

																	<tr class="text-center">
																		<td>{{$getPolicyStop["textOption"]}}</td>
																		<td>
																			<a class="handcursor"
																			   onclick="ConfirmForDelete('گزینه انتخاب شده حذف خواهد شد!!!','Question/deletePolicy&id={{$getPolicyStop[0]}}&collapsin=stopPol&idPol=2&idQuestion={{$rowQuestion["id"]}}');">
																				{{--                                                       href="{{url("Question/deleteQuestion",["id"=>$optionsLista["id"]])}}">--}}
																				<img title="حذف"
																					 src="{{public_url("./images/del.png")}}"/>
																			</a>
																		</td>
																		<td style="visibility: hidden"></td>
																	</tr>
																@endforeach
																</tbody>
															</table>
														</div>
													</div>
												</div>
											</div>
											<div class="panel panel-primary">
												<div class="panel-heading">
													<h4 class="panel-title">
														<a data-toggle="collapse" data-parent="#accordion13"
														   href="#collapse0131">پرش به سوال خاص
															<a target="_blank"
															   href="{{url("admin/help",["tag"=>"QPalicy"])}}"><i
																		class="fa fa-question-circle"></i></a></a></a>
													</h4>
												</div>
												<div id="collapse0131"
													 class="panel-collapse collapse @if($collapsin=="jumpPol") {{"in"}} @endif">
													<div class="panel-body">
														<div class="alert alert-success "> سوال:
															<strong>{{$rowQuestion["rowNumber"]."- ".$rowQuestion["text"]}}</strong>
														</div>

														<form action="{{url("Question/setPolicy")}}" method="post"
															  onsubmit="">
															<input type="hidden" name="collapsin" value="jumpPol">

															<input type="hidden" name="idPolicy"
																   id="idPolicyJump"
																   value="{{$policyslists[4][0]}}"/>
															<input type="hidden" name="idQuestion" id="idQuestion"
																   value="{{$rowQuestion["id"]}}">
															{{--<input type="text" name="idPolicy" id="idPolicyStopDiv">--}}
															<div class="col-md-5 col-md-offset-1 buttom10">
																<label class="align-right">گزینه ای که باعث پرش شود(محرک
																	پرش):</label>
																<select name="idOption" class="form-control nopadding"
																		id="idOption">
																	@foreach($optionsLists as $optionsLista)
																		<option value="{{$optionsLista["id"]}}">{{$optionsLista["textOption"]}}</option>
																	@endforeach
																</select>
															</div>
															<div class="col-md-5 buttom10">
																<label class="align-right"> سوال مقصد را انتخاب
																	کنید:</label>
																<select name="JumpDestIdQ"
																		class="form-control nopadding" id="idOtherQ">
																	@foreach($allQuestions as $allQuestion)
																		<option value="{{$allQuestion["id"]}}">{{$allQuestion["rowNumber"]}}
																			-&nbsp;{{$allQuestion["text"]}}</option>
																	@endforeach
																</select>
															</div>
															<div class="col-md-11 leftAlign">
																<input type="submit" id="insertQuestion" value="ثبت"
																	   class="btn-success btn-lg">
															</div>
														</form>
														<div class="col-md-10 col-md-offset-1">
															<table id="optionsTable"
																   class="reportHTMLTables table table-bordered margintop5">
																<thead>
																<tr>
																	<th class="text-center">متن گزینه-محرک پرش</th>
																	<th class="text-center"> سوال مقصد</th>
																	<th class="text-center">حذف سیاست</th>
																</tr>
																</thead>
																<tbody>

																@foreach($getPolicyJumps as $getPolicyJump)

																	<tr class="text-center">
																		<td>{{$getPolicyJump["textOption"]}}</td>
																		<td>{{ $getPolicyJump["rowNumber"]}}
																			-&nbsp;{{$getPolicyJump["text"]}}</td>
																		<td>
																			<a class="handcursor"
																			   onclick="ConfirmForDelete('گزینه انتخاب شده حذف خواهد شد!!!','Question/deletePolicy&id={{$getPolicyJump[0]}}&idPol=5&collapsin=jumpPol&idQuestion={{$rowQuestion["id"]}}');">
																				{{--                                                       href="{{url("Question/deleteQuestion",["id"=>$optionsLista["id"]])}}">--}}
																				<img title="حذف"
																					 src="{{public_url("./images/del.png")}}"/>
																			</a>
																		</td>
																		<td style="visibility: hidden"></td>
																	</tr>
																@endforeach
																</tbody>
															</table>
														</div>
													</div>
												</div>
											</div>
											<div class="panel panel-primary">
												<div class="panel-heading">
													<h4 class="panel-title">
														<a data-toggle="collapse" data-parent="#accordion13"
														   href="#collapse014">تعداد پرسشنامه ها از جواب این سوال
															<a target="_blank"
															   href="{{url("admin/help",["tag"=>"QPalicy"])}}"><i
																		class="fa fa-question-circle"></i></a></a></a>
													</h4>
												</div>
												<div id="collapse014"
													 class="panel-collapse collapse @if($collapsin=="countPol") {{"in"}} @endif">
													<div class="panel-body">
														<div class="alert alert-success "> سوال:
															<strong>{{$rowQuestion["rowNumber"]."- ".$rowQuestion["text"]}}</strong>
														</div>

														<form action="{{url("Question/setPolicy")}}" method="post"
															  onsubmit="">
															<input type="hidden" name="collapsin" value="countPol">

															<input type="hidden" name="idPolicy"
																   id="idPolicyStopDiv"
																   value="{{$policyslists[2][0]}}"/>
															<input type="hidden" name="idQuestion" id="idQuestion"
																   value="{{$rowQuestion["id"]}}">
															{{--<input type="text" name="idPolicy" id="idPolicyStopDiv">--}}
															<div class="col-md-5 col-md-offset-1 buttom10">
																<label class="align-right">گزینه محدود کننده:</label>
																<select name="idOption" class="form-control nopadding"
																		id="idOption">
																	@foreach($optionsLists as $optionsLista)
																		<option value="{{$optionsLista["id"]}}">{{$optionsLista["textOption"]}}</option>
																	@endforeach
																</select>
															</div>
															<div class="col-md-5 buttom10">
																<label class="align-right">تعداد:</label>
																<input type="number" name="count"
																	   class="form-control required"
																	   alt="تعداد محدود کننده" value="0">
															</div>
															<div class="col-md-11 leftAlign">
																<input type="submit" id="insertQuestion" value="ثبت"
																	   class="btn-success btn-lg">
															</div>
														</form>
														<div class="col-md-10 col-md-offset-1">
															<table id="optionsTable"
																   class="reportHTMLTables table table-bordered margintop5">
																<thead>
																<tr>
																	<th class="text-center">متن گزینه</th>
																	<th class="text-center">تعداد</th>
																	<th class="text-center">حذف سیاست</th>
																</tr>
																</thead>
																<tbody>

																@foreach($getPolicyCount as $getPolicyCountt)

																	<tr class="text-center">
																		<td>{{$getPolicyCountt["textOption"]}}</td>
																		<td>{{$getPolicyCountt["count"]}}</td>
																		<td>
																			<a class="handcursor"
																			   onclick="ConfirmForDelete('گزینه انتخاب شده حذف خواهد شد!!!','Question/deletePolicy&id={{$getPolicyCountt[0]}}&idPol=2&collapsin=countPol&idQuestion={{$rowQuestion["id"]}}');">
																				{{--                                                       href="{{url("Question/deleteQuestion",["id"=>$optionsLista["id"]])}}">--}}
																				<img title="حذف"
																					 src="{{public_url("./images/del.png")}}"/>
																			</a>
																		</td>
																		<td style="visibility: hidden"></td>
																	</tr>
																@endforeach
																</tbody>
															</table>
														</div>
													</div>
												</div>
											</div>
											<div class="panel panel-primary">
												<div class="panel-heading">
													<h4 class="panel-title">
														<a data-toggle="collapse" data-parent="#accordion13"
														   href="#collapse015">جواب این سوال در صورت کدام سوال ها نمایش
															داده شود؟
															<a target="_blank"
															   href="{{url("admin/help",["tag"=>"QPalicy"])}}"><i
																		class="fa fa-question-circle"></i></a></a></a>
													</h4>
												</div>
												<div id="collapse015"
													 class="panel-collapse collapse @if($collapsin=="ansToOtherQPol") {{"in"}} @endif">
													<div class="panel-body">
														<div class="alert alert-success "> سوال:
															<strong>{{$rowQuestion["rowNumber"]."- ".$rowQuestion["text"]}}</strong>
														</div>
														<form action="{{url("Question/setPolicy")}}" method="post"
															  onsubmit="">
															<input type="hidden" name="collapsin"
																   value="ansToOtherQPol">

															<input type="hidden" name="idPolicy"
																   id="idPolicyAnsToOtherQDiv"
																   value="{{$policyslists[3][0]}}"/>
															<input type="hidden" name="idQuestion" id="idQuestion"
																   value="{{$rowQuestion["id"]}}">
															{{--<input type="text" name="idPolicy" id="idPolicyStopDiv">--}}
															<div class="col-md-10 col-md-offset-1 buttom10">
																<label class="align-right">سوال مورد نظر را انتخاب
																	نمائید:</label>
																<select name="idOtherQ" class="form-control nopadding"
																		id="idOtherQ">
																	@foreach($allQuestions as $allQuestion)
																		<option value="{{$allQuestion["id"]}}">{{$allQuestion["rowNumber"]}}
																			-&nbsp;{{$allQuestion["text"]}}</option>
																	@endforeach
																</select>
															</div>
															<div class="col-md-11 leftAlign">
																<input type="submit" id="insertQuestion" value="ثبت"
																	   class="btn-success btn-lg">
															</div>
														</form>
														<div class="col-md-110 col-md-offset-1">
															<table id="optionsTable"
																   class="reportHTMLTables table table-bordered margintop5">
																<thead>
																<tr>
																	<th class="text-center">ردیف</th>
																	<th class="text-center">صورت سوال</th>
																	<th class="text-center">حذف سیاست</th>
																</tr>
																</thead>
																<tbody>
																@foreach($getPolicyansToOtherQPols as $getPolicyansToOtherQPol)

																	<tr class="text-center">
																		<td>{{$getPolicyansToOtherQPol["rowNumber"]}}</td>
																		<td>{{$getPolicyansToOtherQPol["text"]}}</td>
																		<td>
																			<a class="handcursor"
																			   onclick="ConfirmForDelete('گزینه انتخاب شده حذف خواهد شد!!!','Question/deletePolicy&id={{$getPolicyansToOtherQPol[0]}}&collapsin=ansToOtherQPol&idQuestion={{$rowQuestion["id"]}}');">
																				{{--                                                       href="{{url("Question/deleteQuestion",["id"=>$optionsLista["id"]])}}">--}}
																				<img title="حذف"
																					 src="{{public_url("./images/del.png")}}"/>
																			</a>
																		</td>
																		<td style="visibility: hidden"></td>
																	</tr>
																@endforeach
																</tbody>
															</table>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						@endif
						<div class="panel panel-primary">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapse2"> ویرایش
										تصویر </a>
								</h4>
							</div>
							<div id="collapse2" class="panel-collapse collapse">
								<div class="panel-body">
									<form action="{{url("Question/updateImg")}} " id="updateImg" method="post"
										  enctype="multipart/form-data"></form>
									<form class="form" action="{{url("Question/deleteImg")}} " id="deleteImg"
										  method="post" enctype="multipart/form-data"></form>
									<input type="hidden" name="idQ" value="{{$rowQuestion["id"]}}" form="updateImg">
									<div class="row buttom10">
										<div class="col-md-6 col-md-offset-1">
											<label>تصویر گزینه(حداکثر 50 کیلوبایت) :</label>
											<label class="myLabel">
												<input type="file" name="uploadFile" id="uploadFile"
													   accept="image/jpeg" form="updateImg"/>
												<span>انتخاب کنید</span>
											</label>
										</div>
										<div class="row buttom10">
											<div class="col-md-3 col-md-offset-8 buttom10">
												<div id="imagePreview" class="imagePreview"></div>
												@if($rowQuestion["haveImage"]=="Yes")
													<img name="img3"
														 src="{{public_url("imagesUpload/question/") . $rowQuestion["id"] . ".jpeg"}}"
														 class="imagePreviewOptionEdit" id="tempImg">
												@endif
											</div>
										</div>
									</div>
									<input type="hidden" name="id" value="{{$rowQuestion["id"]}}" form="deleteImg">
									{{--<input type="hidden" name="idP" value="{{_get("idP")}}" form="deleteImg">--}}
									{{--                                    <input type="hidden" name="nP" value="{{_get("nP")}}" form="deleteImg">--}}
									<div class="col-md-12 centerAlign buttom10">
										<input type="submit" id="insertQuestion" value="ثبت"
											   class="btn-success btn-lg" form="updateImg">
										<a class="btn-danger btn-lg handcursor"
										   onclick="ConfirmForDelete('تصویر سوال حذف خواهد شد؟!!!','Question/deleteImg&idQ={{_get("idQ")}}');">حذف
										</a>
									</div>

									<hr>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
	</div>
@endsection