@extends("AdminLteMaster")
@section("content")

    <strong>
        @if(isset($error))
            @include("messages.messages")
        @endif
    </strong>
    <div id="spin"></div>
    <div class="row padding5" >
        <div class="panel panel-primary">
            <div id="collapse1" class="panel-collapse collapse in">
                <div class="panel-body">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion"  href="#collapse3">تعریف پروژه</a>
                                </h4>
                            </div>
                            <div id="collapse3" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <form class="form" action="{{url("projects/insertProject")}}" method="post">
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion"  href="#collapse2">لیست پروژه
                                        ها</a>
                                </h4>
                            </div>
                            <div id="collapse2" class="panel-collapse collapse in">
                                <div class="panel-body">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection