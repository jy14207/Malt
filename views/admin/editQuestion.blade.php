@extends("AdminLteMaster")
@section("content")
    <title>ویرایش سوال</title>
    <strong>
        @if(isset($error))
            @include("messages.messages")
        @endif
    </strong>
    <!-- ======================================== پیغام ها  ================================== -->
    <script type="text/javascript">

    </script>
    <div class="row padding5">
        <div class="panel panel-primary ">
            <div id="collapse1" class="panel-collapse collapse in">
                <div class="panel-body">
                    <div class="panel-group" id="accordion">
                        <div class="alert alert-success ">
                            <a class="btn btn-info" href="{{url("admin/selectedProject",["active"=>"manageP"])}}">برگشت</a>
                            <div class="col-md-11">سوال : <strong>{{$qRow["rowNumber"]."- ".$qRow["text"]}}</strong> </div>
                        </div>

                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse2"> ویرایش تصویر </a>
                                </h4>
                            </div>
                            <div id="collapse2" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <form action="{{url("Question/updateImg")}} "id="updateImg" method="post" enctype="multipart/form-data"></form>
                                    <form class="form" action="{{url("Question/deleteImg")}} "id="deleteImg" method="post" enctype="multipart/form-data"></form>
                                    <input type="hidden" name="idQ" value="{{$qRow["id"]}}" form="updateImg">
                                    <div class="row buttom10">
                                        <div class="col-md-6 col-md-offset-1">
                                            <label>تصویر گزینه(حداکثر 50 کیلوبایت) :</label>
                                            <label class="myLabel">
                                                <input type="file" name="uploadFile" id="uploadFile"
                                                       accept="image/jpeg" form="updateImg"/>
                                                <span>انتخاب کنید</span>
                                            </label>
                                        </div>
                                        <div class="row buttom10">
                                            <div class="col-md-3 col-md-offset-8 buttom10">
                                                <div id="imagePreview" class="imagePreview"></div>
                                                @if($qRow["haveImage"]=="Yes")
                                                    <img name="img3"
                                                         src="{{public_url("imagesUpload/question/") . $qRow["id"] . ".jpeg?".rand(0,10000000)}}"
                                                         class="imagePreviewOptionEdit" id="tempImg">
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="id" value="{{$qRow["id"]}}" form="deleteImg">
                                    {{--<input type="hidden" name="idP" value="{{_get("idP")}}" form="deleteImg">--}}
{{--                                    <input type="hidden" name="nP" value="{{_get("nP")}}" form="deleteImg">--}}
                                    <div class="col-md-12 centerAlign buttom10">
                                        <input type="submit" id="insertQuestion" value="ثبت"
                                               class="btn-success btn-lg" form="updateImg">
                                        <a class="btn-danger btn-lg handcursor" onclick="ConfirmForDelete('تصویر سوال حذف خواهد شد؟!!!','Question/deleteImg&idQ={{_get("idQ")}}');">حذف
                                        </a>
                                    </div>

                                    <hr>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection