@extends("AdminLteMaster")
@section("content")
	<title>مدیریت پروژه</title>
	<strong>
{{--	@if($error=="error"){{"error"}} @endif--}}
	@if(isset($error))
		@include("messages.messages")
	@endif
</strong>
<!-- ======================================== پیغام ها  ================================== -->
<script type="text/javascript">

</script>
<div class="row padding5">
	<div class="panel panel-primary ">
		<div id="collapse1" class="panel-collapse collapse in">
			<div class="panel-body">
				<div class="panel-group" id="accordion">
					<div class="alert alert-success ">
						<div class="row">
							<div class="col-xs-8 col-sm-10 col-md-10"> پروژه :<strong>{{$projectInfo["subject"]}}</strong></div>
							<div class="col-xs-4 col-sm-2 col-md-1 pull-left">
								<a class="btn btn-info" href="{{url("admin/allProjects",["active"=>"manageP"])}}">برگشت</a>

							</div>
						</div>
					</div>
					<div class="panel panel-primary">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#collapse3">ثبت سوال </a>
							</h4>
						</div>
						<div id="collapse3"
							 class="panel-collapse collapse @if($collapsin=="insertQ") {{"in"}} @endif">
							<div class="panel-body">
								<form class="form" action="{{url("Question/storeQuestion")}}" method="post"
									  id="storeQ" enctype="multipart/form-data">
									<input type="hidden" name="actionQuestion" id="actionQuestion" value="insert">
									<input type="hidden" name="idQ" id="idQ" value="0">
									<div class="row buttom10">
										<div class="col-md-4 ">
											<label>نام پروژه(فعال) :</label>
											<select name="idProject" class="form-control nopadding">
												<option id="idP" name="idProject"
														value="{{$projectInfo["id"]}}">{{$projectInfo["subject"]}}</option>
											</select>
										</div>
										<div class="col-md-4">

											<label class="align-right">نوع سوال :</label>
											<select name="idQuestionType"
													class="form-control questionTypeSelect nopadding"
													id="questionTypeSelect">
												@foreach($questiontypes as $questiontype)
													<option value="{{$questiontype["id"]}}">{{$questiontype["pLabel"]}}</option>
												@endforeach
											</select>
										</div>
										<div class="col-md-4">
											<label>نوع پاسخ :</label>
											<select name="allowNull" class="form-control nopadding" id="allowNull">
												<option name="allowNull" value="no">اجباری</option>
												<option name="allowNull" value="yes">اختیاری</option>
											</select>
										</div>
									</div>

									<div class="row" id="teifDivQuestionType" hidden>
										<div class="col-md-10 col-md-offset-1 padding20"
											 style="border:outset ;border-color:rgba(232,35,247,0.89);border-radius: 15px;">
											<div class="col-md-12 buttom10 leftDirection" style="direction: ltr;">
												<span id="currentRangeValue">6</span>
												<input type="range" name="rangeTeifi" min="1" max="10"
													   id="rangeTeifi" value="6"
													   class=" noTopMargin">
											</div>
											<div class="col-md-12">
												<div class="col-md-3">
													<input type="number" class="form-control" name="maxValue"
														   id="maxValue" max="50"
														   placeholder="حداکثر:پیش فرض =10 ">
												</div>
												<div class="col-md-3">
													<input type="text" class="form-control" name="rigthLable"
														   id="rigthLable" placeholder="برچسب راست">
												</div>
												<div class="col-md-3">
													<input type="text" class="form-control" name="centerLable"
														   id="centerLable"
														   placeholder=" وسط"></div>
												<div class="col-md-3">
													<input type="text" class="form-control" name="leftLeble"
														   id="leftLeble" placeholder="چپ"></div>
											</div>
										</div>
									</div>
									<div class="row" id="numberDiv" hidden>
										<div class="col-md-10 col-md-offset-1 padding20"
											 style="border:outset ;border-color:rgba(232,35,247,0.89);border-radius: 15px;">
											<div class="col-md-12">
												<div class="col-md-3 col-md-offset-3">
													<label> حداقل :</label>
													<input type="number" class="form-control required"
														   name="minNumberType" min="0" id="minNumberType" value="1"
														   alt="حداقل">
												</div>
												<div class="col-md-3">
													<label> حداکثر :</label>
													<input type="number" class="form-control required"
														   name="maxNumberType" id="maxNumberType" max="1000000000"
														   value="10"
														   alt="حداکثر"></div>
											</div>
										</div>
									</div>

									<div class="row" id="videoDiv" hidden>
										<div class="col-md-10 col-md-offset-1 padding20"
											 style="border:outset ;border-color:rgba(232,35,247,0.89);border-radius: 15px;">
											<div class="col-md-12">
												<div class="col-md-6 col-md-offset-3">
													<label> آدرس ویدئو :</label>
													<a target="_blank"
													   href="{{url("admin/help",["tag"=>"QPalicy"])}}"><i
																class="fa fa-question-circle"></i></a></a>
													<input type="text" class="form-control required "
														   style="text-align: left"
														   name="videoURL" min="0" id="videoURL"
														   placeholder="آدرس اینترنتی ویدئو را وارد نمائید"
														   alt="آدرس ویدئو">
												</div>
											</div>
											<div class="col-md-12 col-sm-12" id="videoLineSpliter">
												<hr/>
											</div>
											<div class="col-md-6 col-sm-6 col-md-offset-3 col-sm-offset-3"
												 id="videPlayerDiv" style="text-align: center">
												{{--<video width="80	%" height="80%" id="videPlayer" controls><source id="srcVideoPlayer"  type="video/mp4"></video>--}}
											</div>
										</div>
									</div>
									<div class="row" id="imageDiv" hidden>
										<div class="col-md-10 col-md-offset-1 padding20"
											 style="border:outset ;border-color:rgba(232,35,247,0.89);border-radius: 15px;">
											<div class="col-md-6 col-md-offset-1">
												<label>تصویر گزینه(حداکثر 2 مگابایت) :</label>
												<label class="myLabel">
														<input class="required" type="file" name="uploadFile" id="uploadFile" alt="فیلد تصویر"
														   form="storeQ"
														   accept=".jpg" form="updateImg"/>
													<span>انتخاب کنید</span>
												</label>
											</div>
											<div class="row buttom10">
												<div class="col-md-3 col-md-offset-8 buttom10"
													 id="imageHideWhenNofile" hidden>
													<div id="imagePreview" class="imagePreview"></div>
													<img name="img3" src=""
														 class="imagePreviewOptionEdit" id="tempImg">
												</div>
											</div>
										</div>
									</div>
									<div class="row" id="audioDiv" hidden>
										<div class="col-md-10 col-sm-12 col-md-offset-1 padding20"
											 style="border:outset ;border-color:rgba(232,35,247,0.89);border-radius: 15px;">
											<div class="col-md-4 col-sm-12 col-md-offset-1 buttom10">
												<label>فایل mp3(حداکثر 5 مگابایت) :</label>
												<label class="myLabel">
													<input class="required" type="file" name="uploadAudioFile" id="uploadAudioFile" alt="فیلد فایل صوتی"
														   form="storeQ"
														   accept="audio/mpeg" form="updateImg"/>
													<span>انتخاب کنید</span>
												</label>
											</div>
											<div class="col-md-4 col-sm-12">
												<div class="col-sm-12 embed-responsive embed-responsive-4by3" id="audioPlayerDiv" style="text-align: center">
													<audio class="embed-responsive-item" id="audioPlayer" controls></audio>
																																									{{--<video width="80	%" height="80%" id="videPlayer" controls><source id="srcVideoPlayer"  type="video/mp4"></video>--}}
												</div>
											</div>
										</div>
									</div>

									<div class="row" id="1degreeDivQuestionType" hidden>
										<div class="col-md-10 col-md-offset-1 padding20"
											 style="border:outset ;border-color:rgba(232,35,247,0.89);border-radius: 15px;">
											<div class="col-md-12 buttom10 leftDirection" style="direction: ltr;">
												<span id="currentRangedegreeValue">6</span>
												<input type="range" name="rangeDegree" min="1" max="10"
													   id="rangeDegree"
													   class="ng-pristine ng-valid ng-scope ng-not-empty ng-touched noTopMargin">
											</div>
											<div class="col-md-12">
												<div class="col-md-3">
													<input type="number" class="form-control" name="maxValueDegree"
														   id="maxValueDegree" max="50"
														   placeholder="حداکثر:پیش فرض =10 ">
												</div>
											</div>
										</div>

									</div>
									<div class="row">
										<hr>
									</div>
									<div class="row buttom10">
										<div class="col-md-4">
											<label> ردیف(خودافزایشی) :</label>
											<input type="text" autofocus id="rowNumber" name="rowNumber"
												   class="form-control" value="{{$nextRowNumber}}">
										</div>
										<div class="col-md-8">
											<label>متن سوال :</label>
											<input type="text" name="text" class="form-control required" id="textQ"
												   alt="متن سوال">

										</div>
									</div>
									<div class="row buttom10">
										<div class="col-md-4">
											<label>کد سوال :</label>
											<input type="text" name="questionCode" id="questionCode"
												   class="form-control required"
												   alt="کد سوال" value="{{"Q".$nextRowNumber}}">
										</div>
										<div class="col-md-8">
											<label>توضیح بیشتر(برای راهنمایی پرسشگر): </label>
											<input type="text" id="moreExplanation" name="moreExplanation"
												   class="form-control "/>
										</div>

									</div>
									<div class="row">
										<div class="col-md-8 col-md-offset-4"
											 style="padding-top: 10px;text-align: left;">
											<input type="submit" id="insertQuestion" value="ثبت"
												   class="btn-success btn-lg">
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div class="panel panel-primary">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#collapse2"> سوالات ثبت
									شده </a>
							</h4>
						</div>
						<div id="collapse2"
							 class="panel-collapse collapse @if($collapsin=="listQ") {{"in"}} @endif">
							<div class="panel-body " style="max-height: 700px; overflow-y: scroll;">
								<table class="reportHTMLTables table table-bordered margintop5 pre-scrollable">
									<thead>
									<tr>
										<th class="text-center">ردیف</th>
										<th class="text-center hidden-xs">کد</th>
										<th class="text-center hidden-xs">نوع</th>
										<th class="text-center hidden-xs">نوع پاسخ</th>
										<th class="text-center">متن</th>
										<th class="text-center hidden-xs"> تصویر</th>
										<th class="text-center hidden-xs">پیش نمایش</th>
										<th class="text-center">ویرایش</th>
										<th class="text-center hidden-xs">حذف</th>
										<th class="text-center">بیشتر</th>
									</tr>
									</thead>
									<tbody>

									@foreach($QuestionList as $QuestionLista)
										<tr>
											<td class="text-center"> {{$QuestionLista["rowNumber"]}}</td>
											<td class="text-center hidden-xs"> {{$QuestionLista["questionCode"]}}</td>
											<td class="text-right hidden-xs">
												@foreach($questiontypes as $questiontype)
													@if($QuestionLista["idQuestionType"]==$questiontype["id"]){{$questiontype["pLabel"]}}@endif
												@endforeach
											</td>
											<td class="text-right hidden-xs">@if($QuestionLista["allowNull"]=="no") {{"اجباری"}} @else {{"اختیاری"}} @endif</td>
											<td class="text-right"> {{$QuestionLista["text"]}}</td>
											<td class="hidden-xs">
												@if($QuestionLista["haveImage"]=="Yes")
													<div id="myModal" class="modal">
														<span class="close">*</span>
														<img class="modal-content" id="img01">
														<div id="caption"></div>
													</div>
													<img alt="تصویر سوال" class="myImg"
														 id="{{$QuestionLista["id"]}}"
														 src="{{public_url("imagesUpload/question/").$QuestionLista["id"].".jpeg?".rand(0,326545214578)}}">
												@else
													<a class="handcursor"
													   href="{{url("Question/editQuestion",["idQ"=>$QuestionLista["id"]])}}">
														<img title="ویرایش تصویر"
															 src="{{public_url("./images/exchangeImageQuestion.png")}}"/>
													</a>
												@endif
											</td>

											<td class=" text-center hidden-xs">
												<a class="handcursor" target="_blank"
												   href="{{url("projects/loadQuestionPreviewAdmin",["rowNum"=>$QuestionLista["rowNumber"]])}}" {{--onclick="getProjectRowValueByAjaxForUpdate({{$allProject["id"]}})"--}}>
													<img title="پیش نمایش سوال در سمت پرسشگر"
														 src="{{public_url("./images/PreviewQuestion.png")}}"/></a>
											</td>
											<td class=" text-center">
												<a class="handcursor"
												   onclick="getRowForEditQuestion({{$QuestionLista["id"]}})"
												>
													<img title="ویرایش"
														 src="{{public_url("./images/update.png")}}"/></a>
											</td>
											<td class="text-center hidden-xs">
												<a class="handcursor"
												   onclick="ConfirmForDelete('سوال انتخاب شده با گزینه ها حذف خواهد شد!!!','Question/deleteQuestion&id={{$QuestionLista["id"]}}');">
													{{--                                                       href="{{url("Question/deleteQuestion",["id"=>$QuestionLista["id"]])}}">--}}
													<img title="حذف" src="{{public_url("./images/del.png")}}"/>
												</a>
											</td>
											<td class=" text-center">
												<a class="handcursor" target="_self"
												   href="{{url("Question/manageOptions",["idQ"=>$QuestionLista["id"],"collapsin"=>"optiona"])}}" {{--onclick="getProjectRowValueByAjaxForUpdate({{$allProject["id"]}})"--}}>
													<img title="بیشتر"
														 src="{{public_url("./images/more.png")}}"/></a>

											</td>
											<td style="visibility: hidden"></td>
										</tr>
									@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="panel panel-primary">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#collapse111">اختصاص
									پرسشگر </a>
							</h4>
						</div>
						<div id="collapse111"
							 class="panel-collapse collapse @if($collapsin=="setPorseshgar") {{"in"}} @endif">
							<div class="panel-body">
								<form class="form-horizontal" action="{{url("projects/setPorseshgar",["t"=>"po"])}}"
									  method="post">
									<div class="row buttom10">
										<div class="col-md-4 col-lg-offset-2">
											<div class="form-group">
												<label>نام پرسشگران :</label>
												<select name="idPorseshgar" class="form-control nopadding">
													@foreach($PorseshgarLists as $porseshgarList)
														<option value="{{$porseshgarList["id"]}}">{{$porseshgarList["name"]." ".$porseshgarList["fName"]}}</option>
													@endforeach
												</select>
											</div>
										</div>
									</div>
									<div class="col-md-4 col-lg-offset-6 leftAlign top10">
										<input type="submit" id="btnInsert" class="btn-success btn-lg"
											   value="ثبت"/>
									</div>
								</form>
								<div class="col-md-12 ">
									<hr>
									<table class="table table-bordered margintop5 ">
										<thead>
										<tr>
											<th class="text-center">ردیف</th>
											<th class="text-center">نام ونام خانوادگی</th>
											<th class="text-center">حذف</th>
										</tr>
										</thead>
										<tbody>
										@foreach($thisProjectPorseshgars as $thisProjectPorseshgar)

											<tr>
												<td class="text-center">{{$loop->iteration }}</td>
												<td class="text-center">{{$thisProjectPorseshgar["name"]." ".$thisProjectPorseshgar["fName"]}}</td>
												<td class=" text-center" style="text-align: center">
													<a class="handcursor"
													   onclick="ConfirmForDelete('رکورد انتخاب شده حذف خواهد شد!!!','projects/deletePorseshgarProject&id={{$thisProjectPorseshgar[0]}}&t=po');">
														<img title="حذف" src="{{public_url("./images/del.png")}}"/></a>
												</td>
											</tr>
											<tr></tr>
										@endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>

					@if(authUser("level")!="projectAdmin")
						<div class="panel panel-primary">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapse1111">اختصاص
										مدیر پروژه </a>
								</h4>
							</div>
							<div id="collapse1111"
								 class="panel-collapse collapse @if($collapsin=="setprojectAdmin") {{"in"}} @endif">
								<div class="panel-body">
									<form class="form-horizontal"
										  action="{{url("projects/setPorseshgar",["t"=>"pa"])}}"
										  method="post">
										<div class="row buttom10">
											<div class="col-md-4 col-lg-offset-2">
												<div class="form-group">
													<label>نام مدیران پروژه :</label>
													<select name="idPorseshgar" class="form-control nopadding">
														@foreach($adminProjectLists as $adminProjectList)
															<option value="{{$adminProjectList["id"]}}">{{$adminProjectList["name"]." ".$adminProjectList["fName"]}}</option>
														@endforeach
													</select>
												</div>
											</div>
										</div>
										<div class="col-md-4 col-lg-offset-6 leftAlign top10">
											<input type="submit" id="btnInsert" class="btn-success btn-lg"
												   value="ثبت"/>
										</div>
									</form>
									<div class="col-md-12 ">
										<hr>
										<table class="table table-bordered margintop5 ">
											<thead>
											<tr>
												<th class="text-center">ردیف</th>
												<th class="text-center">نام ونام خانوادگی</th>
												<th class="text-center">حذف</th>
											</tr>
											</thead>
											<tbody>
											@foreach($thisProjectprojectAdmins as $thisProjectprojectAdmin)
												<tr>
													<td class="text-center">{{$loop->iteration }}</td>
													<td class="text-center">{{$thisProjectprojectAdmin["name"]." ".$thisProjectprojectAdmin["fName"]}}</td>
													<td class=" text-center" style="text-align: center">
														<a class="handcursor"
														   onclick="ConfirmForDelete('رکورد انتخاب شده حذف خواهد شد!!!','projects/deletePorseshgarProject&id={{$thisProjectprojectAdmin[0]}}&t=pa');">
															<img title="حذف"
																 src="{{public_url("./images/del.png")}}"/></a>
													</td>
												</tr>
												<tr></tr>
											@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					@endif


					<div class="panel panel-primary">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#collapse8">اختصاص
									شهر </a>
							</h4>
						</div>
						<div id="collapse8"
							 class="panel-collapse collapse @if($collapsin=="setCity") {{"in"}} @endif">
							<div class="panel-body">
								<form class="form-horizontal" action="{{url("projects/insertCity")}}" method="post">
									<div class="row">
										<div class="col-md-4 col-md-offset-3">
											<div class="form-group">
												<label class="col-md-6 ">نام شهر را وارد کنید
													:</label>
												<input type="text" name="cityName"
													   class="col-md-6 form-control"
													   alt="نام شهر">
											</div>
										</div>
										<div class="col-md-2">
											<input type="submit" id="insertCity" value="ثبت"
												   class="btn-success btn">
										</div>
									</div>
								</form>
								<hr>
								<div class="row">
									<div class="col-md-10 col-lg-offset-1">
										<table class="table table-bordered margintop5">
											<thead>
											<tr>
												<th class="text-center">ردیف</th>
												<th class="text-center">نام شهر</th>
												<th class="text-center">حذف</th>
											</tr>
											</thead>
											<tbody>
											@foreach($citysProject as $cityProject)
												<tr>
													<td class="text-center">
														{{$loop->index+1}}
													</td>
													<td class="text-center"> {{$cityProject["name"]}}</td>
													<td class="text-center alignCenter">
														<a class="confirming"
														   href="{{url("projects/deleteCity",["id"=>$cityProject["id"]])}}">
															<img title="حذف"
																 src="{{public_url("./images/del.png")}}"/>
														</a>
													</td>
													<td style="visibility: hidden"></td>
												</tr>
											@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="panel panel-primary">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#collapse9"> پرسشنامه
									های جواب داده شده ( بازبینی پروژه) </a>
							</h4>
						</div>
						<div id="collapse9"
							 class="panel-collapse collapse @if($collapsin=="listPorseshname") {{"in"}} @endif">
							<div class="panel-body">

								<div class="row">
									<table class="table table-bordered">
										<thead>
										<tr>
											<th class="text-center">ردیف</th>
											<th class="text-center">شماره پرسشنامه</th>
											<th class="text-center">نام ونام خانوادگی پرسشگر</th>
											<th class="text-center hidden-xs">تاریخ تکمیل</th>
											<th class="text-center hidden-xs">ساعت شروع</th>
											<th class="text-center hidden-xs">ساعت پایان</th>
											<th class="text-center hidden-xs">ویرایش توسط بازبین</th>
											<th class="text-center hidden-xs">پاسخگویی مجدد</th>
											<th class="text-center hidden-xs">تائید/عدم تائید</th>
											<th class="text-center hidden-xs">حذف</th>
											<th class="text-center">بیشتر</th>
										</tr>
										</thead>
										<tbody>
										@foreach($listPorseshnamehs as $listPorseshnameh)
											<tr>
												<td class="text-center">
													{{$loop->index+1}}
												</td>
												<td class="text-center"> {{$listPorseshnameh["porseshnamehNumber"]}}</td>
												<td class="text-center"> {{$listPorseshnameh["name"]." ".$listPorseshnameh["fName"]}}</td>
												<td class="text-center hidden-xs"> {{$listPorseshnameh["completedDate"]}}</td>
												<td class="text-center hidden-xs"> {{$listPorseshnameh["startTime"]}}</td>
												<td class="text-center hidden-xs"> {{$listPorseshnameh["endTime"]}}</td>
												<td class="text-center hidden-xs">
													@if($listPorseshnameh["EditedByReviewer"]=="Yes")
														<a>
															<img title="این پرسشنامه توسط بازبین ویرایش شده است"
																 src="{{public_url("./images/EditedByViewer.png")}}"/>
														</a>
													@endif
												</td>
												<td class="text-center hidden-xs">
													<a style="font-size: large; cursor: hand"
													   id="{{$listPorseshnameh["id"]}}" class="lockState"
													   data-id="{{$listPorseshnameh["id"]}}"
													   data-lockedbyeditor="{{$listPorseshnameh["lockedByEditor"]}}"
													   href="#">
														<img title=" پرسشنامه قفل/برای دادن مجوز ویرایش به پرسشگر کلیک کنید"
															 @if($listPorseshnameh["lockedByEditor"]==0) style="display: none"
															 @endif
															 src="{{public_url("./images/lock.png")}}"/>
														<img title=" پرسشنامه باز/برای سلب مجوز ویرایش از پرسشگر کلیک کنید"
															 src="{{public_url("./images/unLock.png")}}"
															 @if($listPorseshnameh["lockedByEditor"]==1) style="display: none" @endif
														/>
													</a>
												</td>
												<td class="text-center hidden-xs">
													@if($listPorseshnameh["Confirmed"]=="Yes")
														<a href="{{url("porseshname/confirmNo",["id"=>$listPorseshnameh["id"]])}}">
															<img title=" تائید/برای باطل کردن پرسشنامه کلیک کنید(توجه داشته باشید پرسشنامه باطل شده از آمار پرسشگر و گزارشات کاسته خواهد شد)"
																 src="{{public_url("./images/active.png")}}"/>
														</a>
													@endif
													@if($listPorseshnameh["Confirmed"]=="No")

														<a href="{{url("porseshname/confirmYes",["id"=>$listPorseshnameh["id"]])}}">
															<img title=" باطل/برای فعال کردن پرسشنامه کلیک کنید(توجه داشته باشید پرسشنامه فعال شده در آمار پرسشگر و گزراشات لحاظ خواهد شد)"
																 src="{{public_url("./images/inactive.png")}}"/>
														</a>
													@endif
												</td>
												<td class=" text-center hidden-xs" style="text-align: center">
													<a class="handcursor"
													   onclick="ConfirmForDelete('رکورد انتخاب شده حذف خواهد شد!!!','porseshname/softDeletePorseshnameh&id={{$listPorseshnameh["id"]}}');">
														<img title="حذف"
															 src="{{public_url("./images/del.png")}}"/></a>
												</td>


												<td class=" text-center" style="text-align: center">
													<a class="handcursor" target="_self"
													   href="{{url("porseshname/manage",["id"=>$listPorseshnameh["id"]])}}" {{--onclick="getProjectRowValueByAjaxForUpdate({{$allProject["id"]}})"--}}>
														<img title="بیشتر"
															 src="{{public_url("./images/more.png")}}"/></a>

												</td>
												<td style="visibility: hidden"></td>
											</tr>
										@endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="panel panel-primary">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#collapse10"> تعریف طبقه
									اجتماعی </a>
							</h4>
						</div>
						<div id="collapse10"
							 class="panel-collapse collapse @if($collapsin=="SocialClass") {{"in"}} @endif">
							<div class="panel-body">
								<div class="panel-body">
									<form class="form-horizontal" action="{{url("projects/setSocialclass")}}"
										  method="post" role="form" id="SocialClassForm"
										  onsubmit="return validationSocialClass(this)">
										<input type="hidden" name="act" id="act" value="insert">
										<input type="hidden" name="idSocialClass" id="idSocialClass">
										<div class="row buttom10 form-group">
											<div class="col-md-3 col-md-offset-2 ">
												<label>نام (به انگلیسی) :</label>
												<input type="text" name="SECName" id="SECName" class="form-control"
													   alt="نام (به انگلیسی)">
											</div>
											<div class="col-md-3">
												<label>عنوان :</label>
												<input type="text" name="caption" id="SECcaption"
													   class="form-control" alt="عنوان">
											</div>
											<div class="col-md-3">
												<label>تعداد سهمیه :</label>
												<input type="text" name="count" id="count" class="form-control"
													   alt="تعداد سهمیه">
											</div>
										</div>
										<div class="row buttom10 form-group">
											<div class="col-md-3 col-md-offset-2 ">
												<label>حد پایین :</label>
												<input type="text" name="SECScoreMin" id="SECScoreMin"
													   class="form-control" alt="حد پایین">
											</div>
											<div class="col-md-3">
												<label>حد بالا :</label>
												<input type="text" name="SECScoreMax" id="SECScoreMax"
													   class="form-control" alt="حد بالا">
											</div>

										</div>
										<div class="row alignCenter">
											<input type="submit" value="ثبت" class="btn-success btn-lg">
										</div>
									</form>
									<hr>
									<div class="row">
										<div class="col-md-10 col-lg-offset-1">
											<table class="table table-bordered margintop5">
												<thead>
												<tr>
													<th class="text-center hidden-xs">ردیف</th>
													<th class="text-center hidden-xs">نام (به انگلیسی)</th>
													<th class="text-center">عنوان</th>
													<th class="text-center hidden-xs">تعداد سهمیه</th>
													<th class="text-center hidden-xs">حد پایین</th>
													<th class="text-center hidden-xs">حد بالا</th>
													<th class="text-center">ویرایش</th>
													<th class="text-center">حذف</th>
												</tr>
												</thead>
												<tbody>
												@foreach($SocialClass as $socialClas)
													<tr>
														<td class="text-center hidden-xs">
															{{$loop->index+1}}
														</td>
														<td class="text-center hidden-xs"> {{$socialClas["SECName"]}}</td>
														<td class="text-center"> {{$socialClas["caption"]}}</td>
														<td class="text-center hidden-xs"> {{$socialClas["count"]}}</td>
														<td class="text-center hidden-xs"> {{$socialClas["SECScoreMin"]}}</td>
														<td class="text-center hidden-xs"> {{$socialClas["SECScoreMax"]}}</td>
														<td class=" text-center">
															<a class="handcursor"
															   onclick="getRowForUpdateSocialClass({{$socialClas["id"]}})">
																<img title="ویرایش"
																	 src="{{public_url("./images/update.png")}}"/></a>
														</td>
														<td class=" text-center" style="text-align: center">
															<a class="handcursor"
															   onclick="ConfirmForDelete('رکورد انتخاب شده حذف خواهد شد!!!','projects/deleteSocialclass&id={{$socialClas["id"]}}');">
																<img title="حذف"
																	 src="{{public_url("./images/del.png")}}"/></a>
														</td>
														<td style="visibility: hidden"></td>
													</tr>
												@endforeach
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection