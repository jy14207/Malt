@extends("AdminLteMaster")
@section("content")
    <title>ویرایش گزینه ها</title>
    <strong>
        @if(isset($error))
            @include("messages.messages")
        @endif
    </strong>
    <title >ویرایش سوال</title>
    <!-- ======================================== پیغام ها  ================================== -->
    <div class="row padding5">
        <div class="panel panel-primary ">
            <div id="collapse1" class="panel-collapse collapse in">
                <div class="panel-body">
                    <div class="panel-group" id="accordion">
                        <div class="alert alert-success ">
                            <div class="row">
                                <div class="col-xs-8 col-sm-10 col-md-10">  متن گزینه: <strong>{{$optionRow["textOption"]}}</strong></div>
                                <div class="col-xs-4 col-sm-2 col-md-1 pull-left">
                                    <a class="btn btn-info" href="{{url("Question/manageOptions",["idQ"=>$optionRow["idQuestion"]])}}">برگشت</a>

                                </div>
                            </div>
                        </div>

                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse3"> ویرایش گزینه
                                        </a>
                                </h4>
                            </div>
                            <div id="collapse3" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <form class="form" action="{{url("option/updateTextOption")}}" method="post"
                                          enctype="multipart/form-data">
                                        <input type="hidden" value="{{$optionRow["idQuestion"]}}" name="idQ">
                                        <input type="hidden" name="id" value="{{$optionRow["id"]}}">
                                        <input type="hidden" name="idQuestion" value="{{$optionRow["idQuestion"]}}">
                                        <div class="row buttom10" style="  vertical-align: middle;">
                                            <div class="col-md-3 col-md-offset-3 ">
                                                <label>متن گزینه :</label>
                                                <input type="text" value="{{$optionRow["textOption"]}}"
                                                       name="textOption" class="form-control required"
                                                       alt="متن گزینه">
                                            </div>
                                            <div class="col-md-3">
                                                <label>امتیاز :</label>
                                                <input type="text" value="{{$optionRow["score"]}}" name="score"
                                                       class="form-control "
                                                       alt="امتیاز">
                                            </div>
                                        </div>
                                        <div class="row buttom10">
                                            <div class="col-md-6 col-md-offset-3 buttom10">
                                                <label>توضیح بیشتر(برای راهنمایی پرسشگر): </label>
                                                <input type="text" id="moreExplanation"
                                                       value="{{$optionRow["moreExplanation"]}}" name="moreExplanation"
                                                       class="form-control "
                                                       alt="توضیح بیشتر">
                                            </div>
                                        </div>
                                        <div class="col-md-12 centerAlign buttom10">
                                            @if($optionRow["textOption"]!="Other")
                                                <input type="submit" id="insertQuestion" value="ثبت"
                                                       class="btn-success btn-lg">
                                             @endif
                                        </div>
                                    </form>
                                    <hr>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse2"> ویرایش تصویر
                                        :</a>
                                </h4>
                            </div>
                            <div id="collapse2" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <form action="{{url("option/updateImgOption")}} "id="updateImg" method="post" enctype="multipart/form-data"></form>
                                        <input type="hidden" name="id" value="{{$optionRow["id"]}}" form="updateImg">
                                        <input type="hidden" name="idQ" value="{{$optionRow["idQuestion"]}}" form="updateImg">
                                        <div class="row buttom10">
                                            <div class="col-md-6 col-md-offset-1">
                                                <label>تصویر گزینه(حداکثر 50 کیلوبایت) :</label>
                                                <label class="myLabel">
                                                    <input type="file" name="uploadFile" id="uploadFile"
                                                           accept="image/jpeg" form="updateImg"/>
                                                    <span>انتخاب کنید</span>
                                                </label>
                                            </div>
                                            <div class="row buttom10">
                                                <div class="col-md-3 col-md-offset-8 buttom10">
                                                    <div id="imagePreview" class="imagePreview"></div>
                                                    @if($optionRow["haveImage"]=="Yes")
                                                        <img name="img3"
                                                             src="{{public_url("imagesUpload/options/") . $optionRow["id"] . ".jpeg"}}"
                                                             class="imagePreviewOptionEdit" id="tempImg">
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    <input type="hidden" name="idop" value="{{$optionRow["id"]}}" form="deleteImg">
                                    <input type="hidden" name="idQu" value="{{$optionRow["idQuestion"]}}" form="deleteImg">

                                        <div class="col-md-12 centerAlign buttom10">
                                            <input type="submit" id="insertQuestion" value="ثبت"
                                                   class="btn-success btn-lg" form="updateImg">
                                            <a class="btn-danger btn-lg handcursor" onclick="ConfirmForDelete('تصویر گزینه حذف خواهد شد؟!!!','option/deleteImgOption&idop={{$optionRow["id"]}}&idQ={{$optionRow["idQuestion"]}}');">حذف
                                            </a>
                                            <input type="button" id="insertQuestion" value="برگشت"
                                                   class="goBack btn-warning btn-lg">
                                        </div>

                                    <hr>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection