@extends("AdminLteMaster")
<title>انتخاب پروژه</title>
@section("content")
    <div class="row padding5">
        <div class="col-md-2"></div>
        <div class="col-md-12">
            <div class="list-group">
                <a class="list-group-item active"> انتخاب پروژه </a>
                @foreach($activeProject as $activeProjec)
                    <a class="list-group-item" href="{{url("admin/reports",["idP"=>$activeProjec["id"] , "nP"=>$activeProjec["subject"]])}}">{{$activeProjec["subject"]}}</a>
                @endforeach
            </div>
        </div>
        <div class="col-md-2"></div>
    </div>
@endsection