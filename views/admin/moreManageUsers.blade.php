@extends("AdminLteMaster")
@section("content")

    <strong>
        @if(isset($error))
            @include("messages.messages")
        @endif
    </strong>
    <title>مدیریت کاربر</title>
    <div id="spin"></div>
    <div class="row padding5">
        <div class="panel panel-primary">
            <div id="collapse0" class="panel-collapse collapse in">
                <div class="panel-body">
                    <div class="panel-group" id="accordion">
                        <div class="alert alert-success ">
                            <a class="btn btn-info" href="{{url("Users/manageUsers",["active"=>"mU"])}}">برگشت</a>
                            <div class="col-md-11">نام ونام خانوادگی کاربر : <strong>{{$Userinf["name"]}}&nbsp;{{$Userinf["fName"]}} &nbsp; </strong> </div>
                        </div>
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse"  href="#collapse1">اختصاص پروژه به
                                        کاربر</a>
                                </h4>
                            </div>
                            <div id="collapse1" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <form action="{{url("Users/setProject")}}" method="post">
                                        <input type="hidden" value="{{$Userinf["id"]}}" name="idUser">
                                        <div class="row buttom10">
                                            <div class="col-md-4 col-lg-offset-2">
                                                <label>نام پروژه :</label>
                                                <select name="idProject" class="form-control nopadding">
                                                    @foreach($AllActiveprojects as $allActiveproject)
                                                        <option id="idProject" name="idProject"
                                                                value="{{$allActiveproject["id"]}}">{{$allActiveproject["subject"]}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-12 controls" style="text-align: left;">
                                            <input type="submit" id="btnInsert" class="btn-success btn-lg"
                                                   value="ثبت"/>
                                        </div>
                                    </form>
                                    <div class="col-md-12">
                                        <hr>
                                        <table class="table table-bordered margintop5">
                                            <thead>
                                            <tr>
                                                <th class="text-center">ردیف</th>
                                                <th class="text-center">نام/تیتر/موضوع</th>
                                                <th class="text-center">حذف</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($UserProjects as $userProject)

                                                <tr>
                                                    <td class="text-center">{{$loop->iteration }}</td>
                                                    <td class="text-center">{{$userProject["subject"]}}</td>
                                                    <td class=" text-center" style="text-align: center">
                                                        <a class="handcursor"  onclick="ConfirmForDelete('رکورد انتخاب شده حذف خواهد شد!!!','Users/deleteUserProject&idUP={{$userProject[0]}}&idU={{$Userinf["id"]}}');">
                                                            <img title="حذف" src="{{public_url("./images/del.png")}}"/></a>
                                                    </td>
                                                </tr>
                                                <tr></tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse"  href="#collapse3">تغییر کلمه
                                        عبور</a>
                                </h4>
                            </div>
                            <div id="collapse3" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div class="row">
                                        <form id="changeForm" class="form-horizontal" role="form"
                                              action="{{url("Users/changePassword")}}" method="post" onsubmit="return checkFormStoreQ(this);">
                                            <input type="hidden" value="{{$Userinf["id"]}}" name="idUser">
                                            <div id="oldPass" class="col-md-3 col-md-offset-1">
                                                <label>گذر واژه فعلی:</label><span  id="result"></span>
                                                <input type="text" id="oldPass" class="form-control"
                                                       alt="گذر واژه فعلی" name="oldPass" value="{{$Userinf["password"]}}" readonly/>
                                            </div>
                                            <div id="register" class="col-md-3">
                                                <label>گذر واژه جدید:</label><span  id="result"></span>
                                                <input type="password" id="password" class="form-control required"
                                                       alt="گذرواژه" name="password" placeholder="حداقل شش حرف"/>
                                            </div>
                                            <div class="col-md-3">
                                                <label>تکرار گذر واژه جدید:</label>
                                                <input id="inp_pwd2" type="password" class="form-control" name="pwd2"
                                                       placeholder="حداقل شش حرف">
                                            </div>
                                            <div class="col-md-12 controls" style="text-align: left;">
                                                <input type="submit" class="btn-success btn-lg" id="btnInsert3" value="ثبت">
                                            </div>
                                        </form>
                                        {{--<form action="" id="form" method="post">--}}
                                            {{--<div class="form-group">--}}
                                                {{--<label for="desc">Password:</label>--}}
                                                {{--<input type="password" class="form-control" name="pass" id="pass" required>--}}
                                            {{--</div>--}}
                                            {{--<div class="form-group">--}}
                                                {{--<label for="desc">Confirm Password:</label>--}}
                                                {{--<input type="password" class="form-control" name="confpass" id="confpass" required>--}}
                                            {{--</div>--}}
                                            {{--<div class="form-group">--}}
                                                {{--<span class="error" style="color:red"></span><br />--}}
                                            {{--</div>--}}
                                            {{--<button type="submit" name="submit" class="btn btn-default">Submit</button>--}}
                                        {{--</form>--}}

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse2"> ویرایش تصویر
                                        </a>
                                </h4>
                            </div>
                            <div id="collapse2" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <form action="{{url("Users/setProfileImage")}} "id="updateImg" method="post" enctype="multipart/form-data"></form>
                                    <input type="hidden" value="{{$Userinf["id"]}}" name="idUser" form="updateImg">
                                    <div class="row buttom10">
                                        <div class="col-md-6 col-md-offset-1">
                                            <label>تصویر گزینه(حداکثر 50 کیلوبایت) :</label>
                                            <label class="myLabel">
                                                <input type="file" name="uploadFile" id="uploadFile"
                                                       accept="image/jpeg" form="updateImg"/>
                                                <span>انتخاب کنید</span>
                                            </label>
                                        </div>
                                        <div class="row buttom10">
                                            <div class="col-md-3 col-md-offset-8 buttom10">
                                                <div id="imagePreview" class="imagePreview"></div>
                                                    <img name="img3"
                                                         src="{{public_url("images/noImageSelected.png")}}"
                                                         class="imagePreviewOptionEdit" id="tempImg">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 controls" style="text-align: left;">
                                        <input type="submit" id="insertQuestion" value="ثبت"
                                               class="btn-success btn-lg" form="updateImg">
                                    </div>

                                    <hr>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection