@extends("AdminLteMaster")
@section("content")
    <title>پروژه های فعال</title>
    <div class="row padding5">
        <div class="col-md-2"></div>
        <div class="col-md-12">
            <div class="list-group">
                <a class="list-group-item active"> پروژه های فعال </a>
                @foreach($activeProject as $activeProjec)
                    <a class="list-group-item"
                       href="{{url("admin/selectedProject",["idP"=>$activeProjec["id"] , "nP"=>$activeProjec["subject"]])}}">{{$activeProjec["subject"]}}</a>
                @endforeach
            </div>
        </div>
        <div class="col-md-2"></div>
    </div>
@endsection