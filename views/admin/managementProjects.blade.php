@extends("AdminLteMaster")
@extends("dashbordMaster")
@section("content")
	<title>مدیریت -صفحه اصلی</title>


	<div class="row padding20">
		<div class="row">
			@if(authUser("level")=="projectAdmin")
				<div class="col-md-4 col-sm-6 col-xs-12">
					@else
						<div class="col-md-3 col-sm-6 col-xs-12"> @endif
							<div class="info-box">
								<span class="info-box-icon bg-aqua"><i
											class="fa fa-product-hunt topPadding25"></i></span>

								<div class="info-box-content">
									<span class="info-box-text"> کل پروژه ها</span>
									<a href="{{url("admin/allProjects",["active"=>"manageP"])}}"><span
												class="info-box-number count"
												style="font-size: 18px;">{{$allProjectsCount}}</span></a><span>عدد</span>
								</div>
								<!-- /.info-box-content -->
							</div>
							<!-- /.info-box -->
						</div>
						<!-- /.col -->
						@if(authUser("level")=="projectAdmin")
							<div class="col-md-4 col-sm-6 col-xs-12">
								@else
									<div class="col-md-3 col-sm-6 col-xs-12"> @endif
							<div class="info-box ">
								<span class="info-box-icon bg-green"><i
											class="fa fa-pinterest-p topPadding25"></i></span>
								<div class="info-box-content">
									<span class="info-box-text"> پروژه های فعال</span>
									<a href="{{url("admin/allProjects",["active"=>"manageP"])}}"><span
												class="info-box-number count">{{$ActiveProjectCount}}</span></a><span>عدد</span>

								</div>
								<!-- /.info-box-content -->
							</div>
							<!-- /.info-box -->
						</div>
						<!-- /.col -->

						<!-- fix for small devices only -->
						<div class="clearfix visible-sm-block"></div>

									@if(authUser("level")=="projectAdmin")
										<div class="col-md-4 col-sm-6 col-xs-12">
											@else
												<div class="col-md-3 col-sm-6 col-xs-12"> @endif
							<div class="info-box">
								<span class="info-box-icon bg-red"><i class="fa fa-pinterest topPadding25"></i></span>

								<div class="info-box-content">
									<span class="info-box-text"> پروژه های غیر فعال</span>
									<a href="{{url("admin/allProjects",["active"=>"manageP"])}}"><span
												class="info-box-number count">{{$inActiveProjectCount}}</span></a><span>عدد</span>

								</div>
								<!-- /.info-box-content -->
							</div>
							<!-- /.info-box -->
						</div>
						@if(authUser("level")!="projectAdmin")
							<div class="col-md-3 col-sm-6 col-xs-12">
								<div class="info-box">
						<span class="info-box-icon bg-yellow"><i
									class="ion ion-ios-people-outline topPadding25"></i></span>

									<div class="info-box-content">
										<span class="info-box-text">تعداد کاربران </span>
										<a href="{{url("Users/manageUsers",["active"=>"mU"])}}"><span
													class="info-box-number count">{{$totalUserCount}}</span> </a><span>نفر</span>

									</div>
									<!-- /.info-box-content -->
								</div>
								<!-- /.info-box -->
							</div>
						@endif
				</div>
		</div>
		<div class="row padding20">
			<div class="row padding20">
				{{--گزارش پروژه های فعال--}}
				<div class="col-md-6">
					<div class="box box-info " id="percentProject">
						<div class="box-header with-border">
							<h3 class="box-title">گزارش پروژه های فعال</h3>

							<div class="box-tools pull-right">
								<button type="button" class="btn btn-box-tool" data-widget="collapse"><i
											class="fa fa-minus"></i>
								</button>
								<div class="btn-group">
									<button type="button" class="btn btn-box-tool dropdown-toggle"
											data-toggle="dropdown">
										<i class="fa fa-wrench"></i></button>
									<ul class="dropdown-menu" role="menu">
										@foreach($dashboardDataArrays as $dashboardDataArray)
											<li><a href="#">{{$dashboardDataArray["subject"]}}</a></li>
											<li class="divider "></li>
										@endforeach

									</ul>
								</div>
								<button type="button" class="btn btn-box-tool" data-widget="remove"><i
											class="fa fa-times"></i>
								</button>
							</div>
						</div>
						<script>
                            $(document).ready(function () {
                                $('.progress .progress-bar').css("width",
                                    function () {
                                        return $(this).attr("aria-valuenow") + "%";
                                    }
                                )
                            });
						</script>
						<!-- /.box-header -->
						<div class="box-body" id="projectPercentDiv">
							<div class="row">
								<!-- /.col -->
								<div class="col-md-12">
									<p class="text-center">
										<strong>میزان پیشرفت پروژه ها</strong>
									</p>
									{{--                                {{var_dump($dashboardDataArrays)}}--}}
									@foreach($dashboardDataArrays as $dashboardDataArray)
										<div class="progress-group ">
											<span class="progress-text">{{$dashboardDataArray["subject"]}}</span>
											<span class="progress-number"><b>{{$dashboardDataArray["finishPorseshnamehCount"]}}</b>/{{$dashboardDataArray["totalPorseshnameh"]}}</span>
											<div class="progress skill-bar">
												<div class="progress-bar progress-bar-{{$dashboardDataArray["barType"]}}"
													 role="progressbar"
													 aria-valuenow="{{$dashboardDataArray["projectPercent"]}}"
													 aria-valuemin="0" aria-valuemax="100">
                                                <span class="skill"><i
															class="val">{{$dashboardDataArray["projectPercent"]}}</i></span>
												</div>
											</div>
										</div>
									@endforeach
								</div>
							</div>
						</div>
					</div>
				</div>

				{{--ستونی--}}
				<div class="col-md-6 ">
					<!-- BAR CHART -->
					<div class="box box-info">
						<div class="box-header with-border">
							<h5 class="box-title"> نسبت پروژه های انجام شده به کل
								<small>(براساس تعداد پرسشنامه)</small>
							</h5>

							<div class="box-tools pull-right">
								<button type="button" class="btn btn-box-tool" data-widget="collapse"><i
											class="fa fa-minus"></i>
								</button>
								<button type="button" class="btn btn-box-tool" data-widget="remove"><i
											class="fa fa-times"></i></button>
							</div>
						</div>
						<div class="box-body">
							<div class="chart">
								<canvas id="barChart"></canvas>
							</div>
						</div>
						<div class="box-footer text-center">
							<div class="col-md-4 col-md-offset-2">
								<span> انجام شده</span>
								<small class="label pull-left bg-green">&nbsp;</small>
							</div>
							<div class="col-md-4">
								<span>تعداد کل</span>
								<small class="label pull-left bg-gray">&nbsp;</small>
							</div>

						</div>
					</div>

				</div>

				{{--پروژه های شرکت در یک نگاه--}}
				<div class="col-md-12">
					<div class="box box-danger">
						<div class="box-header with-border">
							<h3 class="box-title">پروژه های شرکت در یک نگاه</h3>

							<div class="box-tools pull-right">
								<button type="button" class="btn btn-box-tool" data-widget="collapse"><i
											class="fa fa-minus"></i>
								</button>
								<button type="button" class="btn btn-box-tool" data-widget="remove"><i
											class="fa fa-times"></i>
								</button>
							</div>
						</div>
						<!-- /.box-header -->
						<div class="box-body">
							<div class="table-responsive centerAlign">
								<table class="table no-margin">
									<thead>
									<tr>
										<th class="hidden-xs">ردیف</th>
										<th>نام/تیتر/موضوع</th>
										<th class="hidden-xs">تاریخ شروع</th>
										<th class="hidden-xs">تعداد کل پرسشنامه ها</th>
										<th>تعداد تکمیل شده</th>
										<th class="hidden-xs">تعداد بازبینی شده</th>
										<th class="hidden-xs">تعداد پرسشگران پروژه</th>
										<th class="hidden-xs">درصد پیشرفت پروژه</th>
										<th>بیشتر</th>
									</tr>
									</thead>
									<tbody>
									@foreach($dashboardDataArrays as $dashboardDataArray)
										<tr>
											<td class="hidden-xs"><a
														href="pages/examples/invoice.html">{{$loop->index +1}}</a></td>
											<td>{{$dashboardDataArray["subject"]}}</td>
											<td class="hidden-xs">{{$dashboardDataArray["startDate"]}}</td>
											<td class="hidden-xs">{{$dashboardDataArray["totalPorseshnameh"]}}</td>
											<td>{{$dashboardDataArray["finishPorseshnamehCount"]}}</td>
											<td class="hidden-xs">{{$dashboardDataArray["reviewPorseshnamehCount"]}}</td>
											<td class="hidden-xs">{{$dashboardDataArray["porseshgarCount"]}}</td>
											<td class="hidden-xs">{{$dashboardDataArray["projectPercent"]}}</td>
											<td class=" text-center" style="text-align: center">
												<a class="handcursor" target="_self"
												   href="{{url("admin/selectedProject",["idP"=>$dashboardDataArray["id"]])}}" {{--onclick="getProjectRowValueByAjaxForUpdate({{$allProject["id"]}})"--}}>
													<img title="بیشتر"
														 src="{{public_url("./images/more.png")}}"/></a>

											</td>
										</tr>
									@endforeach
									</tbody>
								</table>
							</div>
							<!-- /.table-responsive -->
						</div>
					</div>
				</div>
				@if(authUser("level")!="projectAdmin")
					{{--کاربران شرکت--}}
					<div class="col-md-12">
						<!-- USERS LIST -->
						<div class="box box-success">
							<div class="box-header with-border">
								<h3 class="box-title">کاربران شرکت</h3>

								<div class="box-tools pull-right">
									<span class="label label-success">وضعیت کاربران: فعال</span>
									<button type="button" class="btn btn-box-tool" data-widget="collapse"><i
												class="fa fa-minus"></i>
									</button>
									<button type="button" class="btn btn-box-tool" data-widget="remove"><i
												class="fa fa-times"></i>
									</button>
								</div>
							</div>
							<!-- /.box-header -->
							<div class="box-body no-padding">
								<ul class="users-list clearfix">
									@foreach($companyUsers as $companyUser)
										<li>
											<img class="W100100"
												 src="{{public_url("/imagesUpload/Profiles/").$companyUser["id"]}}.jpeg?"
												 onerror="this.onerror=null;this.src='{{public_url("/imagesUpload/Profiles/Def.jpg")}}'"
												 alt="تصویر کاربر">
											<a class="users-list-name"
											   href="{{url("Users/moreManageUsers",["id"=>$companyUser["id"]])}}">{{$companyUser["name"]." ".$companyUser["fName"]}}</a>
											<span class="users-list-date">
                                        @if($companyUser["level"]=="admin")
													مدیر
												@elseif($companyUser["level"]=="porseshgar")
													پرسشگر
												@elseif($companyUser["level"]=="adminProject")
													مدیر پروژه
												@endif
                                    </span>
										</li>
									@endforeach
								</ul>
								<!-- /.users-list -->
							</div>
							<!-- /.box-body -->

							<div class="box-footer text-center">
								<a href="{{url("Users/manageUsers",["active"=>"mU"])}}" class="uppercase">نمایش همه
									کاربران</a>
							</div>
							<!-- /.box-footer -->
						</div>
						<!--/.box -->
					</div>   <!-- /.col -->
				@endif
			</div>
		</div>

@endsection
