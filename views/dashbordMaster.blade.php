<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- jvectormap -->
    <link rel="stylesheet" href="{{public_url("./BrowseFromBower/jvectormap/jquery-jvectormap.css")}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet"  href="{{public_url("./dist/css/skins/_all-skins.min.css")}} ">
    <link rel="stylesheet" href="{{public_url("./BrowseFromBower/Ionicons/css/ionicons.min.css")}}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
{{--    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">--}}

    {{--jQ--}}


    <script src="{{public_url("./BrowseFromBower/jquery/dist/jquery.min.js")}}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{public_url("./BrowseFromBower/jquery-ui/jquery-ui.min.js")}}"></script>
    <script src="{{public_url("./BrowseFromBower/jquery-ui/jquery-ui.js")}}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- FastClick -->
    <script src="{{public_url("./BrowseFromBower/fastclick/lib/fastclick.js")}}"></script>
    <!-- Sparkline -->
    <script src="{{public_url("./BrowseFromBower/jquery-sparkline/dist/jquery.sparkline.min.js")}}"></script>
    <!-- jvectormap  -->
    <script src="{{public_url("./plugins/jvectormap/jquery-jvectormap-1.2.2.min.js")}}"></script>
    <script src="{{public_url("./plugins/jvectormap/jquery-jvectormap-world-mill-en.js")}}"></script>

    <!-- SlimScroll -->
    <script src="{{public_url("./BrowseFromBower/jquery-slimscroll/jquery.slimscroll.min.js")}}"></script>
    <!-- ChartJS -->
    <script src="{{public_url("./BrowseFromBower/charts/Chart.js")}}"></script>

    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="{{public_url("./dist/js/pages/dashboard2.js")}}"></script>
    <script src="{{public_url("./dist/js/pages/dashboard.js")}}"></script>

    <!-- AdminLTE for demo purposes -->
    <script src="{{public_url("./dist/js/demo.js")}}"></script>

</head>
