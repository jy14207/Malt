{{--@extends("master")--}}
@extends("porseshgarSideMasterBefor")

@section("content")
    <title>پروژه های فعال</title>
    <div class="row top10">
        <div class="col-sm-12">
            <div class="list-group">
                <a class="list-group-item active">لیست پروژه های فعال</a>
                @foreach($project as $p)
                    <a class="list-group-item" href="{{url("porseshname/porseshnameFormView",["idProject"=>$p['id']])}}"> {{ $p["subject"]}}</a>
                @endforeach
            </div>
        </div>
        <div class="col-md-2"></div>
    </div>
@endsection