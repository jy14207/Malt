<html>
<head>
    <link rel="stylesheet" href="{{ public_url("./css/css/bootstrap.css") }}"/>
    <link rel="stylesheet" href="{{ public_url("./css/css/bootstrap-rtl.css") }}"/>
    <link rel="stylesheet" href="{{ public_url("./css/css/bootstrap-theme.css") }}"/>
    <link rel="stylesheet" href="{{ public_url("./css/css/mycss.css") }}"/>
    <link rel="stylesheet" href="{{public_url("./css/fonts/css.css")}}">
    <link rel="stylesheet" href="{{public_url("./css/css/datepicker.css")}}">
    <link rel="stylesheet" href="{{public_url("./css/css/style.css")}}">
    <link rel="stylesheet" href="{{public_url("./css/css/sweetalert.css")}}">
    <link rel="icon" type="image/png" href="{{public_url("./images/coImages/fivicon.png")}}">

    <script>
        var URL_ROOT = "{{ URL_ROOT }}";
    </script>
    <script type="text/javascript" src="{{public_url("./js/jquery-3.1.1.min.js")}}"></script>
    <script type="text/javascript" src="{{public_url("./js/jquery.ui.datepicker-cc.all.min.js")}}"></script>
    <script type="text/javascript" src="{{public_url("./css/js/bootstrap.js")}}"></script>
    <script type="text/javascript" src="{{public_url("./js/myscript.js")}}"></script>
    <script type="text/javascript" src="{{public_url("./js/Ajax/ajaxFunction.js")}}"></script>
    <script type="text/javascript" src="{{public_url("./js/sweetalert.min.js")}}"></script>
    <script type="text/javascript" src="{{public_url("./js/sweetalert-dev.js")}}"></script>
</head>
</html>
@yield("content")
<hr>
<div id="footer" class="row" style="text-align: center">
<div class="col-sm-8 col-sm-offset-2">
    <div class="col-sm-4">
        <div class="alert alert-warning small">
            <a href="{{url("projects/index")}}"></a>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="alert alert-danger small">
            <a href="{{url("login/logout")}}">خروج</a>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="alert alert-info small ">
            بیشتر بدانید <strong> <a href="http://amarnegarshargh.ir/" target="_blank">آمار نگر شرق</a></strong>
        </div>
    </div>
</div>
</div>
