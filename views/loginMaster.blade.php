<html>
<head>
{{--    <link rel="stylesheet" href="{{ public_url("./css/css/bootstrap.css") }}"/>--}}
{{--    <link rel="stylesheet" href="{{ public_url("./css/css/bootstrap-rtl.css") }}"/>--}}
{{--    <link rel="stylesheet" href="{{ public_url("./css/css/bootstrap-theme.css") }}"/>--}}
    <link rel="stylesheet" href="{{ public_url("./css/css/mycss.css") }}"/>
    <link rel="stylesheet" href="{{public_url("./css/fonts/css.css")}}">
    <link rel="stylesheet" href="{{public_url("./css/css/datepicker.css")}}">
    <link rel="stylesheet" href="{{public_url("./css/css/style.css")}}">
    <link rel="stylesheet" href="{{public_url("./css/css/sweetalert.css")}}">
    <link rel="icon" type="image/png" href="{{public_url("./images/coImages/fivicon.png")}}">
    <script>
        var URL_ROOT="{{ URL_ROOT }}";
    </script>
{{--    <script type="text/javascript" src="{{public_url("./js/jquery-3.1.1.min.js")}}"></script>--}}
{{--    <script type="text/javascript" src="{{public_url("./js/jquery.ui.datepicker-cc.all.min.js")}}"></script>--}}
{{--    <script type="text/javascript" src="{{public_url("./css/js/bootstrap.js")}}"></script>--}}
    <script src="{{public_url("./BrowseFromBower/jquery/dist/jquery.min.js")}}"></script>
    <script src="{{public_url("./BrowseFromBower/bootstrap/dist/js/bootstrap.min.js")}}"></script>
    <script src="{{public_url("./dist/js/adminlte.min.js")}}"></script>
    <script type="text/javascript" src="{{public_url("./js/myscript.js")}}"></script>
    <script type="text/javascript" src="{{public_url("./js/Ajax/ajaxFunction.js")}}"></script>
    <script type="text/javascript" src="{{public_url("./js/sweetalert.min.js")}}"></script>
    <script type="text/javascript" src="{{public_url("./js/sweetalert-dev.js")}}"></script>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{public_url("./dist/css/bootstrap-theme.css")}}">
    <link rel="stylesheet" href="{{public_url("./dist/css/rtl.css")}}">
    <link rel="stylesheet" href="{{public_url("./BrowseFromBower/font-awesome/css/font-awesome.min.css")}}">
    <link rel="stylesheet" href="{{public_url("./BrowseFromBower/Ionicons/css/ionicons.min.css")}}">
    <link rel="stylesheet" href="{{public_url("./dist/css/AdminLTE.css")}}">
    <link rel="stylesheet" href="{{public_url("./dist/css/skins/skin-blue.min.css")}}">
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <script type="text/javascript" src="{{public_url("./js/jquery.table2excel.js")}}"></script>
    <script type="text/javascript" src="{{public_url("./js/arrayToTable.js")}}"></script>

    {{--<link rel="stylesheet"--}}
          {{--href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">--}}


</head>
</html>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <header class="main-header">
        <!-- Logo -->
        <a href="{{url("admin/index")}}"  class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini">ورود</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><small><b> طراحی آنلاین پرسشنامه</b></small></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">ورود</span>
            </a>
        </nav>
    </header>
    <aside class="main-sidebar">
        <section class="sidebar">
            <div class="user-panel">
                <div class="pull-right image">
                    {{--<img src="{{public_url("./images/icon/amarNegar.png")}}" class="img-circle" alt="User Image">--}}
                </div>
                <div class="pull-right info">
                    {{--<p>آمار نگر</p>--}}
                    {{--<a href="#"><i class="fa fa-circle text-success"></i> آنلاین</a>--}}
                </div>
            </div>


            <ul class="sidebar-menu" data-widget="tree">
                {{--<li class="header">تیتر</li>--}}
                <li class="@if(_get("active")=="mainPage") {{"active"}}@endif"><a href="{{url("login/form",["active"=>"mainPage"])}}"><i class="fa fa-lock"></i> <span>ورود</span></a></li>
                <li class="@if(_get("active")=="manageP") {{"active"}}@endif" ><a href="{{url("login/registerForm",["active"=>"manageP"])}}"><i class="fa fa-user-plus"></i> <span>ثبت نام</span></a></li>
                {{--<li class="@if(_get("active")=="activeP") {{"active"}}@endif"><a href="{{url("admin/activeProjects",["active"=>"activeP"])}}"><i class="fa fa-table"></i> <span>پروژه های فعال</span></a></li>--}}
                <li class="@if(_get("active")=="help") {{"active"}}@endif"><a href="{{url("admin/help",["active"=>"help"])}}"><i class="fa fa-question-circle"></i> <span>راهنما</span></a></li>
                {{--<li class="@if(_get("active")=="rep") {{"active"}}@endif"><a href="{{url("admin/viewActiveProjectListForReports",["active"=>"rep"])}}"><i class="fa fa-files-o"></i> <span>سوالات متدوال</span></a></li>--}}
                {{--<li class="@if(_get("active")=="dash") {{"active"}}@endif"><a href="{{url("admin/underDesign",["active"=>"dash"])}}"><i class="fa fa-dashboard"></i> <span>تماس با ما</span></a></li>--}}
                {{--<li><a href="{{url("reports/createDtatamainingTable")}}"><i class="fa fa-dashboard"></i> <span>ساخت جدول</span></a></li>--}}
                {{--<li><a href="{{url("reports/databaseTemplateReport",["idP"=>_get("idP")])}}"><i class="fa fa-dashboard"></i> <span>Database Template</span></a></li>--}}
                {{--<li class="treeview">--}}
                    {{--<a href="#"><i class="fa fa-gears"></i> <span>تنظیمات</span>--}}
                        {{--<span class="pull-left-container">--}}
                {{--<i class="fa fa-angle-right pull-left"></i>--}}

                {{--</span>--}}
                    {{--</a>--}}
                    {{--<ul class="treeview-menu">--}}
                        {{--<li><a href="#">پروفایل</a></li>--}}
                        {{--<li><a href="#">...</a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
            </ul>
        </section>
    </aside>
    <div class="content-wrapper">
        <section class="content-header">
            @yield("content")
        </section>
    </div>
    <footer class="main-footer text-rightgth">
        <strong>بیشتر بدانید <a href="http://ARADEBARTAR.IR" target="_blank">آراد برتر</a></strong>
    </footer>
    <aside class="control-sidebar control-sidebar-dark">
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
            <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
            <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="control-sidebar-home-tab">
                <h3 class="control-sidebar-heading">فعالیت ها</h3>
                <ul class="control-sidebar-menu">
                    <li>
                        <a href="javascript:;">
                            <i class="menu-icon fa fa-birthday-cake bg-red"></i>

                            <div class="menu-info">
                                <h4 class="control-sidebar-subheading">درحال طراحی</h4>

                                <p>۲۴ بهمن 96</p>
                            </div>
                        </a>
                    </li>
                </ul>
                <!-- /.control-sidebar-menu -->

                <h3 class="control-sidebar-heading">پیشرفت کارها</h3>
                <ul class="control-sidebar-menu">
                    <li>
                        <a href="javascript:;">
                            <h4 class="control-sidebar-subheading">
                                 پروژه پرسشنامه
                                <span class="pull-left-container">
                    <span class="label label-danger pull-left">60%</span>
                  </span>
                            </h4>

                            <div class="progress progress-xxs">
                                <div class="progress-bar progress-bar-danger" style="width: 60%"></div>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="tab-pane" id="control-sidebar-stats-tab">تب وضعیت</div>
            <div class="tab-pane" id="control-sidebar-settings-tab">
                <form method="post">
                    <h3 class="control-sidebar-heading">تنظیمات عمومی</h3>
                    <div class="form-group">
                        <label class="control-sidebar-subheading">
                            گزارش کنترلر پنل
                            <input type="checkbox" class="pull-left" checked>
                        </label>

                        <p>
                            ثبت تمامی فعالیت های مدیران
                        </p>
                    </div>
                </form>
            </div>
        </div>
    </aside>
    <div class="control-sidebar-bg"></div>
</div>
</body>
