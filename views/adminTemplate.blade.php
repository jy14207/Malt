<html>
<head>
    <link rel="stylesheet" href="{{ public_url("./css/css/bootstrap.css") }}" />
    <link rel="stylesheet" href="{{ public_url("./css/css/bootstrap-rtl.css") }}" />
    <link rel="stylesheet" href="{{ public_url("./css/css/bootstrap-theme.css") }}" />
    <link rel="stylesheet" href="{{ public_url("./css/css/mycss.css") }}" />
    <link rel="stylesheet" href="{{public_url("./css/fonts/css.css")}}">
    <link rel="stylesheet" href="{{public_url("./css/css/datepicker.css")}}">
    <link rel="stylesheet" href="{{public_url("./css/css/style.css")}}">
    <link rel="stylesheet" href="{{public_url("./css/css/sweetalert.css")}}">
    <link rel="stylesheet" href="{{public_url("./css/css/adminTemplatecss/morris.css")}}">
    <link rel="stylesheet" href="{{public_url("./css/css/adminTemplatecss/sb-admin.css")}}">
    <link rel="stylesheet" href="{{public_url("./css/css/adminTemplatecss/sb-admin-rtl.css")}}">
    <link rel="icon" type="image/png" href="{{public_url("./images/coImages/fivicon.png")}}">
    <script>
        var URL_ROOT="{{ URL_ROOT }}";
    </script>

{{--    <script type="text/javascript" src="{{public_url("./js/adminTemplateJS/morris/jquery.babypaunch.spinner.js")}}"></script>--}}
{{--    <script type="text/javascript" src="{{public_url("./js/adminTemplateJS/morris/jquery.babypaunch.spinner.min.js")}}"></script>--}}
    <script type="text/javascript" src="{{public_url("./js/jquery-3.1.1.min.js")}}"></script>
    <script type="text/javascript" src="{{public_url("./js/jquery.ui.datepicker-cc.all.min.js")}}"></script>
    <script type="text/javascript" src="{{public_url("./css/js/bootstrap.js")}}"></script>
    <script type="text/javascript" src="{{public_url("./js/myscript.js")}}"></script>
    <script type="text/javascript" src="{{public_url("./js/jquery.table2excel.js")}}"></script>
    <script type="text/javascript" src="{{public_url("./js/Ajax/ajaxFunction.js")}}"></script>
    <script type="text/javascript" src="{{public_url("./js/sweetalert.min.js")}}"></script>
    <script type="text/javascript" src="{{public_url("./js/sweetalert-dev.js")}}"></script>
    <script type="text/javascript" src="{{public_url("./js/adminTemplateJS/morris/raphael.min.js")}}"></script>
    <script type="text/javascript" src="{{public_url("./js/adminTemplateJS/morris/morris.min.js")}}"></script>
    <script type="text/javascript" src="{{public_url("./js/adminTemplateJS/morris/morris-data.js")}}"></script>


</head>
</html>
<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../pages/home.aspx">آمار نگر/مدیر</a>
        </div>
        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav">
            <!--<li class="dropdown">
                 <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i><b class="caret"></b></a>
                 <ul class="dropdown-menu message-dropdown">
                     <li class="message-preview">
                         <a href="#">
                             <div class="media">
                                 <span class="pull-left">
                                     <img class="media-object" src="http://placehold.it/50x50" alt="">
                                 </span>
                                 <div class="media-body">
                                     <h5 class="media-heading"><strong>نام کاربر</strong>
                                     </h5>
                                     <p class="small text-muted"><i class="fa fa-clock-o"></i>Yesterday at 4:32 PM</p>
                                     <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                 </div>
                             </div>
                         </a>
                     </li>
                     <li class="message-preview">
                         <a href="#">
                             <div class="media">
                                 <span class="pull-left">
                                     <img class="media-object" src="http://placehold.it/50x50" alt="">
                                 </span>
                                 <div class="media-body">
                                     <h5 class="media-heading"><strong>John Smith</strong>
                                     </h5>
                                     <p class="small text-muted"><i class="fa fa-clock-o"></i>Yesterday at 4:32 PM</p>
                                     <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                 </div>
                             </div>
                         </a>
                     </li>
                     <li class="message-preview">
                         <a href="#">
                             <div class="media">
                                 <span class="pull-left">
                                     <img class="media-object" src="http://placehold.it/50x50" alt="">
                                 </span>
                                 <div class="media-body">
                                     <h5 class="media-heading"><strong>John Smith</strong>
                                     </h5>
                                     <p class="small text-muted"><i class="fa fa-clock-o"></i>Yesterday at 4:32 PM</p>
                                     <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                 </div>
                             </div>
                         </a>
                     </li>
                     <li class="message-footer">
                         <a href="#">Read All New Messages</a>
                     </li>
                 </ul>
             </li>
             <li class="dropdown">
                 <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i><b class="caret"></b></a>
                 <ul class="dropdown-menu alert-dropdown">
                     <li>
                         <a href="#">Alert Name <span class="label label-default">Alert Badge</span></a>
                     </li>
                     <li>
                         <a href="#">Alert Name <span class="label label-primary">Alert Badge</span></a>
                     </li>
                     <li>
                         <a href="#">Alert Name <span class="label label-success">Alert Badge</span></a>
                     </li>
                     <li>
                         <a href="#">Alert Name <span class="label label-info">Alert Badge</span></a>
                     </li>
                     <li>
                         <a href="#">Alert Name <span class="label label-warning">Alert Badge</span></a>
                     </li>
                     <li>
                         <a href="#">Alert Name <span class="label label-danger">Alert Badge</span></a>
                     </li>
                     <li class="divider"></li>
                     <li>
                         <a href="#">View All</a>
                     </li>
                 </ul>
             </li>-->
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="#"><i class="fa fa-fw fa-user"></i>پروفایل</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-fw fa-gear"></i>تنظیمات</a>
                    </li>
                    <li class="divider"></li>
                    <li>

                        <a href="{{url("login/logout")}}"><i class="fa fa-fw fa-power-off"></i>خروج</a>
                    </li>
                </ul>
            </li>
        </ul>
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li class="active"><a href="{{url("admin/index")}}"><i class="fa fa-fw fa-edit"></i>صفحه اصلی</a></li>
                <li><a href="{{url("admin/allProjects")}}"><i class="fa fa-fw fa-edit"></i>مدیریت پروژه ها</a></li>
                <li><a href="{{url("admin/activeProjects")}}"><i class="fa fa-fw fa-edit"></i>پروژه های فعال</a></li>
                <li><a href="{{url("Users/manageUsers")}}"><i class="fa fa-fw fa-table"></i>مدیریت کاربران(پرسشگران)</a></li>
                <li><a href="{{url("admin/viewActiveProjectListForReports")}}"><i class="fa fa-fw fa-bar-chart-o"></i>گزارشات</a></li>
                <li><a href="{{url("admin/underDesign")}}"><i class="fa fa-fw fa-dashboard"></i>داشبورد مدیریتی</a></li>


            </ul>
        </div>
    </nav>
    <div id="page-wrapper">
        <hr>
        @yield("content")
        <hr>
    </div>
</div>

<div id="footer" class="row"  style="text-align: center">
    <h3>Amar Negar</h3>
</div>
