<html>
<head>
    <link rel="stylesheet" href="{{ public_url("./css/css/mycss.css") }}"/>

    <link rel="stylesheet" href="{{ public_url("./css/css/jquerysctipttop.css") }}"/>
    <link rel="stylesheet" href="{{ public_url("./css/css/normalize.min.css") }}"/>
    <link rel="stylesheet" href="{{ public_url("./css/css/preloader.css") }}"/>

    <link rel="stylesheet" href="{{public_url("./css/fonts/css.css")}}">
    {{--    <link rel="stylesheet" href="{{public_url("./css/css/datepicker.css")}}">--}}
    <link rel="stylesheet" href="{{public_url("./css/css/style.css")}}">
    <link rel="stylesheet" href="{{public_url("./css/css/sweetalert.css")}}">
    <link rel="icon" type="image/png" href="{{public_url("./images/coImages/fivicon.png")}}">
    <script>
        var URL_ROOT = "{{ URL_ROOT }}";
    </script>
    {{--    <script type="text/javascript" src="{{public_url("./js/jquery-3.1.1.min.js")}}"></script>--}}
    {{--    <script type="text/javascript" src="{{public_url("./js/jquery.ui.datepicker-cc.all.min.js")}}"></script>--}}
    {{--    <script type="text/javascript" src="{{public_url("./css/js/bootstrap.js")}}"></script>--}}
    <script src="{{public_url("./BrowseFromBower/jquery/dist/jquery.min.js")}}"></script>
    <script src="{{public_url("./BrowseFromBower/bootstrap/dist/js/bootstrap.min.js")}}"></script>
    <script src="{{public_url("./dist/js/adminlte.min.js")}}"></script>
    <script src="{{public_url("./dist/js/persian-datepicker-0.4.5.min.js")}}"></script>
    <script type="text/javascript" src="{{public_url("./js/myscript.js")}}"></script>
    <script type="text/javascript" src="{{public_url("./js/Ajax/ajaxFunction.js")}}"></script>
    <script type="text/javascript" src="{{public_url("./js/sweetalert.min.js")}}"></script>
    <script type="text/javascript" src="{{public_url("./js/sweetalert-dev.js")}}"></script>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{public_url("./dist/css/bootstrap-theme.css")}}">
    <link rel="stylesheet" href="{{public_url("./dist/css/rtl.css")}}">
    <link rel="stylesheet" href="{{public_url("./BrowseFromBower/font-awesome/css/font-awesome.min.css")}}">
    <link rel="stylesheet" href="{{public_url("./BrowseFromBower/Ionicons/css/ionicons.min.css")}}">
    <link rel="stylesheet" href="{{public_url("./dist/css/AdminLTE.css")}}">
    <link rel="stylesheet" href="{{public_url("./dist/css/skins/skin-blue.min.css")}}">
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <script type="text/javascript" src="{{public_url("./js/jquery.table2excel.js")}}"></script>
    <script type="text/javascript" src="{{public_url("./js/arrayToTable.js")}}"></script>
    <script type="text/javascript" src="{{public_url("./js/jquery.preloader.min.js")}}"></script>

    {{--<link rel="stylesheet"--}}
    {{--href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">--}}


</head>
</html>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <header class="main-header">
        <!-- Logo -->
        <a href="{{url("admin/index")}}" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini">پنل</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>نام پروژه</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="{{public_url("./imagesUpload/Profiles/"). authUser("id").".jpeg"}}"
                                 onerror="this.onerror=null;this.src='{{public_url("/imagesUpload/Profiles/Def.jpg")}}'"
                                 class="user-image"
                                 alt="User Image">
                            <span class="hidden-xs">{{ authUser("name")." ".authUser("fName") }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="{{public_url("./imagesUpload/Profiles/"). authUser("id").".jpeg"}}"
                                     onerror="this.onerror=null;this.src='{{public_url("/imagesUpload/Profiles/Def.jpg")}}'"
                                     class="img-circle"
                                     alt="User Image">

                                <p>
                                    {{ authUser("name")." ".authUser("fName") }}
                                    <small>
                                        @if(authUser("level")=="admin")  مدیر   @endif
                                        @if(authUser("level")=="adminProject")  مدیر پروژه   @endif
                                        @if(authUser("level")=="porseshgar")  پرسشگر   @endif
                                    </small>
                                </p>
                            </li>
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="{{--{{url("Users/moreManageUsers",["id"=> authUser("id")])}}--}}"
                                       class="btn btn-default btn-flat"><i class="fa fa-fw fa-user-md"></i>پروفایل</a>
                                </div>
                                <div class="pull-right">
                                    <a href="{{url("login/logout")}}" class="btn btn-default btn-flat"><i
                                                class="fa fa-fw fa-power-off"></i>خروج</a>

                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <aside class="main-sidebar">
        <section class="sidebar">

            <ul class="sidebar-menu" data-widget="tree">
                {{--<li class="header">تیتر</li>--}}
                <li class="@if(_get("active")=="help") {{"active"}}@endif top10"><a
                            href="{{url("projects/index")}}"><i class="fa fa-list"></i> <span>انتخاب پروژه دیگر</span></a>
                </li>
                <li class="@if(_get("active")=="help") {{"active"}}@endif top10"><a href="{{url("Question/qList")}}"><i class="fa fa-list"></i> <span>لیست سوالات</span></a></li>
            </ul>
        </section>
    </aside>
    <div class="content-wrapper">
        <section class="content-header">
            @yield("content")
        </section>
    </div>
    <footer class="main-footer text-rightgth">
        <strong>بیشتر بدانید <a href="http://amarnegarshargh.ir/" target="_blank">آمار نگر شرق</a></strong>
    </footer>
</div>
</body>
