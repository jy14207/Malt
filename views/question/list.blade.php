{{--@extends("master")--}}
@extends("porseshgarSideMasterBefor")

@section("content")
    <title>پروژه های فعال</title>
    <div class="alert alert-success ">
        <div class="col-sm-11"> پروژه :<strong>{{$projectInfo["subject"]}}</strong></div>
        <a class="btn btn-info goBack" >برگشت</a>
    </div>
    <div class="row top10 centerAlign">
        <div class="col-sm-12">
                <table class="reportHTMLTables table table-bordered margintop5 pre-scrollable">
                    <thead>
                    <tr>
                        <th class="text-center">ردیف</th>
                        <th class="text-center hidden-xs">کد</th>
                        <th class="text-center">متن</th>
                        <th class="text-center">پیش نمایش</th>

                    </tr>
                    </thead>
                    <tbody>

                    @foreach($QuestionList as $QuestionLista)
                        <tr>
                            <td class="text-center"> {{$QuestionLista["rowNumber"]}}</td>
                            <td class="text-center hidden-xs"> {{$QuestionLista["questionCode"]}}</td>
                            <td class="text-right"> {{$QuestionLista["text"]}}</td>
                            <td class=" text-center hidden-xs">
                                <a class="handcursor" target="_blank"
                                   href="{{url("projects/loadQuestionPreviewAdmin",["rowNum"=>$QuestionLista["rowNumber"]])}}" {{--onclick="getProjectRowValueByAjaxForUpdate({{$allProject["id"]}})"--}}>
                                    <img title="پیش نمایش سوال در سمت پرسشگر"
                                         src="{{public_url("./images/PreviewQuestion.png")}}"/></a>
                            </td>
                            <td style="visibility: hidden"></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
        </div>
        <div class="col-md-2"></div>
    </div>
@endsection