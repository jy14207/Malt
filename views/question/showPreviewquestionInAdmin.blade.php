@extends("porseshgarSideMasterBefor")
@section("content")
    <title>پیش نمایش سوال</title>
    <div class="row padding5">
        <div class="col-lg-12 ">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="panel-title">
                        <strong style="font-size: x-large">{{_get("rowNum")."-"." "}}{{$questionRow[0]["text"]}}</strong>
                    </div>
                </div>
                <div onload="t(3)" style="padding-top:30px" class="panel-body">
                    <div style="display:none" class="alert alert-danger col-md-12"></div>
                    <form class="question_form">
                        @if($questionType["questionType"]=="multipleChoice")
                            @foreach($questionRow as $question)
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>
                                            <input
                                                    type="checkbox" class="check" name="answer[]"
                                                    value="{{$questionRow[$loop->index]["idOption"]}}">
                                            {{$questionRow[$loop->index]["opTextOption"]}}
                                        </label>
                                        <small style="color: red">{{$questionRow[$loop->index]["opMoreExplanation"]}}</small>
                                    </div>
                                    <div class="col-sm-6 buttom10">
                                        @if($questionRow[$loop->index]["opHaveImage"]=="Yes")
                                            <div id="myModal" class="modal">
                                                <span class="close">&times;</span>
                                                <img class="modal-content" id="img01">
                                                <div id="caption"></div>
                                            </div>
                                            <img class="ShowQandOPimageinporseshgarside"
                                                 id="{{$questionRow[$loop->index]["idOption"]}}"
                                                 src="{{public_url("imagesUpload/options/").$questionRow[$loop->index]["idOption"].".jpeg?".rand(0,123654789)}}">
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        @endif
                        @if($questionType["questionType"]=="multipleChoiceOthers")
                            @foreach($questionRow as $question)
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>
                                            <input data-label="{{$questionRow[$loop->index]["opTextOption"]}}"
                                                   type="checkbox" class="check" name="answer[]"
                                                   value="{{$questionRow[$loop->index]["idOption"]}}">
                                            @if($questionRow[$loop->index]["opTextOption"]=="Other") {{"سایر"}}  @else {{$questionRow[$loop->index]["opTextOption"]}} @endif
                                        </label>
                                        <small style="color: red">{{$questionRow[$loop->index]["opMoreExplanation"]}}</small>
                                    </div>
                                    <div class="col-sm-6 buttom10">
                                        @if($questionRow[0]["haveImage"]=="Yes")
                                            <div id="myModal" class="modal">
                                                <span class="close">&times;</span>
                                                <img class="modal-content" id="img01">
                                                <div id="caption"></div>
                                            </div>
                                            <img class="ShowQandOPimageinporseshgarside"
                                                 id="{{$questionRow[$loop->index]["idOption"]}}"
                                                 src="{{public_url("imagesUpload/options/").$questionRow[$loop->index]["idOption"].".jpeg?".rand(0,123456789)}}">
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                            <div class="row">
                                <div class="col-sm-6">
                                    <input type="text" class="form-control required borderInsect" name="others"
                                           id="questionAnswer" style="display: none;">
                                    <input type="hidden" id="tempquestionAnswer" value="">
                                </div>

                            </div>

                        @endif
                        @if($questionType["questionType"]=="singleResponse")
                            <div class="row">
                                @foreach($questionRow as $question)
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>
                                                <input type="radio" name="answer"
                                                       value="{{$questionRow[$loop->index]["idOption"]}}">
                                                <span></span>
                                                {{$questionRow[$loop->index]["opTextOption"]}}
                                            </label>
                                            <small style="color: red">{{$questionRow[$loop->index]["opMoreExplanation"]}}</small>
                                        </div>
                                        <div class="col-sm-6 buttom10">
                                            @if($questionRow[$loop->index]["opHaveImage"]=="Yes")
                                                <div id="myModal" class="modal">
                                                    <span class="close">&times;</span>
                                                    <img class="modal-content" id="img01">
                                                    <div id="caption"></div>
                                                </div>
                                                <img class="ShowQandOPimageinporseshgarside"
                                                     id="{{$questionRow[$loop->index]["idOption"]}}"
                                                     src="{{public_url("imagesUpload/options/").$questionRow[$loop->index]["idOption"].".jpeg?".rand(0,123456789)}}">
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                        @if($questionType["questionType"]=="singleResponseOthers")
                            <input type="hidden" name="idOption" value="{{$questionRow[0]["idOption"]}}">
                            @foreach($questionRow as $question)
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>
                                            <input type="radio" id="{{$questionRow[$loop->index]["opTextOption"]}}"
                                                   data-label="{{$questionRow[$loop->index]["opTextOption"]}}"
                                                   name="answer"
                                                   value="{{$questionRow[$loop->index]["idOption"]}}">
                                            @if($questionRow[$loop->index]["opTextOption"]=="Other") {{"سایر"}}  @else {{$questionRow[$loop->index]["opTextOption"]}} @endif
                                        </label>
                                        <small style="color: red">{{$questionRow[$loop->index]["opMoreExplanation"]}}</small>
                                    </div>
                                    <div class="col-sm-6 buttom10">
                                        @if($questionRow[$loop->index][14]=="Yes")
                                            <div id="myModal" class="modal">
                                                <span class="close">&times;</span>
                                                <img class="modal-content" id="img01">
                                                <div id="caption"></div>
                                            </div>
                                            <img class="ShowQandOPimageinporseshgarside"
                                                 id="{{$questionRow[$loop->index]["opHaveImage"]}}"
                                                 src="{{public_url("imagesUpload/options/").$questionRow[$loop->index]["idOption"].".jpeg?".rand(0,123456789)}}">
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                            <div class="row">
                                <div class="col-sm-6">
                                    <input type="text" class="form-control required borderInsect" name="others"
                                           id="questionAnswer" style="display: none;">
                                    <input type="hidden" id="tempquestionAnswer" value="">
                                </div>
                                <div class="col-sm-6"></div>
                            </div>
                        @endif
                        @if($questionType["questionType"]=="openQuestion")
                            <div class="row">
                                <div class="col-sm-3">
                                    <textarea name="answer" class="required focus borderInsect"
                                              id="questionAnswer" rows="4"></textarea>
                                </div>
                                <div class="col-sm-6 col-sm-offset-2 centerAlign">
                                    @if($questionRow[0]["haveImage"]=="Yes")
                                        <div id="myModal" class="modal">
                                            <span class="close">&times;</span>
                                            <img class="modal-content" id="img01">
                                            <div id="caption"></div>
                                        </div>
                                        <img class="ShowQandOPimageinporseshgarside"
                                             id="{{$questionRow[0]["id"]}}"
                                             src="{{public_url("imagesUpload/question/").$questionRow[0]["id"].".jpeg?".rand(0,1236598754)}}">
                                    @endif
                                </div>
                            </div>
                        @endif
                        @if($questionType["questionType"]=="numeric")
                            <div class="row">
                                <input type="number" min="{{$questionRow[0]["minNumberType"]}}"
                                       max="{{$questionRow[0]["maxNumberType"]}}" name="answer"
                                       class="required focus borderInsect ltrDir"
                                       id="questionAnswer" value=""/>
                            </div>
                        @endif
                        @if($questionType["questionType"]=="email")
                            <div class="row">
                                <input type="email" name="answer" class="required focus borderInsect ltrDir"
                                       id="questionAnswer" value=""/>
                            </div>
                        @endif
                        @if($questionType["questionType"]=="websitelink")

                            <div class="row">

                                <input type="url" name="answer" class="required focus borderInsect ltrDir"

                                       id="questionAnswer" value=""/>

                            </div>

                        @endif
                        @if($questionType["questionType"]=="noResponse")

                            @if($questionRow[0]["haveImage"]=="Yes")

                                <div class="col-md-4">

                                    <div class="row top60 buttom60">

                                        <script>

                                            $("#QuestionText").prop("hidden", true);

                                            $("#QuestionImage").prop("hidden", true);

                                        </script>

                                        <div class="col-sm-12 alignCenter">

                                            <label class="redColor buttom10">این یک متن بدون پاسخ است:</label><br/>

                                            <strong style="font-size: xx-large">{{$questionRow[0]["text"]}}</strong>

                                        </div>

                                        <div class="col-sm-12 alignCenter ">


                                        </div>


                                    </div>

                                </div>

                                <div class="col-md-4">

                                    <div class="row buttom10">

                                        <div class="col-md-3 col-md-offset-8 buttom10">

                                            <div id="imagePreview" class="imagePreview"></div>

                                            <img name="img3"

                                                 src="{{public_url("imagesUpload/question/").$questionRow[0]["id"].".jpeg?".rand(0,987654125)}}"

                                                 class="imagePreviewOptionEdit" id="tempImg">

                                        </div>

                                    </div>


                                </div>

                            @else

                                <div class="row top60 buttom60">

                                    <script>

                                        $("#QuestionText").prop("hidden", true);

                                    </script>

                                    <div class="col-sm-12 alignCenter">

                                        <label class="redColor buttom10">این یک متن بدون پاسخ است:</label><br/>

                                        <strong style="font-size: xx-large">{{$questionRow[0]["text"]}}</strong>

                                    </div>

                                    <div class="col-sm-12 alignCenter ">


                                    </div>


                                </div>

                            @endif













                        @endif
                        @if($questionType["questionType"]=="spectral")
                                <link rel="stylesheet" href="{{public_url("./dist/rangslider/rangeslider.css")}}">
                                <script type="text/javascript" src="{{public_url("./dist/rangslider/rangeslider.js")}}"></script>
                                <script type="text/javascript" src="{{public_url("./dist/rangslider/func.js")}}"></script>


                                <div style="direction: ltr;">

                                <input type="range" min="0" max="{{$questionRow[0]["rangeValue"]}}" data-rangeslider
                                       value="">

                                <strong>
                                    <output></output>
                                </strong>

                            </div>

                            <input type="hidden" name="answer" class="required focus borderInsect"
                                   value=""

                                   id="questionAnswer"/>

                        @endif
                        @if($questionType["questionType"]=="degree")
                                <link rel="stylesheet" href="{{public_url("./css/css/star-rating-svg.css")}}">
                                <script src="{{public_url("./js/jquery.star-rating-svg.js")}}"></script>
                            <div class="row buttom10">
                                <div class="col-sm-12 alignCenter">
                                    <span class="my-rating-9 ltrDir" style="direction: ltr"></span>
                                    <span class="live-rating ltrDir hidden"></span>
                                    <input type="hidden" name="answer" class="required focus borderInsect"
                                           id="questionAnswer" />
                                </div>
                            </div>
                        @endif
                        @if($questionType["questionType"]=="prioritize")
                            <div class="jquery-script-clear"></div>
                            <div class="col-sm-12">
                                <link href='{{public_url("./css/css/src/draganddrop.css")}}' rel='stylesheet' type='text/css'>
                                <script type="text/javascript" src="{{public_url("./css/css/src/draganddrop.js")}}"></script>
                                <script type="text/javascript" src="{{public_url("./css/css/src/LiListScript.js")}}"></script>

                                    <ul class="list-group grouped col-xs-12 myu">
                                        @foreach($questionRow as $question)
                                            <li class="list-group-item myli"
                                                id="{{$questionRow[$loop->index]["idOption"]}}">
                                                <i class="fa fa-arrows-v"></i>&nbsp;&nbsp;{{$questionRow[$loop->index]["opTextOption"]}}
                                                <small style="color: red"> {{$questionRow[$loop->index]["opMoreExplanation"]}}</small>
                                            </li>
                                        @endforeach
                                    </ul>
                            </div>

                            <script type="text/javascript">


                                var _gaq = _gaq || [];

                                _gaq.push(['_setAccount', 'UA-36251023-1']);

                                _gaq.push(['_setDomainName', 'jqueryscript.net']);

                                _gaq.push(['_trackPageview']);


                                (function () {

                                    var ga = document.createElement('script');

                                    ga.type = 'text/javascript';

                                    ga.async = true;

                                    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';

                                    var s = document.getElementsByTagName('script')[0];

                                    s.parentNode.insertBefore(ga, s);

                                })();


                            </script>

                            <input type="hidden" id="questionAnswer" name="answer"

                                   value="">

                        @endif
                        @if($questionType["questionType"]=="openOptions")
                                <script src="{{public_url("./js/jquery.star-rating-svg.js")}}"></script>

                            @foreach($questionRow as $question)
                                <div class="row buttom10">
                                    <div class="col-sm-6">
                                        <label>
                                            <input
                                                type="checkbox" class="check" name="answer[]"
                                                value="{{$questionRow[$loop->index]["idOption"]}}"
                                                id="{{$questionRow[$loop->index]["idOption"]}}">
                                            {{$questionRow[$loop->index]["opTextOption"]}}
                                        </label>
                                        <input type="text" name="answerOption[]"
                                               id="answerOption{{$questionRow[$loop->index]["idOption"]}}"
                                               class="form-control" disabled="disabled">
                                        <small style="color: red">{{$questionRow[$loop->index]["opMoreExplanation"]}}</small>
                                    </div>

                                </div>
                            @endforeach
                        @endif
                        @if($questionType["questionType"]=="dateFormat")
                                <link rel="stylesheet" href="{{public_url("./dist/css/persian-datepicker-0.4.5.min.css")}}" />
                                <script src="{{public_url("./dist/js/persian-date.js")}}"></script>
                                <script src="{{public_url("./dist/js/persian-datepicker-0.4.5.min.js")}}"></script>
                            <script defer>

                                $(document).ready(function () {

                                    if ($("#qtempAnswer").val() == "") {
                                        $('.jdate').persianDatepicker({
                                            altField: '#tarikhAlt',
                                            altFormat: 'X',
                                            format: 'YYYY/MM/DD',
                                            observer: true,
                                            timePicker: {
                                                enabled: true
                                            },
                                        });
                                    }
                                    else {
                                        $('.jdate').persianDatepicker({
                                            altField: '#tarikhAlt',
                                            altFormat: 'X',
                                            format: 'YYYY/MM/DD',
                                            observer: true,
                                            timePicker: {
                                                enabled: true
                                            },
                                        });
                                        $('.jdate').val($("#qtempAnswer").val());
                                    }

                                });
                            </script>
                            <div class="col-sm-12 alignCenter buttom10">
                                <input type="text" name="answer" class="required focus borderInsect jdate ltrDir"
                                       autocomplete="off"
                                       id="questionAnswer"/>
                                <input type="hidden" id="qtempAnswer" value=""/>
                            </div>

                        @endif
                        <input type="hidden" id="questionType" name="questionType"
                               value="{{$questionType["questionType"]}}">

                        <input type="hidden" name="idQ" value="{{$questionRow[0][0]}}">

                        <input type="hidden" name="allowNull" class="allowNull"
                               value="{{$questionRow[0]["allowNull"]}}">

                        <div class="col-sm-12 centerAlign" style="padding: 5px;">
                            <input type="button" onclick="close_window();" value="بستن" class="btn btn-danger btn-sm">
                            <script>function close_window() {
                                    close();
                                }
                            </script>

                        </div>


                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>

        $(document).ready(function (e) {
            var questionType = $("input[name='questionType']").val();

            if (questionType == "openOptions") {
                idoptions = '';
                options = idoptions.split(',');
                textAnswer = '';
                $(options).each(function (index) {
                    idInput = '#answerOption' + this;
                    $(idInput).removeAttr('disabled');
                    $(idInput).val(textAnswer[index]);
                })
            }
        })
    </script>

@endsection