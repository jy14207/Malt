@extends("AdminLteMaster")
@section("content")
    <title>بازبینی سوال</title>

    <div class="row" style="padding: 5px;">
        <div class="col-lg-12 ">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="panel-title">
                        <strong style="font-size: x-large"> {{$questionsCount[0][0]}}{{"/"}}{{_get("rowNum")."-"." "}}{{$questionRow[0]["text"]}}</strong>
                        <small style="color: yellow">({{$questionRow[0][6]}})</small>
                        {{--</div>--}}
                        @if($questionRow[0][8]=="Yes")
                            <div id="myModal" class="modal">
                                <span class="close">&times;</span>
                                <img class="modal-content" id="img01">
                                <div id="capt98988ion"></div>
                            </div>
                            <img class="myImg" id="{{$questionRow[0][0]}}"
                                 {{--Get Img Question--}}
                                 src="{{public_url("imagesUpload/question/").$questionRow[0][0].".jpeg?".rand(0,3265478965)}}">
                        @endif
                    </div>
                </div>
                <div onload="t(3)" style="padding-top:30px" class="panel-body">
                    <div style="display:none" class="alert alert-danger col-md-12"></div>
                    <form action=" {{url("Question/reviewerAnswer",["rowNum"=>$questionRow[0][2],"idPorseshname"=>$idPorseshname])}}"
                          method="post"
                          class="question_form">
                        @if($questionType["questionType"]=="multipleChoice")

                            @foreach($questionRow as $question)

                                <div class="row">

                                    <div class="col-sm-6">

                                        <label>

                                            <input

                                                <?php if (preg_match('/' . $questionRow[$loop->index]["idOption"] . '/', $getThisQuestionAnswer["idOption"])) echo 'checked="checked"';?>

                                                type="checkbox" class="check" name="answer[]"

                                                value="{{$questionRow[$loop->index]["idOption"]}}">

                                            {{$questionRow[$loop->index]["opTextOption"]}}

                                        </label>

                                        <small style="color: red">{{$questionRow[$loop->index]["opMoreExplanation"]}}</small>


                                    </div>

                                    <div class="col-sm-6 buttom10">

                                        @if($questionRow[$loop->index]["opHaveImage"]=="Yes")

                                            {{--$questionRow[0][7]=$questionRow[0][id] in options--}}

                                            <div id="myModal" class="modal">

                                                <span class="close">&times;</span>

                                                <img class="modal-content" id="img01">

                                                <div id="caption"></div>

                                            </div>

                                            <img class="ShowQandOPimageinporseshgarside"

                                                 id="{{$questionRow[$loop->index]["idOption"]}}"

                                                 src="{{public_url("imagesUpload/options/").$questionRow[$loop->index]["idOption"].".jpeg?".rand(0,6587542123)}}">

                                        @endif

                                    </div>

                                </div>



                            @endforeach

                        @endif
                        @if($questionType["questionType"]=="multipleChoiceOthers")

                            @foreach($questionRow as $question)

                                <div class="row">

                                    <div class="col-sm-6">

                                        <label>

                                            <input data-label="{{$questionRow[$loop->index]["opTextOption"]}}"

                                                   <?php if (preg_match('/' . $questionRow[$loop->index]["idOption"] . '/', $getThisQuestionAnswer["idOption"])) echo 'checked="checked"';?>

                                                   type="checkbox" class="check" name="answer[]"

                                                   value="{{$questionRow[$loop->index]["idOption"]}}">

                                            @if($questionRow[$loop->index]["opTextOption"]=="Other") {{"سایر"}}  @else {{$questionRow[$loop->index]["opTextOption"]}} @endif

                                        </label>

                                        <small style="color: red">{{$questionRow[$loop->index]["opMoreExplanation"]}}</small>


                                    </div>

                                    <div class="col-sm-6 buttom10">

                                        @if($questionRow[0]["haveImage"]=="Yes")

                                            <div id="myModal" class="modal">

                                                <span class="close">&times;</span>

                                                <img class="modal-content" id="img01">

                                                <div id="caption"></div>

                                            </div>

                                            <img class="ShowQandOPimageinporseshgarside"

                                                 id="{{$questionRow[$loop->index]["idOption"]}}"

                                                 src="{{public_url("imagesUpload/options/").$questionRow[$loop->index]["idOption"].".jpeg?".rand(3,567845215478)}}">

                                        @endif

                                    </div>

                                </div>



                            @endforeach

                            <div class="row">

                                <div class="col-sm-6">

                                    <input type="text" class="form-control required borderInsect" name="others"

                                           id="questionAnswer"

                                           style="display: none;">

                                </div>

                            </div>

                        @endif
                        @if($questionType["questionType"]=="singleResponse")

                            <div class="row">

                                @foreach($questionRow as $question)

                                    <div class="row">

                                        <div class="col-sm-6">

                                            <label>

                                                <input type="radio" name="answer"

                                                       value="{{$questionRow[$loop->index]["idOption"]}}">

                                                <span></span>

                                                {{$questionRow[$loop->index]["opTextOption"]}}

                                            </label>

                                            <small style="color: red">{{$questionRow[$loop->index]["opMoreExplanation"]}}</small>

                                        </div>

                                        <div class="col-sm-6 buttom10">

                                            @if($questionRow[$loop->index]["opHaveImage"]=="Yes")

                                                {{--$questionRow[0][7]=$questionRow[0][id] in options--}}

                                                <div id="myModal" class="modal">

                                                    <span class="close">&times;</span>

                                                    <img class="modal-content" id="img01">

                                                    <div id="caption"></div>

                                                </div>

                                                <img class="ShowQandOPimageinporseshgarside"

                                                     id="{{$questionRow[$loop->index]["idOption"]}}"

                                                     src="{{public_url("imagesUpload/options/").$questionRow[$loop->index]["idOption"].".jpeg?".rand(0,215478658965)}}">

                                            @endif

                                        </div>

                                    </div>

                                @endforeach

                            </div>



                        @endif
                        @if($questionType["questionType"]=="singleResponseOthers")

                            <input type="hidden" name="idOption" value="{{$questionRow[0]["idOption"]}}">

                            @foreach($questionRow as $question)

                                <div class="row">

                                    <div class="col-sm-6">

                                        <label>

                                            <input type="radio" id="{{$questionRow[$loop->index]["opTextOption"]}}"

                                                   data-label="{{$questionRow[$loop->index]["opTextOption"]}}"

                                                   name="answer"

                                                   value="{{$questionRow[$loop->index]["idOption"]}}">

                                            @if($questionRow[$loop->index]["opTextOption"]=="Other") {{"سایر"}}  @else {{$questionRow[$loop->index]["opTextOption"]}} @endif

                                        </label>

                                        <small style="color: red">{{$questionRow[$loop->index]["opMoreExplanation"]}}</small>


                                    </div>

                                    <div class="col-sm-6 buttom10">

                                        @if($questionRow[$loop->index][14]=="Yes")

                                            <div id="myModal" class="modal">

                                                <span class="close">&times;</span>

                                                <img class="modal-content" id="img01">

                                                <div id="caption"></div>

                                            </div>

                                            <img class="ShowQandOPimageinporseshgarside"

                                                 id="{{$questionRow[$loop->index]["opHaveImage"]}}"

                                                 src="{{public_url("imagesUpload/options/").$questionRow[$loop->index]["idOption"].".jpeg"}}">

                                        @endif

                                    </div>

                                </div>

                            @endforeach

                            <div class="row">

                                <div class="col-sm-6">

                                    <input type="text" class="form-control required borderInsect" name="others"

                                           id="questionAnswer"

                                           style="display: none;">

                                </div>

                                <div class="col-sm-6"></div>

                            </div>

                        @endif
                        @if($questionType["questionType"]=="openQuestion")

                            <div class="row">

                                <div class="col-sm-3">

                                    <textarea name="answer" class="required focus borderInsect"

                                              id="questionAnswer" rows="4"

                                    >{{$getThisQuestionAnswer["text"]}}</textarea>

                                </div>

                                <div class="col-sm-6 col-sm-offset-2 centerAlign">

                                    @if($questionRow[0]["haveImage"]=="Yes")

                                        <div id="myModal" class="modal">

                                            <span class="close">&times;</span>

                                            <img class="modal-content" id="img01">

                                            <div id="caption"></div>

                                        </div>

                                        <img class="ShowQandOPimageinporseshgarside"

                                             id="{{$questionRow[0]["id"]}}"

                                             src="{{public_url("imagesUpload/question/").$questionRow[0]["id"].".jpeg"}}">

                                    @endif

                                </div>

                            </div>

                        @endif
                        @if($questionType["questionType"]=="numeric")

                            <div class="row">

                                <input type="number" min="{{$questionRow[0]["minNumberType"]}}"

                                       max="{{$questionRow[0]["maxNumberType"]}}" name="answer"

                                       class="required focus borderInsect ltrDir"

                                       id="questionAnswer" value="{{$getThisQuestionAnswer["text"]}}"/>

                            </div>

                        @endif
                        @if($questionType["questionType"]=="email")

                            <div class="row">

                                <input type="email" name="answer" class="required focus borderInsect ltrDir"

                                       id="questionAnswer" value="{{$getThisQuestionAnswer["text"]}}"/>

                            </div>

                        @endif
                        @if($questionType["questionType"]=="websitelink")

                            <div class="row">

                                <input type="url" name="answer" class="required focus borderInsect ltrDir"

                                       id="questionAnswer"/>

                            </div>

                        @endif
                        @if($questionType["questionType"]=="noResponse")

                            @if($questionRow[0]["haveImage"]=="Yes")

                                <div class="col-md-4">

                                    <div class="row top60 buttom60">

                                        <script>

                                            $("#QuestionText").prop("hidden", true);

                                            $("#QuestionImage").prop("hidden", true);

                                        </script>

                                        <div class="col-sm-12 alignCenter">

                                            <label class="redColor buttom10">این یک متن بدون پاسخ است:</label><br/>

                                            <strong style="font-size: xx-large">{{$questionRow[0]["text"]}}</strong>

                                        </div>

                                        <div class="col-sm-12 alignCenter ">


                                        </div>


                                    </div>

                                </div>

                                <div class="col-md-4">

                                    <div class="row buttom10">

                                        <div class="col-md-3 col-md-offset-8 buttom10">

                                            <div id="imagePreview" class="imagePreview"></div>

                                            <img name="img3"

                                                 src="{{public_url("imagesUpload/question/").$questionRow[0]["id"].".jpeg"}}"

                                                 class="imagePreviewOptionEdit" id="tempImg">

                                        </div>

                                    </div>


                                </div>

                            @else

                                <div class="row top60 buttom60">

                                    <script>

                                        $("#QuestionText").prop("hidden", true);

                                    </script>

                                    <div class="col-sm-12 alignCenter">

                                        <label class="redColor buttom10">این یک متن بدون پاسخ است:</label><br/>

                                        <strong style="font-size: xx-large">{{$questionRow[0]["text"]}}</strong>

                                    </div>

                                    <div class="col-sm-12 alignCenter ">


                                    </div>


                                </div>

                            @endif













                        @endif
                        @if($questionType["questionType"]=="spectral")

                            <div style="direction: ltr;">

                                <input type="range" min="0" max="{{$questionRow[0]["rangeValue"]}}" data-rangeslider
                                       value="{{$getThisQuestionAnswer["text"]}}">

                                <strong>
                                    <output></output>
                                </strong>

                            </div>

                            <input type="hidden" name="answer" class="required focus borderInsect"
                                   value="{{$getThisQuestionAnswer["text"]}}"

                                   id="questionAnswer"/>







                            {{--<div class="row top10 buttom12">--}}

                            {{--{{$questionRow[0]["rangeValue"]}}--}}

                            {{--{{$questionRow[0]["rigthLable"]}}--}}

                            {{--{{$questionRow[0]["centerLable"]}}--}}

                            {{--{{$questionRow[0]["leftLeble"]}}--}}

                            {{--<div class="row alignCenter">--}}

                            {{--<div class="col-xs-12">--}}

                            {{--<table class="tablespectral alignCenter no-margin table-responsive">--}}

                            {{--<tr>--}}

                            {{--@for($i=1;$i<=$questionRow[0]["rangeValue"];$i++)--}}

                            {{--<td >--}}

                            {{--<input type="button" class="btn btn-lg btnpectral"--}}

                            {{--@if($getThisQuestionAnswer["text"]==$i) style="background-color:#287EE3;"--}}

                            {{--@endif id="{{"td".$i}}"--}}

                            {{--style="border: solid;border-color: #53edea;cursor: hand"--}}

                            {{--value="{{$i}}"/>--}}

                            {{--</td>--}}

                            {{--@endfor--}}

                            {{--</tr>--}}



                            {{--</table>--}}

                            {{--<input type="hidden" name="answer" class="required focus borderInsect"--}}

                            {{--id="questionAnswer"/>--}}

                            {{--</div>--}}

                            {{--</div>--}}

                            {{--<div class="row alignCenter">--}}

                            {{--<div class="col-xs-12">--}}

                            {{--<table class="no-margin" style="border: none">--}}

                            {{--<tr>--}}

                            {{--<td class="rightAlign">{{$questionRow[0]["rigthLable"]}}</td>--}}

                            {{--@for($i=1;$i<=$questionRow[0]["rangeValue"];$i++)--}}

                            {{--<td>--}}

                            {{--@if(($questionRow[0]["rangeValue"] % 2) == 1)--}}

                            {{--@if($i-2==(round($questionRow[0]["rangeValue"] / 2)))--}}

                            {{--{{$questionRow[0]["centerLable"]}}--}}

                            {{--@endif--}}

                            {{--@endif--}}

                            {{--</td>--}}

                            {{--@endfor--}}

                            {{--<td class="leftAlign"--}}

                            {{--style="text-align: left;">{{$questionRow[0]["leftLeble"]}}</td>--}}

                            {{--</tr>--}}

                            {{--</table>--}}

                            {{--</div>--}}

                            {{--</div>--}}

                            {{--</div>--}}

                        @endif
                        @if($questionType["questionType"]=="degree")

                            <div class="row buttom10">

                                <div class="col-sm-12 alignCenter">

                                    <span class="my-rating-9 ltrDir" style="direction: ltr"></span>

                                    <span class="live-rating ltrDir hidden"></span>

                                    <input type="hidden" name="answer" class="required focus borderInsect"

                                           id="questionAnswer" value="{{$getThisQuestionAnswer["text"]}}"/>

                                </div>

                            </div>

                        @endif
                        @if($questionType["questionType"]=="prioritize")
                            <div class="jquery-script-clear"></div>
                            <div class="container">
                                @if(!$getThisQuestionAnswer["idOption"] | $getThisQuestionAnswer["idOption"]=="notAnswered")
                                    <ul class="list-group grouped col-xs-12 myu">
                                        @foreach($questionRow as $question)
                                            <li class="list-group-item myli"
                                                id="{{$questionRow[$loop->index]["idOption"]}}">
                                                <i class="fa fa-arrows-v"></i>&nbsp;&nbsp;{{$questionRow[$loop->index]["opTextOption"]}}
                                                <small style="color: red"> {{$questionRow[$loop->index]["opMoreExplanation"]}}</small>
                                            </li>
                                        @endforeach
                                    </ul>
                                @endif
                                @if($getThisQuestionAnswer["idOption"])
                                    <ul class="list-group grouped col-xs-12 myu">
                                        @foreach(explode(',', $getThisQuestionAnswer["idOption"])  as $idOp)
                                            @foreach($questionRow as $question)
                                                @if($questionRow[$loop->index]["idOption"]==$idOp)
                                                    <li class="list-group-item myli"
                                                        id="{{$questionRow[$loop->index]["idOption"]}}">
                                                        <i class="fa fa-arrows-v"></i>&nbsp;&nbsp;{{$questionRow[$loop->index]["opTextOption"]}}
                                                        <small style="color: red"> {{$questionRow[$loop->index]["opMoreExplanation"]}}</small>


                                                    </li>
                                                @endif
                                            @endforeach
                                        @endforeach
                                    </ul>
                                @endif
                            </div>

                            <script type="text/javascript">


                                var _gaq = _gaq || [];

                                _gaq.push(['_setAccount', 'UA-36251023-1']);

                                _gaq.push(['_setDomainName', 'jqueryscript.net']);

                                _gaq.push(['_trackPageview']);


                                (function () {

                                    var ga = document.createElement('script');

                                    ga.type = 'text/javascript';

                                    ga.async = true;

                                    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';

                                    var s = document.getElementsByTagName('script')[0];

                                    s.parentNode.insertBefore(ga, s);

                                })();


                            </script>

                            <input type="hidden" id="questionAnswer" name="answer"

                                   value="{{$getThisQuestionAnswer["idOption"]}}">

                        @endif
                        @if($questionType["questionType"]=="openOptions")
                            @foreach($questionRow as $question)
                                <div class="row buttom10">
                                    <div class="col-sm-6">
                                        <label>
                                            <input
                                                <?php if (preg_match('/' . $questionRow[$loop->index]["idOption"] . '/', $getThisQuestionAnswer["idOption"])) echo 'checked="checked"';?>
                                                type="checkbox" class="check" name="answer[]"
                                                value="{{$questionRow[$loop->index]["idOption"]}}"
                                                id="{{$questionRow[$loop->index]["idOption"]}}">
                                            {{$questionRow[$loop->index]["opTextOption"]}}
                                        </label>
                                        <input type="text" name="answerOption[]"
                                               id="answerOption{{$questionRow[$loop->index]["idOption"]}}"
                                               class="form-control" disabled="disabled">
                                        <small style="color: red">{{$questionRow[$loop->index]["opMoreExplanation"]}}</small>
                                    </div>

                                </div>
                            @endforeach
                        @endif
                        @if($questionType["questionType"]=="dateFormat")
                            <script defer>
                                $(document).ready(function () {
                                    $('.jdate').persianDatepicker({
                                        altField: '#tarikhAlt',
                                        altFormat: 'X',
                                        format: 'YYYY/MM/DD',
                                        observer: true,
                                        timePicker: {
                                            enabled: true
                                        },
                                    });
                                });
                            </script>
                            <div class="col-sm-12 alignCenter buttom10">

                                <input type="text" name="answer" class="required focus borderInsect jdate ltrDir"

                                       id="questionAnswer" value="{{$getThisQuestionAnswer["text"]}}"/>

                            </div>

                        @endif
                        <input type="hidden" id="questionType" name="questionType" value="{{$questionType["questionType"]}}">

                        <input type="hidden" name="idQ" value="{{$questionRow[0][0]}}">

                        <input type="hidden" name="allowNull" class="allowNull"   value="{{$questionRow[0]["allowNull"]}}">



                        <div class="col-md-12" style="padding: 5px;text-align: center;">
                            <input type="submit" value="ثبت" class="btn btn-success btn-lg">
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function (e) {
            $("input[name=answer][value='{{$getThisQuestionAnswer["idOption"]}}']").prop("checked", true)
            var QuestionType = $("input[name='QuestionType']").val();
            switch (QuestionType) {
                case  "openQuestion":
                    $('#openQuestionAnswer').val("{{$getThisQuestionAnswer["text"]}}");
                    break;
            }
        })
    </script>
@endsection