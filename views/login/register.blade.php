@extends("loginMaster")
@section("content")
    <title>سامانه نظر سنجی آراد برتر-ثبت نام</title>
    {{--    {{ $name }}--}}
    <div class="row">
        <strong>
            @if(isset($message))
                @if($message==true)
                    @include("messages.messages")
                @endif
            @endif
        </strong>
        <div class="container">
            موقتا در دسترس نیست
          {{--  <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6 " id="loginbox" style="margin-top:50px;">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="panel-title">ثبت نام</div>
                        </div>
                        <div style="padding-top:30px" class="panel-body">
                            <div style="display:none" id="login-alert" class="alert alert-danger col-md-12"></div>
                            <form id="loginform" class="form-horizontal" role="form"
                                  action="{{ url("login/register") }}" method="post" onsubmit="return checkForm(this);">
                                <div style="margin-bottom: 15px" class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-male"></i></span>
                                    <input id="inp_name" autofocus type="text" class="form-control focus" name="name" value=""
                                           placeholder="نام (حقیقی/حقوقی-شرکت)">
                                </div>
                                <div style="margin-bottom: 15px" class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <input id="userName" type="text" class="form-control" name="userName"
                                           placeholder="نام کاربری">
                                </div>
                                <div style="margin-bottom: 15px" class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-qrcode"></i></span>
                                    <input id="inp_pwd1" type="password" class="form-control" name="pwd1" value=""
                                           placeholder="گذرواژه-حداقل شش حرف">
                                </div>
                                <div style="margin-bottom: 15px" class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-qrcode"></i></span>
                                    <input id="inp_pwd2" type="password" class="form-control" name="pwd2"
                                           placeholder="تکرار گذر واژه-حداقل شش حرف">
                                </div>
                                <div style="margin-top:10px" class="form-group">
                                    <!-- Button -->
                                    <div class="col-md-12 controls" style="text-align: left;">
                                        <input type="submit" class="btn btn-success" id="btnInsert" value="ثبت">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>--}}
        </div>
    </div>
@endsection