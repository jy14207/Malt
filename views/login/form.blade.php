@extends("loginMaster")
@section("content")
	<title>طراحی آنلاین پرسشنامه آراد برتر-ورود</title>
	{{--    {{ $name }}--}}
	<div class="row">
		<strong>
			@if(isset($message))
				@if($message=="true")
					@include("messages.messages")
				@endif
			@endif
		</strong>
		<div class="container">
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6 " id="loginbox" style="margin-top:50px;">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<div class="panel-title">ورود</div>
						</div>
						<div style="padding-top:30px" class="panel-body">
							<div style="display:none" id="login-alert" class="alert alert-danger col-md-12"></div>
							<form id="loginform" class="form-horizontal" role="form" action="{{ url("login/login") }}"
								  method="post" autocomplete="off">
								<div style="margin-bottom: 15px" class="input-group">
									{{--<input type="hidden" name="inp_level" value="porseshgar">--}}
									<span class="input-group-addon"><i class="fa fa-user"></i></span>
									<input id="inp_username" type="text" class="form-control focus " name="inp_username"
										   value="" placeholder="نام کاربری" autofocus>
								</div>
								<div style="margin-bottom: 15px" class="input-group">
									<span class="input-group-addon"><i class="fa fa-qrcode"></i></span>
									<input id="inp_password" type="password" class="form-control" name="inp_password"
										   placeholder="گذرواژه">
								</div>
								<div style="margin-top:10px" class="form-group">
									<!-- Button -->
									<div class="col-md-12 controls" style="text-align: left;">
										<input type="submit" class="btn btn-success" value="تائید">
									</div>
								</div>
							</form>

							{{--<p >نام کاربری ندارید؟<a href="{{url("login/registerForm",["active"=>"manageP"])}}">ثبت نام کنید!</a></p>--}}

						</div>
					</div>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
		<div class="alert alert-warning" style="margin: 20px">
			<div class="row" style="padding-right: 5px;padding-left: 5px; text-align: justify">
				<strong style="color: red">پیام سیستم:</strong>
				<h5 class="blink" style="color: blue;padding-right: 40px">پرسشگران محترم شرکت آمار نگر لطفا از لینک زیر استفاده نمائید.!</h5>
				<h4><a href="http://ans-survey.ir/">سامانه آنلاین نظرسنجی آمار نگر</a></h4>
			</div>
		</div>
	</div>

@endsection