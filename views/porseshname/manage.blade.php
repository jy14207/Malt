@extends("AdminLteMaster")
@section("content")
	<title>بازبینی پرسشنامه</title>
	<strong>
		@if(isset($error))
			@include("messages.messages")
		@endif
	</strong>
	<title>مدیریت پرسشنامه</title>
	<div id="spin"></div>
	<div class="row">
		<div class="panel panel-primary">
			<div class="panel-collapse collapse in">
				<div class="panel-body">
					<div class="panel-group" id="accordion">
						<div class="alert alert-success ">
							<a class="btn btn-info" href="{{url("admin/selectedProject")}}">برگشت</a>
							<div class="col-md-11"> شماره پرسشنامه:
								<strong>{{$porseshname["porseshnamehNumber"]}}</strong>&nbsp; - &nbsp;نام
								پرسشگر:<strong>{{$porseshgarInf["name"]." ".$porseshgarInf["fName"]}}</strong></div>
						</div>
						<div class="panel panel-primary">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapse1">مشخصات
										پاسخگو</a>
								</h4>
							</div>
							<div id="collapse1" class="panel-collapse collapse in">
								<div class="panel-body">
									<form class="form" action="{{url("pasokhgoo/updatePasokhgoo")}}" method="post">
										<input type="hidden" id="action" name="action">
										<input type="hidden" id="id" name="id">
										<input type="hidden" id="idPorseshname" name="idPorseshname">
										<div class="row buttom10">
											<div class="col-md-3">
												<label class="align-right">نام :</label>
												<input tabindex="1" type="text" id="name"
													   class="form-control required" alt="نام" name="name"
													   id="name"/>
											</div>
											<div class="col-md-3">
												<label class="align-right">نام خانودگی :</label>
												<input tabindex="2" type="text" id="fName"
													   class="form-control required" alt="نام خانوادگی"
													   name="fName"
													   id="fName"/>
											</div>
											<div class="col-md-3">
												<label class="align-right">نام شهر :</label>
												<select name="cityName" class="form-control nopadding" id="cityName">
													@foreach($getCity as $city)
														<option name="cityName" id="cityName"
																@if(isset($getPasokhgoo["cityName"]))
																@if($getPasokhgoo["cityName"]==$getCity[$loop->index]["name"])
																selected="selected"
																@endif
																@endif
																value="{{$getCity[$loop->index]["name"]}}">{{$getCity[$loop->index]["name"]}}</option>
													@endforeach
												</select>
											</div>
											<div class="col-md-3">
												<label class="align-right">آدرس محل سکونت : </label>
												<input tabindex="4" type="text" id="homeAddress"
													   class="form-control required" alt="آدرس محل سکونت"
													   name="homeAddress"
													   id="homeAddress"/>
											</div>
											<div class="col-md-3">
												<label class="align-right">آدرس محل کار :</label>
												<input tabindex="4" type="text" class="form-control " id="workAddress"
													   alt="آدرس محل کار"
													   name="workAddress"
													   id="workAddress"/>
											</div>
											<div class="col-md-3">
												<label class="align-right">منطقه شهرداری محل سکونت :</label>
												<select name="cityArea" class="form-control nopadding" id="cityArea">
													@for($i=1;$i<=25;$i++)
														<option
																@if(isset($getPasokhgoo["cityArea"]))
																@if($getPasokhgoo["cityArea"]==$i)
																selected="selected"
																@endif
																@endif
																value="{{$i}}">{{$i}}
														</option>
													@endfor

												</select>

											</div>
											<div class="col-md-3">
												<label class="align-right">تلفن منزل یا محل کار :</label>
												<input tabindex="6" type="text" class="form-control " id="phoneNumber"
													   alt="تلفن" name="phoneNumber"
													   id="phoneNumber"/>
											</div>
											<div class="col-md-3">
												<label class="align-right">شماره همراه:</label>
												<input tabindex="7" type="text"
													   class="form-control required" alt="شماره همراه"
													   name="mobileNumber"
													   id="mobileNumber"/>
											</div>

										</div>
										<div class="row centerAlign">
											<input tabindex="8" type="submit" id="Btn_sabt" accesskey="S"
												   class="btn btn-success btn-lg btn-lg"
												   name="btn_insert" value="ثبت" disabled/>
										</div>
									</form>
									<hr/>
									<table class="table table-bordered margintop5">
										<thead>
										<tr>
											<th class="text-center hidden-xs">نام</th>
											<th class="text-center ">نام خانوادگی</th>
											<th class="text-center ">نام شهر</th>
											<th class="text-center hidden-xs">آدرس محل سکونت</th>
											<th class="text-center hidden-xs">آدرس محل کار</th>
											<th class="text-center hidden-xs">منطقه شهرداری</th>
											<th class="text-center hidden-xs">تلفن منزل یا محل کار</th>
											<th class="text-center hidden-xs">شماره همراه</th>
											<th class="text-center">ویرایش</th>
											{{--<th class="text-center">حذف</th>--}}
										</tr>
										</thead>
										<tbody>
										@if(!empty($pasokhgoo))
											<tr>
												<td class="text-center hidden-xs">{{$pasokhgoo["name"]}}</td>
												<td class="text-center"> {{$pasokhgoo["fName"]}}</td>
												<td class="text-center"> {{$pasokhgoo["cityName"]}}</td>
												<td class="text-center hidden-xs"> {{$pasokhgoo["homeAddress"]}}</td>
												<td class="text-center hidden-xs"> {{$pasokhgoo["workAddress"]}}</td>
												<td class="text-center hidden-xs"> {{$pasokhgoo["cityArea"]}}</td>
												<td class="text-center hidden-xs"> {{$pasokhgoo["phoneNumber"]}}</td>
												<td class="text-center hidden-xs"> {{$pasokhgoo["mobileNumber"]}}</td>
												<td class=" text-center">
													<a class="handcursor"
													   onclick="getPasokhgooRowForUpdate({{$pasokhgoo["id"]}})">
														<img title="ویرایش"
															 src="{{public_url("./images/update.png")}}"/></a>
												</td>

											</tr>
										@endif
										</tbody>
									</table>

								</div>
							</div>
						</div>
						<div class="panel panel-primary">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapse2">جواب های داده
										شده</a>
								</h4>
							</div>
							<div id="collapse2" class="panel-collapse collapse in">
								<div class="panel-body">
									<form class="form" action="" method="post">

									</form>
									<table class="table table-bordered margintop5">
										<thead>
										<tr>
											<th class="text-center">ردیف</th>
											<th class="text-center">متن</th>
											<th class="text-center">گزینه های انتخاب شده</th>
											<th class="text-center">جواب سوال های تشریحی وسایر</th>
											<th class="text-center">ویرایش</th>
										</tr>
										</thead>
										<tbody>

										@foreach($finalListAnswers as $finalListAnswer)
											<tr>
												<td class="text-center">{{$finalListAnswer["rowNumber"]}}</td>
												<td class="text-center"> {{$finalListAnswer["textQ"]}}</td>
												<td class="text-center">
													@if($finalListAnswer["idOption"]=="uploadImage")
														<div id="myModal" class="modal">
															<span class="close">&times;</span>
															<img class="modal-content" id="img01">
															<div id="caption"></div>
														</div>
														<img class="myImg" id="QuestionImage"
															 src="{{public_url("./imagesUpload/answer/uploadImage/".$finalListAnswer["text"].".jpeg?".rand(0,23456789))}}">

													@else
														{{$finalListAnswer["idOption"]}}
													@endif
												</td>
												<td class="text-center">
													@if($finalListAnswer["idOption"]=="uploadImage")
														<a class="handcursor" download target="_blank"
														   href="{{public_url("./imagesUpload/answer/uploadImage/".$finalListAnswer["text"].".jpeg")}}">
															<img title="دانلود تصویر"
																 src="{{public_url("./images/downloaFile.png")}}"/>
														</a>
													@else
														{{$finalListAnswer["text"]}}
													@endif
												</td>
												<td class=" text-center">
													<a class="handcursor"
													   href="{{url("Question/updateByReviewer",["rowNum"=>$finalListAnswer["rowNumber"],"idPorseshname"=>$finalListAnswer["idPorseshname"],"idQ"=>$finalListAnswer["idQuestion"]])}}">
														<img title="ویرایش"
															 src="{{public_url("./images/update.png")}}"/></a>
												</td>
												<td style="visibility: hidden"></td>
											</tr>
										@endforeach
										</tbody>
									</table>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
@endsection